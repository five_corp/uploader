package main

import (
	"archive/zip"
	"crypto"
	"fmt"
	"log"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
)

func uploadQueries(tenant, fileName, token string, signer crypto.Signer) error {
	fmt.Println("Actualizando queries")
	_, _, err := postFile(tenant, "models", fileName, token, signer)
	return err
}

func uploadService(tenant, fileName, token string, signer crypto.Signer) error {
	// Read zip file
	r, err := zip.OpenReader(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()
	// Iterate over files
	for _, f := range r.File {
		fmt.Printf("Procesando fichero %s... ", f.Name)
		dir, fileName := filepath.Split(f.Name)
		dir = strings.ToLower(dir)
		if strings.HasPrefix(dir, "service/current") && fileName != "" {
			if _, _, err = postFile(tenant, "updates", f.Name, token, signer); err != nil {
				break
			}
		}
		fmt.Println("")
	}
	// If no error, do the update
	if err == nil {
		fmt.Print("*** Actualizando servicio...")
		err = put(tenant, "update", token, []byte("{ \"force\": true }"), false)
	}
	return errors.Wrap(err, "Failed update")
}

func uploadJobs(tenant, fileName, token string, signer crypto.Signer) error {
	// Read zip file
	r, err := zip.OpenReader(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()
	// Iterate over files
	for _, f := range r.File {
		fmt.Printf("Procesando fichero %s... ", f.Name)
		dir, fileName := filepath.Split(f.Name)
		dir = strings.ToLower(dir)
		if fileName != "" {
			var prefix string
			if strings.HasPrefix(dir, "jobs/jobs") {
				// It is a job file
				prefix = "jobs"
			} else if strings.HasPrefix(dir, "jobs/xforms") {
				prefix = "xforms"
			} else {
				continue
			}
			_, _, err = postFile(tenant, prefix, f.Name, token, signer)
		}
		if err != nil {
			break
		}
		fmt.Println("")
	}
	return errors.Wrap(err, "Failed to upload files")
}
