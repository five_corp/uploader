package main

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/five_corp/uploader/models/conector"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/tenants"
)

func dumpExcel(excelFolder, modelFolder, model string, tables []string) error {
	// Load the specified conector
	manager, err := conector.NewManager(modelFolder, tenants.Mock("bootstrap", dbtool.NewMock()))
	if err != nil {
		return err
	}
	con, err := manager.Find(model)
	if err != nil {
		return err
	}
	ctx := context.Background()
	// Iterate on all excel files
	engines := make(map[string]conector.Engine)
	engine, err := conector.NewTabularEngine(ctx, excelFolder, dbtool.RFCFormatter{})
	if err != nil {
		return err
	}
	// Build a map to filter out which products do we want.
	filter := make(map[string]bool)
	if tables != nil && len(tables) > 0 {
		for _, table := range tables {
			filter[table] = true
		}
	}
	engines[conector.Tabular] = engine
	engines[conector.Excel] = engine // Alias, for backward compatibility
	maxRows, batchSize := 1000, 100
	for productName, table := range con.Tables {
		if table.Engine == conector.Excel || table.Engine == conector.Tabular {
			// Check if we must skip this table
			if found, ok := filter[productName]; !ok || !found {
				if tables != nil && len(tables) > 0 {
					continue
				}
			}
			iterations, err := con.Eval(ctx, dbtool.NewMock(), productName)
			if err != nil {
				return err
			}
			// Dump a feed from the excel file
			product, _ := con.Package(productName, iterations)
			feed, types, err := product.Feed(ctx, engines, maxRows, batchSize)
			if err != nil {
				return err
			}
			err = func() error {
				// I trust the "early release" semantic of feeds
				defer feed.Close()
				fmt.Println("CONECTOR: ", model, ", PRODUCT: ", productName)
				fmt.Println(strings.Join(feed.Names, ";"))
				fmt.Println(strings.Join(feed.Types, ";"))
				fmt.Println(strings.Join(types, ";"))
				for feed.Next() {
					for _, rows := range feed.Batch() {
						stringRow := make([]string, 0, len(rows))
						for _, item := range rows {
							stringRow = append(stringRow, fmt.Sprintf("%v", item))
						}
						fmt.Println(strings.Join(stringRow, ";"))
					}
				}
				if err := feed.Err(); err != nil {
					return err
				}
				fmt.Println("")
				return nil
			}()
			if err != nil {
				return err
			}
		}
	}
	return nil
}
