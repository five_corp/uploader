package main

import (
	"context"
	"encoding/json"
	"log"
	"sort"

	"bitbucket.org/five_corp/saferest"
	models_conector "bitbucket.org/five_corp/uploader/models/conector"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/excel"
	"github.com/pkg/errors"
)

// Data received to trigger a conector update
type conectorTrigger struct {
	Label     string   `json:"label"`
	Conectors []string `json:"conectors"`
	DryRun    bool     `json:"dryRun"`
}

func dumpConector(tenant, token, conector string, dryRun bool, config dbtool.Config) error {
	// Get the list of conector tables
	url := BaseURL + tenant + "/api/conector/" + conector
	tables := make([]string, 10)
	if err := saferest.RestRequestWrapped(nil, "GET", url+"?_source=all", token, 30, nil, true, &tables); err != nil {
		log.Print("Error obteniendo fuentes del conector " + conector)
		return err
	}
	// So it always runs the conectors in the same order
	sort.Strings(tables)
	// Buld a MS SQL Store
	if config != nil {
		// Build a dumping context
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		// Get an SQL Engine
		engine, err := models_conector.NewSQLEngine(ctx, config)
		if err != nil {
			log.Print("Error obteniendo engine a partir de la config ", config)
			return err
		}
		// Get the product for each table
		log.Print("Available tables: ", tables)
		for _, table := range tables {
			// Get the particular product
			product := models_conector.Package{}
			tableURL := url + "?_source=" + table
			if err := saferest.RestRequestWrapped(nil, "GET", tableURL, token, 30, nil, true, &product); err != nil {
				log.Print("Error obteniendo producto ", product, " del conector ", conector)
				return err
			}
			log.Print("Obtenido producto ", table, " del conector ", conector, ", ejecutando...")
			// Get the result feed
			engines := make(map[string]models_conector.Engine)
			engines[models_conector.Default] = engine
			feed, _, err := product.Feed(ctx, engines, 1000000, 100)
			if err != nil {
				log.Print("Error ejecutando producto ", table, " del conector ", conector)
				return err
			}
			// Write data to excel
			err = func() error {
				defer feed.Close()
				writer, err := excel.NewWriter(".", table)
				if err != nil {
					return errors.Wrap(err, "Failed to create excel writer")
				}
				if err := writer.Write(feed, product.Types); err != nil {
					return errors.Wrapf(err, "Failed to write product %s of conector %s", table, conector)
				}
				if err := feed.Err(); err != nil {
					return errors.Wrapf(err, "Failed to finish product %s of conector %s", table, conector)
				}
				log.Print("Completado volcado de producto ", table, " en fichero ", writer.Fullpath())
				return nil
			}()
			if err != nil {
				return err
			}
		}
	} else {
		data, err := json.Marshal(conectorTrigger{
			Label:     "update.exe - conector",
			Conectors: []string{conector},
			DryRun:    dryRun,
		})
		if dryRun {
			log.Print("Atencion: el conector se ejecuta en modo dryRun (no se lanzan trafos)")
		}
		if err != nil {
			return errors.Wrap(err, "Failed to marshal json")
		}
		if err := post(tenant, "conector.trigger", token, data, false); err != nil {
			log.Print("Error importando datos en el cohete: ", err)
			return err
		}
		log.Print("Completada importación de conector ", conector)
	}
	return nil
}

func runCommand(tenant, token string) error {
	body, err := json.Marshal(conectorTrigger{
		Label:     "update.exe - run",
		Conectors: []string{"excel"},
	})
	if err != nil {
		return errors.Wrapf(err, "Failed to marshal json")
	}
	return post(tenant, "conector.trigger", token, body, false)
}
