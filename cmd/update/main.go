package main

import (
	"crypto"
	"crypto/x509"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/dbtool/mssql"
	"github.com/pkg/errors"
)

// Bootstrap routine
func main() {

	// tenant
	tenant := flag.String("tenant", "", "tenant name")
	jwt := flag.String("jwt", "", "JWT token de autenticación")
	subject := flag.String("subject", "", "Subject del token")
	audience := flag.String("audience", "", "Audience del token")
	entorno := flag.String("entorno", "pro", "Nombre del entorno (devel, pre, pro)")
	conector := flag.String("conector", "", "conector name")
	dryRun := flag.Bool("dry", false, "No ejecutar las trafos al lanzar el conector")
	dbhost := flag.String("host", "", "SQL Server host")
	dbport := flag.Int("port", 1433, "SQL Server port")
	dbuser := flag.String("user", "", "SQL Server user")
	dbname := flag.String("name", "", "SQL Server database name")
	restart := flag.Bool("r", false, "Restart tenant")
	restartAll := flag.Bool("rr", false, "Restart tenant and database")
	timeout := flag.Int("timeout", 30, "Timeout de consulta a la base de datos")
	getToken := flag.Bool("token", false, "Mostrar el token")
	expiration := flag.Int("expiration", 24, "Token expiration (hours)")
	useV1 := flag.Bool("v1", false, "Use v1 version of update protocol")
	modelFolder := flag.String("modelos", "", "Ruta a la carpeta donde están los conectores.yaml")
	excelFolder := flag.String("excels", "", "Ruta a la carpeta local donde están los excels")
	conExcel := flag.String("x", "", "Nombre del conector excel a probar (fichero .yaml)")
	signWith := flag.String("sign", "", "Nombre del fichero PEM con la clave de firma")
	flag.Parse()

	// Special case: -x only tests excels
	if *conExcel != "" {
		fmt.Println("")
		if err := dumpExcel(*excelFolder, *modelFolder, *conExcel, flag.Args()); err != nil {
			fmt.Println("Error: ", err)
		}
		os.Exit(0)
	}

	if tenant == nil || *tenant == "" {
		fail(1, "Falta parametro -tenant")
	}

	// Select the proper URL
	if entorno != nil && *entorno != "" {
		switch *entorno {
		case "pro":
			BaseURL = BaseURLPro
		case "pre":
			BaseURL = BaseURLPre
		case "devel":
			BaseURL = BaseURLDevel
		default:
			BaseURL = fmt.Sprintf("https://%s/tenants/", *entorno)
		}
	}

	// Get database parameters
	var config *mssql.Config
	if conector != nil && *conector != "" {
		required := []*string{
			dbhost, dbuser, dbname,
		}
		found := 0
		for _, req := range required {
			if req != nil && *req != "" {
				found++
			}
		}
		if found >= len(required) {
			config = &mssql.Config{
				Host:   *dbhost,
				Port:   *dbport,
				User:   *dbuser,
				Pass:   "",
				Name:   *dbname,
				UseTLS: false,
			}
			if timeout != nil && *timeout > 0 {
				config.Timeout = *timeout
			}
		} else if found > 0 {
			fail(3, "ATENCION: Ha configurado al menos uno de los parámetros -host, -user o -name, pero es necesario configurar LOS TRES para que se utilicen.\nSi desea usar las credenciales por defecto configuradas en el asistente, no indique ninguno de los tres parámetros.")
		}
	}

	// If no subject specified, use tenant name
	if subject == nil || *subject == "" {
		subject = tenant
	}

	// secret - if JWT not provided
	var token string
	if jwt != nil && *jwt != "" {
		token = *jwt
	} else {
		pass, err := getPassword("Secreto del tenant")
		if err != nil {
			fail(2, err)
		}
		token = saferest.Sign(pass, *tenant, *subject, *audience, time.Duration(*expiration)*time.Hour)
		// If you want to see the token, here it is...
		if *getToken {
			log.Print("TOKEN: " + token)
		}
	}

	// If conection params specified by hand, get DB pass too.
	if config != nil && config.Host != "" {
		dbpass, err := getPassword("Password de la base de datos")
		if err != nil {
			fail(4, err)
		}
		config.Pass = dbpass
	}

	// Dump the conector, if specified
	if conector != nil && *conector != "" {
		var dbtoolConfig dbtool.Config
		if config != nil && config.Host != "" {
			dbtoolConfig = config
		}
		if err := dumpConector(*tenant, token, *conector, *dryRun, dbtoolConfig); err != nil {
			log.Print("ERROR dumping conector: ", err)
		}
	}

	// Read signing file
	var signer crypto.Signer
	if *signWith != "" {
		x509pass := ""
		var err error
		for {
			s, buildErr := saferest.SignerFrom(*signWith, x509pass)
			if errors.Cause(buildErr) == x509.IncorrectPasswordError {
				x509pass, err = getPassword("Contraseña de la clave de firma")
				continue
			}
			signer, err = s, buildErr
			break
		}
		if err != nil {
			log.Fatal("Error construyendo signer: ", err)
		}
	}

	// Read input args
	files := flag.Args()
	if runtime.GOOS == "windows" {
		expanded := make([]string, 0, len(files)*2+1)
		for _, arg := range files {
			matches, err := filepath.Glob(arg)
			if err != nil {
				log.Print("ERROR expanding file name ", arg, ": ", err)
			}
			expanded = append(expanded, matches...)
		}
		files = expanded
	}
	var err error
	for _, arg := range files {
		_, fileName := filepath.Split(arg)
		switch strings.ToLower(fileName) {
		case "service.zip":
			err = updateService(*tenant, arg, token, *useV1, signer)
		case "jobs.zip":
			err = updateJobs(*tenant, arg, token, *useV1, signer)
		case "java.zip":
			err = updateJava(*tenant, arg, token, signer)
		case "pentaho.zip":
			err = updatePentaho(*tenant, arg, token, signer)
		case "run":
			err = runCommand(*tenant, token)
		default:
			if strings.HasSuffix(strings.ToLower(fileName), ".yaml") {
				err = uploadQueries(*tenant, arg, token, signer)
			}
		}
		if err != nil {
			break
		}
	}

	if err != nil {
		fmt.Println("ERROR DE PROCESO: ", err)
	} else {
		if (restart != nil && *restart == true) || (restartAll != nil && *restartAll == true) {
			msg := "{ \"serviceRestart\": true }"
			if restartAll != nil && *restartAll == true {
				msg = "{ \"serviceRestart\": true, \"dbRestart\": true }"
				fmt.Print("Reiniciando servicio y base de datos... ")
			} else {
				fmt.Print("Reiniciando servicio... ")
			}
			if err := put(*tenant, "restart", token, []byte(msg), false); err != nil {
				fmt.Println("ERROR DE RESTART: ", err)
			}
		}
	}
	os.Exit(0)
}
