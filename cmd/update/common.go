package main

import (
	"bytes"
	"crypto"
	"crypto/tls"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"syscall"

	"bitbucket.org/five_corp/saferest"
	"github.com/pkg/errors"
	"golang.org/x/crypto/ssh/terminal"
)

const (
	// BaseURLPro es la Base URL en el entorno de PRO
	BaseURLPro = "https://smartbi.acens.net/tenants/"
	// BaseURLDevel es la Base URL en el entorno de DEVEL
	BaseURLDevel = "https://devel.smartbi.es/tenants/"
	// BaseURLPre es la Base URL en el entorno de PRE
	BaseURLPre = "https://prepro.smartbi.es/tenants/"
)

// BaseURL actual, según el entorno elegido
var BaseURL string

// Exits the program with an error code
func fail(code int, other ...interface{}) {
	log.Print(other...)
	os.Exit(code)
}

func insecureClient() *http.Client {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	return &http.Client{Transport: tr}
}

func getPassword(prompt string) (string, error) {
	fmt.Print(prompt, ": ")
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		return "", errors.Wrap(err, "Failed to read password")
	}
	fmt.Println("") // type newline
	return strings.TrimSpace(string(bytePassword)), nil
}

func get(tenant, method, token string) error {
	url := BaseURL + tenant + "/api/" + method
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Authorization", "Bearer "+token)
	client := insecureClient()
	resp, err := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return errors.Wrapf(err, "Failed to perform GET request to %s", url)
	}
	// exhaust the reader
	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrapf(err, "Failed to read reply body from %s", url)
	}
	fmt.Printf("GET: %s\n%s\n", resp.Status, string(result))
	if resp.StatusCode != 200 {
		return errors.Errorf("GET returned error %s", resp.Status)
	}
	return nil
}

func put(tenant, method, token string, body []byte, yaml bool) error {
	url := BaseURL + tenant + "/api/" + method
	bodyReader := bytes.NewReader(body)
	req, err := http.NewRequest("PUT", url, bodyReader)
	if yaml {
		req.Header.Set("Content-Type", "application/x-yaml; charset=utf-8")
	} else {
		req.Header.Set("Content-Type", "application/json; charset=utf-8")
	}
	req.Header.Set("Authorization", "Bearer "+token)
	client := insecureClient()
	resp, err := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return errors.Wrapf(err, "Failed to perform PUT request to %s", url)
	}
	// exhaust the reader
	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrapf(err, "Failed to read PUT reply from %s", url)
	}
	fmt.Printf("PUT: %s\n%s\n", resp.Status, string(result))
	return nil
}

func post(tenant, method, token string, body []byte, yaml bool) error {
	url := BaseURL + tenant + "/api/" + method
	bodyReader := bytes.NewReader(body)
	req, err := http.NewRequest("POST", url, bodyReader)
	if yaml {
		req.Header.Set("Content-Type", "application/x-yaml; charset=utf-8")
	} else {
		req.Header.Set("Content-Type", "application/json; charset=utf-8")
	}
	req.Header.Set("Authorization", "Bearer "+token)
	client := insecureClient()
	resp, err := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return errors.Wrapf(err, "Failed to POST to %s", url)
	}
	// exhaust the reader
	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return errors.Wrapf(err, "Failed to read POST reply from %s", url)
	}
	fmt.Printf("POST: %s\n%s\n", resp.Status, string(result))
	return nil
}

func postFile(tenant, method, fullName, token string, signer crypto.Signer) (baseName, signature string, err error) {
	rc, err := os.Open(fullName)
	if err != nil {
		return "", "", errors.Wrapf(err, "Failed to open file %s to upload", fullName)
	}
	reader := io.ReadCloser(rc)
	if signer != nil {
		reader = saferest.NewSignReader(reader, signer)
	}
	defer reader.Close()
	_, baseName = filepath.Split(fullName)
	// Load the data in a multipart form
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	// If there is a signature, add the signature part
	part, err := writer.CreateFormFile("file", baseName)
	if err != nil {
		return "", "", errors.Wrapf(err, "Failed to create FormFile to upload %s", baseName)
	}
	// Read the body of the file
	_, err = io.Copy(part, reader)
	if err != nil {
		return "", "", errors.Wrapf(err, "Failed to upstream file %s", fullName)
	}
	// If there is a signer, write the signature too
	if signer != nil {
		signature, err = reader.(saferest.SignReader).Sign()
		if err := writer.WriteField("signature", signature); err != nil {
			return "", "", errors.Wrap(err, "Failed to write file signature")
		}
	}
	// Close the multipart writer
	err = writer.Close()
	if err != nil {
		return "", "", errors.Wrap(err, "Failed to close signing writer")
	}
	// Submit the file
	url := BaseURL + tenant + "/api/" + method
	req, err := http.NewRequest("POST", url, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Set("Authorization", "Bearer "+token)
	client := insecureClient()
	resp, err := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return "", "", errors.Wrapf(err, "Failed to POST file to %s", url)
	}
	// exhaust the reader
	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", "", errors.Wrapf(err, "Failed to read POST file reply from %s", url)
	}
	fmt.Printf("POSTFILE: %s\n%s\n", resp.Status, string(result))
	return baseName, signature, nil
}
