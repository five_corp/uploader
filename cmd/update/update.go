package main

import (
	"crypto"
	"log"

	"github.com/pkg/errors"
)

// Adaptacion de las funciones uploadService y uploadJobs, de v1 a v2
type updateV1 func(string, string, string, crypto.Signer) error

func updateV2(tenant, fileName, token string, v1 updateV1, useV1 bool, signer crypto.Signer) error {
	// Check if versions 3 or 2 of the update protocol are supported
	endpoint, path := "", ""
	if err := get(tenant, "update.v3", token); err == nil {
		endpoint, path = "update.v3", "updates"
	} else {
		log.Print("El cohete no soporta la version 3 del protocolo de update: ", err)
		if err := get(tenant, "update.v2", token); err == nil {
			endpoint, path = "update.v2", "files"
		} else {
			log.Print("El cohete no soporta la version 2 del protocolo de update: ", err)
		}
	}
	// If no support for V2 / V3, try V1
	if endpoint == "" || useV1 {
		log.Print("Intentando updates con version 1 del protocolo")
		return v1(tenant, fileName, token, signer)
	}
	// El path (/files o /updates) depende de la versiópn v2 o v3
	log.Print("Intentando update de ", fileName, " con endpoint ", endpoint)
	baseName, _, err := postFile(tenant, path, fileName, token, signer)
	if err != nil {
		return err
	}
	// El endpoint tambien depende de la version del protocolo.
	if err := put(tenant, endpoint, token, []byte("[\""+baseName+"\"]"), false); err != nil {
		return err
	}
	return nil
}

func dummyV1(tenant, fileName, token string, signer crypto.Signer) error {
	return errors.Errorf("La version 1 de update no permite actualizar %s", fileName)
}

func updateService(tenant, fileName, token string, useV1 bool, signer crypto.Signer) error {
	if signer != nil && useV1 {
		return errors.New("No es posible firmar ficheros en la API v1")
	}
	return updateV2(tenant, fileName, token, uploadService, useV1, signer)
}

func updateJobs(tenant, fileName, token string, useV1 bool, signer crypto.Signer) error {
	if signer != nil && useV1 {
		return errors.New("No es posible firmar ficheros en la API v1")
	}
	return updateV2(tenant, fileName, token, uploadJobs, useV1, signer)
}

func updateJava(tenant, fileName, token string, signer crypto.Signer) error {
	return updateV2(tenant, fileName, token, dummyV1, false, signer)
}

func updatePentaho(tenant, fileName, token string, signer crypto.Signer) error {
	return updateV2(tenant, fileName, token, dummyV1, false, signer)
}
