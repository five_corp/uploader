package endpoints

import (
	"log"
	"net/http"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"bitbucket.org/five_corp/uploader/sections"
)

// Database route handler
type dbHandler struct {
	saferest.Sectioner
	tenants.Switcher
}

// DBRoutes defines the routes for this endpoint
func DBRoutes(manager saferest.Sectioner, switcher tenants.Switcher, unprotect bool) []saferest.Route {
	handler := dbHandler{
		Sectioner: manager,
		Switcher:  switcher,
	}
	return []saferest.Route{
		saferest.Route{
			Audience: "g:db",
			Method:   "GET",
			Pattern:  "/db",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
	}
}

// Pings the database and gets the config params
func (h dbHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	// Get a copy of the config object, we are going to modify it
	config := sections.GetDatabase(h)
	issuer := saferest.IssuerOf(r)
	store, ok := h.Switcher.Switch(issuer)
	if !ok {
		log.Print("ERROR: Database not found for issuer ", issuer)
		//config.Pass = "************"
		config.Healthy = false
	} else if err := store.Ping(); err != nil {
		log.Print("ERROR: Ping error for issuer ", issuer, ": ", err)
		config.Healthy = false
	} else {
		config.Healthy = true
	}
	saferest.Success(w, config)
}
