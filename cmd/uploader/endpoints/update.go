package endpoints

import (
	"net/http"
	"path/filepath"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/update"
	"bitbucket.org/five_corp/uploader/sections"
	"github.com/pkg/errors"
)

// restart route handler
type updateHandler struct {
	saferest.Sectioner
}

// UpdateRoutes build routes for update endpoints
func UpdateRoutes(manager saferest.Sectioner, unprotect bool) []saferest.Route {
	handler := updateHandler{
		Sectioner: manager,
	}
	return []saferest.Route{
		// Remove old update mechanism. It doesn't use signatures.
		/*saferest.Route{
			Audience: "u:update",
			Method:   "PUT",
			Pattern:  "/update",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPut(w, r)
			},
		},*/
		saferest.Route{
			Audience: "g:update",
			Method:   "GET",
			Pattern:  "/update.v3",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
		saferest.Route{
			Audience: "u:update",
			Method:   "PUT",
			Pattern:  "/update.v3",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPut(w, r)
			},
		},
	}
}

// Moves ScratchFolder to UpdateFolder
/*
func (h updateHandler) httpPut(w http.ResponseWriter, r *http.Request) {
	uploadsConfig := sections.GetUploads(h)
	updateConfig := sections.GetUpdate(h)
	isEmpty, err := saferest.IsDirEmpty(uploadsConfig.ScratchFolder)
	if err != nil {
		saferest.Fail(http.StatusExpectationFailed, w, err.Error(), err)
		return
	}
	if isEmpty {
		err := errors.New("Nothing to update")
		saferest.Fail(http.StatusNotFound, w, err.Error(), err)
		return
	}
	os.RemoveAll(updateConfig.UpdateFolder)
	if err := os.Rename(uploadsConfig.ScratchFolder, updateConfig.UpdateFolder); err != nil {
		saferest.Fail(http.StatusInternalServerError, w, err.Error(), errors.Wrap(err))
		return
	}
	// Leave the scratch dir in place, case we want to update again
	// before restarting the server
	os.MkdirAll(uploadsConfig.ScratchFolder, 0755)
	saferest.Success(w, "Upload folder in place, now /restart")
}
*/

// Just to check that update.v2 protocol is supported
func (h updateHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	saferest.Success(w, true)
}

// Unzips the specified zip
func (h updateHandler) httpPut(w http.ResponseWriter, r *http.Request) {
	uploadsConfig := sections.GetUploads(h)
	updateConfig := sections.GetUpdate(h)
	data := make([]string, 0, 10)
	if err := saferest.GetJSONBody(r.Header, r.Body, &data); err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	for _, module := range data {
		folderMap, ok := updateConfig.UpdateRules[module]
		if !ok {
			err := errors.Errorf("Module %s not recognized", module)
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		modPath := filepath.Join(uploadsConfig.UpdatesFolder, module)
		// Check the signature of the file while extracting
		if err := update.Extract(modPath, folderMap); err != nil {
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
	}
	saferest.Success(w, "Updated folders in place, now /restart")
}
