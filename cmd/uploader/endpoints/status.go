package endpoints

import (
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/five_corp/saferest"
)

// Status route handler
type statusHandler time.Time

// StatusRoutes defines the routes for this endpoint
func StatusRoutes(unprotect bool) []saferest.Route {
	handler := statusHandler(time.Now())
	return []saferest.Route{
		saferest.Route{
			Audience: "g:status",
			RBAC:     saferest.NullRBAC, // Allow any user to get status
			Method:   "GET",
			Pattern:  "/status",
			Private:  false,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
	}
}

// Returns uptime
func (h statusHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	saferest.Success(w, fmt.Sprintf("Current date: %v. Service uptime: %s", time.Now().Local(), h.String()))
}

// Formats the uptime
func (h statusHandler) String() string {
	return round(time.Since(time.Time(h)), time.Second).String()
}

// Round a time.Duration. See https://play.golang.org/p/QHocTHl8iR
func round(d, r time.Duration) time.Duration {
	if r <= 0 {
		return d
	}
	neg := d < 0
	if neg {
		d = -d
	}
	if m := d % r; m+m < r {
		d = d - m
	} else {
		d = d + r - m
	}
	if neg {
		return -d
	}
	return d
}
