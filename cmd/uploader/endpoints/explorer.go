package endpoints

import (
	"net/http"
	"path/filepath"
	"strings"

	"bitbucket.org/five_corp/saferest"
	"github.com/pkg/errors"
)

// API explorer handler
type explorerHandler struct {
	fileServer http.Handler
	err        error
}

// ExplorerRoutes defines the routes for this endpoint
func ExplorerRoutes(manager saferest.Sectioner, pattern, folder string, unprotect bool) []saferest.Route {
	handler := explorerHandler{}
	finalPath, err := filepath.Abs(folder)
	if err != nil {
		handler.err = errors.Wrapf(err, "Failed to get absolute path of %s", folder)
	} else {
		prefix := strings.Join([]string{saferest.GetServer(manager).Prefix, pattern}, "/")
		handler.fileServer = http.StripPrefix(prefix+"/", http.FileServer(http.Dir(finalPath)))
	}
	return []saferest.Route{
		saferest.Route{
			Audience: "g:" + pattern,
			Method:   "GET",
			Pattern:  "/" + pattern + "/",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
		saferest.Route{
			Audience: "g:" + pattern,
			Method:   "GET",
			Pattern:  "/" + pattern + "/{path}",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
	}
}

// Returns the selected files
func (h explorerHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	if h.err != nil {
		saferest.Fail(http.StatusInternalServerError, w, h.err)
	} else if h.fileServer == nil {
		err := errors.New("No files to serve")
		saferest.Fail(http.StatusInternalServerError, w, err)
	} else {
		h.fileServer.ServeHTTP(w, r)
	}
}
