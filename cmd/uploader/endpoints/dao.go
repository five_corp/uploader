package endpoints

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/olap"
	"bitbucket.org/five_corp/uploader/models/policy"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"bitbucket.org/five_corp/uploader/sections"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

// Structure of the data sent by ag-grid library

// FieldDescriptor defines a field or aggregation
type FieldDescriptor struct {
	Field string `json:"field"`
	Agg   string `json:"aggFunc"`
}

// FilterDescriptor defines the filtering for a field
type FilterDescriptor struct {
	Type       string        `json:"type"`
	Filter     interface{}   `json:"filter"`
	FilterTo   interface{}   `json:"filterTo"`
	FilterType string        `json:"filterType"`
	Values     []interface{} `json:"values"`
}

// SortDescriptor defines the sorting order for a field
type SortDescriptor struct {
	ID   string `json:"colId"`
	Sort string `json:"sort"`
}

// RequestDescriptor defines the queries made to the DAO endpoint
type RequestDescriptor struct {
	StartRow     int                         `json:"startRow"`
	EndRow       int                         `json:"endRow"`
	RowGroupCols []FieldDescriptor           `json:"rowGroupCols"`
	ValueCols    []FieldDescriptor           `json:"valueCols"`
	PivotCols    []FieldDescriptor           `json:"pivotCols"`
	GroupKeys    []interface{}               `json:"groupKeys"`
	FilterModel  map[string]FilterDescriptor `json:"filterModel"`
	SortModel    []SortDescriptor            `json:"sortModel"`
	GroupAll     bool                        `json:"groupAll"`
	Echo         bool                        `json:"echo"`
	Options      map[string]interface{}      `json:"options"`
}

type responseDescriptor struct {
	Limit     int
	PivotKeys []dbtool.Row
	Statement dbtool.Statement
	dbtool.Feed
}

type daoKey int

const daoKey0 daoKey = 0

type daoHandler struct {
	sections  saferest.Sectioner
	daoStore  olap.Store
	switcher  tenants.Switcher
	pMap      policy.Map
	batchSize int
	unprotect bool
}

// DaoRoutes for this endpoint
func DaoRoutes(sectioner saferest.Sectioner, switcher tenants.Switcher, daoStore olap.Store, pm policy.Map, batchSize int, unprotect bool) []saferest.Route {
	// Create the handler and reload queries
	handler := daoHandler{
		sections:  sectioner,
		daoStore:  daoStore,
		switcher:  switcher,
		pMap:      pm,
		unprotect: unprotect,
		batchSize: batchSize,
	}
	if daoStore == nil {
		return nil
	}
	return []saferest.Route{
		saferest.Route{
			Keyer:    switcher,
			Audience: "p:dao",
			RBAC: func(r *http.Request, claims *jwt.StandardClaims) (*http.Request, int, error) {
				return handler.rbac(r, claims)
			},
			Method:   "POST",
			Pattern:  "/dao/{schema}",
			Private:  !unprotect,
			Compress: true,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPost(w, r)
			},
		},
	}
}

// Data shared between rbac and the handlers
type rbacData struct {
	Request RequestDescriptor
	Slice   *olap.Slice
}

// Checks RBAC for POST requests
func (h *daoHandler) rbac(r *http.Request, claims *jwt.StandardClaims) (*http.Request, int, error) {
	var policy policy.Policy
	var err error
	if claims == nil {
		// If unprotected, go ahead without JWT
		if h.unprotect {
			log.Print("Dao request without token, allowed because of unprotect.")
		} else {
			// Otherwise return error
			return r, http.StatusUnauthorized, errors.New("Missing jwt claims in endpoints.dao request")
		}
	} else {
		// Get the policy from the claims
		policy, err = h.pMap.Policy(claims)
		if err != nil {
			return r, http.StatusForbidden, err
		}
	}
	// Get the schema
	schema := mux.Vars(r)["schema"]
	slice, err := h.daoStore.Slice(policy, schema)
	if err != nil {
		return r, http.StatusNotFound, err
	}
	// Get the request
	request := RequestDescriptor{}
	if err := saferest.GetJSONBody(r.Header, r.Body, &request); err != nil {
		return r, http.StatusBadRequest, err
	}
	// Save the request and slices for later
	r = r.WithContext(context.WithValue(r.Context(), daoKey0, rbacData{
		Request: request,
		Slice:   slice,
	}))
	return r, http.StatusOK, nil
}

// Caches a set of parameters for further usage with GET
func (h *daoHandler) httpPost(w http.ResponseWriter, r *http.Request) {
	// Get the request and slice after RBAC
	data, ok := r.Context().Value(daoKey0).(rbacData)
	if !ok {
		err := errors.New("POST parameters are invalid")
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Get the Slice
	params := dbtool.Params(data.Request.Options)
	if params == nil {
		params = make(dbtool.Params)
	}
	if err := h.reslice(data.Slice, params, data.Request); err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Get the request
	query, err := data.Request.Query()
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Get the selection
	selector, err := h.daoStore.Select(data.Slice, query, params)
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Get the store
	issuer := saferest.IssuerOf(r)
	store, ok := h.switcher.Switch(issuer)
	if !ok {
		err := errors.Errorf("Failed to switch to database of issuer %s", issuer)
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	rowLimit := sections.GetQueries(h.sections).RowLimit
	// Pivot the Selector
	pivotKeys, err := selector.PivotKeys(r.Context(), store, rowLimit, h.batchSize)
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	selector = selector.Pivot(pivotKeys)
	// Get the statement and the database
	statement := selector.Statement()
	feed, err := store.Run(r.Context(), statement, nil, selector.Columns, rowLimit, h.batchSize)
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	defer feed.Close()
	feed.Names = selector.Columns
	rd := responseDescriptor{
		Feed:  feed,
		Limit: -1,
	}
	if data.Request.Echo {
		rd.Statement = statement
	}
	if data.Request.StartRow >= 0 && data.Request.EndRow > data.Request.StartRow {
		rd.Limit = (data.Request.EndRow - data.Request.StartRow) // semi-open interval
	}
	_ = (saferest.Streamer)(rd)
	saferest.SuccessPlain(w, rd)
}

// reslice further slices the selection by applying the filters in the Request
func (h *daoHandler) reslice(slice *olap.Slice, params dbtool.Params, request RequestDescriptor) error {
	// Add filter parameters
	filters, err := request.Filters(params)
	if err != nil {
		return err
	}
	if err := h.daoStore.Intersect(slice, filters); err != nil {
		return err
	}
	return nil
}

// Query builds an olap.Query from the request descriptor
func (request *RequestDescriptor) Query() (olap.Query, error) {
	query := olap.Query{
		Offset: request.StartRow,
		Limit:  (request.EndRow - request.StartRow), // semi-open interval
		Sort:   make([]olap.Sorting, 0, len(request.SortModel)),
		Fields: make([]string, 0, len(request.RowGroupCols)+len(request.ValueCols)),
		Groups: make([]olap.GroupDescriptor, 0, len(request.ValueCols)),
		Pivot:  make([]string, 0, len(request.PivotCols)),
	}
	// Add sorting parameters
	for _, m := range request.SortModel {
		query.Sort = append(query.Sort, olap.Sorting{Field: m.ID, Descending: (strings.EqualFold(m.Sort, "DESC"))})
	}
	// Select * if there is no grouping, or the number of groups matches the number of keys
	if len(request.RowGroupCols) == 0 || len(request.GroupKeys) >= len(request.RowGroupCols) {
		query.Fields = append(query.Fields, "*__*")
	} else {
		// Add up to the first unfiltered key, or all if GroupAll == true
		top := len(request.GroupKeys) + 1
		if request.GroupAll {
			top = len(request.RowGroupCols)
		}
		for i := 0; i < top; i++ {
			query.Fields = append(query.Fields, request.RowGroupCols[i].Field)
		}
		// Add aggregated values
		for _, m := range request.ValueCols {
			if m.Agg == "" {
				query.Fields = append(query.Fields, m.Field)
			} else if group, err := request.newGroup(m.Field, m.Agg); err == nil {
				query.Groups = append(query.Groups, group)
			} else {
				return olap.Query{}, err
			}
		}
	}
	// Add pivot columns
	for _, m := range request.PivotCols {
		query.Pivot = append(query.Pivot, m.Field)
	}
	return query, nil
}

// Filters combines regular filters with grouping filters, into a single filter list
func (request *RequestDescriptor) Filters(params dbtool.Params) (map[string]olap.Filter, error) {
	filters := make(map[string]olap.Filter)
	for attrib, m := range request.FilterModel {
		switch m.FilterType {
		case "set":
			m.Type = "equals"
			m.Filter = m.Values
		}
		filter, err := m.newFilter(params)
		if err != nil {
			return nil, err
		}
		filters[attrib] = filter
	}
	// Add group filters
	for idx, m := range request.RowGroupCols {
		if idx >= len(request.GroupKeys) {
			break
		}
		filter, err := FilterDescriptor{
			Type:   "equals",
			Filter: request.GroupKeys[idx],
		}.newFilter(params)
		if err != nil {
			return nil, err
		}
		filters[m.Field] = filter
	}
	return filters, nil
}

// NewFilter builds an olap.Filter from the descriptor, adding parameters to params
func (fd FilterDescriptor) newFilter(params dbtool.Params) (olap.Filter, error) {
	value, filterName := fd.Filter, fmt.Sprintf("o%d", len(params))
	var filterFunc olap.Filter
	switch fd.Type {
	case "equals":
		fallthrough
	case "equal":
		filterFunc = olap.Equals(filterName)
	case "notEquals":
		fallthrough
	case "notEqual":
		filterFunc = olap.NotEquals(filterName)
	case "startsWith":
		if vString, ok := value.(string); ok {
			filterFunc = olap.Left(filterName, len(vString))
		}
	case "endsWith":
		if vString, ok := value.(string); ok {
			filterFunc = olap.Right(filterName, len(vString))
		}
	case "contains":
		if vString, ok := value.(string); ok {
			filterFunc = olap.Like(filterName)
			value = fmt.Sprintf("%%%s%%", vString)
		}
	case "lessThan":
		filterFunc = olap.Range("", filterName, false)
	case "lessThanOrEqual":
		filterFunc = olap.Range("", filterName, true)
	case "greaterThan":
		filterFunc = olap.Range(filterName, "", false)
	case "greaterThanOrEqual":
		filterFunc = olap.Range(filterName, "", true)
	case "inRange":
		// Special case, this filter has two parameters
		secondFilter := fmt.Sprintf("%s.to", filterName)
		filterFunc = olap.Range(filterName, secondFilter, true)
		params[secondFilter] = fd.FilterTo
	}
	if filterFunc == nil {
		return nil, errors.Errorf("Filter function %s unknown, or invalid values", fd.Type)
	}
	params[filterName] = value
	return filterFunc, nil
}

// newGroup creates a GroupDescriptor from a Query field and grouping
func (request *RequestDescriptor) newGroup(field, group string) (olap.GroupDescriptor, error) {
	g := olap.Nop
	switch group {
	case "count":
		g = olap.Count
	case "count_distinct":
		g = olap.CountDistinct
	case "sum":
		g = olap.Sum
	case "avg":
		g = olap.Avg
	case "min":
		g = olap.Min
	case "max":
		g = olap.Max
	default:
		return olap.GroupDescriptor{}, errors.Errorf("Aggregation function %s not known", group)
	}
	return olap.GroupDescriptor{
		Group: g,
		Field: field,
	}, nil
}

// Stream implements saferest.Streamer interface
func (r responseDescriptor) Stream(w io.Writer) error {
	pivotKeys, err := json.Marshal(r.PivotKeys)
	if err != nil {
		return errors.New("Failed to dump responseDescriptor Pivot Keys")
	}
	var statement []byte
	if r.Statement.SQL != "" {
		if statement, err = json.Marshal(r.Statement); err != nil {
			return errors.New("Failed to dump responseDescriptor Statement")
		}
	}
	if _, err := w.Write([]byte(fmt.Sprintf("{ \"pivotKeys\": %s, \"statement\": %s, \"rows\": [", pivotKeys, statement))); err != nil {
		return errors.New("Failed to dump responseDescriptor header")
	}
	m := make(map[string]interface{})
	count, sep := 0, ""
	for r.Next() {
		for _, row := range r.Batch() {
			count++
			for idx, name := range r.Feed.Names {
				m[name] = row[idx]
			}
			data, err := json.Marshal(m)
			if err != nil {
				return errors.Wrapf(err, "Failed to marshal row %v", m)
			}
			if _, err = w.Write([]byte(sep + string(data))); err != nil {
				return errors.Wrap(err, "Failed to write data")
			}
			sep = ","
		}
	}
	if err := r.Err(); err != nil {
		return err
	}
	if r.Limit > 0 && count < r.Limit {
		_, err = w.Write([]byte(fmt.Sprintf("], \"lastRow\": %d }", count)))
	} else {
		_, err = w.Write([]byte("], \"lastRow\": null }"))
	}
	if err != nil {
		return errors.Wrap(err, "Failed to dump responseDescriptor tail")
	}
	return nil
}
