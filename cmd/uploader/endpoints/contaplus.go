package endpoints

import (
	"net/http"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/contaplus"
)

// ContaplusRoutes defines the routes for this endpoint
func ContaplusRoutes(unprotect bool) []saferest.Route {
	return []saferest.Route{
		saferest.Route{
			Audience: "g:contaplus",
			Method:   "GET",
			Pattern:  "/contaplus",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler:  contaplusGet,
		},
	}
}

// Gets the list of companies and years
func contaplusGet(w http.ResponseWriter, r *http.Request) {
	data, err := contaplus.Companies(r.URL.Query().Get("path"))
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// And return the list of companies and years!
	saferest.Success(w, data)
}
