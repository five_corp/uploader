package endpoints

import (
	"html/template"
)

// Default embedded index page for uploading content
var defaultIndex, _ = template.New("index").Parse(`
<html>
  <head>
    <title>File upload</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script>
    $(document).ready(function() {
      $('#uploadForm').submit(function() {
        console.log($('#file'));
        $("#status").empty().text("File is uploading...");
        $(this).ajaxSubmit({
          error: function(xhr) {
            $("#status").empty().text("Server returned error " + xhr.status)
          },
          dataType: 'json',
          success: function(response) {
            $("#status").empty().text("Server returned message " + response.message);
          }
        });
        return false;
      });    
    });
    </script>
  </head>
  <body>
      <form id="uploadForm"
          enctype="multipart/form-data"
          method="post">
      <input type="text" name="{{ .FolderField }}" id="{{ .FolderField }}" value=""/>
      <input type="file" name="{{ .UploadField }}" id="{{ .UploadField }}"/>
      <input type="submit" value="Upload File" name="submit">
      <span id="status"></span>
    </form>
  </body>
</html>
`)
