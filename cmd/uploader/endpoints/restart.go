package endpoints

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
)

// Reloader interface that affects all tenants
type Reloader interface {
	Reload() error
}

// What to restart
type restartSpec struct {
	// Fields must be public to be marshaled / unmarshaled!
	ServiceReload  bool `json:"serviceReload"`
	ServiceRestart bool `json:"serviceRestart"`
	DBRestart      bool `json:"dbRestart"`
}

// restart route handler
type restartHandler struct {
	reloaders []Reloader
	dbtool.Store
}

// RestartRoutes defines the routes for this endpoint
func RestartRoutes(store dbtool.Store, reloaders []Reloader, unprotect bool) []saferest.Route {
	handler := restartHandler{
		reloaders: reloaders,
		Store:     store,
	}
	return []saferest.Route{
		saferest.Route{
			Audience: "u:restart",
			Method:   "PUT",
			Pattern:  "/restart",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPut(w, r)
			},
		},
	}
}

func (h restartHandler) httpPut(w http.ResponseWriter, r *http.Request) {
	data := restartSpec{}
	if err := saferest.GetJSONBody(r.Header, r.Body, &data); err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Restart the database, if requested
	if data.ServiceReload {
		log.Print("Requested service reload - cache clean and endpoint reload")
		for _, endpoint := range h.reloaders {
			if err := endpoint.Reload(); err != nil {
				log.Print("Error reloading service: ", err)
			}
		}
	}
	if data.DBRestart {
		log.Print("Requested database reset")
		if err := h.Restart(); err != nil {
			saferest.Fail(http.StatusInternalServerError, w, err)
			return
		}
	}
	if !data.ServiceRestart {
		saferest.Success(w, "No more services to restart")
		return
	}
	saferest.Success(w, "Shutdown started")
	log.Print("Shutting down initiated...")
	defer os.Exit(0)
}
