package endpoints

import (
	"bytes"
	"crypto/x509"
	"encoding/pem"
	"io"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/sections"
)

// Certificate route handler
type certHandler struct {
	saferest.SectionSaver
	dbtool.Store
}

// CertRoutes defines the routes for this endpoint
func CertRoutes(manager saferest.SectionSaver, store dbtool.Store, unprotect bool) []saferest.Route {
	handler := certHandler{
		SectionSaver: manager,
		Store:        store,
	}
	return []saferest.Route{
		saferest.Route{
			Audience: "p:cert",
			Method:   "POST",
			Pattern:  "/cert",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPost(w, r)
			},
		},
	}
}

// struct expected by "post"
type certUpdate struct {
	Enable   bool   `json:"enable"`
	DNSName  string `json:"dnsName"`
	CertFile string `json:"certFile"`
	KeyFile  string `json:"keyFile"`
}

// Updates the certificate config file
func (h certHandler) httpPost(w http.ResponseWriter, r *http.Request) {
	// Get certificate info
	certConfig := saferest.GetCert(h)
	data := certUpdate{}
	if err := saferest.GetJSONBody(r.Header, r.Body, &data); err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	if !data.Enable {
		// If disable stored certs, make sure names are empty
		data.CertFile = ""
		data.KeyFile = ""
	} else {
		// Get the configured Root CAs
		rootCA, err := saferest.GetRootCA(saferest.GetCert(h))
		if err != nil {
			err = errors.Wrap(err, "Could not load root CA")
			saferest.Fail(http.StatusNotFound, w, err)
			return
		}
		// Get the certificate passed in as argument
		certBlock, _ := pem.Decode([]byte(data.CertFile))
		if certBlock == nil {
			err := errors.New("Could not decode PEM certificate data")
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		cert, err := x509.ParseCertificate(certBlock.Bytes)
		if err != nil {
			err = errors.Wrap(err, "Could not make a certificate out of PEM data")
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		// Try to validate it against the known root CAs
		opts := x509.VerifyOptions{
			DNSName: data.DNSName,
			Roots:   rootCA,
		}
		certChain, err := cert.Verify(opts)
		if err != nil {
			err = errors.Wrap(err, "The certificate cannot be validated")
			saferest.Fail(http.StatusUnauthorized, w, err)
			return
		}
		// Get the private key as PEM data
		keyBlock, _ := pem.Decode([]byte(data.KeyFile))
		if keyBlock == nil {
			err := errors.New("Could not decode PEM private key data")
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		// Make sure it is a private key
		if !strings.Contains(keyBlock.Type, "PRIVATE KEY") {
			err := errors.New("PEM block does not look like a Private Key")
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		// Set a name for the new cert and key. Try to avoid overwriting
		// the current ones
		certFile := "cert-0.pem"
		keyFile := "key-0.pem"
		if strings.Contains(certConfig.CertFile, "0") {
			// Flip from 0 to 1, naive but effective
			certFile = "cert-1.pem"
			keyFile = "key-1.pem"
		}
		fileConfig := sections.GetUploads(h)
		data.CertFile = filepath.Join(fileConfig.CertFolder, certFile)
		data.KeyFile = filepath.Join(fileConfig.CertFolder, keyFile)
		// Serialize the PEM certificate and key to the certificate folder
		var certPem []byte
		if len(certChain) <= 0 {
			certPem = pem.EncodeToMemory(certBlock)
		} else {
			// The chain already includes the leaf certificate.
			for _, parent := range certChain[0] {
				block := &pem.Block{Type: "CERTIFICATE", Bytes: parent.Raw}
				certPem = append(certPem, pem.EncodeToMemory(block)...)
			}
		}
		readers := []io.Reader{
			bytes.NewReader(certPem),
			bytes.NewReader(pem.EncodeToMemory(keyBlock)),
		}
		// Save them PEM certificate and key to the certificate folder
		for index, fileName := range []string{data.CertFile, data.KeyFile} {
			// Won't encrypt the certs, they may need to be read by the DB server.
			if err := saferest.OverwriteFile(fileName, readers[index], nil); err != nil {
				err = errors.Wrapf(err, "Could not save file %s", fileName)
				saferest.Fail(http.StatusInternalServerError, w, err)
				return
			}
		}
	}
	// Update a copy of the configo object, and try to save it
	newCertConfig := certConfig
	newCertConfig.CertFile = data.CertFile
	newCertConfig.KeyFile = data.KeyFile
	if err := h.Save(saferest.SectionCert, newCertConfig); err != nil {
		// Restore previous config
		saferest.Fail(http.StatusInternalServerError, w, err)
		return
	}
	// Update the database config too
	// Configure the database certificates
	certs := dbtool.CertFiles{
		RootFile: newCertConfig.RootFile,
		CertFile: newCertConfig.CertFile,
		KeyFile:  newCertConfig.KeyFile,
	}
	if err := h.SetCerts(data.Enable, certs); err != nil {
		saferest.Fail(http.StatusInternalServerError, w, err)
		return
	}
	// If success, get a copy of the DB object and update it
	dbConfig := sections.GetDatabase(h)
	newDBConfig := dbConfig
	newDBConfig.TLS = data.Enable
	if err := h.Save(sections.SectionDatabase, newDBConfig); err != nil {
		saferest.Fail(http.StatusInternalServerError, w, err)
		return
	}
	// And restart database if TLS or path changed
	if newDBConfig.TLS != dbConfig.TLS || newCertConfig.CertFile != certConfig.CertFile {
		// We do not fail the call if the database cannot be restarted,
		// because it may be down for any reason, but it has been
		// correctly configured anyway.
		h.Restart()
	}
	saferest.Success(w, data)
}
