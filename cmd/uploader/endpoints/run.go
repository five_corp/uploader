package endpoints

import (
	"net/http"
	"strconv"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/tasker"
	"github.com/pkg/errors"
)

// Status route handler
type runHandler struct {
	// Task runner
	runner tasker.Runner
}

// RunRoutes defines the routes for this endpoint
func RunRoutes(runner tasker.Runner, keyer saferest.Keyer, unprotect bool) []saferest.Route {
	handler := runHandler{
		runner: runner,
	}
	return []saferest.Route{
		saferest.Route{
			Keyer:    keyer,
			Audience: "g:run",
			Method:   "GET",
			Pattern:  "/run",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
		saferest.Route{
			Keyer:    keyer,
			Audience: "p:run",
			Method:   "POST",
			Pattern:  "/run",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPost(w, r)
			},
		},
	}
}

// Launches the application remotely, if not running yet.
func (h *runHandler) httpPost(w http.ResponseWriter, r *http.Request) {
	h.runPlugin(w, r)
}

// Launches the application remotely, if not running yet.
func (h *runHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	checkFinished := r.URL.Query().Get("check")
	if checkFinished != "" {
		order, err := strconv.ParseUint(checkFinished, 10, 64)
		if err != nil {
			errors.Wrap(err, "check must be an unsigned int")
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		latest, ok := h.runner.Queued(saferest.IssuerOf(r), order)
		status := conectorStatus{
			Running: false,
			Started: false,
		}
		if ok && latest.Order == order {
			// The job we are checking is still queued.
			status = conectorStatus{
				Running: true,
				Order:   strconv.FormatUint(order, 10),
				Since:   latest.Since,
				Started: !latest.Overlap,
			}
		}
		saferest.Success(w, status)
		return
	}
	// Not checking, let's trigger a run then.
	h.runPlugin(w, r)
}

// Runs a task with or without args, depending on the "refresh" param
func (h *runHandler) runPlugin(w http.ResponseWriter, r *http.Request) {
	var args []string
	refresh := r.FormValue("refresh")
	if refresh != "" {
		// Emulating a plugin, let's allow overlap!
		args = []string{refresh}
	}
	result, err := h.runner.AddTask("", saferest.IssuerOf(r), args, false)
	if err != nil {
		saferest.Fail(http.StatusNotAcceptable, w, err)
	} else {
		status := conectorStatus{
			Running: true,
			Order:   strconv.FormatUint(result.Order, 10),
			Since:   result.Since,
			Started: !result.Overlap,
		}
		saferest.Success(w, status)
	}
}
