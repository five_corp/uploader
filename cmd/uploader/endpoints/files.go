package endpoints

import (
	"html/template"
	"log"
	"net/http"
	"path/filepath"
	"strings"
	"sync"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/sections"
	"github.com/pkg/errors"
)

// File upload handler
type fileHandler struct {
	// Upload section config
	config sections.UploadsConfig
	// Upload Folder
	folder string
	// Max depth (nested dirs) below folder
	depth int
	// Error detected while creating directory
	dirError error
	// Template to show on GET
	template *template.Template
	// Max memory size (bytes)
	maxFileMemoryBytes uint64
	// Map of files currently being uploaded
	current map[string]bool
	// Mutex to protect the map
	mutex sync.Mutex
	// Verifier for uploaded files
	verifier saferest.Verifier
}

// FileRoutes defines the routes for this endpoint
func FileRoutes(manager saferest.Sectioner, pattern, folder string, verifier saferest.Verifier, unprotect bool) []saferest.Route {
	serverConfig := saferest.GetServer(manager)
	uploadConfig := sections.GetUploads(manager)
	handler := &fileHandler{
		config:             uploadConfig,
		folder:             folder,
		depth:              uploadConfig.MaxDepth,
		dirError:           nil,
		template:           defaultIndex,
		maxFileMemoryBytes: uint64(serverConfig.MaxFileMemory) << 20,
		current:            make(map[string]bool),
		mutex:              sync.Mutex{},
		verifier:           verifier,
	}
	// Create upload folder if it doesn't exist
	dirError := saferest.EnsureDir(folder, false)
	// Turn folder name into absolute path, so we can check
	// that whatever the user throws at us later, it lies
	// below the same realpath.
	if dirError == nil {
		if abspath, err := filepath.Abs(folder); err != nil {
			dirError = errors.Wrapf(err, "Failed to get absolute path of %s", folder)
		} else {
			handler.folder = abspath
			// We will be comparing absolute paths, so add the
			// allowed depth to the depth of the base path
			handler.depth += len(saferest.PathElems(abspath))
		}
	}
	handler.dirError = dirError
	// Load specified template, if any
	if handler.config.UploadPage != "" {
		if index, err := template.ParseFiles(handler.config.UploadPage); err != nil {
			log.Printf("Error loading template file %s (%v). Defaulting to builtin", handler.config.UploadPage, err)
		} else {
			handler.template = index
		}
	}
	// Supported methods
	return []saferest.Route{
		saferest.Route{
			Audience: "g:" + pattern,
			Method:   "GET",
			Pattern:  "/" + pattern,
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
		saferest.Route{
			Audience: "p:" + pattern,
			Method:   "POST",
			Pattern:  "/" + pattern,
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPost(w, r)
			},
		},
	}
}

// Shows the file upload page
// See https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/04.5.html
func (h *fileHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	// If we could not open the folder, further processing is useless
	if h.dirError != nil {
		err := errors.Wrapf(h.dirError, "Could not stat folder %s", h.folder)
		saferest.Fail(http.StatusInternalServerError, w, err)
		return
	}
	w.Header().Set("Content-Type", "text/html; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if h.template.Execute(w, h.config) != nil {
		log.Print("Error executing template")
	}
}

// Uploads a file
// See https://astaxie.gitbooks.io/build-web-application-with-golang/content/en/04.5.html
func (h *fileHandler) httpPost(w http.ResponseWriter, r *http.Request) {
	// If we could not open the folder, further processing is useless
	if h.dirError != nil {
		err := errors.Wrapf(h.dirError, "Could not stat folder %s", h.folder)
		saferest.Fail(http.StatusInternalServerError, w, err)
		return
	}
	// Limit used memory, convert mbytes to bytes
	r.ParseMultipartForm(int64(h.maxFileMemoryBytes))
	// Read the input folder and file
	source, handler, err := r.FormFile(h.config.UploadField)
	if err != nil {
		err = errors.Wrap(err, "Failed to parse form file")
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	defer source.Close()
	// Get the folder and append it to the configured path
	folder := r.FormValue(h.config.FolderField)
	if folder == "" {
		folder = h.folder
	} else {
		// The folder path should be sent with slashes; we
		// replace it here with the right path separator
		folder = filepath.Join(saferest.PathElems(folder)...)
		folder = filepath.Join(h.folder, folder)
	}
	// Combine the given folder and file into a single path, and
	// split again. This way, we won't be fooled by simple tricks
	// like adding a '/' or '..' to the file name.
	fullpath := filepath.Join(folder, handler.Filename)
	finalfolder, filename := filepath.Split(fullpath)
	// Now check the folder is valid
	if finalfolder != "" {
		// ".." is not allowed in path
		if strings.Contains(finalfolder, "..") {
			err := errors.Errorf("File name '%s' has forbidden characters", folder)
			saferest.Fail(http.StatusUnauthorized, w, err)
			return
		}
		// We request an absolute path to the folder
		finalfolder, err = filepath.Abs(finalfolder)
		if err != nil {
			err = errors.Wrap(err, "Failed to get absolute path to file")
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		// The absolute path must be below the configured path
		if !strings.HasPrefix(finalfolder, h.folder) {
			err := errors.Errorf("Requested path %s is not allowed", folder)
			saferest.Fail(http.StatusUnauthorized, w, err)
			return
		}
		// The path can only be nested the allowed depth
		if len(saferest.PathElems(finalfolder)) > h.depth {
			err := errors.Errorf("Too many nested directories in %s", folder)
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		// If the folder name is valid, it must be a directory
		if err := saferest.EnsureDir(finalfolder, false); err != nil {
			err = errors.Wrapf(err, "The given path %s does not belong to a directory", finalfolder)
			saferest.Fail(http.StatusNotAcceptable, w, err)
			return
		}
		// Everything OK! rebuild the filename
		filename = filepath.Join(finalfolder, filename)
	}
	// Finally check if we are already writing a file the same name.
	if !h.lockFile(filename) {
		err := errors.New("Updating the same file concurrently is not allowed")
		saferest.Fail(http.StatusConflict, w, err)
		return
	}
	defer h.unlockFile(filename)
	// If we have verifiers, check if the file must be hashed
	var vf saferest.VerifyFunc
	if h.verifier != nil {
		signature := r.FormValue(h.config.SignatureField)
		if signature == "" {
			err := errors.New("File upload must be signed")
			saferest.Fail(http.StatusNotAcceptable, w, err)
			return
		}
		vf = saferest.VerifyFunc(func(hash []byte) error {
			return h.verifier.Verify(hash, signature)
		})
	}
	// Copy / Overwrite the file
	// I may decide to store these files encrypted, at some point in time...
	if err := saferest.OverwriteFile(filename, source, vf); err != nil {
		saferest.Fail(http.StatusInternalServerError, w, err)
		return
	}
	saferest.Success(w, filename)
}

// Tries to lock the file for writing
func (h *fileHandler) lockFile(filename string) bool {
	h.mutex.Lock()
	defer h.mutex.Unlock()
	if h.current[filename] {
		return false
	}
	h.current[filename] = true
	return true
}

// Unlocks a locked file
func (h *fileHandler) unlockFile(filename string) {
	h.mutex.Lock()
	delete(h.current, filename)
	h.mutex.Unlock()
}
