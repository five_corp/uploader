package endpoints

import (
	"context"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/policy"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"bitbucket.org/five_corp/uploader/sections"
	"github.com/pkg/errors"
)

// Token route handler
type tokenHandler struct {
	saferest.Sectioner
	tenants.Store
	policyMap policy.Map
}

// TokenRoutes for this endpoint
func TokenRoutes(manager saferest.Sectioner, store tenants.Store, pm policy.Map, unprotect, debug bool) []saferest.Route {
	handler := tokenHandler{Sectioner: manager, Store: store, policyMap: pm}
	routes := []saferest.Route{
		saferest.Route{
			Keyer:    store,
			Audience: "g:token",
			RBAC:     saferest.NullRBAC, // Allow anyone to check their token
			Method:   "GET",
			Pattern:  "/token",
			Private:  true,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
		saferest.Route{
			Audience: "u:token",
			Method:   "PUT",
			Pattern:  "/token",
			Private:  false,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPut(w, r)
			},
		},
		saferest.Route{
			Keyer:    store,
			Audience: "p:token",
			Method:   "POST",
			Pattern:  "/token",
			Private:  true,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPost(w, r)
			},
		},
	}
	return routes
}

// Message sent via PUT to update the token secret
type tokenUpdate struct {
	Secret           string `json:"secret"`
	Subject          string `json:"subject"`
	OverwriteDefault bool   `json:"overwriteDefault"`
}

// Message sent via POST to get a token for a subject
type tokenPost struct {
	Subject  string              `json:"subject"`
	Seconds  int                 `json:"seconds"`
	Audience []string            `json:"audience"`
	Scope    []string            `json:"scope"`
	Filters  map[string][]string `json:"filters"`
}

// Response expected from Home to validate a token
type homeResponse struct {
	Success bool `json:"success"`
}

// Generate a token with the given subject and policy.
func (h tokenHandler) httpPost(w http.ResponseWriter, r *http.Request) {
	// Read token data
	data := tokenPost{}
	if err := saferest.GetJSONBody(r.Header, r.Body, &data); err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Subject cannot be empty
	if data.Subject == "" {
		err := errors.New("Subject not provided for token")
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Update expiration, if not provided
	seconds := data.Seconds
	if seconds <= 0 {
		seconds = 60
	}
	expiration := time.Duration(data.Seconds) * time.Second
	// Add the new policy to the known policy map
	issuer := saferest.IssuerOf(r)
	subjectClaim := h.policyMap.Add(issuer, data.Subject, data.Scope, data.Filters)
	// And build and return a new token
	audienceClaim := strings.Join(data.Audience, saferest.HeaderSeparator)
	token := saferest.GetToken(h).WithSubject(saferest.IssuerOf(r), subjectClaim).Sign(audienceClaim, expiration)
	saferest.Success(w, token)
}

// Just check the token is valid.
func (h tokenHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	saferest.Success(w, "token is valid")
}

// Updates the token secret as provided by PUT
func (h tokenHandler) httpPut(w http.ResponseWriter, r *http.Request) {
	// Body should fit in 1KByte
	data := tokenUpdate{}
	if err := saferest.GetJSONBody(r.Header, r.Body, &data); err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	if err := h.checkUpdate(r.Context(), data); err != nil {
		saferest.Fail(http.StatusUnauthorized, w, err)
		return
	}
	// If the old subject is "bootstrap", assume we are updating the default tenant name and token
	if saferest.GetToken(h).Subject == "bootstrap" || data.OverwriteDefault {
		if _, err := h.Store.Rename(data.Subject, data.Secret); err != nil {
			saferest.Fail(http.StatusInternalServerError, w, err)
			return
		}
	} else {
		if found, err := h.Store.Update(data.Subject, data.Secret); err != nil {
			saferest.Fail(http.StatusInternalServerError, w, err)
			return
		} else if !found {
			err := errors.Errorf("Issuer %s not found, cannot update token", data.Subject)
			saferest.Fail(http.StatusInternalServerError, w, err)
			return
		}
	}
	saferest.Success(w, data)
}

// Validates token credentials against the HomeTokenURL address
func (h tokenHandler) checkUpdate(ctx context.Context, t tokenUpdate) error {
	// Send the request to the backend.
	homeConfig := sections.GetCallHome(h)
	tokenCopy := saferest.GetToken(h)
	tokenCopy.Subject = t.Subject
	tokenCopy.Secret = t.Secret
	// Sign a request with the new token and send it home
	token := tokenCopy.Sign(homeConfig.TokenAudience, time.Second*60)
	data := homeResponse{}
	if err := saferest.RestRequest(ctx, "GET", homeConfig.TokenURL, token, homeConfig.Timeout, nil, false, &data); err != nil {
		return err
	}
	// Check operation success
	if !data.Success {
		return errors.New("Home returned operation unsuccessful")
	}
	return nil
}
