package endpoints

import (
	"context"
	"log"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/policy"
	"bitbucket.org/five_corp/uploader/models/queries"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"bitbucket.org/five_corp/uploader/sections"
)

type queryHandler struct {
	saferest.Sectioner
	tenants.Switcher
	queries.Manager
	unprotect bool
	policyMap policy.Map
}

type key int

var queryKey key

// QueryRoutes for this endpoint
func QueryRoutes(sectioner saferest.Sectioner, keyer saferest.Keyer, manager queries.Manager, pm policy.Map, unprotect bool) []saferest.Route {
	// Create the handler and reload queries
	handler := queryHandler{
		Sectioner: sectioner,
		Manager:   manager,
		unprotect: unprotect,
		policyMap: pm,
	}
	return []saferest.Route{
		saferest.Route{
			Keyer:    keyer,
			Audience: "g:queries",
			Method:   "GET",
			Pattern:  "/queries",
			Private:  !unprotect,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGetQueries(w, r)
			},
		},
		saferest.Route{
			Keyer:    keyer,
			Audience: "g:query",
			RBAC: func(r *http.Request, claims *jwt.StandardClaims) (*http.Request, int, error) {
				return handler.getRBAC(r, claims)
			},
			Method:   "GET",
			Pattern:  "/query/{query}",
			Private:  !unprotect,
			Compress: true,
			Cache:    true,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
		saferest.Route{
			Keyer:    keyer,
			Audience: "g:posted",
			Method:   "GET",
			Pattern:  "/posted/{posted}",
			Private:  !unprotect,
			Compress: true,
			Cache:    true,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGetPosted(w, r)
			},
		},
		saferest.Route{
			Keyer:    keyer,
			Audience: "p:query",
			RBAC: func(r *http.Request, claims *jwt.StandardClaims) (*http.Request, int, error) {
				return handler.postRBAC(r, claims)
			},
			Method:   "POST",
			Pattern:  "/query",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPost(w, r)
			},
		},
	}
}

// Checks RBAC for GET requests
func (h *queryHandler) getRBAC(r *http.Request, claims *jwt.StandardClaims) (*http.Request, int, error) {
	// Get the query parameters from GET queryString
	packed := make(dbtool.Params)
	for key, val := range r.URL.Query() {
		if len(val) > 0 {
			packed[key] = val[0]
		}
	}
	// If one of the parameters is "_posted", replace
	// the params array with the previously posted one
	if postedItem, ok := packed["_posted"]; ok {
		hkey, ok := postedItem.(string)
		if !ok {
			return r, http.StatusBadRequest, errors.New("Unexpected: parameter _posted is not string")
		}
		cachedItem, err := h.Get(saferest.IssuerOf(r), hkey)
		if err != nil {
			// The client relies on this code to repost the parameters
			return r, http.StatusConflict, err
		}
		packed = cachedItem
	}
	// Get the query name too
	name, ok := mux.Vars(r)["query"]
	if !ok {
		return r, http.StatusBadRequest, errors.New("Missing Query Name")
	}
	// Check if authorized
	if err := h.rbac(r, claims, packed, name); err != nil {
		return r, http.StatusForbidden, err
	}
	// Save packed parameters for later
	newRequest := r.WithContext(context.WithValue(r.Context(), queryKey, packed))
	return newRequest, http.StatusOK, nil
}

// Checks RBAC for POST requests
func (h *queryHandler) postRBAC(r *http.Request, claims *jwt.StandardClaims) (*http.Request, int, error) {
	// Get parameters from POST
	packed := make(dbtool.Params)
	if err := saferest.GetJSONBody(r.Header, r.Body, &packed); err != nil {
		return r, http.StatusBadRequest, err
	}
	// Check if authorized
	if err := h.rbac(r, claims, packed, ""); err != nil {
		return r, http.StatusForbidden, err
	}
	// Save packed parameters for later
	newRequest := r.WithContext(context.WithValue(r.Context(), queryKey, packed))
	return newRequest, http.StatusOK, nil
}

// Check authorization to run the query
func (h *queryHandler) rbac(r *http.Request, claims *jwt.StandardClaims, packed dbtool.Params, query string) error {
	// Get the claims
	if claims == nil {
		// If unprotected, go ahead without JWT
		if h.unprotect {
			log.Print("Query request without token, allowed because of unprotect.")
			return nil
		}
		// Otherwise return error
		return errors.New("Missing jwt claims in endpoints.query request")
	}
	// If there is a query name, it must be within scope
	var (
		scope    []string
		defaults dbtool.Params
		err      error
	)
	if query != "" {
		if scope, defaults, err = h.Scope(query); err != nil {
			return err
		}
	}
	// Check if the request is allowed
	p, err := h.policyMap.Policy(claims)
	if err != nil {
		return err
	}
	return p.Check(scope, defaults, packed)
}

// Caches a set of parameters for further usage with GET
func (h *queryHandler) httpPost(w http.ResponseWriter, r *http.Request) {
	// Get the query parameters after rbac filter is applied
	packed, ok := r.Context().Value(queryKey).(dbtool.Params)
	if !ok {
		err := errors.New("POST query parameters are invalid")
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Store the params and return the hash.
	hkey, err := h.Add(saferest.IssuerOf(r), packed, "_posted")
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	saferest.Success(w, hkey)
}

// Queries the database using a query index
// If one of the parameters is "_posted", replace query
// params with referenced array.
func (h *queryHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	// Get the query parameters after rbac filter is applied
	packed, ok := r.Context().Value(queryKey).(dbtool.Params)
	if !ok {
		err := errors.New("POST query parameters are invalid")
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	// Find and run the query
	name, ok := mux.Vars(r)["query"]
	if !ok {
		err := errors.New("Missing Query Name")
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	result, err := h.Run(r.Context(), saferest.IssuerOf(r), name, packed, sections.GetQueries(h).RowLimit)
	if err != nil {
		saferest.Fail(http.StatusInternalServerError, w, err)
		return
	}
	_ = saferest.Streamer(result)
	saferest.Success(w, result)
}

// Returns the listing of loaded queries and views
func (h *queryHandler) httpGetQueries(w http.ResponseWriter, r *http.Request) {
	saferest.Success(w, h.Queries())
}

func (h *queryHandler) httpGetPosted(w http.ResponseWriter, r *http.Request) {
	posted, ok := mux.Vars(r)["posted"]
	if !ok {
		err := errors.New("Missing Posted value")
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	cachedItem, err := h.Get(saferest.IssuerOf(r), posted)
	if err != nil {
		saferest.Fail(http.StatusInternalServerError, w, err)
	}
	saferest.Success(w, cachedItem)
}
