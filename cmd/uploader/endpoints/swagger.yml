swagger: '2.0'
info:
  title: smartBI-Remote API
  description: API del componente Uploader de smartBI
  version: '1.0.0'
schemes:
  - http
  - https
basePath: /remote/api
consumes:
  - application/json
produces:
  - application/json
paths:

  /status:
    get:
      summary: Service status
      description: Get service status message, including uptime
      tags:
        - Status
      responses:
        200:
          description: Service status
          schema:
            $ref: '#/definitions/StringResponse'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/StringResponse'

  /token:
    get:
      summary: Token check
      description: Check token validity
      security:
        - Bearer: []
      tags:
        - Token
      responses:
        200:
          description: Correct Token
          schema:
            $ref: '#/definitions/StringResponse'
        400:
          description: Bad Request
          schema:
            $ref: '#/definitions/StringResponse'
        401:
          description: Token validation failed
          schema:
            $ref: '#/definitions/StringResponse'
    put:
      summary: Token update
      description: Request callhome for token verification
      parameters:
        - name: tokenUpdate
          in: body
          description: JWT signing parameters
          required: true
          schema:
            $ref: '#/definitions/TokenUpdate'
      tags:
        - Token
      responses:
        200:
          description: Token correctly updated
          schema:
            $ref: '#/definitions/TokenResponse'
        400:
          description: Bad Request
          schema:
            $ref: '#/definitions/StringResponse'
        401:
          description: Token validation failed
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Unable to update configuration
          schema:
            $ref: '#/definitions/StringResponse'
    post:
      summary: Generate token
      description: Generate a token with the given credentials
      parameters:
        - name: tokenPost
          in: body
          description: Token parameters
          required: true
          schema:
            $ref: '#/definitions/TokenPost'
      tags:
        - Token
      responses:
        200:
          description: Generated Token
          schema:
            $ref: '#/definitions/StringResponse'
        400:
          description: Bad Request
          schema:
            $ref: '#/definitions/StringResponse'

  /files:
    get:
      summary: Data file upload page
      description: Shows the file upload form
      produces:
        - text/html
      security:
        - Bearer: []
      tags:
        - Files
      responses:
        200:
          description: HTML form for file uploading
          schema:
            type: string
    post:
      summary: Upload a data file
      description: Uploads the file to the uploadFolder path.
      security:
        - Bearer: []
      tags:
        - Files
      parameters:
        - name: folder
          in: formData
          description: sub-folder (defaults to "")
          required: false
          type: string
        - name: file
          in: formData
          description: file contents
          required: true
          type: file
      responses:
        200:
          description: HTML form for file uploading
          schema:
            type: string
        400:
          description: Failed to parse upload
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: Writing to the requested path is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        409:
          description: Simultaneous uploading of same file is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Could not write file or folder
          schema:
            $ref: '#/definitions/StringResponse'

  /jobs:
    get:
      summary: ETL job files upload page
      description: Shows the ETL job file upload form
      produces:
        - text/html
      security:
        - Bearer: []
      tags:
        - ETL
      responses:
        200:
          description: HTML form for job file uploading
          schema:
            type: string
    post:
      summary: Upload a ETL job file
      description: Uploads the file to the jobFolder path.
      security:
        - Bearer: []
      tags:
        - ETL
      parameters:
        - name: folder
          in: formData
          description: sub-folder (defaults to "")
          required: false
          type: string
        - name: file
          in: formData
          description: file contents
          required: true
          type: file
      responses:
        200:
          description: HTML form for ETL job uploading
          schema:
            type: string
        400:
          description: Failed to parse upload
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: Writing to the requested path is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        409:
          description: Simultaneous uploading of same file is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Could not write file or folder
          schema:
            $ref: '#/definitions/StringResponse'

  /xforms:
    get:
      summary: ETL transform files upload page
      description: Shows the ETL transform file upload form
      produces:
        - text/html
      security:
        - Bearer: []
      tags:
        - ETL
      responses:
        200:
          description: HTML form for transform file uploading
          schema:
            type: string
    post:
      summary: Upload a ETL transform file
      description: Uploads the file to the xformFolder path.
      security:
        - Bearer: []
      tags:
        - ETL
      parameters:
        - name: folder
          in: formData
          description: sub-folder (defaults to "")
          required: false
          type: string
        - name: file
          in: formData
          description: file contents
          required: true
          type: file
      responses:
        200:
          description: HTML form for ETL transform uploading
          schema:
            type: string
        400:
          description: Failed to parse upload
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: Writing to the requested path is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        409:
          description: Simultaneous uploading of same file is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Could not write file or folder
          schema:
            $ref: '#/definitions/StringResponse'

  /models:
    get:
      summary: Upload model files (queries to local and remote folders)
      description: Show upload form
      produces:
        - text/html
      security:
        - Bearer: []
      tags:
        - ETL
      responses:
        200:
          description: HTML form for model file uploading
          schema:
            type: string
    post:
      summary: Upload a Model file
      description: Uploads the file to the modelsFolder path.
      security:
        - Bearer: []
      tags:
        - ETL
      parameters:
        - name: folder
          in: formData
          description: sub-folder (defaults to "")
          required: false
          type: string
        - name: file
          in: formData
          description: file contents
          required: true
          type: file
      responses:
        200:
          description: Success
          schema:
            type: string
        400:
          description: Failed to parse upload
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: Writing to the requested path is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        409:
          description: Simultaneous uploading of same file is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Could not write file or folder
          schema:
            $ref: '#/definitions/StringResponse'

  /run:
    post:
      summary: Run the ETL Process
      description: Launch the ETL process, if not already running
      security:
        - Bearer: []
      tags:
        - ETL
      parameters:
        - name: refresh
          in: formData
          description: Perform a plugin refresh instead of a regular run
          required: false
          type: string
      responses:
        200:
          description: Job status info
          schema:
            $ref: '#/definitions/JobStatusResponse'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/StringResponse'
    get:
      summary: Run the ETL Process
      description: Launch the ETL process, if not already running
      security:
        - Bearer: []
      parameters:
        - name: check
          in: query
          description: Instead of running the process, check if the given task has already finished
          required: false
          type: string
        - name: refresh
          in: query
          description: Perform a plugin refresh instead of a regular run
          required: false
          type: string
      tags:
        - ETL
      responses:
        200:
          description: Job status info
          schema:
            $ref: '#/definitions/JobStatusResponse'
        default:
          description: Unexpected error
          schema:
            $ref: '#/definitions/StringResponse'

  /query:
    post:
      summary: Save parameters
      description: Saves a set of parameters for later use. Returns a post ID that can be used with GET.
      security:
        - Bearer: []
      parameters:
        - name: parameterMap
          in: body
          schema:
            $ref: '#/definitions/AnyTypeMap'
      tags:
        - Query
      responses:
        200:
          description: Parameters correctly saved
          schema:
            $ref: '#/definitions/StringResponse'
        400:
          description: Bad Request
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Unable to update certificate configuration
          schema:
            $ref: '#/definitions/StringResponse'
    get:
      summary: Run query
      description: Runs a preconfigured query. Parameters are taken from a posted value.
      security:
        - Bearer: []
      parameters:
        - name: "_posted"
          in: query
          description: reference to previously posted params
          required: false
          type: string
      tags:
        - Query
      responses:
        200:
          description: Parameters correctly saved
          schema:
            $ref: '#/definitions/QueryResult'
        400:
          description: Bad Request
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: Query or parameters not found
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Unable to update certificate configuration
          schema:
            $ref: '#/definitions/StringResponse'

  /cert:
    post:
      summary: Certificate info
      description: Push certificate configuration to the server. Certificate files should have been uploaded previously using the /certs endpoint.
      security:
        - Bearer: []
      parameters:
        - name: certUpdate
          in: body
          description: File names for certificate files
          required: true
          schema:
            $ref: '#/definitions/CertUpdate'
      tags:
        - Certs
      responses:
        200:
          description: Certificates correctly updated
          schema:
            $ref: '#/definitions/CertUpdateResponse'
        400:
          description: Bad Request
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: Certificates not found
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Unable to update certificate configuration
          schema:
            $ref: '#/definitions/StringResponse'

  /db:
    get:
      summary: Database info
      description: Get database configuration
      security:
        - Bearer: []
      tags:
        - Database
      responses:
        200:
          description: Database info
          schema:
            $ref: '#/definitions/DatabaseResponse'
        412:
          description: Unable to connect to database
          schema:
            $ref: '#/definitions/StringResponse'

  /updates:
    get:
      summary: Update files upload page
      description: Shows the update file upload form
      produces:
        - text/html
      security:
        - Bearer: []
      tags:
        - Updates
      responses:
        200:
          description: HTML form for update file uploading
          schema:
            type: string
    post:
      summary: Upload an update file
      description: Uploads the file to the scratchFolder path.
      security:
        - Bearer: []
      tags:
        - Updates
      parameters:
        - name: folder
          in: formData
          description: sub-folder (defaults to "")
          required: false
          type: string
        - name: file
          in: formData
          description: file contents
          required: true
          type: file
      responses:
        200:
          description: HTML form for update uploading
          schema:
            type: string
        400:
          description: Failed to parse upload
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: Writing to the requested path is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        409:
          description: Simultaneous uploading of same file is not allowed
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Could not write file or folder
          schema:
            $ref: '#/definitions/StringResponse'

  /update:
    put:
      summary: Prepare service for update
      description: Prepare service for update. Update files should have been previously uploaded using the /updates endpoint.
      security:
        - Bearer: []
      tags:
        - Updates
      responses:
        200:
          description: Update staged
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: Nothing to update
          schema:
            $ref: '#/definitions/StringResponse'
        412:
          description: Could not stat updates
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Error staging update folder
          schema:
            $ref: '#/definitions/StringResponse'

  /restart:
    put:
      summary: Force server restart
      description: Force server restart
      security:
        - Bearer: []
      parameters:
        - name: restartSpec
          in: body
          description: Services to restart
          required: true
          schema:
            $ref: '#/definitions/RestartSpec'
      tags:
        - Restart
      responses:
        200:
          description: Database restarted and/or uploader restart initiated
          schema:
            $ref: '#/definitions/StringResponse'
        412:
          description: No known way to restart the database
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Error while restarting the database
          schema:
            $ref: '#/definitions/StringResponse'

  /sched:
    get:
      summary: Lists tasks
      description: Lists the currently scheduled tasks
      produces:
        - application/json
      security:
        - Bearer: []
      tags:
        - Sched
      responses:
        200:
          description: Scheduled tasks
          schema:
            type: array
            items:
              $ref: '#/definitions/CronTask'
    put:
      summary: Create or update a task
      description: Creates or replaces a task
      security:
        - Bearer: []
      tags:
        - Sched
      responses:
        200:
          description: Task successfully created / updated
          schema:
            $ref: '#/definitions/CronTask'
        400:
          description: Failed to parse task or cronSpec
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Failed to persist scheduled
          schema:
            $ref: '#/definitions/StringResponse'

  /sched/{label}:
    delete:
      summary: Delete a task
      description: Removes an scheduled task
      parameters:
        - name: label
          in: path
          description: Label of the task to remove
          required: true
          type: string
      security:
        - Bearer: []
      tags:
        - Sched
      responses:
        200:
          description: Task successfully removed
          schema:
            $ref: '#/definitions/CronTask'
        400:
          description: Task not found
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Failed to persist scheduled
          schema:
            $ref: '#/definitions/StringResponse'

  /contaplus:
    get:
      summary: Lists Contaplus companies
      description: Lists the available Contaplus companies and years
      parameters:
        - name: path
          in: query
          description: Root Folder of Contaplus (C:\\GrupoSP\\CONTABLD by default)
          required: false
          type: string
      produces:
        - application/json
      security:
        - Bearer: []
      tags:
        - Contaplus
      responses:
        200:
          description: Available Companies
          schema:
            type: array
            items:
              $ref: '#/definitions/CompanyData'
        400:
          description: Failed to read Contaplus top level directory
          schema:
            $ref: '#/definitions/StringResponse'
        404:
          description: File 'EMP/Empresa.dbf'' not found in the given path
          schema:
            $ref: '#/definitions/StringResponse'
        500:
          description: Failed to read 'EMP/Empresa.dbf' database
          schema:
            $ref: '#/definitions/StringResponse'
        410:
          description: There are no companies in 'EMP/Empresa.dbf' file
          schema:
            $ref: '#/definitions/StringResponse'
        406:
          description: Company entries are invalid (wrong year)
          schema:
            $ref: '#/definitions/StringResponse'

definitions:

  StringResponse:
    type: object
    properties:
      success:
        type: boolean
        description: Success flag (true/false)
      message:
        type: string
        description: Error message

  CertUpdate:
    type: object
    properties:
      enable:
        type: boolean
        description: Enable externally-provided certificates
      dnsName:
        type: string
        description: Domain Name in the certificate
      certFile:
        type: string
        description: PEM-encoded certificate
      keyFile:
        type: string
        description: PEM-encoded certificate private key

  CertUpdateResponse:
    type: object
    properties:
      success:
        type: boolean
        description: Success flag (true/false)
      message:
        $ref: '#/definitions/CertUpdate'

  DatabaseConfig:
    type: object
    properties:
      version:
        type: integer
        description: Configuration version
      host:
        type: string
        description: Database host
      port:
        type: integer
        description: Database port
      name:
        type: string
        description: Database name
      user:
        type: string
        description: Database user name
      pass:
        type: string
        description: Database user password
      tls:
        type: boolean
        description: TLS Enabled
      service:
        type: string
        description: Name of the database service
      iniFile:
        type: string
        description: Full path to database config file

  DatabaseResponse:
    type: object
    properties:
      success:
        type: boolean
        description: Success flag (true/false)
      message:
        $ref: '#/definitions/DatabaseConfig'

  RestartSpec:
    type: object
    properties:
      serviceReload:
        type: boolean
        description: Request reload uploader service
      serviceRestart:
        type: boolean
        description: Request restart uploader service
      dbRestart:
        type: boolean
        description: Request restart database

  JobStatus:
    type: object
    properties:
      running:
        type: boolean
        description: The task has been launched
      order:
        type: string
        description: Handler to identify the task
      started:
        type: boolean
        description: The task has been started because of the current call
      since:
        type: string
        format: date-time
        description: Time the command was last started

  JobStatusResponse:
    type: object
    properties:
      success:
        type: boolean
        description: Success flag (true/false)
      message:
        $ref: '#/definitions/JobStatus'

  TokenUpdate:
    type: object
    properties:
      secret:
        type: string
        description: Secret string for JWT signing
      subject:
        type: string
        description: JWT subject (tenant name)

  TokenPost:
    type: object
    properties:
      subject:
        type: string
        description: Subject (username or rolename)
      seconds:
        type: int
        description: Token duration
      audience:
        type: array
        description: List of allowed methods (e.g ["g:query", "p:query"])
        items:
          type: string
      scope:
        type: array
        description: List of allowed query scopes (e.g ["financiero", "global"])
        items:
          type: string
      filters:
        $ref: '#/definitions/StringArrayMap'

  TokenResponse:
    type: object
    properties:
      success:
        type: boolean
        description: Success flag (true/false)
      message:
        $ref: '#/definitions/TokenUpdate'

  CronTask:
    type: object
    properties:
      label:
        type: string
        description: Label for the task
      cronSpec:
        type: string
        description: Cron specification (see https://github.com/gorhill/cronexpr)
      tasks:
        type: array
        description: List of arguments for the runner (e.g. name of the job file)
        items:
          type: string

  CompanyData:
    type: object
    properties:
      name:
        type: string
        description: Name of the company
      years:
        type: array
        description: List of years
        items:
          type: integer

  StringArrayMap:
    description: Filter map, filter to array of values (e.g. { "pEmpresa" ["empresa 1", "empresa 2"] }
    type: object
    additionalProperties:
      type: array
      items:
        type: string

  AnyTypeMap:
    description: Key - Value Map. Values are not necessarily strings!
    type: object
    additionalProperties:
      type: string

  QueryView:
    type: array
    description: Array of columns
    items:
      type: integer

  QueryViewMap:
    type: object
    additionalProperties:
      $ref: '#/definitions/QueryView'

  Query:
    type: object
    description: Query object. Stores SQL code, default values, and views
    properties:
      query:
        type: string
        description: SQL query. Placeholders are enclosed in "{}"
      params:
        $ref: '#/definitions/AnyTypeMap'
      views:
        $ref: '#/definitions/QueryViewMap'

  QueryResult:
    type: object
    description: Rows and views resulting from running a Query
    properties:
      names:
        type: array
        description: List of column names
        items:
          type: string
      views:
        $ref: '#/definitions/QueryViewMap'
      rows:
        type: array
        description: Row List
        items:
          type: array
          description: Row values (Not really strings, but any type!)
          items:
            type: string

securityDefinitions:
  Bearer:
    type: apiKey
    name: Authorization
    in: header