package endpoints

import (
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/conector"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/home"
	"bitbucket.org/five_corp/uploader/models/queries"
	"bitbucket.org/five_corp/uploader/models/tasker"
	"bitbucket.org/five_corp/uploader/models/tenants"
)

// Data received to trigger a conector update
type conectorTrigger struct {
	Label     string   `json:"label"`
	Conectors []string `json:"conectors"`
	DryRun    bool     `json:"dryRun"`
}

// Result of triggering a conector
type conectorStatus struct {
	// True if the task is running
	Running bool `json:"running"`
	// Handler or the last started task
	// Returned as a string instead of a number, because json does
	// not support integers over 48 bits wide.
	Order string `json:"order"`
	// Time since last start
	Since time.Time `json:"since"`
	// True if the task has been started by the current request
	Started bool `json:"started"`
}

// Database route handler
type conectorHandler struct {
	switcher    tenants.Switcher
	sectioner   saferest.Sectioner
	manager     *conector.Manager
	homeManager *home.Manager
	runner      tasker.Runner
}

// ConectorRoutes defines the routes for this endpoint
func ConectorRoutes(sectioner saferest.Sectioner, switcher tenants.Switcher, manager *conector.Manager, homeManager *home.Manager, runner tasker.Runner, unprotect, debug bool) []saferest.Route {
	// Create the handler and reload queries
	handler := conectorHandler{
		switcher:    switcher,
		sectioner:   sectioner,
		manager:     manager,
		homeManager: homeManager,
		runner:      runner,
	}
	routes := []saferest.Route{
		saferest.Route{
			Keyer:    switcher,
			Audience: "g:conector.test",
			Method:   "GET",
			Pattern:  "/conector.test/{conector:[a-zA-Z0-9\\-_]+}",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpTest(w, r)
			},
		},
		saferest.Route{
			Keyer:    switcher,
			Audience: "g:conector.feed",
			Method:   "GET",
			Pattern:  "/conector.feed/{conector:[a-zA-Z0-9\\-_]+}",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpFeed(w, r)
			},
		},
		saferest.Route{
			Keyer:    switcher,
			Audience: "g:conector",
			Method:   "GET",
			Pattern:  "/conector/{conector:[a-zA-Z0-9\\-_]+}",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
		saferest.Route{
			Keyer:    switcher,
			Audience: "p:conector.trigger",
			Method:   "POST",
			Pattern:  "/conector.trigger",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpTrigger(w, r)
			},
		},
		saferest.Route{
			Keyer:    switcher,
			Audience: "g:conector.trigger",
			Method:   "GET",
			Pattern:  "/conector.trigger",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpStatus(w, r)
			},
		},
	}
	return routes
}

// Get the conector and table. Returns name, ConectorContext, source and error.
func (h *conectorHandler) find(r *http.Request) (string, *conector.Conector, string, error) {
	// Conector name and parameters from URL path and query
	name, ok := mux.Vars(r)["conector"]
	if !ok || name == "" {
		err := errors.New("Missing conector Name")
		return "", nil, "", err
	}
	// Make sure such conector exists
	conector, err := h.manager.Find(name)
	if err != nil {
		return name, nil, "", err
	}
	// Put the params in the expected format
	packed := make(dbtool.Params)
	for key, val := range r.URL.Query() {
		if len(val) > 0 {
			packed[key] = val[0]
		}
	}
	// If there is a selected _source, get it
	selected, ok := packed["_source"].(string)
	if !ok {
		selected = ""
		delete(packed, "_source")
	}
	return name, conector, selected, nil
}

// Tests a connector is properly configured
func (h *conectorHandler) test(r *http.Request) error {
	// Get the conector name and parameters from URL path and query
	name, _, _, err := h.find(r)
	if err != nil {
		return err
	}
	// Get the credentials from home
	issuer := saferest.IssuerOf(r)
	signer := saferest.GetToken(h.sectioner).WithSubject(issuer, issuer)
	ctx := r.Context()
	// Build engine for the given conector
	store, ok := h.switcher.Switch(issuer)
	if !ok {
		return errors.Errorf("ERROR: failed to test conector %s, issuer %s not found", name, issuer)
	}
	engines, err := h.homeManager.Engine(ctx, store, name, signer)
	if err != nil {
		return err
	}
	// Ping the engine to check it is healthy
	for _, engine := range engines {
		if err := engine.Ping(); err != nil {
			return err
		}
	}
	return nil
}

// Gets a feed with the imported data from the requested colector and product.
func (h *conectorHandler) feed(r *http.Request) (result queries.ResultSet, err error) {
	// Get the conector name and parameters from URL path and query
	name, conector, selected, err := h.find(r)
	if err != nil {
		return result, nil
	}
	// If no source selected, raise an error
	if selected == "" {
		return result, errors.Errorf("Must select some source with ?_source=... for conector %s", name)
	}
	// Get the conector engine
	issuer := saferest.IssuerOf(r)
	signer := saferest.GetToken(h.sectioner).WithSubject(issuer, issuer)
	store, ok := h.switcher.Switch(issuer)
	if !ok {
		return result, errors.Errorf("ERROR: failed to feed from conector %s, issuer %s not found", name, issuer)
	}
	engines, err := h.homeManager.Engine(r.Context(), store, name, signer)
	if err != nil {
		return result, err
	}
	// Get the product
	iterations, err := conector.Eval(r.Context(), store, selected)
	if err != nil {
		return result, err
	}
	// Get a feed of data
	product, _ := conector.Package(selected, iterations)
	feed, types, err := product.Feed(r.Context(), engines, 1000, 100)
	if err != nil {
		return result, err
	}
	defer feed.Close()
	// Collect the feed
	rows, err := feed.Collect(1000)
	if err != nil {
		return result, err
	}
	// And return it as a JsonResultSet
	return queries.ResultSet{
		Names: feed.Names,
		Types: types,
		Rows:  rows,
	}, nil
}

// Triggers a conector
func (h *conectorHandler) trigger(r *http.Request) (conectorStatus, error) {
	// Get the conector name and parameters from POST body
	status := conectorStatus{Running: true}
	conectors := conectorTrigger{}
	err := saferest.GetJSONBody(r.Header, r.Body, &conectors)
	if err != nil {
		return status, err
	}
	// Default conector is excel
	if conectors.Conectors == nil || len(conectors.Conectors) <= 0 {
		conectors.Conectors = []string{"excel"}
	}
	// If dry run requested, add the flag
	if conectors.DryRun {
		conectors.Conectors = append(conectors.Conectors, "dryRun")
	}
	// Run the task. Do not allow two times the same label
	result, err := h.runner.AddTask(conectors.Label, saferest.IssuerOf(r), conectors.Conectors, false)
	if err != nil {
		return status, err
	}
	// Return the result
	status.Order = strconv.FormatUint(result.Order, 10)
	status.Since = result.Since
	status.Started = !result.Overlap
	return status, nil
}

// Queries for a conector plus its context.
func (h *conectorHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	// Get the conector name and parameters from URL path and query
	name, conector, selected, err := h.find(r)
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	issuer := saferest.IssuerOf(r)
	signer := saferest.GetToken(h.sectioner).WithSubject(issuer, issuer)
	switch selected {
	// If no particular connection specified, just enumerate tables
	case "":
		available, err := h.homeManager.Sources(r.Context(), name, conector.Packages(), signer)
		if err != nil {
			saferest.Fail(http.StatusBadRequest, w, err)
			return
		}
		saferest.Success(w, available)
		return
	// Enumerate all available products, not just the configured ones
	case "all":
		saferest.Success(w, conector.Packages())
		return
	}
	// Run the selected connection
	store, ok := h.switcher.Switch(issuer)
	if !ok {
		err := errors.Errorf("Cannot run conector %s for issuer %s: issuer not found", name, issuer)
		saferest.Fail(http.StatusNotFound, w, err)
	}
	result, err := conector.Eval(r.Context(), store, selected)
	if err != nil {
		saferest.Fail(http.StatusNotFound, w, err)
		return
	}
	// Return the result!
	saferest.Success(w, result)
}

// Queries for a conector, with context.
func (h *conectorHandler) httpTest(w http.ResponseWriter, r *http.Request) {
	if err := h.test(r); err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	saferest.Success(w, true)
}

// Runs a conector query remotely, return rows
func (h *conectorHandler) httpFeed(w http.ResponseWriter, r *http.Request) {
	// Get the conector name and parameters from URL path and query
	result, err := h.feed(r)
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	saferest.Success(w, result)
}

// Triggers a conector
func (h *conectorHandler) httpTrigger(w http.ResponseWriter, r *http.Request) {
	// Get the conector name and parameters from POST body
	status, err := h.trigger(r)
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	saferest.Success(w, status)
}

// Queries the status of a triggered conector
func (h *conectorHandler) httpStatus(w http.ResponseWriter, r *http.Request) {
	// Get the conector name and parameters from POST body
	issuer := saferest.IssuerOf(r)
	check := r.URL.Query().Get("check")
	if check == "" {
		// If no parameter, just return the pending queue
		pending := h.runner.Pending(issuer)
		status := make(map[string][]conectorStatus)
		for k, v := range pending {
			items := make([]conectorStatus, 0, len(v))
			for _, p := range v {
				items = append(items, conectorStatus{
					Running: true,
					Started: !p.Overlap,
					Since:   p.Since,
					Order:   strconv.FormatUint(p.Order, 10),
				})
			}
			status[k] = items
		}
		saferest.Success(w, status)
		return
	}
	// If there is some check parameter, it must be an int
	order, err := strconv.ParseUint(check, 10, 64)
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	status := conectorStatus{Order: strconv.FormatUint(order, 10)}
	if pending, ok := h.runner.Queued(issuer, order); ok {
		status.Running = true
		status.Started = !pending.Overlap
		status.Since = pending.Since
	}
	saferest.Success(w, status)
}
