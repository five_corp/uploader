package endpoints

import (
	"net/http"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/tasker"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"github.com/pkg/errors"
	"gopkg.in/robfig/cron.v2"
)

// Map from cron entry ID to Cron Task
type cronMap map[cron.EntryID]tenants.CronTask

// Cron Handler
type schedHandler struct {
	tenants.Store
	tasker.Runner
}

// SchedRoutes defines the routes for this endpoint
func SchedRoutes(store tenants.Store, runner tasker.Runner, unprotect bool) []saferest.Route {
	handler := schedHandler{Store: store, Runner: runner}
	return []saferest.Route{
		saferest.Route{
			Keyer:    store,
			Audience: "g:sched",
			Method:   "GET",
			Pattern:  "/sched",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpGet(w, r)
			},
		},
		saferest.Route{
			Keyer:    store,
			Audience: "g:sched.history",
			Method:   "GET",
			Pattern:  "/sched.history",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpHistory(w, r)
			},
		},
		saferest.Route{
			Keyer:    store,
			Audience: "u:sched",
			Method:   "PUT",
			Pattern:  "/sched",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpPut(w, r)
			},
		},
		saferest.Route{
			Keyer:    store,
			Audience: "d:sched",
			Method:   "DELETE",
			Pattern:  "/sched",
			Private:  !unprotect,
			Compress: false,
			Cache:    false,
			Handler: func(w http.ResponseWriter, r *http.Request) {
				handler.httpDelete(w, r)
			},
		},
	}
}

// Returns a list of scheduled tasks
func (h *schedHandler) httpGet(w http.ResponseWriter, r *http.Request) {
	issuer := saferest.IssuerOf(r)
	tasks, _ := h.Store.Tasks(issuer)
	saferest.Success(w, tasks)
}

// Returns a list of historical tasks
func (h *schedHandler) httpHistory(w http.ResponseWriter, r *http.Request) {
	issuer := saferest.IssuerOf(r)
	history := h.Runner.History(issuer)
	saferest.Success(w, history)
}

// Adds a new scheduled task
func (h *schedHandler) httpPut(w http.ResponseWriter, r *http.Request) {
	issuer := saferest.IssuerOf(r)
	task := tenants.CronTask{}
	if err := saferest.GetJSONBody(r.Header, r.Body, &task); err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	found, err := h.Store.AddTask(issuer, task)
	if err != nil {
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	} else if !found {
		err := errors.Errorf("Failed to add task %s, issuer %s not found", task.Label, issuer)
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	saferest.Success(w, task)
}

// Removes a scheduled task given its label
func (h *schedHandler) httpDelete(w http.ResponseWriter, r *http.Request) {
	issuer := saferest.IssuerOf(r)
	label := r.URL.Query().Get("label")
	if label == "" {
		err := errors.New("Missing label parameter")
		saferest.Fail(http.StatusBadRequest, w, err)
		return
	}
	found, err := h.Store.RemoveTask(issuer, label)
	if err != nil {
		saferest.Fail(http.StatusInternalServerError, w, err)
		return
	}
	saferest.Success(w, found)
}
