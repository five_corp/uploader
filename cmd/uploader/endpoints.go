package main

import (
	"log"
	"os"
	"path/filepath"
	"runtime"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/cmd/uploader/endpoints"
	"bitbucket.org/five_corp/uploader/models/conector"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/home"
	"bitbucket.org/five_corp/uploader/models/olap"
	"bitbucket.org/five_corp/uploader/models/policy"
	"bitbucket.org/five_corp/uploader/models/queries"
	"bitbucket.org/five_corp/uploader/models/tasker"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"bitbucket.org/five_corp/uploader/sections"
)

type endpointPack struct {
	Reloaders       []endpoints.Reloader
	Purgers         []Purger
	Manager         saferest.ConfigManager
	Tenants         tenants.Store
	Olap            olap.Store
	Store           dbtool.Store
	Runner          tasker.Runner
	ConectorManager *conector.Manager
	HomeManager     *home.Manager
	Verifier        saferest.Verifier
}

// Endpoint pack belonging to an Acens server
func (ep *endpointPack) Deploy(unprotect, debug bool) ([]saferest.Route, error) {

	purgers := ep.Purgers
	reloaders := ep.Reloaders
	manager := ep.Manager
	store := ep.Store
	olap := ep.Olap
	runner := ep.Runner
	tStore := ep.Tenants
	switcher := tStore.Switcher(store)
	conectorManager := ep.ConectorManager
	homeManager := ep.HomeManager
	routes := make([]saferest.Route, 0, 16)
	policyMap := policy.New()

	// Easy routes
	routes = append(routes, endpoints.StatusRoutes(unprotect)...)
	log.Print("Loaded endpoint /status")
	routes = append(routes, endpoints.TokenRoutes(manager, tStore, policyMap, unprotect, debug)...)
	log.Print("Loaded endpoint /token")
	routes = append(routes, endpoints.DBRoutes(manager, switcher, unprotect)...)
	log.Print("Loaded endpoint /db")
	routes = append(routes, endpoints.CertRoutes(manager, store, unprotect)...)
	log.Print("Loaded endpoint /cert")
	routes = append(routes, endpoints.FileRoutes(manager, "files", sections.GetUploads(manager).UploadFolder, nil, unprotect)...)
	log.Print("Loaded endpoint /files")
	routes = append(routes, endpoints.RunRoutes(runner, tStore, unprotect)...)
	log.Print("Loaded endpoint /run")
	routes = append(routes, endpoints.SchedRoutes(tStore, runner, unprotect)...)
	log.Print("Loaded endpoint /sched")

	// Log dir
	cmdConfig := sections.GetCommand(manager)
	if cmdConfig.LogDir == "" {
		log.Print("Logging to stdout, skipping /logs/ endpoint")
	} else {
		routes = append(routes, endpoints.ExplorerRoutes(manager, "logs", cmdConfig.LogDir, unprotect)...)
		log.Print("Loaded endpoint /logs/")
	}

	// Models dir
	modelsDir := filepath.Join(filepath.Dir(os.Args[0]), "../../models")
	routes = append(routes, endpoints.ExplorerRoutes(manager, "models", modelsDir, unprotect)...)
	log.Print("Loaded endpoint /models/")

	// Upload folders
	routes = append(routes, endpoints.FileRoutes(manager, "jobs", sections.GetUpdate(manager).JobFolder, ep.Verifier, unprotect)...)
	log.Print("Loaded endpoint /jobs")
	routes = append(routes, endpoints.FileRoutes(manager, "xforms", sections.GetUpdate(manager).XformFolder, ep.Verifier, unprotect)...)
	log.Print("Loaded endpoint /xforms")
	routes = append(routes, endpoints.FileRoutes(manager, "updates", sections.GetUploads(manager).UpdatesFolder, ep.Verifier, unprotect)...)
	log.Print("Loaded endpoint /updates")
	routes = append(routes, endpoints.FileRoutes(manager, "models", sections.GetUploads(manager).ModelsFolder, ep.Verifier, unprotect)...)
	log.Print("Loaded endpoint /models")

	// Update endpoint.
	routes = append(routes, endpoints.UpdateRoutes(manager, unprotect)...)
	log.Print("Loaded endpoint /update")

	// OS-specific endpoints
	if runtime.GOOS == "windows" {
		routes = append(routes, endpoints.ContaplusRoutes(unprotect)...)
		log.Print("Loaded endpoint /contaplus")
	}

	// Query endpoint
	queryFolder := sections.GetUploads(manager).ModelsFolder
	queryFile := sections.GetQueries(manager).QueryFile
	throttle := sections.GetQueries(manager).Throttle
	cacheSize := sections.GetQueries(manager).CacheSize
	// The 'principal' filter will be a hash for queries
	hashes := sections.GetQueries(manager).Principal
	queryManager, err := queries.NewManager(queryFolder, queryFile, switcher, hashes, cacheSize, throttle)
	if err != nil {
		return nil, err
	}
	routes = append(routes, endpoints.QueryRoutes(manager, switcher, queryManager, policyMap, unprotect)...)
	log.Print("Loaded endpoint /query")

	// Load DAO endpoint
	routes = append(routes, endpoints.DaoRoutes(manager, switcher, olap, policyMap, BatchSize, unprotect)...)

	// Add the conector manager and query manager to the purger list
	reloaders = append(reloaders, conectorManager, queryManager, olap)

	// conector endpoints
	routes = append(routes, endpoints.ConectorRoutes(manager, switcher, conectorManager, homeManager, runner, unprotect, debug)...)
	log.Print("Loaded endpoint /conector")

	// Restart endpoint
	routes = append(routes, endpoints.RestartRoutes(store, reloaders, unprotect)...)
	log.Print("Loaded endpoint /restart")

	ep.Purgers = purgers
	return routes, nil
}
