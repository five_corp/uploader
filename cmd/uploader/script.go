package main

import (
	"context"
	"io"
	"log"
	"os/exec"
	"strconv"
	"strings"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/conector"
	"bitbucket.org/five_corp/uploader/models/home"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"bitbucket.org/five_corp/uploader/sections"
	"github.com/pkg/errors"
)

// Purger is an interface for cache cleaners
type Purger interface {
	Purge(issuer string) error
}

// MaxConectorRows is the max number of rows a conector can retrieve
const MaxConectorRows = 10000000

// Runner plugin that triggers a command line script
type scriptPlugin struct {
	purgers         []Purger
	manager         saferest.Sectioner
	conectorManager *conector.Manager
	homeManager     *home.Manager
	switcher        tenants.Switcher
}

func (s *scriptPlugin) Run(plugin, issuer string, env []string, stdout, stderr io.Writer, args []string) error {
	// Read inside the loop, in case they change during operation.
	token := saferest.GetToken(s.manager)
	config := sections.GetCommand(s.manager)
	db := sections.GetDatabase(s.manager)
	// 'refresh' is populated if this is an scheduled task
	refresh := ""
	dryRun := false
	// Check if there is some import to do
	log.Print("TaskRunner: Starting plugin management for issuer ", issuer)
	store, ok := s.switcher.Switch(issuer)
	if !ok {
		return errors.Errorf("Issuer %s does not have resources, ending task", issuer)
	}
	if args != nil && len(args) > 0 {
		// Set to refresh all plugins given by "args"
		refresh = strings.Join(args, ",")
		// Build context and signer, in case we need then later.
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		signer := token.WithSubject(issuer, issuer)
	forLoop:
		for _, arg := range args {
			switch arg {
			// Tasks not handled by any conector
			case "backup":
				// Backup is an special case, it is no conector.
				// The param after backup string is the backup period.
				break forLoop
			// Contaplus and excel do not trigger imports
			case "contaplus":
				break // breaks switch, not for loop!
			case "excel":
				break // breaks switch, not for loop!
			// Can order a dry run, for connectors to be loaded
			// without running the transformations
			case "dryRun":
				dryRun = true
			// Other conectors trigger import
			default:
				log.Print("TaskRunner: Request to run plugin ", arg, " for issuer ", issuer)
				// Try to find the corresponding conector
				conectorName := arg
				conector, err := s.conectorManager.Find(conectorName)
				if err != nil {
					return err
				}
				// Get the sources enabled
				sources, err := s.homeManager.Sources(ctx, conectorName, conector.Packages(), signer)
				if err != nil {
					return err
				}
				// Build an engine to run the conector
				engines, err := s.homeManager.Engine(ctx, store, conectorName, signer)
				if err != nil {
					return err
				}
				// Run the update!
				if err := conector.Trigger(ctx, store, engines, sources, MaxConectorRows, 100); err != nil {
					return err
				}
				log.Print("TaskRunner: Pre-process of plugin ", arg, " for issuer ", issuer, "completed")
			}
		}
	}
	// If this is a dry run, just load the connectors,
	// do not trigger the transformations
	if dryRun {
		log.Print("TaskRunner: dryRun requested, stopping execution - but clearing cache")
		for _, p := range s.purgers {
			p.Purge(issuer)
		}
		return nil
	}
	log.Print("TaskRunner: Prepare to run transformations for issuer ", issuer)
	// Prepare the command
	cmd := exec.Command(config.RemoteCmd, config.RemoteArgs...)
	if config.RemoteDir != "" {
		cmd.Dir = config.RemoteDir
	}
	// NOTE: "copy" copies min(len(src), len(dst)) elements
	// See http://stackoverflow.com/questions/30182538/why-can-not-i-copy-a-slice-with-copy-in-golang
	cmd.Env = make([]string, len(env), len(env)+10)
	copy(cmd.Env, env)
	cmd.Env = append(cmd.Env, "TENANT_NAME="+token.Subject)
	cmd.Env = append(cmd.Env, "LABEL="+plugin)
	cmd.Env = append(cmd.Env, "REFRESH="+refresh)
	// Check if MultiCompany is enabled
	multi := "NO"
	if config.Multi {
		multi = "YES"
	}
	cmd.Env = append(cmd.Env, "MULTI="+multi)
	// By default, clear the cache
	clearCache := true
	// If the refresh operation is a backup, add database credentials
	if strings.HasPrefix(refresh, "backup") {
		resource, ok := s.switcher.Resource(issuer)
		if !ok {
			resource = db.Name
		}
		// SSL Mode for the SQL server: REQUIRED if Tls is enabled.
		sqlMode := "REQUIRED"
		//if !db.Tls {
		sqlMode = "DISABLED"
		//}
		cmd.Env = append(cmd.Env, "SQL_HOST="+db.Host)
		cmd.Env = append(cmd.Env, "SQL_PORT="+strconv.Itoa(db.Port))
		cmd.Env = append(cmd.Env, "SQL_USER="+db.User)
		cmd.Env = append(cmd.Env, "SQL_PASS="+db.Pass)
		cmd.Env = append(cmd.Env, "SQL_NAME="+resource)
		cmd.Env = append(cmd.Env, "SQL_MODE="+sqlMode)
		// Do not clear cache if this is just a backup
		clearCache = false
	}
	cmd.Stdout = stdout
	cmd.Stderr = stderr
	err := cmd.Run()
	if err != nil {
		clearCache = false
		log.Print(err)
	}
	if clearCache {
		for _, p := range s.purgers {
			p.Purge(issuer)
		}
	}
	log.Print("TaskRunner: Transformations completed")
	return errors.Wrap(err, "Failed transformations")
}
