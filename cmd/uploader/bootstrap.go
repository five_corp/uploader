package main

import (
	"log"
	"os"
	"path/filepath"
	"runtime"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/dbtool/mysql"
	"bitbucket.org/five_corp/uploader/models/update"
	"bitbucket.org/five_corp/uploader/sections"
	"github.com/kardianos/osext"
)

// Version of the config files created by this utility
const Version = 1

// Error codes reported by the app
const (
	ErrSuccess           = 0
	ErrShouldNotHappen   = 1
	ErrMissingInstallDir = 2
	ErrWrongInstallDir   = 3
	ErrMissingFlags      = 4
	ErrMissingFolders    = 5
	ErrDBServiceNotFound = 6
	ErrCreateUserFail    = 7
	ErrTemplateFail      = 8
)

// Exits the program with an error code
func fail(code int, other ...interface{}) {
	log.Print(other...)
	os.Exit(code)
}

type bootstrapOptions struct {
	installDir    *string
	updateFlag    *bool
	bootstrapFlag *bool
	hostFlag      *string
	portFlag      *int
	passFlag      *string
	restPortFlag  *int
	dbServiceFlag *string
}

// Bootstrap routine
func bootstrap(manager saferest.ConfigManager, options bootstrapOptions) {

	// Fixed flags
	installDir := options.installDir
	updateFlag := options.updateFlag
	hostFlag := options.hostFlag
	portFlag := options.portFlag
	passFlag := options.passFlag
	restPortFlag := options.restPortFlag
	dbServiceFlag := options.dbServiceFlag

	// Get the installation dir (absolute)
	if installDir == nil || *installDir == "" {
		// Guess install dir from executable path
		execDir, err := osext.ExecutableFolder()
		if err != nil {
			fail(ErrMissingInstallDir, "Configuration parameter -installDir is required")
		}
		// The program should be in %INSTALL_PATH%/Service/current,
		// so we have to go two levels up
		*installDir = filepath.Join(execDir, "..", "..")
	}
	finalDir, err := filepath.Abs(*installDir)
	if err != nil {
		fail(ErrWrongInstallDir, "Could not normalize installation directory")
	}
	log.Print("InstallDir is ", finalDir)

	// If this is a full install, all parameters are required
	install := updateFlag == nil || !*updateFlag
	if install {
		switch {
		case hostFlag == nil || *hostFlag == "":
			log.Fatal("missing Database host name (-host)")
		case portFlag == nil || *portFlag == 0:
			log.Fatal("missing Database port number (-port)")
		case passFlag == nil || *passFlag == "":
			log.Fatal("missing Database root password (-password)")
		case restPortFlag == nil || *restPortFlag == 0:
			log.Fatal("missing Service port number (-restport)")
		}
	}

	// Save all paths as a convenience for the templates.
	// runner: script to run Pentaho Kitchen
	// uploader: service name, relative to "current" (not absolute path).
	// java: JRE java executable
	// kitchen: PDI start script
	runner, uploader, java, kitchen := "run.sh", "uploader", "java", "kitchen.sh"
	if runtime.GOOS == "windows" {
		runner = "run.bat"
		uploader = "uploader.exe"
		java = "java.exe"
		kitchen = "Kitchen.bat"
	}
	folderMap := map[string]update.FolderDescriptor{
		"Install":      {Required: true, Create: false, Path: []string{}},
		"Config":       {Required: false, Create: true, Path: []string{"config"}},
		"Models":       {Required: false, Create: true, Path: []string{"models"}},
		"Log":          {Required: false, Create: true, Path: []string{"logs"}},
		"Upload":       {Required: false, Create: true, Path: []string{"uploads"}},
		"AutoUpdates":  {Required: false, Create: true, Path: []string{"updates"}},
		"Backup":       {Required: false, Create: true, Path: []string{"backups"}},
		"Static":       {Required: false, Create: true, Path: []string{"static"}},
		"Cert":         {Required: false, Create: true, Path: []string{"certs"}},
		"Service":      {Required: true, Create: false, Path: []string{"service"}},
		"Current":      {Required: true, Create: false, Path: []string{"service", "current"}},
		"Scratch":      {Required: false, Create: true, Path: []string{"service", "scratch"}},
		"Update":       {Required: false, Create: false, Path: []string{"service", "updated"}},
		"Command_Exec": {Required: false, Create: false, Path: []string{"service", "current", runner}},
		"Pentaho":      {Required: true, Create: false, Path: []string{"pentaho"}},
		"Temp":         {Required: false, Create: true, Path: []string{"pentaho", "temp"}},
		"Kettle":       {Required: true, Create: false, Path: []string{"pentaho", "data-integration"}},
		"Kitchen":      {Required: true, Create: false, Path: []string{"pentaho", "data-integration", kitchen}},
		"Job":          {Required: true, Create: false, Path: []string{"jobs", "jobs"}},
		"Xform":        {Required: true, Create: false, Path: []string{"jobs", "xforms"}},
		"Java":         {Required: true, Create: false, Path: []string{"java8"}},
		"ETL":          {Required: true, Create: false, Path: []string{"pentaho"}},
		"Java_Exec":    {Required: true, Create: false, Path: []string{"java8", "bin", java}},
		"Kettle_Start": {Required: true, Create: false, Path: []string{"jobs", "jobs", "stage1_bootstrap.kjb"}},
		"RootCA":       {Required: true, Create: false, Path: []string{"service", "current", "root-ca.pem"}},
	}
	folders, err := update.FullPath(finalDir, folderMap)
	if err != nil {
		fail(ErrMissingFolders, "Error validating folder, ", err)
	}
	// This path is relative, it is for the serman configuration.
	folders["Uploader_Exec"] = uploader

	// Load current configuration, to keep state
	if err := manager.LoadFrom(filepath.Join(finalDir, "config"), true); err != nil {
		fail(ErrMissingFlags, "Error processing command line parameters, ", err)
	}

	// Update DatabaseConfig and ServerConfig parameters with command-line
	dbConfig := sections.GetDatabase(manager)
	serverConfig := saferest.GetServer(manager)
	if hostFlag != nil && *hostFlag != "" {
		dbConfig.Host = *hostFlag
	}
	if portFlag != nil && *portFlag != 0 {
		dbConfig.Port = *portFlag
	}
	if restPortFlag != nil && *restPortFlag != 0 {
		serverConfig.Port = *restPortFlag
	}
	if passFlag != nil && *passFlag != "" {
		// behave just as a clean installation, i.e. create a new database user
		install = true
	}

	// Guess MySql management parameters, if not known
	mysqlConfig := mysql.Config{
		CertFiles:   dbtool.CertFiles{},
		Host:        dbConfig.Host,
		Port:        dbConfig.Port,
		User:        dbConfig.User,
		Name:        dbConfig.Name,
		Pass:        dbConfig.Pass,
		UseTLS:      dbConfig.TLS,
		ServiceName: "",
		IniFile:     "",
		SQLDump:     "",
		SQLImport:   "",
	}
	// if install == true || some mysql param is missing, guess them
	if install || dbConfig.Service == "" || dbConfig.IniFile == "" || dbConfig.SQLDump == "" || dbConfig.SQLImport == "" {
		// Update database service management information
		serviceHint := dbConfig.Service
		if dbServiceFlag != nil {
			switch *dbServiceFlag {
			case "auto":
				serviceHint = ""
			case "":
				serviceHint = dbConfig.Service
			default:
				serviceHint = *dbServiceFlag
			}
		}
		if serviceHint == "" {
			serviceHint = "MySQL"
		}
		if err := mysqlConfig.GuessService(serviceHint); err != nil {
			// Failure is only severe if installing, but updating is allowed
			// while the database is down
			if install && runtime.GOOS == "windows" {
				fail(ErrDBServiceNotFound, "Error searching MySQL service, ", err)
			} else {
				log.Print("Failed to get MySQL service info, following anyway: ", err)
			}
		} else {
			dbConfig.Service = mysqlConfig.ServiceName
			dbConfig.IniFile = mysqlConfig.IniFile
			dbConfig.SQLDump = mysqlConfig.SQLDump
			dbConfig.SQLImport = mysqlConfig.SQLImport
			manager.Replace(sections.SectionDatabase, dbConfig)
		}
	}
	// If install == true, create database user
	if install {
		mysqlConfig.UseRootPass(*passFlag)
		store := dbtool.New(nil, &mysqlConfig)
		// Use root credentials and get service parameters
		// Create the database user. Logged as root.
		if err := dbCreateUser(store, &dbConfig); err != nil {
			log.Print("Could not configure the database, ", err)
			fail(ErrCreateUserFail, "Make sure MySQL is running and the password provided is correct")
		}
		// Update database data
		manager.Replace(sections.SectionDatabase, dbConfig)
	}
	folders["SqlDump"] = dbConfig.SQLDump
	folders["SqlImport"] = dbConfig.SQLImport

	// Other templates files, not directly related to sections
	templates := update.NewTemplates(finalDir, []string{"config"}, sections.Descriptors())
	templates.Add(finalDir, map[string]update.FolderDescriptor{
		// Add saferest sections, which are not in the sections.Descriptors()
		"cert":    {Required: false, Create: false, Path: []string{"config", "cert.yaml"}},
		"server":  {Required: false, Create: false, Path: []string{"config", "server.yaml"}},
		"token":   {Required: false, Create: false, Path: []string{"config", "token.yaml"}},
		// Other templates
		"paths":   {Required: false, Create: false, Path: []string{"config", "paths.xml"}},
		"serman":  {Required: false, Create: false, Path: []string{"service", "svc", "serman.yaml"}},
		"service": {Required: false, Create: false, Path: []string{"service", "current", "service.yaml"}},
		"run":     {Required: false, Create: false, Path: []string{"service", "current", runner}},
	})

	if err := templates.Execute(filepath.Join(finalDir, "service", "current"), Version, manager, folders); err != nil {
		fail(ErrTemplateFail, "Error executing templates, ", err)
	}
	log.Print("Configuration complete")
}
