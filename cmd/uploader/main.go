package main

import (
	"context"
	"flag"
	"log"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/cmd/uploader/endpoints"
	"bitbucket.org/five_corp/uploader/models/conector"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/dbtool/mysql"
	"bitbucket.org/five_corp/uploader/models/home"
	"bitbucket.org/five_corp/uploader/models/olap"
	"bitbucket.org/five_corp/uploader/models/tasker"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"bitbucket.org/five_corp/uploader/sections"
)

const (
	// MaxRows is the maximum number of rows a conector will retrieve
	MaxRows = 10000000
	// BatchSize is the max number of rows a feed will collect per batch
	BatchSize = 1000
)

// Array of option Strings
type arrayFlags []string

func (i *arrayFlags) String() string {
	if i == nil {
		return ""
	}
	return strings.Join(*i, "; ")
}

func (i *arrayFlags) Set(value string) error {
	*i = append(*i, value)
	return nil
}

// Uploader server
func main() {

	// Prepare the configmanager before parsing flags
	manager := saferest.NewConfigManager(true)
	for _, d := range sections.Descriptors() {
		manager.AddFactory(d)
	}

	// bootstrap flags
	options := bootstrapOptions{}
	options.installDir = flag.String("bootstrap-installDir", "", "Path to installation dir")
	options.updateFlag = flag.Bool("bootstrap-update", false, "Update instead of install")
	options.bootstrapFlag = flag.Bool("bootstrap", false, "Install the application")
	options.hostFlag = flag.String("bootstrap-host", "", "mysql server name")
	options.portFlag = flag.Int("bootstrap-port", 0, "MySQL server port")
	options.passFlag = flag.String("bootstrap-password", "", "MySQL root password")
	options.restPortFlag = flag.Int("bootstrap-restPort", 0, "Restful service port number")
	options.dbServiceFlag = flag.String("bootstrap-dbService", "", "MySQL service name, 'auto' to guess")

	// additionall flags
	configDir := flag.String("config", "", "Path to config directory")
	debug := flag.Bool("debug", false, "Enable debugging (not valid when using certs)")
	unprotect := flag.Bool("unprotect", false, "Unprotect all endpoints (not valid when using certs)")

	flag.Parse()
	if options.bootstrapFlag != nil && *options.bootstrapFlag {
		bootstrap(manager, options)
		os.Exit(0)
	}

	// Default value for config dir
	if configDir == nil || *configDir == "" {
		defaultConfigDir := "config"
		configDir = &defaultConfigDir
	}

	// Prepare the server
	server, err := saferest.NewServer(manager, *configDir, *debug)
	if err != nil {
		log.Panic("Error initializing server", err)
	}

	// Unprotect and debug modes only work without certificates.
	// Once you get a certificate from the backend, you can no
	// longer play unsafe...
	serverConfig := saferest.GetServer(manager)
	certConfig := saferest.GetCert(manager)
	if certConfig.KeyFile == "" || !serverConfig.HTTPS {
		if *unprotect {
			log.Print("BEWARE! Server is running in unprotected mode")
			log.Print("BEWARE! ALL ENDPOINTS ARE UNPROTECTED!")
		}
		if *debug {
			log.Print("BEWARE! Server is running in debug mode")
			log.Print("BEWARE! YOUR TOKEN IS EXPOSED!")
			server.Debug()
		}
	} else if *unprotect || *debug {
		log.Print("Unprotect and Debug flags are disabled!")
	}

	// Set up the DB Manager
	dbConfig := sections.GetDatabase(manager)
	mysqlConfig := mysql.Config{
		CertFiles: dbtool.CertFiles{
			RootFile: certConfig.RootFile,
			CertFile: certConfig.CertFile,
			KeyFile:  certConfig.KeyFile,
		},
		Host:        dbConfig.Host,
		Port:        dbConfig.Port,
		User:        dbConfig.User,
		Name:        dbConfig.Name,
		Pass:        dbConfig.Pass,
		UseTLS:      dbConfig.TLS,
		ServiceName: dbConfig.Service,
		IniFile:     dbConfig.IniFile,
		SQLDump:     dbConfig.SQLDump,
		SQLImport:   dbConfig.SQLImport,
	}
	if dbConfig.SQLDump == "" || dbConfig.IniFile == "" || dbConfig.SQLImport == "" {
		if err := mysqlConfig.GuessService("MySQL"); err != nil {
			log.Print("ERROR: Unable to initialize DB manager, ", err)
			log.Print("ERROR: Will not be able to manage MySQL service")
		}
	}
	store := dbtool.New(nil, &mysqlConfig)

	// Start the task runner
	var logFile string
	cmdConfig := sections.GetCommand(manager)
	// Log to stdout, stderr if no log folder specified
	if cmdConfig.LogDir != "" {
		logFile = filepath.Join(cmdConfig.LogDir, "command.log")
	}
	runnerParams := tasker.Params{
		TaskBuffer:  4,
		LogFile:     logFile,
		LogSize:     cmdConfig.LogSize,
		LogBackups:  cmdConfig.LogBackups,
		Environment: cmdConfig.RemoteEnv,
	}
	runner := tasker.New(runnerParams)

	// Starts the keepalive runner, if configured
	if sections.GetCallHome(manager).PingTime > 0 {
		PingStart(context.Background(), manager)
	}

	// Initialize the Home Conector Manager
	callHome := sections.GetCallHome(manager)
	uploads := sections.GetUploads(manager)
	homeManager := &home.Manager{
		SourcesURL:          callHome.ConectorURL,
		SourcesAudience:     callHome.ConectorAudience,
		CredentialsURL:      callHome.CredentialsURL,
		CredentialsAudience: callHome.CredentialsAudience,
		SourceOverride:      callHome.SourceOverride,
		ExcelFolder:         uploads.UploadFolder,
		ReqTimeout:          callHome.Timeout,
		ConectorTimeout:     callHome.ConectorTimeout,
		MaxRows:             MaxRows,
		BatchSize:           BatchSize,
	}

	// Initialize the verifier with all certificates in the folder
	certsFolder := sections.GetUpdate(manager).CertsFolder
	certs, err := filepath.Glob(filepath.Join(certsFolder, "*.crt"))
	if err != nil {
		log.Fatal("Error scanning for certificates, ", err)
	}
	if len(certs) <= 0 {
		log.Fatal("No .crt files found in ", certsFolder, ", cannot build verifier")
	}
	verifier, err := saferest.VerifierFrom(certs)
	if err != nil {
		log.Fatal("Error building verifier from ", certs, ", error: ", err)
	}
	log.Print("Verifier loaded: ", verifier)

	// Initialize the tenant manager
	tenantConfig := sections.GetTenants(manager)
	schedConfig := sections.GetSched(manager)
	tokenConfig := saferest.GetToken(manager)
	tStore, err := tenants.New(
		tenantConfig.Tenants,
		tokenConfig.Subject,
		tenants.Tenant{
			Resource: dbConfig.Name,
			Token:    tokenConfig.Secret,
			Tasks:    schedConfig.Tasks,
		},
		runner)
	if err != nil {
		log.Fatal("Error building tenants manager: ", err)
	}
	// Susbscribe to tenant events to save changes
	tStore.Subscribe("save", ^tenants.Bootstrap, func(mask tenants.EventType, issuer string, tenant tenants.Tenant) {
		tenantConfig.Save(manager, tStore, mask, issuer, tenant)
	})

	// Initialize the conector Manager
	queryFolder := sections.GetUploads(manager).ModelsFolder
	conectorManager, err := conector.NewManager(queryFolder, tStore.Switcher(store))
	if err != nil {
		log.Fatal("Error building conector manager, ", err)
	}

	// Initialize the Olap store
	olapStore, err := olap.NewManager(queryFolder, "schema")
	if err != nil {
		log.Print("Olap store failed, olap endpoint will not be available: ", err)
		olapStore = nil
	}

	// Build the routes
	pack := endpointPack{
		Reloaders:       []endpoints.Reloader{},
		Purgers:         []Purger{server},
		Manager:         manager,
		Tenants:         tStore,
		Store:           store,
		Olap:            olapStore,
		Runner:          runner,
		ConectorManager: conectorManager,
		HomeManager:     homeManager,
		Verifier:        verifier,
	}
	routes, err := pack.Deploy(*unprotect, *debug)
	if err != nil {
		log.Fatal("FATAL: Could not build endpoints! ", err)
	}

	// Register the runner plugins
	runner.AddPlugin("", &scriptPlugin{
		manager:         manager,
		conectorManager: conectorManager,
		homeManager:     homeManager,
		switcher:        tStore.Switcher(store),
		purgers:         pack.Purgers,
	})

	// Start the server!
	log.Print("Starting server")
	log.Fatal(server.Start(routes))
}
