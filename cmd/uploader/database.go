package main

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"log"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/sections"
	"github.com/pkg/errors"
)

// Create database user. DBMan must be root!
func dbCreateUser(store dbtool.Store, config *sections.DatabaseConfig) error {
	// Generate random password for mysql user
	buffer := make([]byte, 32)
	if _, err := rand.Read(buffer); err != nil {
		return errors.Wrap(err, "Failed to read random stream")
	}
	secret := base64.StdEncoding.EncodeToString(buffer)
	// Create database and users
	statements := []dbtool.Statement{
		{SQL: "CREATE DATABASE IF NOT EXISTS mysmartbi_data"},
		{SQL: "CREATE USER IF NOT EXISTS mysmartbi_user@'localhost'"},
		{SQL: "CREATE USER IF NOT EXISTS mysmartbi_user@'%'"},
		// Placeholders not working with this statement. Fortunately the
		// password is a base64 string, so we don't expect strange
		// characters here.
		{SQL: "ALTER USER mysmartbi_user@'localhost' IDENTIFIED BY '" + secret + "'"},
		{SQL: "ALTER USER mysmartbi_user@'%' IDENTIFIED BY '" + secret + "'"},
		{SQL: "GRANT ALL ON mysmartbi_data.* TO mysmartbi_user@'localhost' WITH GRANT OPTION"},
		{SQL: "GRANT ALL ON mysmartbi_data.* TO mysmartbi_user@'%' WITH GRANT OPTION"},
	}
	ctx := context.Background()
	for index, statement := range statements {
		result, err := store.Exec(ctx, statement, nil)
		if err != nil {
			return err
		}
		affected, err := result.RowsAffected()
		if err != nil {
			affected = 0
		}
		log.Print("Statement ", index+1,
			"... OK (", affected, " rows affected)")
	}
	// update configuration parameters
	config.User = "mysmartbi_user"
	config.Pass = secret
	config.Name = "mysmartbi_data"
	return nil
}
