package main

import (
	"context"
	"log"
	"net"
	"time"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/sections"
	"github.com/pkg/errors"
)

// Contents of the Ping Request
type pingRequest struct {
	Tenant    string   `json:"tenant"`
	Addresses []net.IP `json:"addresses"`
}

// Response expected from Home
type pingResponse struct {
	Success bool `json:"success"`
}

// Enumerates all IP addresses of the device
func getAddresses() ([]net.IP, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, errors.Wrap(err, "Failed to collect network interface information")
	}
	result := []net.IP{}
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err == nil {
			for _, addr := range addrs {
				switch v := addr.(type) {
				case *net.IPNet:
					result = append(result, v.IP)
				case *net.IPAddr:
					result = append(result, v.IP)
				}
			}
		}
	}
	return result, nil
}

// Sends a ping to the backend, including tenant and IP addresses.
func pingHome(ctx context.Context, manager saferest.Sectioner, addresses []net.IP) error {
	// Build the request object
	homeConfig := sections.GetCallHome(manager)
	tenantConfig := saferest.GetToken(manager)
	// Encode the tenant and IP addresses in a JSON object
	req := pingRequest{Tenant: tenantConfig.Subject, Addresses: addresses}
	res := pingResponse{}
	if err := saferest.RestRequest(ctx, "POST", homeConfig.PingURL, "", homeConfig.Timeout, req, false, &res); err != nil {
		return err
	}
	// Check operation success
	if !res.Success {
		return errors.New("Home returned operation unsuccessful")
	}
	return nil
}

// Keeps pinging the home server
func keepAlive(ctx context.Context, manager saferest.Sectioner, timer time.Duration) {
	addresses, err := getAddresses()
	if err != nil {
		log.Panic("Error collecting server IP addresses")
	}
	log.Print("Detected the following IP addresses: ", addresses)
	for {
		// Pings the remote server
		err := pingHome(ctx, manager, addresses)
		if err != nil {
			log.Print("Could not ping Home server: ", err)
		}
		// Schedules a new ping in a few minutes
		select {
		case <-ctx.Done():
			return
		case <-time.After(timer):
		}
	}
}

// PingStart starts pinging home
func PingStart(ctx context.Context, manager saferest.Sectioner) {
	timer := time.Duration(sections.GetCallHome(manager).PingTime) * time.Minute
	go keepAlive(ctx, manager, timer)
}
