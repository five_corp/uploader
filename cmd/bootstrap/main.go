package main

import (
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"syscall"

	"github.com/kardianos/osext"
)

// Bootstrap routine
func main() {

	// legacy flags and typos
	translations := map[string]string{
		"-installdir": "-bootstrap-installDir",
		"-installDir": "-bootstrap-installDir",
		"-update":     "-bootstrap-update",
		"-host":       "-bootstrap-host",
		"-port":       "-bootstrap-port",
		"-password":   "-bootstrap-password",
		"-restport":   "-bootstrap-restPort",
		"-restPort":   "-bootstrap-restPort",
		"-dbservice":  "-bootstrap-dbService",
		"-dbService":  "-bootstrap-dbService",
	}

	// Find uploader executable
	uploader := "uploader"
	if runtime.GOOS == "windows" {
		uploader = "uploader.exe"
	}
	execDir, err := osext.ExecutableFolder()
	if err != nil {
		execDir = "."
	}
	execFile, err := filepath.Abs(filepath.Join(execDir, uploader))
	if err != nil {
		panic(err)
	}

	// Parse command line arguments
	args := make([]string, 2, len(os.Args)+1)
	args[0] = uploader
	args[1] = "-bootstrap"
	for _, arg := range os.Args[1:] {
		for k, v := range translations {
			if strings.HasPrefix(arg, k) {
				arg = strings.Replace(arg, k, v, 1)
			}
		}
		args = append(args, arg)
	}

	// Run the uploader
	if runtime.GOOS != "windows" {
		syscall.Exec(execFile, args, os.Environ())
	}
	cmd := exec.Command(execFile, args[1:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		panic(err)
	}
}
