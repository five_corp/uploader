package sections

import (
	"io"

	"bitbucket.org/five_corp/saferest"
)

// SectionUploads identifies the upload section of configs
const SectionUploads = "uploads"

// UploadsConfig defines the configuration of upload folders
type UploadsConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Path of the upload page template file, or "" to use the builtin
	UploadPage string `json:"uploadPage" xml:"upload_page" yaml:"uploadPage"`
	// Name of the "file" field in the upload form
	UploadField string `json:"uploadField" xml:"upload_field" yaml:"uploadField"`
	// Name of the "folder" field in the upload form
	FolderField string `json:"folderField" xml:"folder_field" yaml:"folderField"`
	// Name of the "signature" field in the upload form
	SignatureField string `json:"signatureField" xml:"signature_field" yaml:"signatureField"`
	// Maximum depth of nested directories in a folder
	MaxDepth int `json:"maxDepth" xml:"max_depth" yaml:"maxDepth"`

	// Path of the upload folder
	UploadFolder string `json:"uploadFolder" xml:"upload_folder" yaml:"uploadFolder"`
	// Path of the update folder
	UpdatesFolder string `json:"updatesFolder" xml:"updates_folder" yaml:"updatesFolder"`
	// Path of the models folder
	ModelsFolder string `json:"modelsFolder" xml:"models_folder" yaml:"modelsFolder"`
	// Path to the certificates folder
	CertFolder string `json:"certFolder" xml:"cert_folder" yaml:"certFolder"`

	// TCP Port number for tunnel server. DEPRECATED.
	TunnelPort int `json:"tunnelPort" xml:"tunnel_port" yaml:"tunnelPort"`
	// Maximum number of tunnels. DEPRECATED.
	MaxTunnels int `json:"maxTunnels" xml:"max_tunnels" yaml:"maxTunnels"`
}

// NewUploadsConfig creates a Config object with default values
func NewUploadsConfig(parser saferest.Parser, input io.Reader) saferest.Config {
	// Set sane default values
	config := UploadsConfig{
		Version:        1,
		UploadPage:     "",
		UploadField:    "file",
		MaxDepth:       2,
		FolderField:    "folder",
		SignatureField: "signature",
		UploadFolder:   "uploads",
		UpdatesFolder:  "updates",
		ModelsFolder:   "models",
		TunnelPort:     3001,
		MaxTunnels:     5,
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetUploads returns the uploads section of config
func GetUploads(manager saferest.Sectioner) UploadsConfig {
	return manager.Section(SectionUploads).(UploadsConfig)
}
