package sections

import (
	"io"
	"log"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/tenants"
)

// SectionTenants is the ID of the tenants section
const SectionTenants = "tenants"

// TenantsConfig contains the map of tenants
type TenantsConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	Tenants map[string]tenants.Tenant `json:"tenants" xml:"tenants" yaml:"tenants"`
	Sources map[string][]string       `json:"sources" xml:"sources" yaml:"sources"`
}

// NewTenantsConfig creates a Config object with default values
func NewTenantsConfig(parser saferest.Parser, input io.Reader) saferest.Config {
	// Set sane default values
	config := TenantsConfig{
		Version: 1,
		Tenants: make(map[string]tenants.Tenant),
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetTenants returns a tenant config object
func GetTenants(manager saferest.Sectioner) TenantsConfig {
	return manager.Section(SectionTenants).(TenantsConfig)
}

// Save tenant information
// TODO: This should be synchronized but it is not, there is a potential for race conditions
// if someone modifies tenants while we are still saving them.
// However, we expect that to be very unlikely.
func (tenantConfig *TenantsConfig) Save(manager saferest.SectionSaver, store tenants.Store, mask tenants.EventType, issuer string, tenant tenants.Tenant) {
	// If the default has been modified, save resources and tasks
	if issuer == store.DefaultIssuer() {
		if mask&tenants.Update != 0 {
			tokenConfig := saferest.GetToken(manager)
			tokenConfig.Subject = issuer
			tokenConfig.Secret = tenant.Token
			if err := manager.Save(saferest.SectionToken, tokenConfig); err != nil {
				log.Print("Error saving tenant", issuer, "info to token section:", err)
			}
		}
		if mask&tenants.Task != 0 {
			schedConfig := GetSched(manager)
			schedConfig.Tasks = tenant.Tasks
			if err := manager.Save(SectionSched, schedConfig); err != nil {
				log.Print("Error saving tenant", issuer, "info to sched section:", err)
			}
		}
	} else {
		// Save the tenant config
		if err := manager.Save(SectionTenants, tenantConfig); err != nil {
			log.Print("Error saving tenant", issuer, "info to tenants section:", err)
		}
	}
	log.Print("Tenant configuration saved successfully after changes to ", issuer)
}
