package sections

import (
	"io"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/tenants"
)

// SectionSched identified the scheduled tasks config section
const SectionSched = "schedules"

// SchedConfig contains the scheduled tasks for the default tenant
type SchedConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Scheduled tasks
	Tasks []tenants.CronTask `json:"tasks" xml:"tasks" yaml:"tasks"`
}

// NewSchedConfig creates a Config object with default values
func NewSchedConfig(parser saferest.Parser, input io.Reader) saferest.Config {
	// Set sane default values
	config := SchedConfig{
		Version: 1,
		Tasks:   []tenants.CronTask{},
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetSched gets the scheduled tasks config
func GetSched(manager saferest.Sectioner) SchedConfig {
	return manager.Section(SectionSched).(SchedConfig)
}
