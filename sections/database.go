package sections

import (
	"encoding/xml"
	"io"

	"bitbucket.org/five_corp/saferest"
)

// SectionDatabase identifies the database config section
const SectionDatabase = "database"

// DatabaseConfig defines the configuration of database endpoints
type DatabaseConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Enclosing tag of the XML data
	XMLName xml.Name `xml:"db_config"  json:"-" yaml:"-"`

	// DB Connection parameters
	Host string `xml:"db_server"    json:"host" yaml:"host"`
	Port int    `xml:"db_port"      json:"port" yaml:"port"`
	Name string `xml:"db_name"      json:"name" yaml:"name"`
	User string `xml:"db_user"      json:"user" yaml:"user"`
	Pass string `xml:"db_password"  json:"pass" yaml:"pass"`

	// Additional config
	TLS     bool   `xml:"db_tls"     json:"tls" yaml:"tls"`
	Service string `xml:"db_service" json:"service" yaml:"service"`
	IniFile string `xml:"db_ini"     json:"iniFile" yaml:"iniFile"`

	// Path to SQL backup tool
	SQLDump   string `xml:"db_dump"    json:"sqlDump" yaml:"sqlDump"`
	SQLImport string `xml:"db_import"    json:"sqlImport" yaml:"sqlImport"`

	// Status flag, just to avoid having to create a new struct...
	Healthy bool `xml:"-" json:"healthy" yaml:"healthy" flags:"skip"`
}

// NewDatabaseConfig creates a Config object with default values
func NewDatabaseConfig(parser saferest.Parser, input io.Reader) saferest.Config {
	// Set sane default values
	config := DatabaseConfig{
		Version: 1,
		TLS:     false,
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetDatabase returns the database config
func GetDatabase(manager saferest.Sectioner) DatabaseConfig {
	return manager.Section(SectionDatabase).(DatabaseConfig)
}
