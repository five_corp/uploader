package sections

import "bitbucket.org/five_corp/saferest"

// Descriptors list all the co nfig section descriptors
func Descriptors() []saferest.ConfigDescriptor {
	return []saferest.ConfigDescriptor{
		{Section: SectionCallHome, Parser: saferest.YAMLParser(SectionCallHome), Factory: NewCallHomeConfig},
		{Section: SectionCommand, Parser: saferest.YAMLParser(SectionCommand), Factory: NewCommandConfig},
		{Section: SectionDatabase, Parser: saferest.XMLParser(SectionDatabase), Factory: NewDatabaseConfig},
		{Section: SectionQueries, Parser: saferest.YAMLParser(SectionQueries), Factory: NewQueriesConfig},
		{Section: SectionSched, Parser: saferest.YAMLParser(SectionSched), Factory: NewSchedConfig},
		{Section: SectionTenants, Parser: saferest.YAMLParser(SectionTenants), Factory: NewTenantsConfig},
		{Section: SectionUpdate, Parser: saferest.YAMLParser(SectionUpdate), Factory: NewUpdateConfig},
		{Section: SectionUploads, Parser: saferest.YAMLParser(SectionUploads), Factory: NewUploadsConfig},
	}
}
