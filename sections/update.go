package sections

import (
	"io"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/update"
)

// SectionUpdate identifies the update section of config
const SectionUpdate = "update"

// UpdateConfig defines the configuration of update manager
type UpdateConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Path of the service updates folder
	UpdateFolder string `json:"updateFolder" xml:"update_folder" yaml:"updateFolder"`
	// Path to the folder with signing certificate
	CertsFolder string `json:"certsFolder" xml:"certs_folder" yaml:"certsFolder"`
	// Path of the Java Folder
	JavaFolder string `json:"javaFolder" xml:"java_folder" yaml:"javaFolder"`
	// Path of the Pentaho Folder
	ETLFolder string `json:"etlFolder" xml:"etl_folder" yaml:"etlFolder"`
	// Path of the jobs folder
	JobFolder string `json:"jobFolder" xml:"job_folder" yaml:"jobFolder"`
	// Path of the transforms folder
	XformFolder string `json:"xformFolder" xml:"xform_folder" yaml:"xformFolder"`

	// Update Rules
	UpdateRules map[string]update.UnzipMap `json:"updateRules" xml:"update_rules" yaml:"updateRules"`
}

// NewUpdateConfig creates a Config object with default values
func NewUpdateConfig(parser saferest.Parser, input io.Reader) saferest.Config {
	// Set sane default values
	config := UpdateConfig{
		Version:      1,
		UpdateFolder: "service/updated",
		CertsFolder:  "service/current",
		JavaFolder:   "java8",
		ETLFolder:    "pentaho",
		JobFolder:    "jobs/jobs",
		XformFolder:  "jobs/xforms",
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetUpdate returns the update section of config
func GetUpdate(manager saferest.Sectioner) UpdateConfig {
	return manager.Section(SectionUpdate).(UpdateConfig)
}
