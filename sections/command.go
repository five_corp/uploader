package sections

import (
	"io"

	"bitbucket.org/five_corp/saferest"
)

// SectionCommand identifies the command config section
const SectionCommand = "command"

// CommandConfig defines the configuration of command runner
type CommandConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Command to run remotely
	RemoteCmd string `json:"remoteCmd" xml:"remote_cmd" yaml:"remoteCmd"`
	// Working directory for the remote command
	RemoteDir string `json:"remoteDir" xml:"remote_dir" yaml:"remoteDir"`
	// Args for the remote command
	RemoteArgs []string `json:"remoteArgs" xml:"remote_args" yaml:"remoteArgs"`
	// Environment vars for the remote command.
	// The environment var "TENANT_NAME" is added automagically
	// before running any command
	RemoteEnv []string `json:"remoteEnv" xml:"remote_env" yaml:"remoteEnv"`
	// Directory for remote command logs
	LogDir string `json:"logDir" xml:"log_dir" yaml:"logDir"`
	// Maximum size of logs (MBytes)
	LogSize int `json:"logSize" xml:"log_size" yaml:"logSize"`
	// Number of log backups to save
	LogBackups int `json:"logBackups" xml:"log_backups" yaml:"logBackups"`
	// Names of dashboards to refresh after updating data
	Requests []string `json:"requests" xml:"requests" yaml:"requests"`
	// MultiCompany flag: If true, support multiple companies
	Multi bool `json:"multi" xml:"multi" yaml:"multi"`
}

// NewCommandConfig createa a Config object with default values
func NewCommandConfig(parser saferest.Parser, input io.Reader) saferest.Config {
	// Set sane default values
	config := CommandConfig{
		Version:    1,
		RemoteCmd:  "",
		RemoteDir:  "",
		RemoteArgs: nil,
		RemoteEnv:  nil,
		LogDir:     "logs",
		LogSize:    1,
		LogBackups: 5,
		Requests:   []string{},
		Multi:      false,
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetCommand returns the command section of config
func GetCommand(manager saferest.Sectioner) CommandConfig {
	return manager.Section(SectionCommand).(CommandConfig)
}
