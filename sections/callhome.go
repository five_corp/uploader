package sections

import (
	"io"

	"bitbucket.org/five_corp/saferest"
)

// SectionCallHome is the name of the "CallHome" section config file
const SectionCallHome = "callhome"

// CallHomeConfig contains configs for the CallHomje service
type CallHomeConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// Call Home URLs
	PingURL             string   `json:"pingURL" xml:"ping_url" yaml:"pingURL"`
	DatabaseURL         string   `json:"databaseURL" xml:"database_url" yaml:"databaseURL"`
	DatabaseAudience    string   `json:"databaseAudience" xml:"database_audience" yaml:"databaseAudience"`
	TokenURL            string   `json:"tokenURL" xml:"token_url" yaml:"tokenURL"`
	TokenAudience       string   `json:"tokenAudience" xml:"token_audience" yaml:"tokenAudience"`
	CredentialsURL      string   `json:"credentialsURL" xml:"credentials_url" yaml:"credentialsURL"`
	CredentialsAudience string   `json:"credentialsAudience" xml:"credentials_audience" yaml:"credentialsAudience"`
	ConectorURL         string   `json:"conectorURL" xml:"conector_url" yaml:"conectorURL"`
	ConectorAudience    string   `json:"conectorAudience" xml:"conector_audience" yaml:"conectorAudience"`
	CacheURL            string   `json:"cacheURL" xml:"cache_url" yaml:"cacheURL"`
	CacheAudience       string   `json:"cacheAudience" xml:"cache_audience" yaml:"cacheAudience"`
	SourceOverride      []string `json:"sourceOverride" xml:"sourceOverride" yaml:"sourceOverride"`
	// Keepalive period (minutes)
	PingTime int `json:"pingTime" xml:"ping_time" yaml:"pingTime"`

	// Call home and connector timeouts, seconds
	Timeout         int `json:"timeout" xml:"timeout" yaml:"timeout"`
	ConectorTimeout int `json:"conectorTimeout" xml:"conector_timeout" yaml:"conectorTimeout"`
}

// NewCallHomeConfig creates a Config object with default values
func NewCallHomeConfig(parser saferest.Parser, input io.Reader) saferest.Config {
	// Set sane default values
	config := CallHomeConfig{
		Version:         1,
		Timeout:         15,
		ConectorTimeout: 600,
		PingTime:        30,
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetCallHome returns the CallHomeConfig stored by a section manager
func GetCallHome(manager saferest.Sectioner) CallHomeConfig {
	return manager.Section(SectionCallHome).(CallHomeConfig)
}
