package sections

import (
	"io"

	"bitbucket.org/five_corp/saferest"
)

// SectionQueries identifies the queries section of config
const SectionQueries = "queries"

// QueriesConfig defines the configuration of Query repository
type QueriesConfig struct {
	// Saving and versioning.
	Version int `json:"version" xml:"version" yaml:"version" flags:"skip"`

	// name of the query field used for RBAC
	Principal []string `json:"principal" xml:"principal" yaml:"principal"`
	// Row limit
	RowLimit int `json:"rowLimit" xml:"row_limit" yaml:"rowLimit"`
	// Size of the Parameter Cache (not Query Cache), in units
	CacheSize int `json:"cacheSize" xml:"cache_size" yaml:"cacheSize"`
	// Throttle concurrent queries to a maximum
	Throttle int `json:"throttle" xml:"throttle" yaml:"throttle"`
	// Name of the Query File (without extension)
	QueryFile string `json:"queryFile" xml:"queryFile" yaml:"queryFile"`
}

// NewQueriesConfig creates a Config object with default values
func NewQueriesConfig(parser saferest.Parser, input io.Reader) saferest.Config {
	// Set sane default values
	config := QueriesConfig{
		Version:   1,
		RowLimit:  10000,
		CacheSize: 512,
		Throttle:  20,
		QueryFile: "queries",
	}
	if input != nil {
		parser.Decode(input, &config)
	}
	return config
}

// GetQueries returns the queries config section
func GetQueries(manager saferest.Sectioner) QueriesConfig {
	return manager.Section(SectionQueries).(QueriesConfig)
}
