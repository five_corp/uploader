package olap

import (
	"fmt"
	"sort"
	"strings"
)

// Link to another relation: always many to one.
type Link struct {
	// SQL alias for joins
	Alias string `json:"alias" yaml:"alias"`
	// Foreign key from current table to linked table (field names)
	Keys map[string]string `json:"keys" yaml:"keys"`
	// Must include this link in every query to this relation
	Must bool `json:"must" yaml:"must"`
}

// Builds the join clause for the link
func (l Link) join(parentAlias, childTable, childAlias string) string {
	inner := fmt.Sprintf("INNER JOIN %s AS %s ON", childTable, childAlias)
	conds := make(sort.StringSlice, 0, len(l.Keys))
	for top, bottom := range l.Keys {
		if strings.HasSuffix(top, "()") {
			// The join criteria includes a function call,
			// do not prefix it with the parent table name.
			conds = append(conds, fmt.Sprintf("%s=%s.%s",
				top, childAlias, bottom))
		} else {
			conds = append(conds, fmt.Sprintf("%s.%s=%s.%s",
				parentAlias, top, childAlias, bottom))
		}
	}
	// Sort conditions so that the order of the clauses is
	// fixed, and test can run consistently
	conds.Sort()
	return fmt.Sprintf("%s (%s)", inner, strings.Join(conds, ") AND ("))
}

// Assigns an alias to the link
func (l Link) alias(counter int) string {
	if l.Alias != "" {
		return l.Alias
	}
	return fmt.Sprintf("d%d", counter)
}
