package olap

import (
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/policy"
	"github.com/pkg/errors"
)

// Slice of fact and dimension tables for a given set of fields
type Slice struct {
	// Root relation
	root string
	// Alias assigned to each member table of the join
	data joinMap
	// Cond defines the slice boundaries
	cond string
	// Policed contains predefined filters based on policy
	policed dbtool.Params
}

// constraint for a parameter
type constraint struct {
	inSelection bool
	reqs        string
	cond        string
}

// flattens relation information, useful for working with the tree
type flatRelation struct {
	// Root of this flattened tree
	root            Relation
	rootName, alias string
	// Merged scope of all relations in tree
	scope []string
	// Minimum set of relations to include in a slice
	// (all constrained relations, indexed by param name)
	constrained map[string][]constraint
	// Join clauses for each nested table
	data joinMap
}

// newFlattened flattens the given relation
func newFlattened(rels map[string]Relation, relName, alias string, root Relation) (*flatRelation, error) {
	result := &flatRelation{
		root:        root,
		rootName:    relName,
		alias:       alias,
		scope:       make([]string, 0, 8),
		constrained: make(map[string][]constraint),
		data:        make(map[string]joinData),
	}
	result.data[relName] = joinData{
		insertOrder: len(result.data),
		table:       root.Table,
		selection:   root.Selection,
		alias:       alias,
		attribs:     root.attribs("", alias, true),
		options:     root.Options,
		must:        root.must(),
	}
	if err := result.flatten(rels, relName, alias, root); err != nil {
		return result, err
	}
	sortedNames := result.data.sortedNames()
	if err := result.mergeScopes(rels, sortedNames); err != nil {
		return result, err
	}
	if err := result.mergeConstraints(rels, sortedNames); err != nil {
		return result, err
	}
	// Sanity check to make sure no two relations share the same alias
	visited := make(map[string]struct{})
	for _, data := range result.data {
		if _, repeated := visited[data.alias]; repeated {
			return nil, errors.Errorf("Alias %s is repeated under relation %s", data.alias, relName)
		}
		visited[data.alias] = struct{}{}
	}
	return result, nil
}

// Flatten the relation with all links, fills in the flatRelation struct
func (flat *flatRelation) flatten(rels map[string]Relation, relName, alias string, r Relation) error {
	// Iterate links in order, so that tests are repeatable
	sortedNames := make(sort.StringSlice, 0, len(r.Links))
	for nestedName := range r.Links {
		sortedNames = append(sortedNames, nestedName)
	}
	for _, nestedName := range sortedNames {
		if _, loop := flat.data[nestedName]; loop {
			// data is already there, break recursion
			return errors.Errorf("Loop detected while flatening %s, current list: %s", nestedName, flat.data.sortedNames())
		}
		// Check the link is valid
		nested, ok := rels[nestedName]
		if !ok {
			return errors.Errorf("Relation %s not found for flattening", nestedName)
		}
		link := r.Links[nestedName]
		// Add the join clause for this link
		newAlias := link.alias(len(flat.data))
		join := link.join(alias, nested.Table, newAlias)
		flat.data[nestedName] = joinData{
			insertOrder: len(flat.data),
			table:       nested.Table,
			selection:   nested.Selection,
			alias:       newAlias,
			attribs:     nested.attribs(nestedName, newAlias, true),
			join:        join,
			require:     relName,
			options:     nested.Options,
			must:        nested.must(),
		}
		// Recurse the link
		if err := flat.flatten(rels, nestedName, newAlias, nested); err != nil {
			return err
		}
	}
	return nil
}

// MergeScopes makes sure there is at least a common scope in all relations of the flattened list
func (flat *flatRelation) mergeScopes(rels map[string]Relation, sortedNames []string) error {
	scope := flat.scope
	for _, relName := range sortedNames {
		if rel, ok := rels[relName]; ok && rel.Scope != nil && len(rel.Scope) > 0 {
			if scope == nil || len(scope) <= 0 {
				// Copy the scope
				scope = append(scope, rel.Scope...)
				continue
			}
			// Truncate the scopes array, leave only scopes that intersect
			match := 0
			for i := 0; i < len(scope); i++ {
				current := scope[i]
				for j := 0; j < len(rel.Scope); j++ {
					if current == rel.Scope[j] {
						scope[match] = current
						match++
						break
					}
				}
			}
			if match == 0 {
				return errors.Errorf("Invalid schema: there are no amtching scopes in Relations under %s", relName)
			}
			scope = scope[0:match]
		}
	}
	// Sort so that scope list is deterministic, for tests
	sort.StringSlice(scope).Sort()
	flat.scope = scope
	return nil
}

// MergeConstraints merges all the constraints of the flattened relations
func (flat *flatRelation) mergeConstraints(rels map[string]Relation, sortedNames []string) error {
	for _, relName := range sortedNames {
		data := flat.data[relName]
		if rel, ok := rels[relName]; ok && rel.Filters != nil && len(rel.Filters) > 0 {
			for field, param := range rel.Filters {
				inSelection, cond := false, ""
				if selection, found := rel.inSelection(field); selection && !found {
					inSelection = true
					cond = fmt.Sprintf("{%s=%s}", field, param)
				} else {
					cond = fmt.Sprintf("{%s.%s=%s}", data.alias, field, param)
				}
				current := constraint{
					inSelection: inSelection,
					cond:        cond,
					reqs:        relName,
				}
				cached, ok := flat.constrained[param]
				if !ok || cached == nil {
					cached = make([]constraint, 0, 8)
				}
				cached = append(cached, current)
				flat.constrained[param] = cached
			}
		}
	}
	return nil
}

// slice a cube according to the given policy
func (flat *flatRelation) slice() (*Slice, error) {
	slice := &Slice{
		root: flat.rootName,
		data: make(map[string]joinData),
	}
	join := flat.data[flat.rootName]
	slice.data[flat.rootName] = joinData{
		insertOrder: len(slice.data),
		table:       flat.root.Table,
		selection:   join.selection,
		alias:       flat.alias,
		attribs:     join.attribs,
		join:        fmt.Sprintf("%s AS %s", flat.root.Table, flat.alias),
		options:     flat.root.Options,
		must:        join.must,
	}
	for _, must := range join.must {
		flat.data.join(slice, must)
	}
	return slice, nil
}

// Police: polices an slice
func (flat *flatRelation) police(slice *Slice, p policy.Policy) error {
	if p.Empty() {
		return nil
	}
	if err := p.Check(flat.scope, nil, nil); err != nil {
		return err
	}
	data := make(dbtool.Params)
	cond := make(sort.StringSlice, 0, len(p.Filters)+1)
	for _, k := range p.Sorted() {
		constraints, ok := flat.constrained[k]
		if !ok {
			continue
		}
		data[k] = p.Filters[k]
		for _, c := range constraints {
			if err := flat.data.join(slice, c.reqs); err != nil {
				return err
			}
			if c.inSelection {
				slice.pushFilter(c.reqs, c.cond)
			} else {
				cond = append(cond, c.cond)
			}
		}
	}
	if len(cond) > 0 {
		if slice.cond != "" {
			cond = append(cond, slice.cond)
		}
		cond.Sort()
		slice.cond = fmt.Sprintf("(%s)", strings.Join(cond, ") AND ("))
	}
	slice.policed = data
	return nil
}

// PushFilter pushes a filter to the embedded Selection in a joinData
func (slice *Slice) pushFilter(relName, cond string) error {
	nested, ok := slice.data[relName]
	if !ok || nested.selection.Aliases == nil || len(nested.selection.Aliases) <= 0 {
		return errors.Errorf("Failed to find relation %s in slice %s for nested filter", relName, slice.root)
	}
	// Update the nested selection condition with the filter
	where := nested.selection.Where
	if where == "" {
		nested.selection.Where = cond
	} else {
		nested.selection.Where = fmt.Sprintf("(%s) AND (%s)", where, cond)
	}
	slice.data[relName] = nested
	return nil
}

// Selectors builds a list of 'addresses' from the given prefix
// and the attributes of the joinData object.
func concatSelectors(buffer []joinColumn, attribs map[string]joinColumn) []joinColumn {
	for _, v := range attribs {
		buffer = append(buffer, v)
	}
	return buffer
}
