package olap

import (
	"strings"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/policy"
	"github.com/pkg/errors"
	yaml "gopkg.in/yaml.v2"
)

// Store is the OLAP schema manager
type Store interface {
	Reload() error
	// Slice returns a Slice object for the requested facts and dimensions
	Slice(p policy.Policy, alias string) (*Slice, error)
	// With adds a Selection as a dynamic join property
	With(slice *Slice, link Link, selection Selection) error
	// Intersect updates a Slice intersecting it with some dimension, and optional filter.
	Intersect(slice *Slice, filters map[string]Filter) error
	// Select builds a Select statement from the Slice.
	// Returns the select statement along with the list of column names
	Select(slice *Slice, query Query, params dbtool.Params) (Selection, error)
}

// Query is the set of parameters used to run a query against a Slice
type Query struct {
	Fields []string          `json:"fields" yaml:"fields"`
	Groups []GroupDescriptor `json:"groups,omitempty" yaml:"groups,omitempty"`
	Pivot  []string          `json:"pivot,omitempty" yaml:"pivot,omitempty"`
	Sort   []Sorting         `json:"sort,omitempty" yaml:"sort,omitempty"`
	Offset int               `json:"offset,omitempty" yaml:"offset,omitempty"`
	Limit  int               `json:"limit,omitempty" yaml:"limit,omitempty"`
}

type store struct {
	folder   string
	fileName string
	rels     map[string]Relation
	flat     map[string]*flatRelation
}

// NewManager reads the schema and creates an OLAP manager
func NewManager(folder, fileName string) (Store, error) {
	s := &store{folder: folder, fileName: fileName}
	if err := s.Reload(); err != nil {
		return nil, err
	}
	return s, nil
}

func newFrom(data string) (Store, error) {
	data = strings.Replace(data, "\t", "  ", -1)
	s := &store{}
	if err := yaml.Unmarshal([]byte(data), &s.rels); err != nil {
		return nil, err
	}
	return s, s.load(s.rels)
}

// Reload implements Store interface
func (s *store) Reload() error {
	parser := saferest.YAMLParser(s.fileName)
	var raw map[string]Relation
	if err := saferest.ParseFile(s.folder, parser, &raw); err != nil {
		return err
	}
	return s.load(raw)
}

func (s *store) load(raw map[string]Relation) error {
	rels := make(map[string]Relation)
	flat := make(map[string]*flatRelation)
	// Copy the raw array, in case it is reused by the caller...
	for k, v := range raw {
		// Deprecated: relation names are always treated as literals
		// _, val := split(k)
		rels[k] = v
	}
	for k, v := range rels {
		f, err := newFlattened(rels, k, "v", v)
		if err != nil {
			return err
		}
		flat[k] = f
	}
	s.rels = rels
	s.flat = flat
	return nil
}

// Slice implements Store interface
func (s *store) Slice(p policy.Policy, alias string) (*Slice, error) {
	flat, ok := s.flat[alias]
	if !ok {
		return nil, errors.Errorf("Failed to find alias %s in schema", alias)
	}
	slice, err := flat.slice()
	if err != nil {
		return nil, err
	}
	if err := flat.police(slice, p); err != nil {
		return nil, err
	}
	return slice, nil
}

// Intersect implements Store interface
func (s *store) Intersect(slice *Slice, filters map[string]Filter) error {
	flat, ok := s.flat[slice.root]
	if !ok {
		return errors.Errorf("Failed to find relation %s", slice.root)
	}
	return flat.data.intersect(slice, filters)
}

// Select implements Store interface
func (s *store) Select(slice *Slice, query Query, params dbtool.Params) (Selection, error) {
	root, ok := s.flat[slice.root]
	if !ok {
		return Selection{}, errors.Errorf("Failed to find relation %s", slice.root)
	}
	return root.data.selection(slice, query, params)
}

// With implements Store
func (s *store) With(slice *Slice, link Link, selection Selection) error {
	// Get the columns from the selection
	root, ok := s.flat[slice.root]
	if !ok {
		return errors.Errorf("Relation %s not found while building With", slice.root)
	}
	return root.data.with(slice, link, selection)
}

// Fields returns all the fields in the query
func (q Query) fields() []string {
	fields := make([]string, 0, len(q.Fields)+len(q.Groups)+len(q.Pivot)+len(q.Sort))
	fields = append(fields, q.Fields...)
	for _, g := range q.Groups {
		fields = append(fields, g.Field)
	}
	fields = append(fields, q.Pivot...)
	for _, s := range q.Sort {
		fields = append(fields, s.Field)
	}
	return fields
}
