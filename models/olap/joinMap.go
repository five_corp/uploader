package olap

import (
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// joinColumn describes the various "names" of a column
type joinColumn struct {
	label    string // Public name of the field
	selector string // Database selector (`alias.column`)
	alias    string // Resulting column name (AS `alias`)
}

// Data associated to each table in a join
type joinData struct {
	insertOrder int // joins must be ordered by insertOrder
	table       string
	selection   Selection
	attribs     map[string]joinColumn
	alias       string
	join        string
	require     string   // joins to include before this one
	must        []string // joins to include after this one
	options     dbtool.Params
}

// Map from relation to joinData
type joinMap map[string]joinData

// Join adds the required relation, and its dependencies, to the slice
func (root joinMap) join(s *Slice, relName string) error {
	if _, ok := s.data[relName]; ok {
		// Already intersected
		return nil
	}
	nested, ok := root[relName]
	if !ok {
		return errors.Errorf("Relation %s not found for slicing", relName)
	}
	// 'require' goes before
	if nested.require != "" {
		root.join(s, nested.require)
	}
	s.data[relName] = joinData{
		insertOrder: len(s.data),
		table:       nested.table,
		selection:   nested.selection,
		alias:       nested.alias,
		attribs:     nested.attribs,
		join:        nested.join,
		options:     nested.options,
		must:        nested.must,
	}
	// 'must' will be processed by "query", to avoid a few issues.
	// e.g. suppose that:
	// Rel1 - must -> Rel2
	// Rel2 - reqs -> Rel1
	// then, if you filter or select Rel2__Field,
	// 1.- There are too many calls to join
	//     join(Rel2) - (req) -> join(Rel1) - (must) -> join(Rel2) - (req) -> join(Rel1)
	// 2.- Order may get corrupted because of that
	//for _, must := range nested.must {
	//	root.join(s, must)
	//}
	return nil
}

// Address represents the result of resolving a reference
type address struct {
	Relation string
	Columns  []joinColumn
}

// Resolve an attribute path to a pair of relation name and column list.
// Understands the following patterns:
// [path]__* => all attributes in path
// *__* => All attributes in all paths
func (root joinMap) resolve(slice *Slice, attrib string) (addr address, err error) {
	// Get the base flatten object
	base, ok := slice.data[slice.root]
	if !ok {
		return addr, errors.Errorf("Failed to find root %s of Slice %v", slice.root, slice)
	}
	prefix, value := split(attrib)
	total := 0
	switch prefix {
	case "":
		prefix = slice.root
	case "*":
		// "star" prefix intersects all linked relations
		for p, item := range root {
			total += len(item.attribs)
			if p != slice.root {
				if err := root.join(slice, p); err != nil {
					return addr, err
				}
			}
		}
	default:
		if err := root.join(slice, prefix); err != nil {
			return addr, err
		}
		base = slice.data[prefix]
	}
	// Default values for alias and field
	var selectors []joinColumn
	switch value {
	case "*":
		switch prefix {
		case "*":
			// Add the full list of fields to the selectors
			selectors = make([]joinColumn, 0, total)
			for _, base := range root {
				selectors = concatSelectors(selectors, base.attribs)
			}
		default:
			// Add the full list of fields to the selectors
			selectors = concatSelectors(make([]joinColumn, 0, len(base.attribs)), base.attribs)
		}
	default:
		field, ok := base.attribs[value]
		if !ok {
			return addr, errors.Errorf("Attribute %s in path %s does not match a dimension or property", value, attrib)
		}
		selectors = []joinColumn{field}
	}
	// Sort attributes so that test can be deterministic
	sort.Slice(selectors, func(i, j int) bool {
		return strings.Compare(selectors[i].label, selectors[j].label) < 0
	})
	addr.Relation, addr.Columns = prefix, selectors
	return addr, nil
}

// Intersect the slice with the given filters
func (root joinMap) intersect(slice *Slice, filters map[string]Filter) error {
	// Sort the attribs, so that slicing is deterministic
	tail, sorted := 0, make(sort.StringSlice, 0, len(filters)+1)
	for attrib := range filters {
		sorted = append(sorted, attrib)
	}
	sorted.Sort()
	for _, attrib := range sorted {
		// The resolve / intersect runs for every filter, even nil ones
		addr, err := root.resolve(slice, attrib)
		if err != nil {
			return err
		}
		filter := filters[attrib]
		if filter == nil {
			continue
		}
		if len(addr.Columns) != 1 {
			return errors.Errorf("Path %s does not resolve to a single field to filter, but to %v", attrib, addr.Columns)
		}
		// Get the first item in the result
		item := addr.Columns[0]
		cond := filter(item.selector)
		if item.filterOnly() {
			// This attribute is not part of the Relation, it is filter-only.
			// push it to the selection.Where
			if err := slice.pushFilter(addr.Relation, cond); err != nil {
				return err
			}
		} else {
			// This attribute is part of the Relation
			sorted[tail] = cond
			tail++
		}
	}
	if tail <= 0 {
		return nil
	}
	// truncate sorted list to include only valid conditions
	sorted = sorted[:tail]
	if slice.cond != "" {
		sorted = append(sorted, slice.cond)
	}
	slice.cond = fmt.Sprintf("(%s)", strings.Join(sorted, ") AND ("))
	return nil
}

// Selection runs a request against a slice, returns a Selection
func (root joinMap) selection(slice *Slice, query Query, params dbtool.Params) (Selection, error) {
	addresses, err := root.resolveAll(slice, query.fields())
	if err != nil {
		return Selection{}, err
	}
	// Slice.data is final, allocate the buffers
	buffer := sliceBuffer{}
	buffer.Init(
		len(slice.data),                     // max length of withList
		len(slice.data),                     // max length of FromList
		len(query.Fields)+len(query.Groups), // max length of queryList
		len(query.Fields)+len(query.Groups), // max length of namesList
		len(query.Fields)+len(query.Groups), // max length of aliasList
		len(query.Fields),                   // max length of groupList
		len(query.Sort),                     // max length of orderList
		len(query.Pivot),                    // max length of pivotList
	)
	withList := buffer.Next()
	fromList := buffer.Next()
	queryList := buffer.Next()
	namesList := buffer.Next()
	aliasList := buffer.Next()
	groupList := buffer.Next()
	orderList := buffer.Next()
	pivotList := buffer.Next()
	// Fields to select
	mustGroup := len(query.Groups) > 0
	for _, field := range query.Fields {
		for _, item := range addresses[field].Columns {
			if !item.filterOnly() {
				namesList = append(namesList, item.label)
				aliasList = append(aliasList, item.alias)
				queryList = append(queryList, strings.Join([]string{item.selector, "AS", item.alias}, " "))
				if mustGroup {
					groupList = append(groupList, item.selector)
				}
			}
		}
	}
	// Fields to group
	for _, group := range query.Groups {
		columns := addresses[group.Field].Columns
		if len(columns) != 1 {
			return Selection{}, errors.Errorf("Aggregating by field %s is not supported", group.Field)
		}
		if item := columns[0]; !item.filterOnly() {
			namesList = append(namesList, item.label)
			aliasList = append(aliasList, item.alias)
			queryList = append(queryList, strings.Join([]string{group.Group.Of(item.selector), "AS", item.alias}, " "))
		}
	}
	// Pivot columns
	for _, p := range query.Pivot {
		// Make sure the slice includes the tables required for pivoting
		columns := addresses[p].Columns
		if len(columns) != 1 || columns[0].filterOnly() {
			return Selection{}, errors.Errorf("Pivoting by field %s is not supported", p)
		}
		pivotList = append(pivotList, columns[0].selector)
	}
	// Fields to sort
	template := []string{"", "ASC"}
	for _, sort := range query.Sort {
		if sort.Descending {
			template[1] = "DESC"
		} else {
			template[1] = "ASC"
		}
		for _, item := range addresses[sort.Field].Columns {
			// Skip filter-only attribs
			if !item.filterOnly() {
				template[0] = item.selector
				orderList = append(orderList, strings.Join(template, " "))
			}
		}
	}
	// Finally, collect options and withs, in insertion order
	for _, key := range slice.data.sortedNames() {
		data := slice.data[key]
		// If the selection is not empty, add it to the with list
		if len(data.selection.Aliases) > 0 {
			withStmt := data.selection.Statement()
			withList = append(withList, fmt.Sprintf("%s AS (%s)", data.table, withStmt.SQL))
			params = params.Merge(withStmt.Defaults)
		}
		if data.join != "" {
			fromList = append(fromList, data.join)
		}
		params = params.Merge(data.options)
	}
	// reverse withList. We assume that "with" tables do not
	// depend on parent tables, but may depend on child tables
	for i, j := 0, len(withList)-1; i < j; i, j = i+1, j-1 {
		withList[i], withList[j] = withList[j], withList[i]
	}
	// And create the selection
	result := Selection{
		Columns:    namesList,
		Aliases:    aliasList,
		With:       strings.Join(withList, ", "),
		Select:     strings.Join(queryList, ", "),
		From:       strings.Join(fromList, " "),
		Where:      slice.cond,
		Group:      strings.Join(groupList, ", "),
		Order:      strings.Join(orderList, ", "),
		PivotList:  pivotList,
		PivotGroup: query.Groups,
		Options:    params,
	}
	if query.Offset >= 0 && query.Limit > 0 {
		result.Limit = fmt.Sprintf("%d OFFSET %d", query.Limit, query.Offset)
	}
	return result, nil
}

// resolveAll resolves all the references in the query
func (root joinMap) resolveAll(slice *Slice, fields []string) (map[string]address, error) {
	result := make(map[string]address, len(fields))
	for _, f := range fields {
		addr, err := root.resolve(slice, f)
		if err != nil {
			return nil, err
		}
		result[f] = addr
	}
	// We are done selecting fields, the slice should grow no more.
	// Now we can traverse the "must"· list and add any missing relation
	for _, data := range slice.data {
		for _, must := range data.must {
			if err := root.join(slice, must); err != nil {
				return nil, err
			}
		}
	}
	return result, nil
}

// filterOnly returns true is the column is intended for filtering only
// (no selection, sorting or pivoting)
func (col joinColumn) filterOnly() bool {
	return col.alias == ""
}

// With implements Store
func (root joinMap) with(slice *Slice, link Link, selection Selection) error {
	// Build the join. We cannot use link.Join because it assumes the
	// key name and values are column names, but in this case, key names
	// are Relation paths.
	alias := link.Alias
	join := selection.joinData(link.Alias)
	if len(link.Keys) > 0 {
		cond := make(sort.StringSlice, 0, len(link.Keys))
		for k, v := range link.Keys {
			addr, err := root.resolve(slice, k)
			if err != nil {
				return err
			}
			if len(addr.Columns) != 1 {
				return errors.Errorf("Failed to resolve %s in Relation %s for With", k, slice.root)
			}
			parent := addr.Columns[0]
			child, ok := join.attribs[v]
			// Joining on filter-only attribs
			if !ok || child.filterOnly() {
				return errors.Errorf("Failed to resolve %s in Sub-Relation %+v for With", v, join)
			}
			cond = append(cond, strings.Join([]string{parent.selector, child.selector}, "="))
		}
		cond.Sort()
		join.join = fmt.Sprintf("INNER JOIN %s AS %s ON (%s)", alias, alias, strings.Join(cond, ") AND ("))
	}
	join.insertOrder = len(slice.data)
	slice.data[link.Alias] = join
	return nil
}

// Returns the keys of the map, in insertion order
func (root joinMap) sortedNames() []string {
	sorted := make(sort.StringSlice, len(root))
	for k, v := range root {
		sorted[v.insertOrder] = k
	}
	return sorted
}

// SliceBuffer is just a simple construct to create sub-slices from a single slice
type sliceBuffer struct {
	buffer []string
	sizes  []int
}

// Next subslice from the buffer
func (b *sliceBuffer) Next() []string {
	curlen := b.sizes[0]
	result := b.buffer[0:0:curlen]
	b.buffer = b.buffer[curlen:]
	b.sizes = b.sizes[1:]
	return result
}

// Initializes a sliceBuffer
func (b *sliceBuffer) Init(sizes ...int) {
	total := 0
	for _, s := range sizes {
		total += s
	}
	b.buffer = make([]string, total)
	b.sizes = sizes
}
