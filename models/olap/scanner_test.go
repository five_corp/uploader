package olap

import "testing"

type scannerResult [2]string
type scannerTest struct {
	label         string
	attrib        string
	prefix, value string
}

func TestScanner(t *testing.T) {
	iterations := []scannerTest{
		{
			label:  "Short path without brackets",
			attrib: "test without brackets",
			value:  "test without brackets",
		},
		{
			label:  "Short path with dots",
			attrib: "[test__with__dots]",
			value:  "test__with__dots",
		},
		{
			label:  "Long path without brackets",
			attrib: "test__without__brackets",
			prefix: "test", value: "without",
		},
		{
			label:  "Long path with brackets",
			attrib: "[test]__with__[brackets]",
			prefix: "test", value: "with",
		},
		{
			label:  "Long path with dots",
			attrib: "[test__with]__dots",
			prefix: "test__with", value: "dots",
		},
	}
	for _, iter := range iterations {
		iter := iter // for the closure below
		t.Run(iter.label, func(t *testing.T) {
			prefix, value := split(iter.attrib)
			match := (prefix == iter.prefix && value == iter.value)
			if !match {
				t.Errorf("simpleScaner{path:\"%s\"}.Split() = %v.%v -- expected %v.%v", iter.attrib, prefix, value, iter.prefix, iter.value)
			}
		})
	}
}
