package olap

import (
	"fmt"
	"strings"
)

// Relation table
type Relation struct {
	// Scope of the relation, for Policy Enforcement
	Scope []string `json:"scope,omitempty" yaml:"scope,omitempty"`
	// Name of the dimension table
	Table string `json:"table" yaml:"table"`
	// Selection: if the table is not static, but a sub-query
	Selection Selection `json:"selection" yaml:"selection"`
	// Dimensions in this relation, alias => name
	Dimensions map[string]string `json:"dimensions" yaml:"dimensions"`
	// Properties in this relation, alias => name
	Properties map[string]string `json:"properties" yaml:"properties"`
	// Filters map: field name => filter name.  For instance, { "empresa": "pEmpresa" }
	Filters map[string]string `json:"filters,omitempty" yaml:"filters,omitempty"`
	// Sorting criteria, for type 2 dimensions (how to get the most recent entry)
	Sorting []Sorting `json:"sorting,omitempty" yaml:"sorting,omitempty"`
	// Links to other relations
	Links map[string]Link `json:"links,omitempty" yaml:"links,omitempty"`
	// Options for dynamic fields
	Options map[string]interface{} `json:"options,omitempty" yaml:"options,omitempty"`
}

// Sorting criteria: field name and type of sorting, ascending / descending
type Sorting struct {
	Field      string `json:"field" yaml:"field"`
	Descending bool   `json:"descending" yaml:"descending"`
}

// attribs names all the attribs in the relation, prefixing the proper alias
func (r Relation) attribs(prefix, alias string, includeDynamic bool) map[string]joinColumn {
	result := make(map[string]joinColumn)
	// Automatically add r.Selection properties
	inputs := make([]map[string]string, 0, 3)
	clen, alen := len(r.Selection.Columns), len(r.Selection.Aliases)
	if clen > alen {
		clen = alen
	}
	if clen > 0 {
		auto := make(map[string]string)
		for index := 0; index < clen; index++ {
			auto[r.Selection.Columns[index]] = r.Selection.Aliases[index]
		}
		inputs = append(inputs, auto)
	}
	// Add other properties
	inputs = append(inputs, r.Dimensions, r.Properties)
	for _, m := range inputs {
		// Add the fields of the relation
		for key, name := range m {
			selector, label := "", merge(prefix, key)
			switch {
			// Manage dynamic selectors
			case !strings.Contains(name, "%alias%"):
				if subSelect, found := r.inSelection(name); subSelect && !found {
					// If the field name does not exist in the aliases list, it is a filter-only field
					selector = name
					name, label = "", ""
				} else {
					selector = fmt.Sprintf("%s.%s", alias, name)
				}
			case includeDynamic:
				selector = strings.Replace(name, "%alias%", alias, -1)
				name = fmt.Sprintf("d%d", len(result))
			default:
				continue
			}
			// manage names
			if name != "" && prefix != "" && alias != "" {
				name = strings.Join([]string{alias, name}, "_")
			}
			result[key] = joinColumn{
				label:    label,
				alias:    name,
				selector: selector,
			}
		}
	}
	return result
}

// inSelection tests if this Relation comes from a Selection, and
// the attribute belongs to selection.Aliases
func (r *Relation) inSelection(attrib string) (subSelect, found bool) {
	if r.Selection.Aliases != nil && len(r.Selection.Aliases) > 0 {
		// Only fields exposed in the selection.Aliases are selectable
		for _, inner := range r.Selection.Aliases {
			if strings.EqualFold(attrib, inner) {
				return true, true
			}
		}
		return true, false
	}
	return false, false
}

// Must returns all relations that must be joined with this one
func (r *Relation) must() []string {
	if len(r.Links) <= 0 {
		return nil
	}
	must := make([]string, 0, len(r.Links))
	for relName, link := range r.Links {
		if link.Must {
			must = append(must, relName)
		}
	}
	return must
}
