package olap

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"bitbucket.org/five_corp/uploader/models/policy"
	yaml "gopkg.in/yaml.v2"
)

type withTest struct {
	Link      Link      `json:"link" yaml:"link"`
	Selection Selection `json:"selection" yaml:"selection"`
}

type joinTest struct {
	Label     string        `json:"label" yaml:"label"`
	Query     Query         `json:"query" yaml:"query"`
	With      []withTest    `json:"with,omitempty" yaml:"with,omitempty"`
	Intersect []string      `json:"intersect,omitempty" yaml:"intersect,omitempty"`
	Policy    policy.Policy `json:"policy,omitempty" yaml:"policy,omitempty"`
	Expected  Selection     `json:"expected" yaml:"expected"`
}

func TestJoin(t *testing.T) {
	runTest(t, testCube(), "join_test.yaml")
}

func TestIntersect(t *testing.T) {
	runTest(t, testCube(), "intersect_test.yaml")
}

func TestGroup(t *testing.T) {
	runTest(t, testCube(), "group_test.yaml")
}

func TestPolicy(t *testing.T) {
	runTest(t, testCube(), "policy_test.yaml")
}

func TestWith(t *testing.T) {
	runTest(t, testCube(), "with_test.yaml")
}

func runTest(t *testing.T, store Store, testFile string) {
	f, err := os.Open(testFile)
	if err != nil {
		t.Errorf("Error opening test file %v: %v", testFile, err)
	}
	joinTests, err := ioutil.ReadAll(f)
	f.Close()
	if err != nil {
		t.Errorf("Error reading test file %v: %v", testFile, err)
	}
	var iterations []joinTest
	if err := yaml.Unmarshal([]byte(strings.Replace(string(joinTests), "\t", "    ", -1)), &iterations); err != nil {
		t.Errorf("Error loading iterations yaml: %v", err)
		return
	}
	outFile := fmt.Sprintf("%s.out", testFile)
	out, err := os.Create(outFile)
	if err != nil {
		t.Errorf("Error opening test output file %v: %v", outFile, err)
	}
	defer out.Close()
	for _, item := range iterations {
		item := item // for the closure below
		t.Run(item.Label, func(t *testing.T) {
			slice, err := store.Slice(item.Policy, "Facts")
			if err != nil {
				t.Errorf("Slice([%v]) error: %v", "Facts", err)
				return
			}
			filters := make(map[string]Filter)
			for _, attr := range item.Intersect {
				filters[attr] = Equals("param1")
			}
			if err := store.Intersect(slice, filters); err != nil {
				t.Errorf("Intersect error: %v", err)
				return
			}
			if item.With != nil && len(item.With) > 0 {
				for _, w := range item.With {
					if err := store.With(slice, w.Link, w.Selection); err != nil {
						t.Errorf("Intersect with error: %v", err)
					}
				}
			}
			s, err := store.Select(slice, item.Query, nil)
			if err != nil {
				t.Errorf("Select([%v]) error: %v", item.Query, err)
				return
			}
			expected := item.Expected
			if len(expected.Columns) > 0 && !expected.equals(s) {
				t.Errorf("Select error: expected %+v, got %+v", expected, s)
			}
			item.Expected = s
			output, _ := yaml.Marshal([]joinTest{item})
			fmt.Fprintln(out, string(output))
		})
	}
}
