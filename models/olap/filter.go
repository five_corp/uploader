package olap

import (
	"fmt"
	"sort"
	"strings"
)

// Filter implements a filtering criteria which generates a
// "Where" SQL clause from a sqlAlias and a field name
type Filter func(selector string) string

// Equals tests a field for equality
func Equals(param string) Filter {
	return Filter(func(selector string) string {
		return fmt.Sprintf("{%s=%s}", selector, param)
	})
}

// IsNull tests a field for nullity
func IsNull() Filter {
	return Filter(func(selector string) string {
		return fmt.Sprintf("%s IS NULL", selector)
	})
}

// IsNotNull tests a field for nullity
func IsNotNull() Filter {
	return Filter(func(selector string) string {
		return fmt.Sprintf("%s IS NOT NULL", selector)
	})
}

// NotEquals tests a field for inequality
func NotEquals(param string) Filter {
	return Filter(func(selector string) string {
		return fmt.Sprintf("%s <> {%s}", selector, param)
	})
}

// Left tests a field for startsWith
func Left(param string, size int) Filter {
	return Filter(func(selector string) string {
		return fmt.Sprintf("LEFT(%s, %d) = {%s}", selector, size, param)
	})
}

// Right tests a field for endsWith
func Right(param string, size int) Filter {
	return Filter(func(selector string) string {
		return fmt.Sprintf("RIGHT(%s, %d) = {%s}", selector, size, param)
	})
}

// Range tests a field for range
func Range(fromParam, toParam string, inclusive bool) Filter {
	opLT, opGT := "<", ">"
	if inclusive {
		opLT, opGT = "<=", ">="
	}
	return Filter(func(selector string) string {
		if fromParam == "" && toParam == "" {
			return "true"
		}
		if fromParam == "" {
			return fmt.Sprintf("(%s %s {%s})", selector, opLT, toParam)
		}
		if toParam == "" {
			return fmt.Sprintf("(%s %s {%s})", selector, opGT, fromParam)
		}
		return fmt.Sprintf("(%s %s {%s}) AND (%s %s {%s})",
			selector, opGT, fromParam,
			selector, opLT, toParam)
	})
}

// Like tests a field with the LIKE operator
func Like(param string) Filter {
	return Filter(func(selector string) string {
		if param == "" {
			return "true"
		}
		return fmt.Sprintf("(%s LIKE {%s})", selector, param)
	})
}

func combine(filters []Filter, join string) Filter {
	return Filter(func(selector string) string {
		result := make(sort.StringSlice, 0, len(filters))
		for _, f := range filters {
			result = append(result, f(selector))
		}
		return fmt.Sprintf("(%s)", strings.Join(result, join))
	})
}

// Or combines several filters together
func Or(filters ...Filter) Filter {
	return combine(filters, ") OR (")
}

// And combines several filters together
func And(filters ...Filter) Filter {
	return combine(filters, ") AND (")
}
