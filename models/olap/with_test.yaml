- label: With for single dimension
  query:
    fields:
    - Sample fact
  with:
    - link:
        alias: with1
        keys:
          Sample degenerate dim: k1
      selection:
        column: ["col1"]
        aliases: ["k1"]
        select: 1 AS k1
        from: test
        where: "true"
        options:
          p1: v1
          p2: v2
  expected:
    columns:
    - Sample fact
    aliases:
    - fact1
    with: with1 AS (SELECT 1 AS k1 FROM test WHERE true)
    select: v.fact1 AS fact1
    from: facts AS v INNER JOIN with1 AS with1 ON (v.deg1=with1.k1)
    options:
      p1: v1
      p2: v2

- label: With for single dimension, getting field
  query:
    fields:
    - Sample fact
    - with1__k1
  with:
    - link:
        alias: with1
        keys:
          Sample degenerate dim: k1
      selection:
        column: ["col1"]
        aliases: ["k1"]
        select: 1 AS k1
        from: test
        where: "true"
        options:
          p1: v1
          p2: v2
  expected:
    columns:
    - Sample fact
    - with1_k1
    aliases:
    - fact1
    - with1_k1
    with: with1 AS (SELECT 1 AS k1 FROM test WHERE true)
    select: v.fact1 AS fact1, with1.k1 AS with1_k1
    from: facts AS v INNER JOIN with1 AS with1 ON (v.deg1=with1.k1)
    options:
      p1: v1
      p2: v2

- label: With for dynamic dimension, getting field
  query:
    fields:
    - Sample fact
    - Dynamic dimension__col1
  expected:
    columns:
    - Sample fact
    - Dynamic dimension__col1
    aliases:
    - fact1
    - alias5_a1
    with: dim5 AS (SELECT x as a1, y as a2, sum(z) as a3 FROM base_table WHERE z > 0 GROUP BY x, y)
    select: v.fact1 AS fact1, alias5.a1 AS alias5_a1
    from: facts AS v INNER JOIN dim5 AS alias5 ON (v.deg4=alias5.a1) AND (v.deg5=alias5.a2)
    options:
      p3: v3
      p4: v4

- label: With for dynamic dimension, getting field with nested policy
  query:
    fields:
    - Sample fact
    - Dynamic dimension__col1
  policy:
    f:
      pNested:
      - pnested1
  expected:
    columns:
    - Sample fact
    - Dynamic dimension__col1
    aliases:
    - fact1
    - alias5_a1
    with: dim5 AS (SELECT x as a1, y as a2, sum(z) as a3
      FROM base_table
      WHERE (z > 0) AND ({base_table.n1=pNested})
      GROUP BY x, y)
    select: v.fact1 AS fact1, alias5.a1 AS alias5_a1
    from: facts AS v INNER JOIN dim5 AS alias5 ON (v.deg4=alias5.a1) AND (v.deg5=alias5.a2)
    options:
      p3: v3
      p4: v4

- label: With for dynamic dimension, getting field with must
  query:
    fields:
    - Sample fact
    - Must dimension__from1
  expected:
    columns:
    - Sample fact
    - Must dimension__from1
    aliases:
    - fact1
    - alias6_d0
    with: dim7 AS (SELECT x as a1, sum(z) as a2 FROM base_table WHERE z > 0 GROUP BY x)
    select: v.fact1 AS fact1, alias6.field / alias5.dim11 AS alias6_d0
    from: facts AS v INNER JOIN must_dim5 AS alias6 ON (v.deg4=alias6.m1)
      INNER JOIN dim7 AS alias7 ON (alias6.foreign6=alias7.unique6)

- label: With for two relations, deep nesting
  query:
    fields:
    - Sample fact
  with:
    - link:
        alias: with2
        keys:
          Sample degenerate dim: k1
          Flake 1__Field 1: k2
      selection:
        column: ["col1", "col2", "col3"]
        aliases: ["k1", "k2", "k3"]
        select: 1 AS k1, 2 AS k2, 3 AS k3
        from: test
        where: "true"
        options:
          p1: v1
          p2: v2
  intersect:
  - Sample fact
  - Sample dimension__Field 1
  - Flake 1__[Field__2]
  expected:
    columns:
    - Sample fact
    aliases:
    - fact1
    with: with2 AS (SELECT 1 AS k1, 2 AS k2, 3 AS k3 FROM test WHERE true)
    select: v.fact1 AS fact1
    from: facts AS v INNER JOIN dim3 AS alias3 ON (v.foreign4=alias3.unique4)
      INNER JOIN flake1 AS flake1 ON (alias3.foreign5=flake1.unique5) AND (alias3.foreign6=flake1.unique6)
      INNER JOIN dim1 AS alias1 ON (v.foreign1=alias1.unique1)
      INNER JOIN with2 AS with2 ON (flake1.flake11=with2.k2) AND (v.deg1=with2.k1)
    where: ({flake1.flake12=param1}) AND ({alias1.dim11=param1}) AND ({v.fact1=param1})
    options:
      p1: v1
      p2: v2

- label: With for two relations, deep nesting, getting field
  query:
    fields:
    - Sample fact
    - with2__k3
  with:
    - link:
        alias: with2
        keys:
          Sample degenerate dim: k1
          Flake 1__Field 1: k2
      selection:
        column: ["col1", "col2", "col3"]
        aliases: ["k1", "k2", "k3"]
        select: 1 AS k1, 2 AS k2, 3 AS k3
        from: test
        where: "true"
        options:
          p1: v1
          p2: v2
  intersect:
  - Sample fact
  - Sample dimension__Field 1
  - Flake 1__[Field__2]
  expected:
    columns:
    - Sample fact
    - with2_k3
    aliases:
    - fact1
    - with2_k3
    with: with2 AS (SELECT 1 AS k1, 2 AS k2, 3 AS k3 FROM test WHERE true)
    select: v.fact1 AS fact1, with2.k3 AS with2_k3
    from: facts AS v INNER JOIN dim3 AS alias3 ON (v.foreign4=alias3.unique4)
      INNER JOIN flake1 AS flake1 ON (alias3.foreign5=flake1.unique5) AND (alias3.foreign6=flake1.unique6)
      INNER JOIN dim1 AS alias1 ON (v.foreign1=alias1.unique1)
      INNER JOIN with2 AS with2 ON (flake1.flake11=with2.k2) AND (v.deg1=with2.k1)
    where: ({flake1.flake12=param1}) AND ({alias1.dim11=param1}) AND ({v.fact1=param1})
    options:
      p1: v1
      p2: v2
