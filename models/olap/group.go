package olap

import "fmt"

// Group enumerates supported grouping functions
type Group string

// Supported grouping functions
const (
	Nop           Group = ""
	Count         Group = "count"
	CountDistinct Group = "count_distinct"
	Sum           Group = "sum"
	Avg           Group = "avg"
	Max           Group = "max"
	Min           Group = "min"
)

// GroupDescriptor describes a grouping operation on a field
type GroupDescriptor struct {
	Group Group  `json:"group" yaml:"group"`
	Field string `json:"field" yaml:"field"`
}

// Of builds the SQL sentence to select a group of 'selector'
func (g Group) Of(selector string) string {
	switch g {
	case CountDistinct:
		return fmt.Sprintf("COUNT(DISTINCT %s)", selector)
	default:
		return fmt.Sprintf("%s(%s)+0E0", g, selector)
	}
}
