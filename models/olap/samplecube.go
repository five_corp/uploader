package olap

import (
	"log"
)

const sampleCube = `
Facts:
	table: facts
	dimensions:
		Sample degenerate dim:       deg1
		Degenerate__dim__with__dots: deg2
		Overlapping dimension:       deg3
	links:
		Sample dimension:
			alias: alias1
			keys:
				foreign1: unique1
		Dimension__with__dots:
			alias: alias2
			keys:
				foreign2: unique2
				foreign3: unique3
		Snowflake dimension:
			alias: alias3
			keys:
				foreign4: unique4
		Overlapping dimension:
			alias: alias4
			keys:
				deg3: deg3
		Dynamic dimension:
			alias: alias5
			keys:
				deg4: a1
				deg5: a2
		Must dimension:
			alias: alias6
			keys:
				deg4: m1
	properties:
		Sample fact:      fact1
		Fact__with__dots: fact2

Sample dimension:
	table: dim1
	dimensions:
		Field 1:  dim11
		Field__2: dim12
	filters:
		dim11: pFilter1

Dimension__with__dots:
	table: dim2
	dimensions:
		Field 1:  dim21
		Field__2: dim22

Snowflake dimension:
	table: dim3
	dimensions:
		Field 1:  dim31
		Field__2: dim32
	links:
		Flake 1:
			alias: flake1
			keys:
				foreign5: unique5
				foreign6: unique6

Flake 1:
	table: flake1
	dimensions:
		Field 1:  flake11
		Field__2: flake12
	filters:
		flake11: pFilter2
		flake12: pFilter3

Overlapping dimension:
	table: dim4
	dimensions:
		Year:  year
		Month: month
	filters:
		year: pFilter2

Dynamic dimension:
	table: dim5
	selection:
		columns: ["col1", "col2", "col3"]
		aliases: ["a1", "a2", "a3"]
		select: x as a1, y as a2, sum(z) as a3
		from: base_table
		where: z > 0
		group: x, y
		options:
			p3: v3
			p4: v4
	dimensions:
		nested1: base_table.n1
	filters:
		base_table.n1: pNested

Must dimension:
	table: must_dim5
	dimensions:
		from1: "%alias%.field / alias5.dim11"
	links:
		Required dimension:
			alias: alias7
			must: true
			keys:
				foreign6: unique6

Required dimension:
	table: dim7
	selection:
		columns: ["req1", "req2"]
		aliases: ["a1", "a2"]
		select: x as a1, sum(z) as a2
		from: base_table
		where: z > 0
		group: x
`

func testCube() Store {
	cube, err := newFrom(sampleCube)
	if err != nil {
		log.Fatal("Error building testCube: ", err)
		return nil
	}
	return cube
}
