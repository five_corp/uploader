package olap

import (
	"strings"
)

// Runes to process path strings
const (
	pathSeparator = "__"
	openQuote     = "["
	closeQuote    = "]"
)

// Split a path into steps separated by the "pathSeparator" string ('__').
// Each item in the path can optionally be enclosed in braces ('[', ']').
// The braces are needed if the steps contain a separator. They are removed
// from the resulting CubePath.
func split(path string) (prefix, value string) {
	parts := strings.Split(path, pathSeparator)
	start := 0
	final := [2]string{}
	index := 0
	for ; index < 2 && start < len(parts); index++ {
		if strings.HasPrefix(parts[start], openQuote) {
			stop, found := start, false
			for ; stop < len(parts) && !found; stop++ {
				if strings.HasSuffix(parts[stop], closeQuote) {
					found = true
				}
			}
			result := strings.Join(parts[start:stop], pathSeparator)
			if found {
				result = strings.TrimPrefix(strings.TrimSuffix(result, closeQuote), openQuote)
			}
			final[index] = result
			start = stop
		} else {
			final[index] = parts[start]
			start++
		}
	}
	switch index {
	case 1:
		return "", final[0]
	case 2:
		return final[0], final[1]
	default:
		return "", ""
	}
}

// Merge a prefix and attribute into a cubePath. Path elements are separated by the
// "pathSeparator" string ('__'), and each item in the path can optionally be enclosed
// in braces ('[', ']'). The braces are needed if the steps contain a separator.
func merge(prefix, value string) (path string) {
	items := make([]string, 0, 2)
	if p := escape(prefix); p != "" {
		items = append(items, p)
	}
	if p := escape(value); p != "" {
		items = append(items, p)
	}
	return strings.Join(items, pathSeparator)
}

// escape a path / attribute value. Values including an 'pathSeparator'
// are enclosed between 'openQuote' and 'closeQuote'
func escape(raw string) string {
	if raw == "" || !strings.Contains(raw, pathSeparator) {
		return raw
	}
	if strings.HasPrefix(raw, openQuote) && strings.HasSuffix(raw, closeQuote) {
		return raw
	}
	return strings.Join([]string{openQuote, raw, closeQuote}, "")
}
