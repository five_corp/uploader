package olap

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/five_corp/uploader/models/dbtool"
)

// Selection is the result of running a Request against a Slice
type Selection struct {
	Columns    []string          `json:"columns" yaml:"columns"` // Columns is the list of labels in the selection
	Aliases    []string          `json:"aliases" yaml:"aliases"` // Aliases is the list of SQL column names
	With       string            `json:"with" yaml:"with"`
	Select     string            `json:"select" yaml:"select"`
	From       string            `json:"from,omitempty" yaml:"from,omitempty"`
	Where      string            `json:"where,omitempty" yaml:"where,omitempty"`
	Group      string            `json:"group,omitempty" yaml:"group,omitempty"`
	Order      string            `json:"order,omitempty" yaml:"order,omitempty"`
	Limit      string            `json:"limit,omitempty" yaml:"limit,omitempty"`
	PivotList  []string          `json:"pivotList,omitempty" yaml:"pivotList,omitempty"`
	PivotGroup []GroupDescriptor `json:"pivotGroup,omitempty" yaml:"pivotGroup,omitempty"`
	Options    dbtool.Params     `json:"options,omitempty" yaml:"options,omitempty"`
}

// PivotKeys return the values of the fields to pivot by
func (s Selection) PivotKeys(ctx context.Context, dbStore dbtool.Store, rowLimit, batchSize int) ([]dbtool.Row, error) {
	if len(s.PivotList) == 0 {
		return nil, nil
	}
	pivotFields := strings.Join(s.PivotList, ",")
	st := Selection{
		With:    s.With,
		Select:  pivotFields,
		From:    s.From,
		Where:   s.Where,
		Group:   pivotFields,
		Order:   pivotFields,
		Options: s.Options,
	}.Statement()
	feed, err := dbStore.Run(ctx, st, nil, s.PivotList, rowLimit, batchSize)
	if err != nil {
		return nil, err
	}
	defer feed.Close()
	return feed.Collect(rowLimit)
}

// Pivot builds a statement pivoted with the given pivot keys
// For each combination of pivot key values and pivot groups,
// adds a new column to the query
func (s Selection) Pivot(pivotKeys []dbtool.Row) Selection {
	pivotSize := len(s.PivotList) * len(s.PivotGroup)
	if pivotSize <= 0 {
		return s
	}
	newOptions := s.Options.Clone()
	pivotCols := make([]string, 0, pivotSize+1)
	colNames := make([]string, 0, len(s.Columns)+pivotSize)
	aliases := make([]string, 0, len(s.Aliases)+pivotSize)
	pivotCols = append(pivotCols, s.Select)
	colNames = append(colNames, s.Columns...)
	aliases = append(aliases, s.Aliases...)
	// Add a "CASE WHEN" column for each value of the pivot keys
	for rowNumber, row := range pivotKeys {
		caseItems := make([]string, 0, len(s.PivotList))
		for index, field := range s.PivotList {
			paramName := fmt.Sprintf("x_%d_%d", rowNumber, index)
			caseItems = append(caseItems, fmt.Sprintf("{%s=%s}", field, paramName))
			newOptions[paramName] = row[index]
		}
		caseCond := strings.Join(caseItems, ") AND (")
		for groupNumber, group := range s.PivotGroup {
			caseLine := fmt.Sprintf("CASE WHEN (%s) THEN %s ELSE NULL END", caseCond, group.Field)
			colName := fmt.Sprintf("PIVOT_%d_%d", rowNumber, groupNumber)
			colNames = append(colNames, colName)
			aliases = append(aliases, colName)
			pivotCols = append(pivotCols, fmt.Sprintf("%s AS %s", group.Group.Of(caseLine), colName))
		}
	}
	return Selection{
		Columns: colNames,
		Aliases: s.Aliases,
		With:    s.With,
		Select:  strings.Join(pivotCols, ","),
		From:    s.From,
		Where:   s.Where,
		Group:   s.Group,
		Order:   s.Order,
		Limit:   s.Limit,
		Options: newOptions,
	}
}

// Statement build an SQL statement from the selection
func (s Selection) Statement() dbtool.Statement {
	query := make([]string, 0, 12)
	if s.With != "" {
		query = append(query, "WITH", s.With)
	}
	query = append(query, "SELECT", s.Select, "FROM", s.From)
	if s.Where != "" {
		query = append(query, "WHERE", s.Where)
	}
	if s.Group != "" {
		query = append(query, "GROUP BY", s.Group)
	}
	if s.Order != "" {
		query = append(query, "ORDER BY", s.Order)
	}
	return dbtool.Statement{
		SQL:      strings.Join(query, " "),
		Defaults: s.Options,
	}
}

// Builds a joinData from a selection
func (s *Selection) joinData(alias string) joinData {
	// Build one attrib per selection column
	cols := make(map[string]joinColumn)
	for _, colName := range s.Aliases {
		pair := []string{alias, colName}
		data := joinColumn{
			selector: strings.Join(pair, "."),
			alias:    strings.Join(pair, "_"),
		}
		data.label = data.alias
		cols[colName] = data
	}
	// Data associated to each table in a join
	return joinData{
		table:     alias,
		selection: *s,
		alias:     alias,
		attribs:   cols,
		options:   s.Options,
	}
}

// Equals compares two selections
func (s *Selection) equals(o Selection) bool {
	if s.With != o.With || s.Select != o.Select || s.From != o.From || s.Where != o.Where || s.Group != o.Group || s.Order != o.Order || s.Limit != o.Limit {
		return false
	}
	if len(s.Columns) != len(o.Columns) || len(s.Options) != len(o.Options) {
		return false
	}
	for idx, k := range s.Columns {
		if o.Columns[idx] != k {
			return false
		}
	}
	for k, v := range s.Options {
		if ov, ok := o.Options[k]; !ok || v != ov {
			return false
		}
	}
	return true
}
