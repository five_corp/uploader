package contaplus

import (
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"bitbucket.org/five_corp/saferest"
	"github.com/pkg/errors"
	"github.com/tadvi/dbf"
)

// DefaultPath is the default path for Contaplus files
const DefaultPath = "C:\\GrupoSP\\CONTABLD"

// Company data
type Company struct {
	Name  string   `json:"name"`
	Years []int    `json:"years"`
	Codes []string `json:"codes"`
}

// Companies gets the list of Contaplus companies and years
func Companies(path string) ([]Company, error) {
	// Split the path and merge again
	if path == "" {
		path = DefaultPath
	}
	drive := ""
	elems := saferest.PathElems(path)
	if strings.HasSuffix(elems[0], ":") {
		// It is a windows path, filepath.Join fails on it.
		// Remove the drive letter and merge it later.
		drive = elems[0]
		elems = elems[1:]
	}
	path = filepath.Join(elems...)
	if len(drive) > 0 {
		// using filepath.Join fails here
		path = strings.Join([]string{drive, path}, string(filepath.Separator))
	}
	// Check the given path exists
	if _, err := saferest.IsDir(path); err != nil {
		return nil, err
	}
	// Add the path to the contaplus directory file.
	// Once there is something after the drive letter and ":",
	// filepath.Join no longer fails in Windows.
	path = filepath.Join(path, "EMP", "Empresa.dbf")
	// Check the file exists
	_, err := os.Stat(path)
	if err != nil {
		return nil, errors.Wrapf(err, "Failed to stat path %s", path)
	}
	// Try to open the dbf file
	dbfFile, err := dbf.LoadFile(path)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to load DBF file %s", path)
	}
	// Search the column which has the company name
	nameRow := -1
	yearRow := -1
	codeRow := -1
	for index, field := range dbfFile.Fields() {
		if field.Name == "NOMBRE" {
			nameRow = index
		} else if field.Name == "EJERCICIO" {
			yearRow = index
		} else if field.Name == "COD" {
			codeRow = index
		}
	}
	if nameRow < 0 || yearRow < 0 || codeRow < 0 {
		err = errors.New("No columns 'Nombre', 'Ejercicio', 'Cod' in input")
		return nil, err
	}
	// Get all company names and years
	companies := make(map[string]*Company)
	iter := dbfFile.NewIterator()
	for iter.Next() {
		// Get the company name and year, as an integer
		row := iter.Row()
		company, yearStr, code := row[nameRow], row[yearRow], row[codeRow]
		year, err := strconv.Atoi(yearStr)
		if err != nil {
			return nil, errors.Wrapf(err, "Failed to convert str (%s) to int", yearStr)
		}
		// Create a Company Data object, if not existing
		data := companies[company]
		if data == nil {
			data = &Company{
				Name:  company,
				Years: make([]int, 0, 10),
				Codes: make([]string, 0, 10),
			}
			companies[company] = data
		}
		// Update list of years
		data.Years = append(data.Years, year)
		data.Codes = append(data.Codes, code)
	}
	// Turn the map into a list
	compList := make([]Company, 0, len(companies))
	for _, company := range companies {
		compList = append(compList, *company)
	}
	// And return the list of companies and years!
	return compList, nil
}
