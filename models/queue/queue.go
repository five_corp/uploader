package queue

type queue struct {
	size, maxSize     int
	buf               []interface{}
	head, tail, count int
}

// New returns a pair of buffered channels connected through an elastic buffer
// - if maxSize == 0, the buffer size is not bounded, and doubles on overflow
// - if maxSze > 0, the buffer is bounded and behaves as a circular queue
func New(size, maxSize int) (push chan<- interface{}, pop <-chan interface{}) {
	inp := make(chan interface{}, size)
	out := make(chan interface{}, size)
	q := queue{
		size:    size,
		maxSize: maxSize,
		buf:     make([]interface{}, size),
		head:    0,
		tail:    0,
		count:   0,
	}
	go q.dispatch(inp, out)
	return inp, out
}

func (q *queue) dispatch(inp <-chan interface{}, out chan<- interface{}) {
	defer close(out)
	for {
		if q.count == 0 {
			item, ok := <-inp
			if !ok {
				// channel closed
				return
			}
			q.push(item)
		}
		item := q.buf[q.head]
		select {
		case out <- item:
			// take the item out of the queue
			q.head = (q.head + 1) % q.size
			q.count--
		case newItem, ok := <-inp:
			if !ok {
				// channel closed
				q.flush(out)
				return
			}
			q.push(newItem)
		}
	}
}

func (q *queue) push(item interface{}) {
	// Grow the buffer if needed
	for q.count >= q.size {
		if q.maxSize > 0 && q.size >= q.maxSize {
			// Remove the oldest item in the pool, to make room
			q.head = (q.head + 1) % q.size
			q.count--
		} else {
			// Grow the pool
			newSize := 2 * q.size
			newBuf := make([]interface{}, newSize)
			copy(newBuf, q.buf[q.head:])
			copy(newBuf[(q.size-q.head):], q.buf[:q.head])
			q.buf = newBuf
			q.head = 0
			q.tail = q.size
			q.size = newSize
		}
	}
	// Add the item to the queue
	q.buf[q.tail] = item
	q.tail = (q.tail + 1) % q.size
	q.count++
}

func (q *queue) flush(out chan<- interface{}) {
	buf, size, head := q.buf, q.size, q.head
	for count := q.count; count > 0; count-- {
		item := buf[head]
		head = (head + 1) % size
		out <- item
	}
}
