package conector

import (
	"testing"
	"time"
)

type formatTest struct {
	strftimeFormat string
	golangFormat   string
	input          string
	output         time.Time
}

// TestTimeFormat tests the format function
func TestTimeFormat(t *testing.T) {
	iterations := []formatTest{
		{
			strftimeFormat: "%Y-%m-%d %H:%M:%S",
			golangFormat:   "2006-01-02 15:04:05",
			input:          "2018-12-30 18:28:00",
			output:         time.Date(2018, time.December, 30, 18, 28, 0, 0, time.UTC),
		},
	}
	for _, iter := range iterations {
		iter := iter // for the closure below
		t.Run(iter.strftimeFormat, func(t *testing.T) {
			format := timeFormat(iter.strftimeFormat)
			if format != iter.golangFormat {
				t.Fatalf("Traslating %s: expected %s, got %s", iter.strftimeFormat, iter.golangFormat, format)
			}
			result, err := time.Parse(format, iter.input)
			if err != nil {
				t.Fatalf("Parsing %s: error %+v", iter.input, err)
			}
			if result != iter.output {
				t.Fatalf("Parsing %s: expected %s, got %s", iter.input, iter.output, result)
			}
		})
	}
}
