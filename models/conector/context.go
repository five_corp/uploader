package conector

import (
	"context"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// MaxContexts is the maximum number of contexts per query,
// and results per context.
const MaxContexts = 16

// ContextQuery is a single query definition in the context model.
// How to behave if the context yields multiples rows:
// - "group": each context value is turned into an array.
// - "repeat": Repeat the queries for each row.
// - Anything else: use just the first row.
type ContextQuery struct {
	Query string   `json:"query" xml:"query" yaml:"query"`
	Types []string `json:"types" xml:"types" yaml:"types"`
	Rows  string   `json:"rows" xml:"rows" yaml:"rows"`
}

// Context represents a set of parameters for conectors.
// It is a set of one or more queries that return the
// values conectors use to limit what data to retrieve.
type Context struct {
	// Context can be a single Query
	ContextQuery
	// Or a series of Queries
	Queries []ContextQuery `json:"queries" xml:"queries" yaml:"queries"`
	// Default results for query
	Defaults dbtool.Params `json:"defaults" xml:"defaults" yaml:"defaults"`
}

func allDefaults(contexts []string, directory map[string]Context) (dbtool.Params, map[string]string) {
	result := make(dbtool.Params)
	types := make(map[string]string)
	if contexts == nil || len(contexts) <= 0 {
		return result, types
	}
	// Go backwards for defaults, params listed in the first contexts have precedence over the others
	for i := len(contexts) - 1; i >= 0; i-- {
		name := contexts[i]
		if c, ok := directory[name]; ok {
			for k, v := range c.Defaults {
				result[k] = v
				// This will be updated later by "collect"
				types[k] = "string"
			}
		}
	}
	return result, types
}

// Evaluates a list of contexts
func evalList(ctx context.Context, store dbtool.Store, contexts []string, directory map[string]Context) ([]Iteration, error) {
	defaults := make(dbtool.Params)
	types := make(map[string]string)
	if contexts == nil || len(contexts) <= 0 {
		return nil, nil
	}
	// Go backwards for defaults, params listed in the first contexts have precedence over the others
	for i := len(contexts) - 1; i >= 0; i-- {
		name := contexts[i]
		if c, ok := directory[name]; ok {
			for k, v := range c.Defaults {
				defaults[k] = v
				// This will be updated later by "collect"
				types[k] = "string"
			}
		}
	}
	return recurse(ctx, store, contexts, directory, defaults, types)
}

func recurse(ctx context.Context, store dbtool.Store, contexts []string, directory map[string]Context, params dbtool.Params, types map[string]string) ([]Iteration, error) {
	name := contexts[0]
	// Check for missing requirements and loops
	current, ok := directory[name]
	if !ok {
		return nil, errors.Errorf("Context %s not defined", name)
	}
	// Collect first results
	iters, err := current.collect(ctx, store, params, types)
	if err != nil || len(contexts) <= 1 {
		return iters, err
	}
	// If there are more results, recurse ourselves to resolve remaining contexts
	result := make([]Iteration, 0, len(iters))
	params = params.Clone()
	for _, i := range iters {
		localParams := params.Merge(i.Params())
		other, err := recurse(ctx, store, contexts[1:], directory, localParams, types)
		if err != nil {
			return nil, err
		}
		result = append(result, times(i, other)...)
	}
	return result, nil
}

// Collect the result of running all innerqueries in a context
func (c Context) collect(ctx context.Context, store dbtool.Store, params dbtool.Params, types map[string]string) ([]Iteration, error) {
	var queries = c.Queries
	if queries == nil || len(queries) == 0 {
		if c.Query != "" {
			queries = []ContextQuery{ContextQuery{
				Query: c.Query,
				Types: c.Types,
				Rows:  c.Rows,
			}}
		} else {
			return nil, nil
		}
	}
	result := make([]Iteration, 0, len(queries))
	for _, q := range queries {
		partial, err := q.collect(ctx, store, params, types)
		if err != nil {
			return nil, err
		}
		result = append(result, partial...)
	}
	return result, nil
}

// Collect the result of running a single InnerQuery
func (query ContextQuery) collect(ctx context.Context, store dbtool.Store, params dbtool.Params, types map[string]string) ([]Iteration, error) {
	stmt := dbtool.Statement{SQL: query.Query}
	feed, err := store.Run(ctx, stmt, params, query.Types, MaxContexts, MaxContexts)
	if err != nil {
		return nil, err
	}
	defer feed.Close()
	// Even if there are no results, update types
	// so that context can be managed properly
	for index, key := range feed.Names {
		types[key] = feed.Types[index]
	}
	rows, err := feed.Collect(MaxContexts)
	if err != nil {
		return nil, err
	}
	result := make([]Iteration, 0, len(rows)+1)
	shadow := false
	switch query.Rows {

	// When Rows == "group", we have to turn result values into lists.
	// Each list will have one member per result row.
	// The overall outcome of this context will be a single row.
	case "group":
		for index, key := range feed.Names {
			grouped := make([]interface{}, 0, len(rows)+1)
			if oldParam, ok := params[key]; ok {
				// TODO: if oldParam is itself grouped (it is a list), append item by item
				grouped = append(grouped, oldParam)
			}
			for _, row := range rows {
				grouped = append(grouped, row[index])
			}
			// Side effect: update params
			params[key] = grouped
		}
		result = append(result, newIteration(params, types))

	// When Rows == "repeat", generate one set of params per row.
	case "repeat":
		for _, row := range rows {
			cloned := make(dbtool.Params).Merge(params)
			for index, key := range feed.Names {
				cloned[key] = row[index]
			}
			result = append(result, newIteration(cloned, types))
			// no side effect
		}

	// when Rows == "shadow", it does not generate an iteration. Just used for side effects.
	case "shadow":
		shadow = true
		fallthrough

	default:
		if len(rows) > 0 {
			// Update the query params with the query result
			firstRow := rows[0]
			for index, key := range feed.Names {
				params[key] = firstRow[index]
			}
		}
		if !shadow {
			result = append(result, newIteration(params, types))
		}
	}
	return result, nil
}
