package conector

import (
	"log"
	"os"
	"path"
	"strings"
	"sync"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"github.com/pkg/errors"
)

// Manager for several conectors.
type Manager struct {
	folder    string
	conectors map[string]*Conector
	switcher  tenants.Switcher
	mutex     sync.Mutex
}

// NewManager builds a new conector manager
func NewManager(folder string, switcher tenants.Switcher) (*Manager, error) {
	return &Manager{
		folder:    folder,
		conectors: make(map[string]*Conector),
		switcher:  switcher,
		mutex:     sync.Mutex{},
	}, nil
}

// Reload all the loaded models.
func (manager *Manager) Reload() error {
	log.Print("models::ConnectorManager::Reload: reloading conectors")
	manager.mutex.Lock()
	manager.conectors = make(map[string]*Conector)
	manager.mutex.Unlock()
	return nil
}

// Find a particular conector
func (manager *Manager) Find(conector string) (*Conector, error) {
	manager.mutex.Lock()
	defer manager.mutex.Unlock()
	result, ok := manager.conectors[conector]
	if !ok {
		// This may take some time, but I prefer not to unlock the mutex,
		// so that if several requests arrive for the same collector,
		// I do not read the file concurrently.
		trail := make(map[string]bool)
		newResult, err := manager.importFile(conector, trail)
		if err != nil {
			return nil, err
		}
		result = newResult
		// Check the debugging folder exists, otherwise try to create it
		folder := path.Join(manager.folder, conector)
		if stat, err := os.Stat(folder); os.IsNotExist(err) {
			if err := os.Mkdir(folder, os.ModePerm); err != nil {
				log.Print("Failed to create debug folder ", folder, ": ", err)
				folder = ""
			}
		} else if err != nil {
			log.Print("Could not stat debug folder ", folder, ": ", err)
			folder = ""
		} else if !stat.IsDir() {
			log.Print("Debug folder ", folder, " is not a directory")
			folder = ""
		}
		result.Folder = folder
		// Subscribe to tenants so we can create the schemas needed by the conector
		switcher := manager.switcher
		switcher.Subscribe("conector-"+conector, tenants.Create|tenants.Bootstrap, func(mask tenants.EventType, issuer string, tenant tenants.Tenant) {
			result.createSchemas(switcher, issuer, conector)
		})
		log.Print("Conector ", conector, " subscribed to tenant manager")
		// Everything OK, save the conector
		manager.conectors[conector] = result
	}
	return result, nil
}

func (manager *Manager) importFile(fileName string, trail map[string]bool) (*Conector, error) {
	// Avoid loops
	if _, ok := trail[fileName]; ok {
		return nil, errors.Errorf("Loop detected loading conector file %s: %v", fileName, trail)
	}
	trail[fileName] = true
	log.Print("Importing conector file ", fileName)
	// Read the requested file
	parser := saferest.YAMLParser(fileName)
	result := &Conector{}
	if err := saferest.ParseFile(manager.folder, parser, result); err != nil {
		return nil, err
	}
	// Build types from names, if needed
	if result.Tables != nil {
		// First sweep: Expand field names into names + types.
		// A field can be a space-separated group of field name and field type
		for key, table := range result.Tables {
			if table.Types != nil && len(table.Types) > 0 {
				continue
			}
			types := make([]string, len(table.Fields))
			for index, field := range table.Fields {
				// If the field name has spaces, consider it
				// a group of name and type
				parts := strings.Fields(field)
				if len(parts) > 1 {
					table.Fields[index] = parts[0]
					types[index] = parts[1]
				}
			}
			table.Types = types
			// Update the array entry
			result.Tables[key] = table
		}
		// Second sweep: expand repetitions.
		// A table name can have a field "repeat: xxx", where xxx is the name of
		// another entry. If it does, we get as much parameters from the
		// other table as possible, except alias.
		for key, table := range result.Tables {
			if table.Repeat == "" {
				continue
			}
			if repeated, ok := result.Tables[table.Repeat]; ok {
				table.Merge(&repeated)
				result.Tables[key] = table
			}
		}
	}
	// If the file has imports, read recursively
	for _, importFile := range result.Imports {
		imported, err := manager.importFile(importFile, trail)
		if err != nil {
			return nil, err
		}
		// Merge imported conector to current one
		result.Merge(imported)
	}
	return result, nil
}
