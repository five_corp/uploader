package conector

import (
	"context"
	"encoding/json"
	"strings"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// an instance of the sqlEngine
type sqlFeeder struct {
	store  dbtool.Store
	Stmt   dbtool.Statement `json:"stmt"`
	DBName string           `json:"dbName"`
	Types  []string         `json:"types"`
}

// SQL Database-based engine
type sqlEngine struct {
	dbtool.Store
}

// NewSQLEngine builds a db connection based on the DBConfig type
func NewSQLEngine(ctx context.Context, config dbtool.Config) (Engine, error) {
	return sqlEngine{Store: dbtool.New(ctx, config)}, nil
}

// NewLocalEngine builds an SQL engine against the local DB
func NewLocalEngine(ctx context.Context, store dbtool.Store) (Engine, error) {
	return sqlEngine{Store: store}, nil
}

// Parse the query params, returns a dbName and statement
func (engine sqlEngine) Parse(name string, types []string, query json.RawMessage) (Feeder, error) {
	// Decode the conector parameters into a query string
	stmt := dbtool.Statement{}
	if err := json.Unmarshal(query, &stmt.SQL); err != nil {
		return nil, errors.Wrapf(err, "Failed to unmarshal query %s", query)
	}
	// If the query does not start with "USE ...", we are finished
	sql := strings.TrimSpace(strings.Replace(stmt.SQL, "\r\n", "\n", -1))
	if !strings.HasPrefix(strings.ToUpper(sql), "USE ") {
		return sqlFeeder{DBName: "", Stmt: stmt, Types: types, store: engine}, nil
	}
	// First statement is "USE something;"!
	// the actual statement is after the ";"
	// Caution: multi-line yaml values are turned into single
	// line, space separated strings. Must have the ";" in order
	// to be able to parse properly.
	lines := strings.SplitN(sql, ";", 2)
	if len(lines) < 2 {
		return nil, errors.Errorf("Wrong USE syntax: USE must be followed by ';' and query in %s", sql)
	}
	stmt.SQL = strings.TrimSpace(strings.TrimLeft(lines[1], ";"))
	// Remove the "USE " prefix
	parts := strings.SplitN(lines[0], " ", 2)
	if len(parts) < 2 {
		return nil, errors.Errorf("Wrong USE syntax: Must specify db name in %s", lines[0])
	}
	dbName := strings.TrimSpace(parts[1])
	if dbName == "" {
		return nil, errors.Errorf("Empty USE sentence: %s", lines[0])
	}
	return sqlFeeder{
		store:  engine,
		DBName: dbName,
		Stmt:   stmt,
		Types:  types,
	}, nil
}

// Iterate over the product's iterables, running the query each time,
// and merging all the feeds into one.
func (f sqlFeeder) Feed(ctx context.Context, iteration Iteration, maxRows, batchSize int) (dbtool.Feed, error) {
	// The product may come from the WAN over json.
	// if that happens, dates have been converted to ISO format.
	// Turn them back into dates.
	if err := iteration.Format(f.store); err != nil {
		return dbtool.Feed{}, err
	}
	params := iteration.Params()
	currentName, err := dbtool.Interpolate(f.DBName, params, f.store)
	if err != nil {
		return dbtool.Feed{}, err
	}
	f.Stmt.Defaults = params
	store := f.store.Switch(currentName)
	return store.Run(ctx, f.Stmt, nil, f.Types, maxRows, batchSize)
}
