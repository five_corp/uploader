package conector

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/five_corp/uploader/models/dbtool"
)

// Schema is an intermediate table to operate on conector's raw results.
type Schema struct {
	Fields  map[string]string `json:"fields" yaml:"fields" xml:"fields"`
	Indexes []string          `json:"indexes" yaml:"indexes" xml:"indexes"`
}

// SQL for dropping a schema
func (schema Schema) dropSQL(name string) string {
	return fmt.Sprintf("DROP TABLE IF EXISTS `%s`", name)
}

// SQL for creating a schema
func (schema Schema) createSQL(name string) string {
	// Make a list of fields
	fields := make([]string, 0, len(schema.Fields)+len(schema.Indexes))
	for name, desc := range schema.Fields {
		fields = append(fields, fmt.Sprintf("`%s` %s", name, desc))
	}
	// Add Indexes
	for _, name := range schema.Indexes {
		fields = append(fields, fmt.Sprintf("INDEX (%s)", name))
	}
	createBody := strings.Join(fields, ", ")
	return fmt.Sprintf("CREATE TABLE `%s` (%s)", name, createBody)
}

// Create the specified intermediate table using the schema
func (schema Schema) Create(name string, store dbtool.Store) error {
	ctx := context.Background()
	if _, err := store.Exec(ctx, dbtool.Statement{SQL: schema.dropSQL(name)}, nil); err != nil {
		return err
	}
	_, err := store.Exec(ctx, dbtool.Statement{SQL: schema.createSQL(name)}, nil)
	return err
}
