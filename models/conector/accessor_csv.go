package conector

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"

	"github.com/pkg/errors"
)

// CSVAccessor is a tabular accessor for CSV files
type csvAccessor struct {
	file      io.Closer
	reader    *csv.Reader
	line      []string
	err       error
	remaining int
	// Cache for string -> interface{} parsers
	formatCache map[string]func(string) interface{}
}

// newCSVAccessor creates a csv reader
func newCSVAccessor(fileName, sheetName string, config tabularPath) (accessor, error) {
	skip, remaining := config.FirstRow, -1
	if config.StopRow > config.FirstRow {
		remaining = config.StopRow - config.FirstRow
	}
	file, err := os.Open(fileName)
	if err != nil {
		return nil, errors.Wrapf(err, "CSV_Accessor: Import error opening file %s", fileName)
	}
	b := bufio.NewReader(file)
	for skip > 0 {
		_, err := b.ReadBytes('\n')
		if err != nil {
			if errors.Cause(err) == io.EOF {
				break
			}
			file.Close()
			return nil, err
		}
		skip--
	}
	reader := csv.NewReader(b)
	if config.Comment != "" {
		reader.Comment, _ = utf8.DecodeRuneInString(config.Comment)
	}
	if config.Comma != "" {
		reader.Comma, _ = utf8.DecodeRuneInString(config.Comma)
	}
	reader.ReuseRecord = true
	reader.TrimLeadingSpace = true
	reader.FieldsPerRecord = -1
	return &csvAccessor{
		remaining:   remaining,
		file:        file,
		reader:      reader,
		formatCache: make(map[string]func(string) interface{}),
	}, nil
}

// Err implements accessor
func (c *csvAccessor) Err() error {
	return c.err
}

// Next implements accessor
func (c *csvAccessor) Next() bool {
	if c.err != nil || c.remaining == 0 {
		if c.file != nil {
			c.file.Close()
			c.file = nil
		}
		return false
	}
	line, err := c.reader.Read()
	if err != nil {
		if c.file != nil {
			c.file.Close()
			c.file = nil
		}
		if errors.Cause(err) != io.EOF {
			c.err = err
		}
		c.remaining = 0
		return false
	}
	c.line = line
	if c.remaining > 0 {
		c.remaining--
	}
	return true
}

// Cell implements accessor
func (c *csvAccessor) Cell(col *tabularCol, curCol int) interface{} {
	if curCol >= len(c.line) {
		return nil
	}
	strVal := strings.TrimSpace(c.line[curCol])
	if strVal == "" {
		return nil
	}
	var val interface{}
	var err error
	val = strVal
	if col.Format != "" {
		convert, ok := c.formatCache[col.Format]
		if !ok {
			switch {
			case strings.Contains(col.Format, "%"):
				format := timeFormat(col.Format)
				convert = func(strVal string) interface{} {
					val, err = time.Parse(format, strVal)
					if err != nil {
						c.err = err
						return nil
					}
					return val
				}
			case col.Format == "." || col.Format == ",":
				sep, _ := utf8.DecodeRuneInString(col.Format)
				convert = func(strVal string) interface{} {
					result := make([]rune, 0, len(strVal))
					for _, r := range strVal {
						switch {
						case r == '+' || r == '-' || unicode.IsDigit(r):
							result = append(result, r)
						case r == sep:
							// SQL standard mandates that decimal separator is "."
							result = append(result, '.')
						}
					}
					return string(result)
				}
			default:
				c.err = errors.Errorf("Unrecognized format %s for column %s", col.Format, col.Name)
				return nil
			}
			c.formatCache[col.Format] = convert
		}
		val = convert(strVal)
		if c.err != nil {
			return nil
		}
	}
	return val
}

// Close implements accessor
func (c *csvAccessor) Close() {
	if c.file != nil {
		c.file.Close()
		c.file = nil
	}
	c.remaining = 0
}
