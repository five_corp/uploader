package conector

import (
	"context"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/tenants"
	"github.com/pkg/errors"
)

// Table struct encapsulates the definition of a conector
// capable of filling some schema table, including:
// - Name of the table where the result should be stored
// - Name of the context queries.
// - Alias list for context parameters
// - Fields of the database table to save the data to.
// - Types of the fields above
// - If the conector updates instead of inserting, name of the
//   fields to be updated. Other fields behave as keys to locate
//   the entry to update.
// - Persist: True if the table must not be cleared between conector runs
// - Engine name: "local", "mssql" (default), etc.
// - Static parameters for the engine.
// - Dependencies for tables: one table may require another.
type Table struct {
	Repeat   string            `json:"repeat" xml:"repeat" taml:"repeat"`
	Table    string            `json:"table" xml:"table" yaml:"table"`
	Context  []string          `json:"context" xml:"context" yaml:"context"`
	Alias    map[string]string `json:"alias" xml:"alias" yaml:"alias"`
	Fields   []string          `json:"fields" xml:"fields" yaml:"fields"`
	Types    []string          `json:"types" xml:"types" yaml:"types"`
	Updates  []string          `json:"updates" xml:"updates" yaml:"updates"`
	Persist  bool              `json:"persist" xml:"persist" yaml:"persist"`
	Engine   string            `json:"engine" xml:"engine" yaml:"engine"`
	Query    json.RawMessage   `json:"query" xml:"query" yaml:"query"`
	Requires []string          `json:"requires" xml:"requires" yaml:"requires"`
}

// Conector defines the structure of a descriptor file
type Conector struct {
	Folder  string             `json:"-" xml:"-" yaml:"-"`
	Context map[string]Context `json:"context" xml:"context" yaml:"context"`
	Schemas map[string]Schema  `json:"schemas" xml:"schemas" yaml:"schemas"`
	Tables  map[string]Table   `json:"tables" xml:"tables" yaml:"tables"`
	Imports []string           `json:"imports" xml:"imports" yaml:"imports"`
}

// Merge / update this table with another one
func (table *Table) Merge(other *Table) {
	if table.Table == "" {
		table.Table = other.Table
	}
	if table.Context == nil || len(table.Context) <= 0 {
		table.Context = other.Context
	}
	if table.Fields == nil || len(table.Fields) <= 0 {
		table.Fields = other.Fields
	}
	if table.Types == nil || len(table.Types) <= 0 {
		table.Types = other.Types
	}
	if table.Updates == nil || len(table.Updates) <= 0 {
		table.Updates = other.Updates
	}
	if table.Requires == nil || len(table.Requires) <= 0 {
		table.Requires = other.Requires
	}
	if table.Query == nil || len(table.Query) <= 0 {
		table.Query = other.Query
		// It doesn't make sense to copy the query, and not the engine.
		table.Engine = other.Engine
	}
}

// Merge contexts, schemas and tables from an imported conector
func (conector *Conector) Merge(imported *Conector) {
	if conector.Context == nil {
		conector.Context = imported.Context
	} else {
		for k, v := range imported.Context {
			conector.Context[k] = v
		}
	}
	if conector.Schemas == nil {
		conector.Schemas = imported.Schemas
	} else {
		for k, v := range imported.Schemas {
			conector.Schemas[k] = v
		}
	}
	if conector.Tables == nil {
		conector.Tables = imported.Tables
	} else {
		for k, v := range imported.Tables {
			conector.Tables[k] = v
		}
	}
}

// Packages available for the conector
func (conector *Conector) Packages() []string {
	keys := make([]string, 0, len(conector.Tables))
	for c := range conector.Tables {
		keys = append(keys, c)
	}
	return keys
}

// Eval a Table query, returns a conector product
func (conector *Conector) Eval(ctx context.Context, store dbtool.Store, name string) ([]Iteration, error) {
	table, ok := conector.Tables[name]
	if !ok {
		return nil, errors.Errorf("Connection %s cannot be found in conector", name)
	}
	// Check if the query requires a context
	contextList := table.Context
	if contextList == nil || len(contextList) <= 0 {
		// Connection without context means full scan.
		return nil, nil
	}
	// Run the context
	iterations, err := evalList(ctx, store, contextList, conector.Context)
	if err != nil {
		return nil, err
	}
	// Add the aliases
	if table.Alias != nil && len(table.Alias) > 0 && iterations != nil && len(iterations) > 0 {
		for idx, i := range iterations {
			iterations[idx] = i.Alias(table.Alias)
		}
	}
	return iterations, err
}

// Package builds a package for the given iterations
func (conector *Conector) Package(name string, iterations []Iteration) (Package, error) {
	table, ok := conector.Tables[name]
	if !ok {
		return Package{}, errors.Errorf("Connection %s cannot be found in conector", name)
	}
	return Package{
		Name:       name,
		Iterations: iterations,
		Engine:     table.Engine,
		Types:      table.Types,
		Query:      table.Query,
	}, nil
}

// Import a Feed into the local database
func (conector *Conector) Import(ctx context.Context, store dbtool.Store, name string, feed dbtool.Feed) error {
	table, ok := conector.Tables[name]
	if !ok {
		return errors.Errorf("Connection %s cannot be found in conector", name)
	}
	updates := table.Updates
	// If it is a full import and not an update, clear the table.
	// No need to clear the table: all the affected tables are cleared
	// before starting the whole import.
	/*
		if updates == nil || len(updates) <= 0 {
			if err := conector.clear(ctx, table); err != nil {
				return err
			}
		}*/
	// Replace feed names and types with values specified in the conector
	feed.Types = table.Types
	if table.Fields != nil && len(table.Fields) > 0 {
		feed.Names = table.Fields
	}
	// Hook on the feed and memorize the latest batch
	var latestBatch []dbtool.Row
	feed = dbtool.HookFeed(feed, func(batch []dbtool.Row) {
		if batch != nil && len(batch) > 0 {
			latestBatch = batch
		}
	})
	// Process the feed
	if err := store.Upsert(ctx, feed, table.Table, updates); err != nil {
		// If there is an error, save the latest feed batch to CSV, for debugging
		csvFile := table.Table + ".csv"
		if newErr := conector.saveBatch(csvFile, feed, latestBatch); newErr != nil {
			log.Print("Failed to save connector dump to ", csvFile, ": ", newErr)
		}
		// raise old error
		return err
	}
	return nil
}

// saveBatch saves a batch to a CSV file
func (conector *Conector) saveBatch(fileName string, feed dbtool.Feed, batch []dbtool.Row) error {
	if conector.Folder == "" {
		return errors.New("Save folder not specified")
	}
	if batch == nil || len(batch) <= 0 {
		return errors.New("Nothing to save in batch")
	}
	// Create CSV file
	csvPath := path.Join(conector.Folder, fileName)
	csvFile, err := os.Create(csvPath)
	if err != nil {
		return err
	}
	defer csvFile.Close()
	// Write the latest batch
	writer := csv.NewWriter(csvFile)
	writer.Write(feed.Names)
	writer.Write(feed.Types)
	line := make([]string, 0, len(feed.Names))
	for _, row := range batch {
		current := line[:0]
		for _, item := range row {
			current = append(current, fmt.Sprint(item))
		}
		writer.Write(current)
	}
	writer.Flush()
	return writer.Error()
}

// Clear the table used by the ConectorTable
func (conector *Conector) clear(ctx context.Context, store dbtool.Store, table Table) error {
	// Empty the local table
	stmt := dbtool.Statement{SQL: "DELETE FROM " + table.Table}
	if _, err := store.Exec(ctx, stmt, nil); err != nil {
		return err
	}
	return nil
}

// dependencies includes the dependencies for the given sources.
// They are returned in depth-first order
func (conector *Conector) dependencies(sourcesTree map[string]int, sources []string) error {
	for _, source := range sources {
		if _, ok := sourcesTree[source]; ok {
			continue
		}
		table, ok := conector.Tables[source]
		if !ok {
			return errors.Errorf("Source %s does not exist", source)
		}
		if table.Requires != nil && len(table.Requires) > 0 {
			if err := conector.dependencies(sourcesTree, table.Requires); err != nil {
				return err
			}
		}
		sourcesTree[source] = len(sourcesTree)
	}
	return nil
}

// Trigger the conector and run it for the given sources
func (conector *Conector) Trigger(ctx context.Context, store dbtool.Store, engines map[string]Engine, sources []string, maxRows, batchSize int) error {
	// Sort the sources according to weight.
	log.Print("Request to run conector for sources ", sources)
	sourcesTree := make(map[string]int)
	if err := conector.dependencies(sourcesTree, sources); err != nil {
		return err
	}
	// Enumerate the sources in depth-first order
	sortedSources := make([]string, len(sourcesTree))
	for k, v := range sourcesTree {
		sortedSources[v] = k
	}
	log.Print("Sorted out dependencies, sources to run: ", sortedSources)
	// Clear all the tables of these sources, just in case
	for _, name := range sortedSources {
		table, ok := conector.Tables[name]
		if !ok {
			return errors.Errorf("Connection %s cannot be found in conector", name)
		}
		if !table.Persist {
			conector.clear(ctx, store, table)
		}
	}
	// And process them
	for _, name := range sortedSources {
		log.Print("Running source ", name, " for conector ")
		iterations, err := conector.Eval(ctx, store, name)
		if err != nil {
			return err
		}
		product, _ := conector.Package(name, iterations)
		// Build a feed from the product
		feed, _, err := product.Feed(ctx, engines, maxRows, batchSize)
		if err != nil {
			return err
		}
		// Trust the early release semantics of feeds
		defer feed.Close()
		// Import the data
		if err := conector.Import(ctx, store, name, feed); err != nil {
			return err
		}
	}
	return nil
}

// createSchemas create schemas for the given issuer if tenants.Event == tenants.Create
func (conector *Conector) createSchemas(switcher tenants.Switcher, issuer, conectorName string) {
	store, ok := switcher.Switch(issuer)
	if !ok {
		log.Print("tenant ", issuer, " does not have resources, skipping schema creation for conector ", conector)
		return
	}
	for name, schema := range conector.Schemas {
		if err := schema.Create(name, store); err != nil {
			log.Print("Error creating schema ", schema, " of conector ", conectorName, " for issuer ", issuer, ": ", err)
		}
	}
}
