package conector

import (
	"context"
	"encoding/json"
	"io"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/tunnel"
	"github.com/pkg/errors"
)

type requestType int

const (
	rts  requestType = iota
	ping             = (1 << iota)
	parse
	feed
)

type request struct {
	ReqType requestType
	// In case type == parse
	Name  string
	Types []string
	Query json.RawMessage
	// In case type == feed
	Handle    int
	Iteration Iteration
	MaxRows   int
	BatchSize int
}

type parseReply struct {
	RepType requestType
	Error   string
	Handle  int
}

type pingReply struct {
	RepType requestType
	Error   string
}

type feedReply struct {
	RepType requestType
	Last    bool
	Names   []string
	Types   []string
	Rows    []dbtool.Row
	Error   string
}

type engineStub struct {
	pipe tunnel.Pipe
}

type instanceStub struct {
	engineStub
	handle int
}

// NewTunnelEngine creates a tunnel-based engine
func NewTunnelEngine(pipe tunnel.Pipe) (Engine, error) {
	return engineStub{pipe: pipe}, nil
}

// Parse implements Engine interface
func (s engineStub) Parse(name string, types []string, query json.RawMessage) (Feeder, error) {
	req, reply := request{
		ReqType: parse,
		Name:    name,
		Types:   types,
		Query:   query,
	}, parseReply{}
	if err := s.roundTrip(context.Background(), req, &reply); err != nil {
		return nil, err
	}
	if reply.Error != "" {
		return nil, errors.New(reply.Error)
	}
	return instanceStub{engineStub: s, handle: reply.Handle}, nil
}

// Ping implements Engine interface
func (s engineStub) Ping() error {
	req, reply := request{
		ReqType: ping,
	}, pingReply{}
	if err := s.roundTrip(context.Background(), req, &reply); err != nil {
		return err
	}
	if reply.Error != "" {
		return errors.New(reply.Error)
	}
	return nil
}

// Feed implements Feeder interface
func (s instanceStub) Feed(ctx context.Context, iteration Iteration, maxRows, batchSize int) (dbtool.Feed, error) {
	req := request{
		ReqType:   feed,
		Handle:    s.handle,
		Iteration: iteration,
		MaxRows:   maxRows,
		BatchSize: batchSize,
	}
	var reply *feedReply
	cursor := dbtool.CursorFunc(func() ([]dbtool.Row, error) {
		// On first iteration, do a full roundtrip
		if reply == nil {
			reply = &feedReply{}
			if err := s.roundTrip(ctx, req, reply); err != nil {
				return nil, err
			}
		} else if err := s.pipe.Recv(ctx, reply); err != nil {
			return nil, err
		}
		if reply.Error != "" {
			return nil, errors.New(reply.Error)
		}
		if reply.Last || len(reply.Rows) <= 0 {
			return nil, nil
		}
		return reply.Rows, nil
	})
	return dbtool.Feed{
		Names:  reply.Names,
		Types:  reply.Types,
		Cursor: cursor,
	}, nil
}

// ForwardToEngine forwards incoming requests through the tunnel to the engine
func ForwardToEngine(ctx context.Context, engine Engine, pipe tunnel.Pipe) error {
	var req request
	var feeder Feeder
	for {
		if err := pipe.Recv(ctx, &req); err != nil {
			if errors.Cause(err) == io.EOF {
				return nil
			}
			return err
		}
		textErr := ""
		switch req.ReqType {
		case parse:
			if f, parseErr := engine.Parse(req.Name, req.Types, req.Query); parseErr != nil {
				textErr = parseErr.Error()
			} else {
				feeder = f
			}
			if err := pipe.Send(ctx, parseReply{RepType: parse, Handle: 0, Error: textErr}); err != nil {
				return err
			}
		case ping:
			if pingErr := engine.Ping(); pingErr != nil {
				textErr = pingErr.Error()
			}
			if err := pipe.Send(ctx, pingReply{RepType: ping, Error: textErr}); err != nil {
				return err
			}
		case feed:
			if err := flushFeed(ctx, pipe, feeder, req); err != nil {
				return err
			}
		}
	}
}

func flushFeed(ctx context.Context, pipe tunnel.Pipe, feeder Feeder, req request) error {
	f, err := feeder.Feed(ctx, req.Iteration, req.MaxRows, req.BatchSize)
	if err != nil {
		return pipe.Send(ctx, feedReply{RepType: feed, Last: true, Error: err.Error()})
	}
	defer f.Close()
	names := f.Names
	types := f.Types
	for f.Next() {
		err := pipe.Send(ctx, feedReply{
			RepType: feed,
			Names:   names,
			Types:   types,
			Rows:    f.Batch(),
		})
		if err != nil {
			return err
		}
		names = nil
		types = nil
	}
	var lastErr string
	if err := f.Err(); err != nil {
		lastErr = err.Error()
	}
	return pipe.Send(ctx, feedReply{
		RepType: feed,
		Names:   names,
		Types:   types,
		Last:    true,
		Error:   lastErr,
	})
}

// roundTrip sends a request and expects a response. Pretty simple.
func (s engineStub) roundTrip(ctx context.Context, req, reply typer) error {
	err := s.pipe.Send(ctx, req)
	if err == nil {
		if err = s.pipe.Recv(ctx, &reply); err == nil {
			if req.Type() != reply.Type() {
				return errors.Errorf("Request type %d does not match reply type %d", req.Type(), reply.Type())
			}
			return nil
		}
	}
	return err
}

type typer interface {
	Type() requestType
}

func (r request) Type() requestType {
	return r.ReqType
}

func (r parseReply) Type() requestType {
	return r.RepType
}

func (r pingReply) Type() requestType {
	return r.RepType
}

func (r feedReply) Type() requestType {
	return r.RepType
}
