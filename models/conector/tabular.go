package conector

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
)

// MaxAllowedEmptyRows is the maximum number of allowed consecutive empty rows
const MaxAllowedEmptyRows = 50

// TabularCol is a descriptor for each tabular column
// - Name: name to assign to the column (descriptive).
//   If not provided, the column index is used.
// - Required: True if the column must have a value.
// - Skip: If > 0, ignore "skip" columns.
// - Drag: If true, 'nil' cells inherit the previous non-nil value
// - Format: for types like "date", a format to parse text:
//     For dates: strftime format, like "%Y-%m-%d %H:%M:%S"
//     For integers, with "." decimal separator: "."
//     For integers, with "," decimal separator: ","
// - Constant: if provided, the value for this column is used
//   as the name of the context var to use as cell value.
//   Constant rows do not count as excel rows (i.e. they do not
//   advance the internal column counter to the next column)
type tabularCol struct {
	Name     string `json:"name"`
	Required bool   `json:"req"`
	Skip     int    `json:"skip"`
	Format   string `json:"format"`
	Constant string `json:"const"`
	Drag     bool   `json:"drag"`
}

// TabularPath is the path to the data table inside a tabular file.
// - FileName: name of excel file, can include wildcards.
// - Sheet: sheet name. Can include %context vars%
// - Debug: ignore required cells, keep reading the row
// - FirstRow, FirstCol: where does the table start inside the sheet.
//   start counting from "1", as excel does.
// - StopRow: stop when reaching this row.
// - Comma: For CSV files, field delimiter (default: ",")
// - Comment: For CSV files, comment delimiter (default: "#")
// - Cols: descriptor for each col.
type tabularPath struct {
	Folder   string       `json:"folder"`
	FileName string       `json:"file"`
	Sheet    string       `json:"sheet"`
	FirstRow int          `json:"firstRow"`
	StopRow  int          `json:"stopRow"`
	FirstCol int          `json:"firstCol"`
	Debug    bool         `json:"debug"`
	Comma    string       `json:"comma"`
	Comment  string       `json:"comment"`
	Cols     []tabularCol `json:"cols"`
}

// Tabular file based engine
type tabularEngine struct {
	folder    string
	formatter dbtool.Formatter
}

type tabularInstance struct {
	tabularEngine
	Name      string      `json:"name"`
	Query     tabularPath `json:"query"`
	FeedNames []string    `json:"feedNames"`
	FeedTypes []string    `json:"feedTypes"`
}

// NewTabularEngine builds an excel engine
func NewTabularEngine(ctx context.Context, folder string, formatter dbtool.Formatter) (Engine, error) {
	currentEngine := tabularEngine{folder: folder, formatter: formatter}
	return currentEngine, nil
}

// Feed retrieved from the Engine
func (engine tabularEngine) Parse(name string, types []string, data json.RawMessage) (Feeder, error) {
	// Get the Conector definition from the product
	query := tabularPath{}
	if err := json.Unmarshal(data, &query); err != nil {
		return nil, err
	}
	// Initialize the feed
	names := make([]string, 0, len(query.Cols))
	for index, col := range query.Cols {
		if col.Skip < 0 {
			col.Skip = -col.Skip
		}
		if col.Skip > 0 {
			continue
		}
		name := col.Name
		if name == "" {
			name = fmt.Sprintf("col%d", index)
		}
		names = append(names, name)
	}
	for x := len(types); x < len(names); x++ {
		types = append(types, "string")
	}
	return tabularInstance{
		tabularEngine: engine,
		Name:          name,
		Query:         query,
		FeedNames:     names,
		FeedTypes:     types,
	}, nil
}

// Ping the conector to test everything is ok.
func (engine tabularEngine) Ping() error {
	if ok, err := saferest.IsDir(engine.folder); !ok || err != nil {
		if err != nil {
			return err
		}
		return errors.Errorf("Folder %s does not exist", engine.folder)
	}
	return nil
}

// Accessor reads a row from the tabular source
type accessor interface {
	// Next() advances to the next row to read, returns true if not EOF
	Next() bool
	// Cell reads a cell from the current row
	Cell(col *tabularCol, curCol int) interface{}
	// Err returns the latest error
	Err() error
	// Close the accessor
	Close()
}

// tabularCursor implements dbtool.Cursor
type tabularCursor struct {
	// input data
	config    tabularPath
	context   dbtool.Params
	batchSize int
	debug     bool
	// iterator status
	accessor  accessor
	drag      []interface{} // last non-null value in each column
	countdown int
	// iterator result
	curr []dbtool.Row
	err  error
}

// Batch implements dbtool.Cursor
func (c *tabularCursor) Batch() []dbtool.Row {
	return c.curr
}

// Err implements dbtool.Cursor
func (c *tabularCursor) Err() error {
	return c.err
}

// Next reads a tabular file according to the column definition
func (c *tabularCursor) Next() bool {
	if c.err != nil || c.countdown <= 0 {
		return false
	}
	rows := make([]dbtool.Row, 0, c.batchSize)
	emptyRows := 0
	for c.countdown > 0 && c.accessor.Next() {
		row := c.readRow()
		if row == nil {
			if emptyRows++; emptyRows < MaxAllowedEmptyRows {
				continue
			}
			log.Print("Tabular: Too many consecutive skipped rows, leaving.")
			c.countdown = 0
			break
		}
		// Check if the batch is full
		rows = append(rows, row)
		c.countdown--
		if len(rows) >= cap(rows) {
			break
		}
	}
	if err := c.accessor.Err(); err != nil {
		c.err = err
		return false
	}
	c.curr = rows
	return len(rows) > 0
}

// Close implements dbtool.Cursor
func (c *tabularCursor) Close() {
	c.accessor.Close()
	c.accessor = nil // let the GC proceed
	c.countdown = 0
}

// read a single row
func (c *tabularCursor) readRow() dbtool.Row {
	curCol := c.config.FirstCol - 1
	row := make([]interface{}, 0, len(c.config.Cols))
	for index, col := range c.config.Cols {
		// If skipping, increment curCol and continue
		if col.Skip > 0 {
			curCol += col.Skip
			continue
		}
		var val interface{}
		// If constant value, just use context
		if col.Constant != "" {
			if c, ok := c.context[col.Constant]; ok {
				val = c
			}
		} else {
			val = c.accessor.Cell(&col, curCol)
			// If dragging, check previous values
			if val == nil {
				if col.Drag {
					val = c.drag[index]
				}
			} else if col.Drag {
				c.drag[index] = val
			}
			curCol++
			// If value is empty and cell is required, fail
			if val == nil && col.Required {
				if !c.debug {
					return nil
				}
				val = "<nil> -> skip!"
			}
			// Append the row value
			row = append(row, val)
		}
	}
	return row
}

// newTabularCursor creates a cursor (excel, csv) depending on the file format
func newTabularCursor(fileName, sheetName string, config tabularPath, context dbtool.Params, maxRows, batchSize int) (dbtool.Cursor, error) {
	var acc accessor
	var err error
	ext := strings.ToLower(filepath.Ext(fileName))
	switch {
	case ext == ".xlsx" || ext == ".xls":
		acc, err = newExcelAccessor(fileName, sheetName, config)
	case ext == ".csv" || ext == ".tsv":
		acc, err = newCSVAccessor(fileName, sheetName, config)
	default:
		err = errors.Errorf("Tabular: Unknown extension %s in fileName %s", ext, fileName)
	}
	if err != nil {
		return nil, err
	}
	return &tabularCursor{
		config:    config,
		context:   context,
		accessor:  acc,
		debug:     config.Debug,
		countdown: maxRows,
		batchSize: batchSize,
		drag:      make([]interface{}, len(config.Cols)),
	}, nil
}

// Run a single engine iteration
func (f tabularInstance) Feed(ctx context.Context, iteration Iteration, maxRows, batchSize int) (dbtool.Feed, error) {
	// Interpolate file and sheet names
	params := iteration.Params()
	path, err := dbtool.Interpolate(f.Query.FileName, params, f.formatter)
	if err != nil {
		return dbtool.Feed{}, err
	}
	sheet, err := dbtool.Interpolate(f.Query.Sheet, params, f.formatter)
	if err != nil {
		return dbtool.Feed{}, err
	}
	// And iterate on all matching files
	folder := f.folder
	if f.Query.Folder != "" {
		folder = filepath.Join(folder, f.Query.Folder)
	}
	files, err := filepath.Glob(filepath.Join(folder, path))
	if err != nil {
		return dbtool.Feed{}, err
	}
	if len(files) <= 0 {
		err := errors.Errorf("Tabular: No files matched %s in %s", path, f.folder)
		return dbtool.Feed{}, err
	}
	// Chain cursors
	index := 0
	return dbtool.Chain(func() (dbtool.Feed, error) {
		if index >= len(files) {
			return dbtool.Feed{}, nil
		}
		cursor, err := newTabularCursor(files[index], sheet, f.Query, params, maxRows, batchSize)
		if err != nil {
			return dbtool.Feed{}, err
		}
		index++
		return dbtool.Feed{
			Names:  f.FeedNames,
			Types:  f.FeedTypes,
			Cursor: cursor,
		}, nil
	})
}
