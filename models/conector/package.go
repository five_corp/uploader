package conector

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// Iteration context for the engines
type Iteration struct {
	Names []string   `json:"names"`
	Types []string   `json:"types"`
	Value dbtool.Row `json:"value"`
}

// Package is the set of instructions required to run a query against a data source.
// It can be serialized and sent over a remote connection.
type Package struct {
	// Name of the conector table that yielded this product.
	Name string `json:"name"`
	// Iterations to run the conector
	Iterations []Iteration `json:"iteration"`
	// Engine parameters.
	Engine string          `json:"engine"`
	Types  []string        `json:"types"`
	Query  json.RawMessage `json:"query"`
}

// Turn a set of params into an iteration object
func newIteration(params dbtool.Params, types map[string]string) Iteration {
	// Serialize context data into arrays
	serialNames := make([]string, 0, len(params))
	serialTypes := make([]string, 0, len(params))
	serialRow := make(dbtool.Row, 0, len(params))
	for key, val := range params {
		serialNames = append(serialNames, key)
		serialTypes = append(serialTypes, types[key])
		serialRow = append(serialRow, val)
	}
	return Iteration{
		Names: serialNames,
		Types: serialTypes,
		Value: serialRow,
	}
}

// times returns the cartesian product of two iteration sets
func times(base Iteration, added []Iteration) []Iteration {
	if added == nil || len(added) == 0 {
		return []Iteration{base}
	}
	result := make([]Iteration, len(added))
	for _, i := range added {
		result = append(result, Iteration{
			Names: append(base.Names[:], i.Names...),
			Types: append(base.Types[:], i.Types...),
			Value: append(base.Value[:], i.Value...),
		})
	}
	return result
}

// Params built from the contents of the iteration
func (i *Iteration) Params() dbtool.Params {
	if i.Names == nil {
		return nil
	}
	defaults := make(dbtool.Params, len(i.Names))
	for index, name := range i.Names {
		defaults[name] = i.Value[index]
	}
	return defaults
}

// Format date fields from RFC3339 strings to the format understood by the formatter
func (i *Iteration) Format(f dbtool.Formatter) error {
	if i.Names == nil {
		return nil
	}
	for index, val := range i.Value {
		if i.Types[index] == "date" {
			if date, ok := val.(string); ok {
				properDate, err := time.Parse(time.RFC3339, date)
				if err != nil {
					return errors.Wrapf(err, "Field %s should be a date, but is not", i.Names[index])
				}
				i.Value[index] = properDate
			}
		}
	}
	i.Value.Format(f)
	return nil
}

// Alias adds a set of aliases to the current Names / Types / Value
func (i *Iteration) Alias(alias map[string]string) Iteration {
	result := *i
	for k, v := range alias {
		for idx, name := range i.Names {
			if name == v {
				result.Names = append(result.Names, k)
				result.Types = append(result.Types, result.Types[idx])
				result.Value = append(result.Value, result.Value[idx])
				break
			}
		}
	}
	return result
}

// Feed from the engine, for the current conector.
func (product Package) Feed(ctx context.Context, engines map[string]Engine, maxRows, batchSize int) (dbtool.Feed, []string, error) {
	if product.Iterations != nil && len(product.Iterations) <= 0 {
		// No iterations, skip
		log.Print("Skipping product ", product.Name, " because of empty context")
		return dbtool.NewFeed([]string{}, product.Types, nil), product.Types, nil
	}
	// Get the proper engine for this product
	engine, ok := engines[product.Engine]
	if !ok {
		engine, ok = engines[Default]
		if product.Engine != "" || !ok {
			return dbtool.Feed{}, nil, errors.Errorf("Engine ID '%s' for product '%s' not found", product.Engine, product.Name)
		}
	}
	// Build a feed from the product
	instance, err := engine.Parse(product.Name, product.Types, product.Query)
	if err != nil {
		return dbtool.Feed{}, nil, err
	}
	// If the iterations array is nil, iterate just once.
	current, iterations := Iteration{}, product.Iterations
	if iterations == nil {
		iterations = []Iteration{current}
	}
	feed, err := dbtool.Chain(func() (dbtool.Feed, error) {
		// Break the loop once we are over the number of iterations
		if len(iterations) <= 0 {
			return dbtool.Feed{}, nil
		}
		current, iterations = iterations[0], iterations[1:]
		log.Print("Running iteration with data ", current)
		return instance.Feed(ctx, current, maxRows, batchSize)
	})
	return feed, product.Types, err
}
