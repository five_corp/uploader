package conector

import (
	"log"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/tealeg/xlsx"
)

// ExcelAccessor is a tabular accessor for excel files
type excelAccessor struct {
	file       *xlsx.File
	sheet      *xlsx.Sheet
	currentRow int
	lastRow    int
	err        error
}

// newExcelAccessor creates an excel reader
func newExcelAccessor(fileName, sheetName string, config tabularPath) (accessor, error) {
	file, err := xlsx.OpenFile(fileName)
	if err != nil {
		return nil, errors.Wrapf(err, "Excel_Accessor: Import error opening file %s", fileName)
	}
	sheet, ok := file.Sheet[sheetName]
	if !ok {
		log.Print("Excel_Accessor: Import error opening sheet ", sheetName, ": ", err)
		return nil, errors.Errorf("Sheet %s not found in file %s", sheetName, fileName)
	}
	lastRow := sheet.MaxRow
	if config.StopRow > config.FirstRow && config.StopRow <= lastRow {
		lastRow = config.StopRow - 1
	}
	return &excelAccessor{
		file:       file,
		sheet:      sheet,
		currentRow: config.FirstRow - 1,
		lastRow:    lastRow,
	}, nil
}

// Err implements accessor
func (c *excelAccessor) Err() error {
	return c.err
}

// Next implements accessor
func (c *excelAccessor) Next() bool {
	if c.err != nil || c.currentRow > c.lastRow {
		return false
	}
	c.currentRow++
	return true
}

// Cell implements accessor
func (c *excelAccessor) Cell(col *tabularCol, curCol int) interface{} {
	var val interface{}
	cell := c.sheet.Cell(c.currentRow, curCol)
	switch cell.Type() {
	case xlsx.CellTypeNumeric:
		val = tryType(c.file, cell, numGetter)
		// Try to check if it is a date
		if strings.ContainsAny(cell.GetNumberFormat(), "yY") {
			// This is actually a date in numeric type
			if num, ok := val.(float64); ok {
				// Just in case the floating point number is
				// a bit below the integer... rounding is a b*tch
				days := int(num + 0.01)
				// Excel stores dates as days from 30th December 1899
				// See https://github.com/tealeg/xlsx/issues/24
				epoch := time.Date(1899, 12, 30, 0, 0, 0, 0, time.Local)
				val = epoch.AddDate(0, 0, days)
			}
		}
	case xlsx.CellTypeDate:
		val = tryType(c.file, cell, dateGetter)
	case xlsx.CellTypeBool:
		val = cell.Bool()
	default:
		val = cell.Value
	}
	// Check if cell was empty
	if val == nil {
		return nil
	}
	if strVal, ok := val.(string); ok {
		strVal = strings.TrimSpace(strVal)
		if strVal == "" {
			return nil
		}
		val = strVal
	}
	return val
}

// Close implements accessor
func (c *excelAccessor) Close() {}

type getter func(file *xlsx.File, cell *xlsx.Cell) (interface{}, error)

func dateGetter(file *xlsx.File, cell *xlsx.Cell) (interface{}, error) {
	result, err := cell.GetTime(file.Date1904)
	return result, err
}

func numGetter(file *xlsx.File, cell *xlsx.Cell) (interface{}, error) {
	result, err := cell.Float()
	return result, err
}

func tryType(file *xlsx.File, cell *xlsx.Cell, g getter) interface{} {
	if option1, err := g(file, cell); err == nil {
		return option1
	}
	if option2, err := cell.FormattedValue(); err == nil {
		return option2
	}
	return cell.Value
}
