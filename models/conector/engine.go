package conector

import (
	"context"
	"encoding/json"

	"bitbucket.org/five_corp/uploader/models/dbtool"
)

// Feeder returns a dbtool.Feed for the given iteration
type Feeder interface {
	Feed(ctx context.Context, iteration Iteration, maxRows, batchSize int) (dbtool.Feed, error)
}

// Engine that retrieves a feed of rows from a Product.
type Engine interface {
	Parse(name string, types []string, query json.RawMessage) (Feeder, error)
	Ping() error
}

const (
	// Local is the Engine ID for queries against the local DB
	Local string = "local"
	// SQL is the Engine ID for queries against remote DBs
	SQL = "sql"
	// Tabular is the Engine ID for queries against excel / CSV files
	Tabular = "tabular"
	// Excel is an alias for Tabular, for backward compatibility
	Excel = "excel.yaml" // as opposed to regular excel conector
	// Default is the default option when the Engine name is not provided
	Default = SQL
)
