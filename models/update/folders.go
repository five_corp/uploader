package update

import (
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

// FolderDescriptor Describes a folder required during the install / update process
type FolderDescriptor struct {
	// True if the folder must already exist
	Required bool
	// True if it is OK to Create the folder
	Create bool
	// Elements of the path to the folder
	Path []string
}

// FullPath walks the list of folders and returns the full psth of each one
func FullPath(installDir string, folders map[string]FolderDescriptor) (map[string]string, error) {
	paths := make(map[string]string)
	// All the previous paths are absolute, prepend the finalDir
	for key, descriptor := range folders {
		// Save the full path
		fullPath := folderFullPath(installDir, descriptor.Path)
		paths[key] = fullPath
		// Validate the folders
		if err := descriptor.validate(fullPath); err != nil {
			return nil, err
		}
	}
	return paths, nil
}

// Returns a full path rooted at the given topDir
func folderFullPath(topDir string, parts []string) string {
	return filepath.Join(append([]string{topDir}, parts...)...)
}

// Validates the given fullPath matches the folder requirements
func (fd *FolderDescriptor) validate(fullPath string) error {
	if fd.Required {
		if _, err := os.Stat(fullPath); err != nil {
			return errors.Wrapf(err, "Failed to stat file %s", fullPath)
		}
	}
	if fd.Create {
		if _, err := os.Stat(fullPath); err != nil {
			if !os.IsNotExist(errors.Cause(err)) {
				return errors.Wrapf(err, "Failed to stat file %s", fullPath)
			}
		}
		return errors.Wrapf(os.MkdirAll(fullPath, 0755), "Failed to create folder %s", fullPath)
	}
	return nil
}
