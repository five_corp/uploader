// Package update manages zipped updates of folders
package update

import (
	"archive/zip"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"

	"bitbucket.org/five_corp/saferest"
	"github.com/pkg/errors"
)

// Suffixes for temporary directories
const (
	newSuffix      = ".new"
	obsoleteSuffix = ".old"
)

// UnzipMap maps inner zip folders to outer filesystem paths
type UnzipMap = map[string]string

// Extract zip file according to the provided folder map.
// Inner paths in the zipped file are matched against the
// folderMap and extracted to the corresponding path.
// E.g. if the zipped file has two folders inside, "config" and "service",
// and folderMap has: {
//   "config":  "C:\\MyApp\\config",
//   "service": "C:\\MyApp\\bin\\service",
// }
// Then all zipped files under "config" will be extracted to
// C:\MyApp\Config, and all zipped files under "service" will
// be extracted to C:\MyApp\bin\service, recursively.
func Extract(fromZip string, folderMap UnzipMap) error {
	for _, fsPath := range folderMap {
		// Create new, empty, fsPath.new directories
		newPath := fsPath + newSuffix
		if err := saferest.EnsureDir(newPath, true); err != nil {
			return err
		}
	}
	// Open zip reader
	r, err := zip.OpenReader(fromZip)
	if err != nil {
		return errors.Wrapf(err, "Failed to open zip file %s", fromZip)
	}
	defer r.Close()
	// Iterate over zipped files
	matchZipped := ""
	matchOuter := ""
	for _, f := range r.File {
		dir, fileName := filepath.Split(f.Name)
		if fileName == "" {
			continue
		}
		// Search for the fs Path where the current file must exist.
		if !strings.EqualFold(dir, matchZipped) {
			matchZipped = dir
			matchOuter = ""
			for zipPath, fsPath := range folderMap {
				// Replace zip prefix with file system prefix.
				newPath := fsPath + newSuffix
				if strings.HasPrefix(dir, zipPath) {
					matchOuter = strings.Replace(dir, zipPath, newPath, 1)
					// Test if the destination folder exists, otherwise try to build it
					if err := saferest.EnsureDir(matchOuter, false); err != nil {
						return err
					}
					break
				}
			}
			if matchOuter == "" {
				log.Print("Update: Zipped folder ", dir, " does not match any update folder in ", folderMap)
			}
		}
		// If no match, this path does not need to be unzipped.
		if matchOuter == "" {
			continue
		}
		// Otherwise, write the file
		if err := copyCompressed(matchOuter, fileName, f); err != nil {
			return err
		}
	}
	for _, fsPath := range folderMap {
		// If nothing to replace, return
		newPath := fsPath + newSuffix
		isNewDir, err := saferest.IsDir(newPath)
		if err != nil {
			return err
		}
		if !isNewDir {
			return errors.Errorf("Did not unpack anything in folder %s", newPath)
		}
		// Remove old path, if exists
		obsoletePath := fsPath + obsoleteSuffix
		if err := os.RemoveAll(obsoletePath); err != nil {
			if !os.IsNotExist(errors.Cause(err)) {
				return errors.Wrapf(err, "Failed to remove file %s", obsoletePath)
			}
		}
	}
	for _, fsPath := range folderMap {
		// Once we are sure we have replacements for all folders,
		// and obsolete folders are removed: start replacement.
		isDir, err := saferest.IsDir(fsPath)
		if err != nil {
			if !os.IsNotExist(errors.Cause(err)) {
				rollback(folderMap)
				return err
			}
		}
		// Rename current folder to obsolete, if it exists
		obsoletePath := fsPath + obsoleteSuffix
		if isDir {
			if err := os.Rename(fsPath, obsoletePath); err != nil {
				rollback(folderMap)
				return errors.Wrapf(err, "Failed to rename %s to %s", obsoletePath, fsPath)
			}
		}
		// Rename folder.new to folder
		newPath := fsPath + newSuffix
		if err := os.Rename(newPath, fsPath); err != nil {
			rollback(folderMap)
			return errors.Wrapf(err, "Failed to rename %s to %s", fsPath, newPath)
		}
	}
	return nil
}

func rollback(folderMap UnzipMap) {
	for _, fsPath := range folderMap {
		oldPath := fsPath + obsoleteSuffix
		isDir, err := saferest.IsDir(oldPath)
		if err != nil {
			log.Print("Ignoring errored folder ", oldPath, " during rollback, ", err)
			continue
		}
		if !isDir {
			continue
		}
		if err := os.RemoveAll(fsPath); err != nil {
			if !os.IsNotExist(errors.Cause(err)) {
				log.Print("Could not remove scratch path ", err)
				continue
			}
		}
		if err := os.Rename(oldPath, fsPath); err != nil {
			log.Print("Could not restore ", oldPath, " to ", fsPath, ": ", err)
		}
	}
}

func copyCompressed(folder, fileName string, input *zip.File) error {
	// Remove destination file if it exists
	dstPath := filepath.Join(folder, fileName)
	// The folders are new, no need to cleanse the files
	/*if _, err := os.Stat(dstPath); err != nil {
		if !saferest.IsNotExist(err) {
			return err
		}
	} else {
		if err := os.Remove(dstPath); err != nil {
			return err
		}
	}*/
	// Open zip file
	rc, err := input.Open()
	if err != nil {
		return errors.Wrap(err, "Failed to open zip file")
	}
	defer rc.Close()
	// Create destination file
	dstFile, err := os.Create(dstPath)
	if err != nil {
		return errors.Wrapf(err, "Failed to create file %s", dstPath)
	}
	defer dstFile.Close()
	// Perform the copy
	copied, err := io.Copy(dstFile, rc)
	if err != nil {
		return errors.Wrapf(err, "Failed to copy file %s", dstFile)
	}
	if uint64(copied) < input.UncompressedSize64 {
		return errors.Errorf("File %s error: size uncompressed=%v, size on disk=%v", input.Name, input.UncompressedSize, copied)
	}
	return nil
}
