package update

import (
	"bytes"
	"encoding/json"
	"text/template"
	"log"
	"path/filepath"
	"reflect"
	"runtime"

	"bitbucket.org/five_corp/saferest"
	"github.com/pkg/errors"
)

// parameters is a Shortcut for a map from string to interface{}
type parameters map[string]interface{}

// Templates is a map fronm section filename to final config file path
type Templates map[string]string

// TemplateData provided to each template for rendering
type TemplateData struct {
	Section   string
	Version   int
	Windows   bool
	Separator string
	Folders   map[string]string
	Config    parameters
}

// NewTemplates returns a map "section name" => "template destination full path"
func NewTemplates(installDir string, dstPath []string, descriptors []saferest.ConfigDescriptor) Templates {
	// Read the template
	result := make(map[string]FolderDescriptor)
	for _, d := range descriptors {
		result[d.Section] = FolderDescriptor{Required: false, Create: false, Path: append(dstPath[:], d.Parser.FileName())}
	}
	paths, _ := FullPath(installDir, result)
	return Templates(paths)
}

// Add more templates
func (tt Templates) Add(installDir string, paths map[string]FolderDescriptor) error {
	fp, err := FullPath(installDir, paths)
	if err != nil {
		return err
	}
	for k, v := range fp {
		tt[k] = v
	}
	return nil
}

// Execute a template read from srcPath, save result to dstPath
func (tt Templates) Execute(srcDir string, version int, manager saferest.Sectioner, folders map[string]string) error {
	// Now the configs are final, run the templates
	templateData := &TemplateData{
		Version:   version,
		Folders:   folders,
		Windows:   false,
		Separator: string(filepath.Separator),
	}
	if runtime.GOOS == "windows" {
		templateData.Windows = true
	}
	for section, dstPath := range tt {
		log.Print("Actualizando plantilla ", section)
		templateData.Section = section
		templateData.Config = nil
		if config, ok := manager.Has(section); ok {
			templateData.Config = toParameters(config)
		}
		srcPath := filepath.Join(srcDir, section+".tmpl")
		if err := execute(dstPath, srcPath, templateData); err != nil {
			return err
		}
	}
	return nil
}

func execute(dstPath, srcPath string, tmplData interface{}) error {
	// Create empty template with helper functions "toSlash" and "toJSON",
	// to replace backward slashes "\\" with forward slashes "/"
	basePath := filepath.Base(srcPath)
	tmpl := template.New(basePath).Funcs(template.FuncMap{
		"toSlash": filepath.ToSlash,
		"toJSON":  toJSON,
	})
	// Read the template body from the file
	tmpl, err := tmpl.ParseFiles(srcPath)
	if err != nil {
		log.Printf("Skipping %s, template not found or invalid, ", srcPath)
		return nil
	}
	// Run the template into a buffer
	buffer := bytes.Buffer{}
	if err := tmpl.Execute(&buffer, tmplData); err != nil {
		return errors.Wrapf(err, "Error executing template from %s", srcPath)
	}
	// Write the resulting buffer to a file
	if err := saferest.OverwriteFile(dstPath, &buffer, nil); err != nil {
		return err
	}
	return nil
}

func toJSON(data interface{}) string {
	b, err := json.Marshal(data)
	if err != nil {
		return "ERROR: " + err.Error()
	}
	return string(b)
}

func toParameters(config saferest.Config) parameters {
	params := make(parameters)
	configItem := reflect.ValueOf(config)
	configType := configItem.Type()
	// Me quedo con los campos que tienen un tag json != "-"
	for i := 0; i < configItem.NumField(); i++ {
		name := configType.Field(i).Name
		params[name] = configItem.Field(i).Interface()
	}
	return params
}
