package policy

import (
	"reflect"
	"sort"
	"strings"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// Policy specifies a set of restrictions that must be
// fulfilled by a request to be allowed
type Policy struct {
	// Scope (set of queries) allowed
	Scope []string `json:"s,omitempty" yaml:"s,omitempty" xml:"s,omitempty"`
	// Filters (query parameters) allowed
	Filters map[string][]string `json:"f,omitempty" yaml:"f,omitempty" xml:"f,omitempty"`
}

// Empty = true if the policy has no rules
func (p *Policy) Empty() bool {
	return (p.Scope == nil || len(p.Scope) <= 0) && (p.Filters == nil || len(p.Filters) <= 0)
}

// Sorted list of filter names
func (p *Policy) Sorted() []string {
	result := make(sort.StringSlice, 0, len(p.Filters))
	for f := range p.Filters {
		result = append(result, f)
	}
	result.Sort()
	return result
}

// Check if the params match all constraints:
// - There must be at least a matching scope between the filter and the provided scope
// - If some of the policy filters is not in "default", it is not applied.
// - For every policy filter not in "defaults", there must be a filter in "filters"
// - All the values in "filters" must be within the values allowed by the policy
func (p *Policy) Check(scope []string, defaults, filters dbtool.Params) error {
	// Scopes can be missing
	if scope != nil && len(scope) > 0 && p.Scope != nil && len(p.Scope) > 0 {
		found := false
	outerLoop:
		for _, requested := range scope {
			for _, allowed := range p.Scope {
				if strings.EqualFold(requested, allowed) {
					found = true
					break outerLoop
				}
			}
		}
		if !found {
			return errors.Errorf("Requested scope %v does not match allowed scope %v", scope, p.Scope)
		}
	}
	// If no filters, all done
	if p.Filters == nil || len(p.Filters) <= 0 {
		return nil
	}
	// Values for filters cannot be missing
	for key, allowed := range p.Filters {
		// If nothing to compare to, skip
		if allowed == nil || len(allowed) <= 0 {
			continue
		}
		// If the query does not use this parameter, skip
		if _, ok := defaults[key]; !ok {
			continue
		}
		// If there are required values but no query filters, it is an error
		if filters == nil || len(filters) <= 0 {
			return errors.New("Missing required filters in query parameters")
		}
		// If no values in the filter, it is an error
		requested, ok := filters[key]
		if !ok || requested == nil {
			return errors.Errorf("Missing required filter '%s' in query parameters", key)
		}
		if err := p.checkFilter(key, requested, allowed); err != nil {
			return err
		}
	}
	return nil
}

// Search a principal (item or list) in the list of allowed ones
func (p *Policy) checkFilter(key string, requested interface{}, allowed []string) error {
	requestedValue := reflect.ValueOf(requested)
	// If requested is not a slice, check it right away
	if !(requestedValue.Type().Kind() == reflect.Slice) {
		return p.checkSingle(key, requested, allowed)
	}
	// Otherwise, iterate value per value
	requestedLen := requestedValue.Len()
	if requestedLen <= 0 {
		return errors.Errorf("At least one requested value must be specified for filter %s", key)
	}
	for i := 0; i < requestedLen; i++ {
		requestedItem := requestedValue.Index(i).Interface()
		if err := p.checkSingle(key, requestedItem, allowed); err != nil {
			return err
		}
	}
	return nil
}

// Search a single principal (no slices!) in the list of allowed values
func (p *Policy) checkSingle(key string, requested interface{}, allowed []string) error {
	r, ok := requested.(string)
	if !ok {
		return errors.Errorf("Requested value (%v) for filter '%s' is not a string", requested, key)
	}
	for _, a := range allowed {
		if strings.EqualFold(r, a) {
			return nil
		}
	}
	return errors.Errorf("Value %s not allowed by RBAC for filter %s", r, key)
}
