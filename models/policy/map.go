package policy

import (
	"encoding/json"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"golang.org/x/sync/syncmap"
)

// Maximum serialized subject size
const maxSubject = 600

// Map subject names to policies
type Map struct {
	// Use a pointer since the syncmap.Map type embeds a mutex.
	policies *syncmap.Map
}

// New Policy Map
func New() Map {
	return Map{policies: &syncmap.Map{}}
}

// Key of the policy map
type pKey struct {
	issuer, subject string
}

// Add a Policy to the map.
// Return the policy index to be used as subject of the claims.
func (m Map) Add(issuer, subject string, scope []string, filters map[string][]string) string {
	p := Policy{Scope: scope, Filters: filters}
	m.policies.Store(pKey{issuer: issuer, subject: subject}, p)
	// Return the new subject. May be the same subject already given,
	// or a new one, encoding the policy. Depends on policy size.
	if s, err := json.Marshal(p); err == nil && len(s) <= maxSubject {
		subject = string(s)
	}
	return subject
}

// Policy to apply based on the given claims.
func (m Map) Policy(claims *jwt.StandardClaims) (Policy, error) {
	issuer, subject := claims.Issuer, claims.Subject
	// If issuer == subject, the request is allowed
	if issuer == subject {
		return Policy{}, nil
	}
	// Get the policy for this principal
	var p Policy
	if stored, ok := m.policies.Load(pKey{issuer: issuer, subject: subject}); ok {
		p = stored.(Policy)
	} else {
		if err := json.Unmarshal([]byte(subject), &p); err != nil {
			return Policy{}, errors.Wrapf(err, "Unknown subject %s", subject)
		}
	}
	// Check the policy
	return p, nil
}
