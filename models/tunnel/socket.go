package tunnel

import (
	"io"
	"log"
	"net"
	"time"

	"github.com/gorilla/websocket"
)

// RecvMsg is a message received from a websocket
type RecvMsg struct {
	Type    int
	Message []byte
	Err     error
}

// SendMsg is a message sent to the writer routing
type SendMsg struct {
	Type    int
	Message []byte
	Err     chan error
}

const (
	pong int = iota
	remoteClose
)

// Socket wraps a websocket. Makes sure writes are serialized, and
// closing is reentrant.
//
// Websockets have a few caveats: they are not reentrant, and they are
// not timed (a read or write can hang forever)
//
// In this program, reads I am sure that will be sequential, because
// there will only ever be one goroutine reading from the socket.
// The ping handler can read text data intermixed with the binary data,
// but the ping handler is called from ws.ReadNext() or ws.Read, so it
// is in the same goroutine.
//
// Writes, on the other hand, may be concurrent if we are not careful,
// because the ping handler is run in the "read" goroutine, but may issue
// a "write" (pong).
//
// So it may be the case we are writing a binary message, get
// a ping, and try to pong at the same time.
//
// To avoid this problem, Socket is concurrency safe, and also times
// every read and write so it does not lock.
type syncSocket struct {
	// websocket and ping pong functions
	ws *websocket.Conn
	// channels for sending and receiving data from the ws, and control
	cntl chan SendMsg
	send chan SendMsg
	recv chan RecvMsg
	// close handler already called
	once         bool
	readTimeout  time.Duration
	writeTimeout time.Duration
	pingInterval time.Duration
}

// NewSocket wraps a websocket connection
func NewSocket(ws *websocket.Conn, window int, readTimeout, writeTimeout, pingInterval int) (chan<- SendMsg, <-chan RecvMsg) {
	safe := syncSocket{
		pingInterval: time.Duration(pingInterval) * time.Second,
		readTimeout:  time.Duration(readTimeout) * time.Second,
		writeTimeout: time.Duration(writeTimeout) * time.Second,
		ws:           ws,
		cntl:         make(chan SendMsg, 1),
		send:         make(chan SendMsg, window),
		recv:         make(chan RecvMsg, window),
	}
	// Set ping / pong handlers that update timeouts
	ws.SetPingHandler(func(appData string) error {
		return safe.pingHandler(appData)
	})
	ws.SetPongHandler(func(appData string) error {
		return safe.pongHandler(appData)
	})
	ws.SetCloseHandler(func(code int, text string) error {
		return safe.closeHandler(code, text)
	})
	// Run reader and writer goroutines
	go safe.reader(ws)
	go safe.writer(ws)
	return safe.send, safe.recv
}

func (safe *syncSocket) pingHandler(appData string) error {
	// Extend the deadline
	safe.ws.SetReadDeadline(time.Now().Add(safe.readTimeout))
	// Only send the message if there are no other waiting. Pongs can wait.
	select {
	case safe.cntl <- SendMsg{Type: pong, Message: []byte(appData)}:
	default:
	}
	return nil
}

func (safe *syncSocket) pongHandler(string) error {
	// Extend the deadline
	safe.ws.SetReadDeadline(time.Now().Add(safe.readTimeout))
	return nil
}

func (safe *syncSocket) closeHandler(code int, text string) error {
	// Make sure the close message is sent only once.
	if safe.once {
		return nil
	}
	safe.once = true
	go func() {
		safe.cntl <- SendMsg{
			Type:    remoteClose,
			Message: websocket.FormatCloseMessage(websocket.CloseNormalClosure, "bye"),
		}
	}()
	return nil
}

// Reader goroutine
func (safe *syncSocket) reader(ws *websocket.Conn) {
	for {
		// Get a reader for the next message
		safe.ws.SetReadDeadline(time.Now().Add(safe.readTimeout))
		mt, message, err := safe.ws.ReadMessage()
		msg := RecvMsg{
			Type:    mt,
			Message: message,
			Err:     err,
		}
		if err != nil {
			if netErr, ok := err.(net.Error); ok && netErr.Timeout() {
				if netErr.Temporary() {
					continue
				}
				// this is a read timeout, we won't receive the close error
				safe.closeHandler(websocket.CloseNoStatusReceived, "net.Timeout")
			} else if _, ok := err.(*websocket.CloseError); !ok {
				// The closeHandler should have sent a message. I leave.
				safe.closeHandler(websocket.CloseAbnormalClosure, err.Error())
			}
			// Any unexpected error, just leave
			close(safe.recv)
			return
		}
		select {
		case safe.recv <- msg:
		default:
			log.Print("Inbound buffer full, discarding message")
		}
	}
}

// Serialized calls to the underlying websocket Write.
func (safe *syncSocket) writer(ws *websocket.Conn) {
	// Prepara a ticker to ping the channel
	var timer <-chan time.Time
	if safe.pingInterval > 0 {
		ticker := time.NewTicker(safe.pingInterval)
		timer = ticker.C
		defer ticker.Stop()
	}
	// First block: manage requests
	var closeMsg []byte
	waitLocal, waitRemote := false, false
	for !waitLocal && !waitRemote {
		var err error
		select {
		case cmd := <-safe.cntl:
			switch cmd.Type {
			case pong:
				err = ws.WriteControl(websocket.PongMessage, cmd.Message, time.Now().Add(safe.writeTimeout))
			case remoteClose:
				waitLocal, closeMsg = true, cmd.Message
			}
		case msg, ok := <-safe.send:
			if !ok {
				waitRemote, closeMsg = true, websocket.FormatCloseMessage(websocket.CloseNormalClosure, "bye")
			} else {
				ws.SetWriteDeadline(time.Now().Add(safe.writeTimeout))
				err = ws.WriteMessage(msg.Type, msg.Message)
				if msg.Err != nil {
					msg.Err <- err
				}
			}
		case <-timer:
			err = ws.WriteControl(websocket.PingMessage, []byte("ping"), time.Now().Add(safe.writeTimeout))
		}
		if err != nil {
			if netErr, ok := err.(net.Error); !ok || !netErr.Timeout() || !netErr.Temporary() {
				// If the error is not a temporary timeout, I'll go ahead and close the channel.
				closeMsg = websocket.FormatCloseMessage(websocket.CloseAbnormalClosure, err.Error())
				waitLocal, waitRemote = true, true
			}
		}
	}
	// Second block: wait for closed channel
	safe.ws.WriteControl(websocket.CloseMessage, closeMsg, time.Now().Add(2*safe.writeTimeout))
	for waitLocal || waitRemote {
		select {
		case cmd := <-safe.cntl:
			if cmd.Type == remoteClose {
				waitRemote = false
			}
		case msg, ok := <-safe.send:
			if !ok {
				waitLocal = false
			} else if msg.Err != nil {
				msg.Err <- io.EOF
			}
		}
	}
	// third block: cleanup
	for msg := range safe.send {
		if msg.Err != nil {
			msg.Err <- io.EOF
		}
	}
	safe.ws.Close()
}
