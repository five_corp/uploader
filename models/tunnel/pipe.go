package tunnel

import (
	"context"
	"encoding/gob"
	"io"

	"github.com/pkg/errors"

	"github.com/gorilla/websocket"
)

type socketReader struct {
	recv    <-chan RecvMsg
	send    chan<- SendMsg
	buff    []byte
	offs    int
	encoder *gob.Encoder
	decoder *gob.Decoder
}

// Error type for this package
type Error string

// Error implements error interface
func (e Error) Error() string {
	return string(e)
}

const (
	// ErrorInterrupted is returned when a message is received while sending another message
	ErrorInterrupted Error = "Protocol error: received message while writing"
)

// Pipe is an interface{} pipe build over a websocket.
type Pipe interface {
	Send(ctx context.Context, data interface{}) error
	Recv(ctx context.Context, data interface{}) error
	Close() error
}

// NewPipe returns a new interface{} pipe over a websocket
func NewPipe(send chan<- SendMsg, recv <-chan RecvMsg) Pipe {
	p := &socketReader{
		send: send,
		recv: recv,
	}
	p.encoder = gob.NewEncoder(p)
	p.decoder = gob.NewDecoder(p)
	return p
}

// Read implements io.Reader
func (s *socketReader) Read(p []byte) (n int, err error) {
	for s.buff == nil || s.offs >= len(s.buff) {
		msg, ok := <-s.recv
		if !ok {
			return 0, io.EOF
		}
		if msg.Err != nil {
			return 0, msg.Err
		}
		s.buff = msg.Message
		s.offs = 0
	}
	chunk := copy(p, s.buff[s.offs:])
	s.offs += chunk
	return chunk, nil
}

// Write implements io.Writer
func (s *socketReader) Write(p []byte) (n int, err error) {
	errChan := make(chan error, 1)
	select {
	case s.send <- SendMsg{Type: websocket.BinaryMessage, Message: p, Err: errChan}:
		if err := <-errChan; err != nil {
			return 0, err
		}
		return len(p), nil
	case recv := <-s.recv:
		// This will be understood as a protocol error: If I intend to write,
		// I don't expect to read anything. So the write will break.
		if recv.Err != nil {
			return 0, recv.Err
		}
		return 0, ErrorInterrupted
	}
}

// Close implements io.Closer
func (s *socketReader) Close() error {
	close(s.send)
	return nil
}

func (s *socketReader) Send(ctx context.Context, data interface{}) error {
	select {
	case <-ctx.Done():
		return errors.New("Pipe.Send: context cancelled")
	default:
	}
	return s.encoder.Encode(data)
}

func (s *socketReader) Recv(ctx context.Context, data interface{}) error {
	select {
	case <-ctx.Done():
		return errors.New("Pipe.Recv: context cancelled")
	default:
	}
	return s.decoder.Decode(data)
}
