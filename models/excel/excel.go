package excel

import (
	"path/filepath"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
	"github.com/tealeg/xlsx"
)

// Writer is capable of writing a database result to some persistent storage
type Writer interface {
	Write(result dbtool.Feed, types []string) error
	Fullpath() string
}

// QueryWriter for excel
type excelWriter struct {
	fullpath string
}

// NewWriter creates an excel writer
func NewWriter(folder, file string) (Writer, error) {
	return &excelWriter{
		fullpath: filepath.Join(folder, file) + ".xlsx",
	}, nil
}

// Writes query data to an excel file
func (writer *excelWriter) Write(result dbtool.Feed, types []string) error {
	xlFile := xlsx.NewFile()
	xlSheet, err := xlFile.AddSheet("Hoja1")
	if err != nil {
		return errors.Wrap(err, "Failed to create excel file object")
	}
	xlRow := xlSheet.AddRow()
	for _, head := range result.Names {
		xlRow.AddCell().SetString(head)
	}
	for result.Next() {
		for _, row := range result.Batch() {
			xlRow := xlSheet.AddRow()
			for _, col := range row {
				xlRow.AddCell().SetValue(col)
			}
		}
	}
	if err := result.Err(); err != nil {
		return err
	}
	if err := xlFile.Save(writer.fullpath); err != nil {
		return errors.Wrapf(err, "Failed to save excel file %s", writer.fullpath)
	}
	return nil
}

func (writer *excelWriter) Fullpath() string {
	return writer.fullpath
}
