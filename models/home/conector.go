package home

import (
	"context"
	"log"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/conector"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/dbtool/mssql"
)

// Manager for connections to Home endpoints
type Manager struct {
	// URL that returns the configured sources
	SourcesURL      string
	SourcesAudience string
	// URL to retrieve the credentials
	CredentialsURL      string
	CredentialsAudience string
	// Folder where Excels are stored (for excel conector)
	ExcelFolder string
	// Parameters of the batch task
	ReqTimeout      int
	ConectorTimeout int
	MaxRows         int
	BatchSize       int
	// SourceOverride fixes the sources to use, ignoring external sources
	SourceOverride []string
}

// Format of the message returned by home when requesting the SourcesURL
type conectorSelected struct {
	Success bool     `json:"success"`
	Message string   `json:"message"`
	Data    []string `json:"data"`
}

// Format of the credentials data returned by home
type conectorCredentials struct {
	Host   string `json:"host"`
	Port   string `json:"port"`
	User   string `json:"user"`
	Pass   string `json:"pass"`
	Name   string `json:"name"`
	UseTLS bool   `json:"tls"`
}

// Format of data returned by home when requesting credentials
type conectorData struct {
	Success bool                `json:"success"`
	Message string              `json:"message"`
	Data    conectorCredentials `json:"data"`
}

// Sources enabled for a given conector, at Home.
// If "products" is provided, return the common subset of
// the configured sources and the given products.
func (manager *Manager) Sources(ctx context.Context, conectorName string, products []string, signer saferest.Signer) ([]string, error) {
	available := manager.SourceOverride
	if available == nil || len(available) <= 0 {
		token := signer.Sign(manager.SourcesAudience, time.Second*60)
		conectorURL := strings.Replace(manager.SourcesURL, "CONECTOR", conectorName, -1)
		resp := conectorSelected{}
		if err := saferest.RestRequest(ctx, "GET", conectorURL, token, manager.ReqTimeout, nil, false, &resp); err != nil {
			log.Print("Request to " + conectorURL + " failed, error: " + err.Error())
			return nil, err
		}
		if !resp.Success {
			return nil, errors.Errorf("ConectorURL: request to %s failed, message: %s", conectorURL, resp.Message)
		}
		available = resp.Data
	}
	// Mix the sources at home with the available products
	if products != nil && len(products) > 0 {
		sort.Strings(products)
		index := 0
		for _, current := range available {
			pos := sort.SearchStrings(products, current)
			if pos < len(products) && products[pos] == current {
				available[index] = current
				index++
			}
		}
		available = available[:index]
	}
	return available, nil
}

// Engine to run a conector
func (manager *Manager) Engine(ctx context.Context, store dbtool.Store, conectorName string, signer saferest.Signer) (map[string]conector.Engine, error) {
	result := make(map[string]conector.Engine)
	// Build the local SQL engine
	local, err := conector.NewLocalEngine(ctx, store)
	if err != nil {
		return nil, errors.New("Could not build local engine")
	}
	result[conector.Local] = local
	// Build the Excel engine
	tabular, err := conector.NewTabularEngine(ctx, manager.ExcelFolder, dbtool.RFCFormatter{})
	if err != nil {
		return nil, errors.New("Could not build tabular engine")
	}
	result[conector.Tabular] = tabular
	result[conector.Excel] = tabular // backward-comp
	// If overriding, we are done
	if manager.SourceOverride != nil && len(manager.SourceOverride) > 0 {
		return result, nil
	}
	// Get credentials for engines
	credentials, err := manager.credentials(ctx, conectorName, signer)
	if err != nil {
		return nil, err
	}
	// Build the remote SQL engine
	engine, err := conector.NewSQLEngine(ctx, credentials)
	if err != nil {
		return nil, errors.New("Unrecognized database credentials format")
	}
	result[conector.SQL] = engine
	return result, nil
}

// Credentials for a given conector, as returned by Home.
func (manager *Manager) credentials(ctx context.Context, conectorName string, signer saferest.Signer) (dbtool.Config, error) {
	token := signer.Sign(manager.CredentialsAudience, time.Second*60)
	credentialsURL := strings.Replace(manager.CredentialsURL, "CONECTOR", conectorName, -1)
	resp := conectorData{}
	if err := saferest.RestRequest(ctx, "GET", credentialsURL, token, manager.ReqTimeout, nil, false, &resp); err != nil {
		log.Print("Request to " + credentialsURL + " failed, error: " + err.Error())
		return nil, err
	}
	if !resp.Success {
		return nil, errors.Errorf("ConectorURL: request to %s failed, message: %s", credentialsURL, resp.Message)
	}
	port, err := strconv.ParseInt(resp.Data.Port, 10, 32)
	if err != nil {
		return nil, errors.Wrapf(err, "Port number %s is not an integer", resp.Data.Port)
	}
	return &mssql.Config{
		Host:    resp.Data.Host,
		Name:    resp.Data.Name,
		Port:    int(port),
		User:    resp.Data.User,
		Pass:    resp.Data.Pass,
		UseTLS:  resp.Data.UseTLS,
		Timeout: manager.ConectorTimeout,
	}, nil
}
