package tenants

import (
	"bitbucket.org/five_corp/uploader/models/dbtool"
)

// Switcher returns the resource (database) of a tenant
type Switcher interface {
	// Resource of the given tenant
	Switch(issuer string) (dbtool.Store, bool)
	// Returns a formatter for the inner dbStore
	Formatter() dbtool.Formatter
	// Resource returns the current resource of a tenant
	Resource(issuer string) (string, bool)
	// KeyFunc implements saferest.Keyer interface
	KeyFunc(issuer string) (string, error)
	// Subscribe to tenant events
	Subscribe(key string, mask EventType, suscriptor func(mask EventType, issuer string, tenant Tenant))
	// Unsubscribe from tenant events
	Unsubscribe(key string)
	// DefaultIssuer returns the defalt tenant name
	DefaultIssuer() string
}

type switcher struct {
	dbStore dbtool.Store
	tStore  Store
}

// Switch implements the Switcher interface
func (s switcher) Switch(issuer string) (dbtool.Store, bool) {
	resource, ok := s.tStore.Resource(issuer)
	if !ok {
		return nil, false
	}
	return s.dbStore.Switch(resource), true
}

// Formatter implements the Switcher interface
func (s switcher) Formatter() dbtool.Formatter {
	return s.dbStore
}

// Resource returns the DB name
func (s switcher) Resource(issuer string) (string, bool) {
	return s.tStore.Resource(issuer)
}

// KeyFunc implements saferest.Keyer interface
func (s switcher) KeyFunc(issuer string) (string, error) {
	return s.tStore.KeyFunc(issuer)
}

// Subscribe to tenant events
func (s switcher) Subscribe(key string, mask EventType, suscriptor func(mask EventType, issuer string, tenant Tenant)) {
	s.tStore.Subscribe(key, mask, suscriptor)
}

// Unsubscribe from tenant events
func (s switcher) Unsubscribe(key string) {
	s.tStore.Unsubscribe(key)
}

// DefaultIssuer returns the defalt tenant name
func (s switcher) DefaultIssuer() string {
	return s.tStore.DefaultIssuer()
}
