package tenants

import (
	"github.com/pkg/errors"
	cron "gopkg.in/robfig/cron.v2"
)

// Credentials to connect to data source
type Credentials struct {
	Host string `json:"host" xml:"host" yaml:"host"`
	Port int    `json:"port" xml:"port" yaml:"port"`
	User string `json:"user" xml:"user" yaml:"user"`
	Pass string `json:"pass" xml:"pass" yaml:"pass"`
	Name string `json:"name" xml:"name" yaml:"name"`
}

// CronTask defining a task label, schedule and argument list
type CronTask struct {
	// Task label
	Label string `json:"label" xml:"label" yaml:"label"`
	// Cron time specification
	CronSpec string `json:"cronSpec" xml:"cron_spec" yaml:"cronSpec"`
	// Task arguments
	Args []string `json:"args" xml:"args" yaml:"args"`
	// Scheduler ID
	ID cron.EntryID `json:"-" xml:"-" yaml:"-"`
}

// Tenant information
type Tenant struct {
	Resource    string                 `json:"resource" xml:"resource" yaml:"resource"`
	Token       string                 `json:"token" xml:"token" yaml:"token"`
	Credentials map[string]Credentials `json:"credentials" xml:"credentials" yaml:"credentials"`
	Tasks       []CronTask             `json:"tasks" xml:"tasks" yaml:"tasks"`
}

type tenantList struct {
	suscriptorList
	defTenant, defToken string
}

// New returns a Tenant store
func (s *tenantList) init(tenants map[string]Tenant, defTenant string, defData Tenant) error {
	if tenants == nil {
		tenants = make(map[string]Tenant)
	}
	// Save or update the data of the default tenant
	if t, ok := tenants[defTenant]; !ok {
		tenants[defTenant] = defData
	} else {
		if defData.Token != "" {
			t.Token = defData.Token
		}
		if defData.Resource != "" {
			t.Resource = defData.Resource
		}
		if defData.Tasks != nil {
			t.Tasks = defData.Tasks
		}
		tenants[defTenant] = t
	}
	// Load and launch scheduled tasks
	s.tenants = tenants
	s.suscriptors = make(map[string]suscriptor)
	s.actions = make(chan mutatorReq)
	s.defTenant = defTenant
	s.defToken = defData.Token
	go s.dispatch()
	return nil
}

// Enumerate returns a list of tenants and credential sets
func (s *tenantList) Enumerate() map[string][]string {
	result := make(map[string][]string)
	s.RLock()
	defer s.RUnlock()
	slice, base := make([]string, 0, len(s.tenants)+1), 0
	for k, v := range s.tenants {
		count := 0
		for c := range v.Credentials {
			slice = append(slice, c)
			count++
		}
		result[k] = slice[base:(base + count)]
		base += count
	}
	return result
}

// DefaultIssuer returns the name of the default issuer
func (s *tenantList) DefaultIssuer() string {
	s.RLock()
	defer s.RUnlock()
	return s.defTenant
}

// Tenant info for the given tenant
func (s *tenantList) Token(issuer string) (token string, found bool) {
	s.RLock()
	if t, ok := s.tenants[issuer]; ok {
		token, found = t.Token, true
	}
	s.RUnlock()
	return
}

// Credentials info for the given tenant
func (s *tenantList) Credentials(issuer, label string) (creds Credentials, found bool) {
	s.RLock()
	if t, ok := s.tenants[issuer]; ok {
		creds, found = t.Credentials[label]
	}
	s.RUnlock()
	return
}

// Resource (database) info for the given tenant
func (s *tenantList) Resource(issuer string) (resource string, found bool) {
	s.RLock()
	if t, ok := s.tenants[issuer]; ok {
		resource, found = t.Resource, true
	}
	s.RUnlock()
	return
}

// KeyFunc implements saferest.Keyer interface
func (s *tenantList) KeyFunc(issuer string) (string, error) {
	s.RLock()
	defer s.RUnlock()
	if t, ok := s.tenants[issuer]; ok {
		return t.Token, nil
	}
	// smartBI default (no issuer): test defaultToken
	return s.defToken, nil
	//return "", errors.Errorf("Could not find token for issuer %s", issuer)
}

// Add creates a tenant
func (s *tenantList) Add(issuer, resource, token string) (bool, error) {
	return s.mutator(func() (EventType, string, Tenant, bool, error) {
		if issuer == s.defTenant {
			return skip, issuer, Tenant{}, false, errors.New("Default tenant cannot be added")
		}
		t, ok := s.tenants[issuer]
		if !ok {
			t = Tenant{}
		}
		t.Resource = resource
		t.Token = token
		s.tenants[issuer] = t
		return Create, issuer, t, true, nil
	})
}

// Remove a tenant
func (s *tenantList) Remove(issuer string) (bool, error) {
	return s.mutator(func() (EventType, string, Tenant, bool, error) {
		if issuer == s.defTenant {
			return skip, issuer, Tenant{}, false, errors.New("Default tenant cannot be removed")
		}
		t, ok := s.tenants[issuer]
		if ok {
			delete(s.tenants, issuer)
		}
		return Remove, issuer, t, true, nil
	})
}

// Rename the default tenant
func (s *tenantList) Rename(newDefaultName, newDefaultSecret string) (bool, error) {
	return s.mutator(func() (EventType, string, Tenant, bool, error) {
		tenant := s.tenants[s.defTenant]
		delete(s.tenants, s.defTenant)
		tenant.Token = newDefaultSecret
		s.defTenant = newDefaultName
		s.defToken = newDefaultSecret
		s.tenants[newDefaultName] = tenant
		return Update, newDefaultName, tenant, true, nil
	})
}

// Update the token of a tenant
func (s *tenantList) Update(issuer, token string) (bool, error) {
	return s.mutator(func() (EventType, string, Tenant, bool, error) {
		t, ok := s.tenants[issuer]
		if !ok {
			return skip, "", Tenant{}, false, errors.Errorf("Issuer %s not found for update", issuer)
		}
		t.Token = token
		// notify if we are updating the default tenant
		if issuer == s.defTenant {
			s.defToken = token
		}
		s.tenants[issuer] = t
		return Update, issuer, t, true, nil
	})
}

// AddCredentials to a tenant
func (s *tenantList) AddCredentials(issuer, label string, creds Credentials) (bool, error) {
	return s.mutator(func() (EventType, string, Tenant, bool, error) {
		t, ok := s.tenants[issuer]
		if !ok {
			return skip, "", Tenant{}, false, errors.Errorf("tenants %s not found for AddCredentials", issuer)
		}
		if t.Credentials == nil {
			t.Credentials = make(map[string]Credentials)
		}
		t.Credentials[label] = creds
		s.tenants[issuer] = t
		return Update, issuer, t, true, nil
	})
}

// RemoveCredentials from a tenant
func (s *tenantList) RemoveCredentials(issuer, label string) (bool, error) {
	return s.mutator(func() (EventType, string, Tenant, bool, error) {
		t, ok := s.tenants[issuer]
		if !ok {
			return skip, "", Tenant{}, false, errors.Errorf("tenant %s not found for RemoveCredentials", issuer)
		}
		if t.Credentials == nil {
			return skip, "", Tenant{}, false, errors.Errorf("credentials %s not found for tenant %s", label, issuer)
		}
		delete(t.Credentials, label)
		s.tenants[issuer] = t
		return Update, issuer, t, true, nil
	})
}
