package tenants

import (
	"sync"
)

// EventType associated to a tenant: create, delete, update
type EventType int

const (
	// skip is an internal actions that does not generate an event
	skip = iota
	// Create Event: tenant is created
	Create EventType = (1 << iota)
	// Update Event: tenant is updated
	Update
	// Task Event: tasks are changed
	Task
	// Remove Event: tenant is deleted
	Remove
	// Bootstrap is an internal event when a new tenant has been added
	Bootstrap
)

// suscriptorFunc is a wrapper to turn a function into as suscriptor
type suscriptorFunc func(mask EventType, issuer string, tenant Tenant)

type suscriptor struct {
	EventType
	suscriptorFunc
	bootstrapped bool
}

type mutatorResp struct {
	ok  bool
	err error
}

type mutatorFunc func() (EventType, string, Tenant, bool, error)

type mutatorReq struct {
	f    mutatorFunc
	resp chan mutatorResp
}

type suscriptorList struct {
	tenants     map[string]Tenant
	suscriptors map[string]suscriptor
	actions     chan mutatorReq
	sync.RWMutex
}

// This gets called with the mutex held
func (l *suscriptorList) dispatch() {
	for m := range l.actions {
		l.Lock()
		etype, issuer, tenant, ok, err := m.f()
		l.Unlock()
		switch etype {
		case skip:
		case Bootstrap:
			for key, s := range l.suscriptors {
				if !s.bootstrapped && (s.EventType&Bootstrap) != 0 {
					s.bootstrapped = true
					l.suscriptors[key] = s
					for issuer, tenant := range l.tenants {
						s.suscriptorFunc(Bootstrap, issuer, tenant)
					}
				}
			}
		default:
			for _, s := range l.suscriptors {
				if s.EventType&etype != 0 {
					s.suscriptorFunc(etype, issuer, tenant)
				}
			}
		}
		m.resp <- mutatorResp{ok: ok, err: err}
	}
}

func (l *suscriptorList) mutator(f func() (EventType, string, Tenant, bool, error)) (bool, error) {
	respChan := make(chan mutatorResp, 1)
	l.actions <- mutatorReq{f: f, resp: respChan}
	resp := <-respChan
	return resp.ok, resp.err
}

// Subscribe to tenant events
func (l *suscriptorList) Subscribe(key string, match EventType, s func(mask EventType, issuer string, tenant Tenant)) {
	l.mutator(func() (EventType, string, Tenant, bool, error) {
		l.suscriptors[key] = suscriptor{suscriptorFunc: s, EventType: match}
		return Bootstrap, "", Tenant{}, true, nil
	})
}

// Unsubscribe from tenant events
func (l *suscriptorList) Unsubscribe(key string) {
	l.mutator(func() (EventType, string, Tenant, bool, error) {
		delete(l.suscriptors, key)
		return skip, "", Tenant{}, true, nil
	})
}
