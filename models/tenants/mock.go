package tenants

import "bitbucket.org/five_corp/uploader/models/dbtool"

type mock struct {
	dbStore    dbtool.Store
	defIssuer  string
	switchedTo string
}

// Mock returns a mock Switcher for testing
func Mock(defIssuer string, store dbtool.Store) Switcher {
	return &mock{dbStore: store, defIssuer: defIssuer}
}

// Switch implements the Switcher interface
func (m *mock) Switch(issuer string) (dbtool.Store, bool) {
	m.switchedTo = issuer
	return m.dbStore, true
}

// Formatter implements the Switcher interface
func (m *mock) Formatter() dbtool.Formatter {
	return m.dbStore
}

// Resource returns the DB name
func (m *mock) Resource(issuer string) (string, bool) {
	return m.switchedTo, true
}

// KeyFunc implements saferest.Keyer interface
func (m *mock) KeyFunc(issuer string) (string, error) {
	return "", nil
}

// Subscribe to tenant events
func (m *mock) Subscribe(key string, match EventType, suscriptor func(mask EventType, issuer string, tenant Tenant)) {
}

// Unsubscribe from tenant events
func (m *mock) Unsubscribe(key string) {
}

// DefaultIssuer returns the defalt tenant name
func (m *mock) DefaultIssuer() string {
	return m.defIssuer
}
