package tenants

import (
	"log"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/tasker"
	"github.com/pkg/errors"
	cron "gopkg.in/robfig/cron.v2"
)

// Store is the tenant manager
type Store interface {
	// Enumerate the list of tenants and credentials
	Enumerate() map[string][]string
	// Token of the given tenant
	Token(issuer string) (string, bool)
	// Credential of the given tenant
	Credentials(issuer, label string) (Credentials, bool)
	// Resource returns the current resource of a tenant
	Resource(issuer string) (string, bool)
	// Update the token of a tenant
	Update(issuer, token string) (bool, error)
	// Add / overwrite a tenant
	Add(issuer, resource, token string) (bool, error)
	// Remove a tenant
	Remove(issuer string) (bool, error)
	// Rename the default tenant
	Rename(newDefaultName, newDefaultSecret string) (bool, error)
	// AddCredentials to a tenant
	AddCredentials(issuer, label string, creds Credentials) (bool, error)
	// RemoveCredentials from a tenant
	RemoveCredentials(issuer, label string) (bool, error)
	// Tasks of a tenant
	Tasks(issuer string) ([]CronTask, bool)
	// AddTask adds a task to the task list
	AddTask(issuer string, task CronTask) (bool, error)
	// RemoveTask removes a task from the task list
	RemoveTask(issuer string, label string) (bool, error)
	// Subscribe to tenant events
	Subscribe(key string, mask EventType, suscriptor func(mask EventType, issuer string, tenant Tenant))
	// Unsubscribe from tenant events
	Unsubscribe(key string)
	// DefaultIssuer returns the defalt tenant name
	DefaultIssuer() string
	// KeyFunc implements saferest.Keyer interface
	KeyFunc(issuer string) (string, error)
	// Switcher returns a switcher for a store
	Switcher(dbtool.Store) Switcher
}

type store struct {
	tenantList
	scheduler *cron.Cron
	runner    tasker.Runner
}

// New returns a Tenant store
func New(tenants map[string]Tenant, defTenant string, defData Tenant, runner tasker.Runner) (Store, error) {
	s := &store{
		runner:    runner,
		scheduler: cron.New(),
	}
	if err := s.init(tenants, defTenant, defData); err != nil {
		return nil, err
	}
	s.scheduler.Start()
	s.Subscribe("tasks", Create|Remove|Bootstrap, func(mask EventType, issuer string, tenant Tenant) {
		s.Event(mask, issuer, tenant)
	})
	return s, nil
}

// Tasks lists the tasks of a tenant
func (s *store) Tasks(issuer string) ([]CronTask, bool) {
	var tasks []CronTask
	s.RLock()
	t, ok := s.tenants[issuer]
	if !ok {
		s.RUnlock()
		return nil, false
	}
	tasks = make([]CronTask, len(t.Tasks))
	for idx, task := range t.Tasks {
		tasks[idx] = task
	}
	s.RUnlock()
	return tasks, true
}

// AddTask adds a task to the task list
func (s *store) AddTask(issuer string, task CronTask) (bool, error) {
	return s.mutator(func() (EventType, string, Tenant, bool, error) {
		t, ok := s.tenants[issuer]
		if !ok {
			return skip, "", Tenant{}, false, errors.Errorf("tenant %s not found for task addition", issuer)
		}
		// Find the position of the label in the list, if it's there
		match := -1
		for idx, prev := range t.Tasks {
			if prev.Label == task.Label {
				match = idx
				s.scheduler.Remove(prev.ID)
				break
			}
		}
		id, err := s.scheduler.AddFunc(task.CronSpec, s.taskBody(issuer, task))
		if err != nil {
			if match >= 0 {
				t.Tasks = remove(t.Tasks, match)
			}
		} else {
			task.ID = id
			if match >= 0 {
				t.Tasks[match] = task
			} else {
				t.Tasks = append(t.Tasks, task)
			}
		}
		s.tenants[issuer] = t
		if match >= 0 || err == nil {
			return Task, issuer, t, true, nil
		}
		return skip, "", Tenant{}, false, err
	})
}

// RemoveTask removes a task from the task list
func (s *store) RemoveTask(issuer string, label string) (bool, error) {
	return s.mutator(func() (EventType, string, Tenant, bool, error) {
		t, ok := s.tenants[issuer]
		if !ok {
			return skip, "", Tenant{}, false, errors.Errorf("tenant %s not found for task removal", issuer)
		}
		for idx, prev := range t.Tasks {
			if prev.Label == label {
				s.scheduler.Remove(prev.ID)
				t.Tasks = remove(t.Tasks, idx)
				s.tenants[issuer] = t
				return Task, issuer, t, true, nil
			}
		}
		return skip, "", Tenant{}, false, errors.Errorf("Task %s for tenant %s not found", label, issuer)
	})
}

func (s *store) Switcher(dbStore dbtool.Store) Switcher {
	return switcher{dbStore: dbStore, tStore: s}
}

// This is a suscriptor, so gets called without the mutex
func (s *store) Event(mask EventType, issuer string, tenant Tenant) {
	for index, task := range tenant.Tasks {
		switch {
		case mask&Bootstrap != 0:
			fallthrough
		case mask&Create != 0:
			id, err := s.scheduler.AddFunc(task.CronSpec, s.taskBody(issuer, task))
			if err != nil {
				log.Print("Error scheduling task ", task.Label, ": ", err)
			}
			tenant.Tasks[index].ID = id
		case mask&Remove != 0:
			s.scheduler.Remove(task.ID)
		}
	}
}

// Body of the task: just enqueues the arguments at the runner
func (s *store) taskBody(issuer string, task CronTask) func() {
	return func() {
		if len(task.Args) > 0 {
			if _, err := s.runner.AddTask(task.Label, issuer, task.Args, false); err != nil {
				log.Print("ERROR: Could not run scheduled task", task.Label, "for tenant", issuer, ":", err)
			} else {
				log.Print("Running scheduled task", task.Label, "for tenant", issuer)
			}
		} else {
			log.Print("Scheduled task without arguments for tenant", issuer, "is not supported")
		}
	}
}

func remove(tasks []CronTask, index int) []CronTask {
	// Move the last task to the position of the removed one,
	// and truncate the list
	tLast := len(tasks) - 1
	if index < tLast {
		tasks[index] = tasks[tLast]
	}
	return tasks[:tLast]
}
