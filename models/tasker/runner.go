// Package tasker manages serialization of tasks that need to run orderly.
// Runner objects manage a queue of tasks that are executed by plugins.
// Each plugin can manage particular tasks.
package tasker

import (
	"io"
	"log"
	"os"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/rjrivero/ring"
	"gopkg.in/natefinch/lumberjack.v2"
)

// Plugin able to run tasks
type Plugin interface {
	// Run some task with the given env, stdout / stderr and args
	Run(plugin, issuer string, env []string, stdout, stderr io.Writer, args []string) error
}

// Info about a task in the queue
type Info struct {
	// Issuer and Plugin of this task
	Issuer, Plugin string
	// Arguments for the task
	Args []string
	// True if a task with the same plugin was already in the queue
	Overlap bool
	// Order of the task, used to check whether it is finished
	Order uint64
	// Time between starting and ending the task
	Since, Until time.Time
	// Error returned, for historic purposes
	Err error
}

// Runner that performs the command line tasks sequentially
type Runner interface {
	// AddPlugin registers a plugin to handle tasks. Default plugin has the name "".
	AddPlugin(name string, plugin Plugin) error
	// AddTask queues a task in the runner.
	// If overlap==false, do not allow two tasks with the same ID in the queue
	AddTask(plugin, issuer string, args []string, overlap bool) (Info, error)
	// Queued returns the data of any task still queued with that order number
	Queued(issuer string, order uint64) (Info, bool)
	// Pending returns the queue status for an issuer
	Pending(issuer string) map[string][]Info
	// History returns a record of tasks completed
	History(issuer string) []Info
	// Close the runner
	Close()
}

// Params for a task runner
type Params struct {
	// TaskBuffer is the length of the task buffer
	TaskBuffer int
	// LogFile is the full path to the log file
	LogFile string
	// LogSize is the maximum size of the log file
	LogSize int
	// LogBackups is the number of log file rotations to keep
	LogBackups int
	// Environment contains the common environment vars for all tasks
	Environment []string
}

type infoKey struct {
	issuer, plugin string
}

// HistorySize is the number of jobs retained for historic purposes
const HistorySize = 16

type infoRing struct {
	ring.Ring
	buffer [HistorySize]Info
}

func (i infoRing) slice() []Info {
	buffer := make([]Info, 0, i.Len())
	for iter := i.Ring; iter.Some(); {
		buffer = append(buffer, i.buffer[iter.PopFront()])
	}
	return buffer
}

// Channel to send tasks to the runner
type runner struct {
	// Task Parameters
	Params
	// mutex to grant that task execution is ordered
	mutex sync.RWMutex
	// Channel to push tasks to be run
	tasks chan Info
	// True when the channel is closed
	closed bool
	// Plugins the runner recognizes
	plugins map[string]Plugin
	// Tasks not finished yet
	pending map[infoKey][]Info
	// History of finished tasks
	history map[string]*infoRing
	// Latest task scheduled
	latest uint64
	// Last task finished running
	current uint64
}

// New builds a Runner and starts the goroutines
func New(params Params) Runner {
	r := &runner{
		Params:  params,
		tasks:   make(chan Info, params.TaskBuffer),
		closed:  false,
		plugins: make(map[string]Plugin),
		pending: make(map[infoKey][]Info, params.TaskBuffer),
		history: make(map[string]*infoRing),
		latest:  1,
		current: 0,
		mutex:   sync.RWMutex{},
	}
	go r.loop()
	return r
}

// AddTask implements Runner interface
func (r *runner) AddTask(plugin, issuer string, args []string, overlap bool) (Info, error) {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	// If the task ID is already in the queue, check the value of overlap, and adjust the flags.
	id := infoKey{issuer: issuer, plugin: plugin}
	infoList, ok := r.pending[id]
	info := Info{
		Order:  r.latest,
		Issuer: issuer,
		Plugin: plugin,
		Args:   args,
		Since:  time.Now(),
	}
	if !ok {
		infoList = make([]Info, 0, 4)
		info.Since = time.Now()
	} else if !overlap && len(infoList) > 0 {
		return infoList[0], nil
	} else {
		info.Overlap = (len(infoList) > 0)
	}
	// Otherwise, assign a new order and push to the queue
	var err error
	select {
	case r.tasks <- info:
		r.latest++
		infoList = append(infoList, info)
		r.pending[id] = infoList
	default:
		if len(infoList) == 0 {
			// Since we allocated some memory for this, at least make sure to save it for later use
			r.pending[id] = infoList
		}
		err = errors.New("Task queue is full")
	}
	return info, err
}

// AddPlugin adds a new plugin
func (r *runner) AddPlugin(name string, plugin Plugin) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	r.plugins[name] = plugin
	return nil
}

// Returns the data of the task, if it is still enqueued. This is deprecated, use Pending instead.
func (r *runner) Queued(issuer string, order uint64) (Info, bool) {
	// In case the server restarts, a client with an old order number
	// may get stuck checking for its job to finish, because its order
	// number may be well ahead.
	// To avoid this, order numbers in the future are always marked
	// as finished.
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	for key, data := range r.pending {
		if key.issuer == issuer {
			for _, info := range data {
				if info.Order == order {
					return info, true
				}
			}
		}
	}
	return Info{}, false
}

// Pending returns the queue status for an issuer
func (r *runner) Pending(issuer string) map[string][]Info {
	// In case the server restarts, a client with an old order number
	// may get stuck checking for its job to finish, because its order
	// number may be well ahead.
	// To avoid this, order numbers in the future are always marked
	// as finished.
	result := make(map[string][]Info)
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	for key, data := range r.pending {
		if key.issuer == issuer {
			clone := make([]Info, len(data))
			for idx, val := range data {
				clone[idx] = val
			}
			result[key.plugin] = clone
		}
	}
	return result
}

// History returns a historic of completed jobs
func (r *runner) History(issuer string) []Info {
	if ring, ok := r.history[issuer]; ok {
		return ring.slice()
	}
	return nil
}

// Close the channel to the task runner
func (r *runner) Close() {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	close(r.tasks)
}

// Goroutine that runs tasks
func (r *runner) loop() {
	// Environment and config parameters are considered static,
	// they can be read once before entering the loop.
	env := os.Environ()
	if r.Environment != nil {
		env = append(env, r.Environment...)
	}
	// Create a logger
	var stdout, stderr io.Writer
	if r.LogFile == "" {
		stdout = os.Stdout
		stderr = os.Stderr
	} else {
		logger := &lumberjack.Logger{
			Filename:   r.LogFile,
			MaxSize:    r.LogSize,
			MaxBackups: r.LogBackups,
			LocalTime:  false,
		}
		stdout = logger
		stderr = logger
		defer logger.Close()
	}
	// Wait for commands
	for currentTask := range r.tasks {
		if plugin, ok := r.plugin(currentTask.Plugin); ok {
			if err := plugin.Run(currentTask.Plugin, currentTask.Issuer, env, stdout, stderr, currentTask.Args); err != nil {
				log.Printf("Error running task: %+v", err)
				currentTask.Err = err
			}
		}
		r.finished(currentTask)
	}
}

// Returns the plugin for a given id
func (r *runner) plugin(plugin string) (Plugin, bool) {
	r.mutex.RLock()
	defer r.mutex.RUnlock()
	p, ok := r.plugins[plugin]
	// If there is no plugin for the given task, return the default plugin
	// (default plugin has name "")
	if !ok {
		p, ok = r.plugins[""]
	}
	return p, ok
}

// Marks the task as finished, updates counters and maps
func (r *runner) finished(info Info) {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	id := infoKey{issuer: info.Issuer, plugin: info.Plugin}
	if queue, ok := r.pending[id]; ok && len(queue) >= 1 { // Sanity check
		last := len(queue) - 1
		for idx, current := range queue {
			// Move everything one item back, so order is preserved
			if current.Order == info.Order && idx < last {
				copy(queue[idx:], queue[(idx+1):])
				break
			}
		}
		r.pending[id] = queue[:last]
	}
	r.current = info.Order
	history, ok := r.history[info.Issuer]
	if !ok {
		history = &infoRing{Ring: ring.New(HistorySize)}
		r.history[info.Issuer] = history
	}
	info.Until = time.Now()
	history.buffer[history.Push()] = info
}
