package queries

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"regexp"
	"strings"
	"sync"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/tenants"
	lru "github.com/hashicorp/golang-lru"
	"github.com/pkg/errors"
)

// Cache manager
type cacheManager struct {
	switcher  tenants.Switcher
	cache     map[string]*lru.Cache
	cacheSize int
	hashes    []string
	lastError error
	sync.RWMutex
}

// Cached item
type cachedItem struct {
	control chan bool
	err     error
	data    dbtool.Params
}

// Initializes cache
func (c *cacheManager) init() error {
	// Validate the hashes
	validHash := regexp.MustCompile(`^[a-zA-Z]+$`).MatchString
	for _, hash := range c.hashes {
		if !validHash(hash) {
			// Invalid hashes are a cause for failure
			return errors.Errorf("Invalid hash for jsonQuery: %s, must be [a-zA-Z]+", hash)
		}
	}
	// Create the cache
	c.cache = make(map[string]*lru.Cache)
	return nil
}

// Name of the table that stores the entries associated to a hash
func hashTable(hash string) string {
	return fmt.Sprintf("hash%s", strings.ToLower(hash))
}

// Reload iterates over the issuers, purging them all. Not thread safe.
func (c *cacheManager) reload() {
	for issuer, cache := range c.cache {
		cache.Purge()
		store, ok := c.switcher.Switch(issuer)
		if ok {
			for _, hash := range c.hashes {
				stmt := dbtool.Statement{
					SQL:      fmt.Sprintf("DELETE FROM %s", hashTable(hash)),
					Defaults: nil,
				}
				if _, err := store.Exec(context.Background(), stmt, nil); err != nil {
					log.Print("Error purging query cache hash", hash, "of issuer", issuer, ": ", err)
				}
			}
		}
	}
	// Restart the cache altogether so that views are re-inited next time someone posts a parameter.
	c.cache = make(map[string]*lru.Cache)
}

// Add a cached QueryParams to the lru. Saves the hash value in the params, too.
func (c *cacheManager) Add(issuer string, packed dbtool.Params, hashName string) (string, error) {
	// Get the hash for the params and append it
	hkey, err := packed.Hash()
	if err != nil {
		return hkey, err
	}
	packed[hashName] = hkey
	c.Lock()
	cache, ok := c.cache[issuer]
	if !ok {
		cache, err = lru.NewWithEvict(c.cacheSize, func(key, val interface{}) {
			// Wait until item initialization is done
			_ = <-(val.(*cachedItem)).control
			c.evict(issuer, key.(string))
		})
		if err != nil {
			// Not being able to build the cache is a cause for failure.
			c.Unlock()
			return "", errors.Wrap(err, "Failed to allocate cache")
		}
		c.cache[issuer] = cache
	}
	item, wait := c.containsOrNew(cache, hkey, packed)
	c.Unlock()
	if wait != nil {
		// Wait until the hashes have been written to the db
		_ = <-wait
		return hkey, item.err
	}
	if store, ok := c.switcher.Switch(issuer); ok {
		for _, hash := range c.hashes {
			// If the data to add includes a hashable parameter, add it to the indexed database
			if data, ok := packed[hash]; ok {
				if err := c.updateHash(store, hash, hkey, data); err != nil {
					item.err = err
					// Remove calls evict, and evict calls c.evict. No need to call it here.
					// c.evict(issuer, hkey)
					// But we have to close the channel, otherwise cache.Remove will freeze forever.
					close(item.control)
					cache.Remove(hkey)
					return hkey, err
				}
			}
		}
	}
	// Close(item.control) so the next caller for this hash knows we are finished
	close(item.control)
	return hkey, nil
}

// Get the params from the lru, if still there
func (c *cacheManager) Get(issuer, hkey string) (dbtool.Params, error) {
	c.RLock()
	cache, ok := c.cache[issuer]
	c.RUnlock()
	if !ok {
		return nil, errors.Errorf("Cache for issuer %s not yet created", issuer)
	}
	data, ok := cache.Get(hkey)
	if !ok {
		return nil, errors.Errorf("Posted Parameters indexed by %s no longer available for issuer %s", hkey, issuer)
	}
	item := data.(*cachedItem)
	// Wait until the item is ready
	_ = <-item.control
	if item.err != nil {
		return nil, item.err
	}
	return item.data, nil
}

// If the key is not found in cache, add new item. Must be called with the mutex held.
func (c *cacheManager) containsOrNew(cache *lru.Cache, hkey string, packed dbtool.Params) (*cachedItem, chan bool) {
	// If the key is already there, return the item and its wait channel.
	if data, found := cache.Get(hkey); found {
		item := data.(*cachedItem)
		wait := item.control
		return item, wait
	}
	// Otherwise, add the item
	item := &cachedItem{data: packed, control: make(chan bool)}
	cache.Add(hkey, item)
	return item, nil
}

// Evicts from the hash tables the entries indexed by the given key
func (c *cacheManager) evict(issuer, key string) {
	if store, ok := c.switcher.Switch(issuer); ok {
		for _, hash := range c.hashes {
			stmt := dbtool.Statement{
				SQL:      fmt.Sprintf("DELETE FROM %s WHERE hash={hash}", hashTable(hash)),
				Defaults: dbtool.Params{"hash": key},
			}
			if _, err := store.Exec(context.Background(), stmt, nil); err != nil {
				log.Print("Error evicting key ", key, " from hash ", hash, ": ", err)
			}
		}
	}
}

// Update the hash table with the given hkey and value
func (c *cacheManager) updateHash(store dbtool.Store, hash, hkey string, data interface{}) error {
	if reflect.TypeOf(data).Kind() == reflect.Slice {
		paramValue := reflect.ValueOf(data)
		paramLen := paramValue.Len()
		if paramLen > 0 {
			rows := make([]dbtool.Row, 0, paramLen)
			for i := 0; i < paramLen; i++ {
				curr := paramValue.Index(i).Interface()
				rows = append(rows, dbtool.Row{hkey, curr})
			}
			feed := dbtool.NewFeed(
				[]string{"hash", "value"},
				[]string{"string", ""},
				rows,
			)
			defer feed.Close()
			return store.Upsert(context.Background(), feed, hashTable(hash), []string{"value"})
		}
	}
	return nil
}
