package queries

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strings"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// View that operates on ResultSets
type View []json.RawMessage

// ViewStep is the typical view built of a sequence of named steps.
type ViewStep map[string]json.RawMessage

// Single query definition in the query model.
// Query: SQLquery to run
// Alias: alias to rename some parameters. For instance,
// if Alias["pFechaIni"] = "pFechaIniAnterior", then when running
// this query, "pFechaIni" will be set to "pFechaIniAnterior"
// Types: expected result types.
type jsonInnerQuery struct {
	Query   string            `json:"query" xml:"query" yaml:"query"`
	Alias   map[string]string `json:"alias" xml:"alias" yaml:"alias"`
	Types   []string          `json:"types" xml:"types" yaml:"types"`
	Filter  string            `json:"filter" xml:"filter" yaml:"filter"`
	Require string            `json:"require" xml:"require" yaml:"require"`
}

/**
 * Query model: encapsulates the definition of a query that can be
 * serialized to and from JSON, including:
 *
 * - Templates: a query can be used as a template to build similar queries,
 *   with string replacement. Each entry in the templates array is turned into
 *   a new query with the same items, without templates, and with all literals
 *   in the templatye replaced by their string values.
 * - SQL string, with parameters enclosed in { }
 * - Types array, describinf the result types.
 *   (may be empty or nil, not used by the object itself)
 * - Queries: List of (SQL String, Alias map, Types array), in case there are
 *   more than one SQL query in this query object.
 * - Scope: list of tags that control who is allowed to run each query.
 *   Tags must be prefixed by "q:"
 * - Map of default param values
 * - List of Views (may be empty or nil, not used by the object itself)
 * - Filter to apply
 */
type jsonQuery struct {
	Templates map[string]dbtool.Params `json:"templates" xml:"templates" yaml:"templates"`
	Queries   []jsonInnerQuery         `json:"queries" xml:"queries" yaml:"queries"`
	Query     string                   `json:"query" xml:"query" yaml:"query"`
	Alias     map[string]string        `json:"alias" xml:"alias" yaml:"alias"`
	Scope     []string                 `json:"scope" xml:"scope" yaml:"scope"`
	Types     []string                 `json:"types" xml:"types" yaml:"types"`
	Params    dbtool.Params            `json:"params" xml:"params" yaml:"params"`
	Views     map[string]View          `json:"views" xml:"views" yaml:"views"`
	Filter    string                   `json:"filter" xml:"filter" yaml:"filter"`
}

// ResultSet returned by a managed query
type ResultSet struct {
	Names []string     `json:"names"`
	Types []string     `json:"types"`
	Rows  []dbtool.Row `json:"rows"`
}

// Result of running a managed query: An object that can Stream itself to JSON
type Result struct {
	Query string
	Views map[string]View
	// cursor management fields
	cache    []ResultSet
	queries  []jsonInnerQuery
	packed   dbtool.Params
	defaults dbtool.Params
	task     func(query jsonInnerQuery, aliased, defaults dbtool.Params) (dbtool.Feed, error)
}

// Apply the aliases to a set of QueryParams
func (q *jsonInnerQuery) aliased(packed, defaults dbtool.Params, prev []ResultSet) (dbtool.Params, error) {
	// If there are no alias, just return the input data
	if q.Alias == nil || len(q.Alias) <= 0 {
		return packed, nil
	}
	// Clone the packed params
	aliased := make(dbtool.Params, len(packed)+len(q.Alias))
	for k, v := range packed {
		aliased[k] = v
	}
	// Apply the aliases
	for k, v := range q.Alias {
		// Recognize the special alias types:
		// field = list:[set]:[colum] - Retrieve list from a set
		// field = value:[set]:[colum]:[row] - Retrieve value from a set
		// from the previous resultsets
		if prev != nil && strings.HasPrefix(v, "list:") {
			var set, column int
			if n, err := fmt.Sscanf(v, "list:%d:%d", &set, &column); err != nil {
				return packed, errors.Wrapf(err, "Failed to parse list %s", v)
			} else if n < 2 {
				return packed, errors.Errorf("Invalid list alias %s. Valid format is list:<set>:<column>", v)
			}
			data, err := resultList(prev, set, column)
			if err != nil {
				return packed, err
			}
			aliased[k] = data
		} else if prev != nil && strings.HasPrefix(v, "value:") {
			var set, column, row int
			if n, err := fmt.Sscanf(v, "value:%d:%d:%d", &set, &column, &row); err != nil {
				return packed, errors.Wrapf(err, "Failed to parse value %s", v)
			} else if n < 3 {
				return packed, errors.Errorf("Invalid value alias %s. Valid format is value:<set>:<column>", v)
			}
			data, err := resultList(prev, set, column)
			if err != nil {
				return packed, err
			}
			if len(data) > row {
				aliased[k] = data[row]
			} else {
				aliased[k] = nil
			}
		} else if data, ok := aliased[v]; ok {
			// If not a recognized format, the alias name is just
			// a different field.
			aliased[k] = data
		} else if data, ok := defaults[v]; ok {
			// In case the aliased field is not defined, but a default
			aliased[k] = data
		}
	}
	// Return the aliased data
	return aliased, nil
}

// clones an innerQuery
func (q *jsonInnerQuery) clone() jsonInnerQuery {
	return jsonInnerQuery{
		Query:   q.Query,
		Alias:   q.Alias,
		Types:   q.Types,
		Filter:  q.Filter,
		Require: q.Require,
	}
}

// Makes a jsonInnerQuery from the values embedded in the query object
func (q *jsonQuery) innerQuery() jsonInnerQuery {
	return jsonInnerQuery{
		Query:  q.Query,
		Types:  q.Types,
		Filter: q.Filter,
		Alias:  q.Alias,
	}
}

// Clones a jsonQuery
func (q *jsonQuery) clone() jsonQuery {
	result := jsonQuery{
		// Inmutable fields, or fields that cannot be templated, are shallow copied
		Query:  q.Query,
		Alias:  q.Alias,
		Scope:  q.Scope,
		Types:  q.Types,
		Params: q.Params,
		Views:  q.Views,
		Filter: q.Filter,
	}
	// Params that can be templated are deep copied
	if q.Queries != nil && len(q.Queries) > 0 {
		cloneQueries := make([]jsonInnerQuery, 0, len(q.Queries))
		for _, inner := range q.Queries {
			cloneQueries = append(cloneQueries, inner.clone())
		}
		result.Queries = cloneQueries
	}
	return result
}

// Replicates the query, for each template.
func (q *jsonQuery) replicate(name string, formatter dbtool.Formatter, globalParams dbtool.Params, provides []string) map[string]jsonQuery {
	templates := q.Templates
	if templates == nil || len(templates) == 0 {
		// If no templates and no globalParams, no need to interpolate
		if globalParams == nil || len(globalParams) == 0 {
			return nil
		}
		// There are no templates but still we have globalParams.
		// Interpolate using the globalParams.
		templates = make(map[string]dbtool.Params, 1)
		templates[name] = nil
	}
	result := make(map[string]jsonQuery, len(templates))
	for name, params := range templates {
		params = params.Merge(globalParams)
		clone := q.clone()
		if clone.Query != "" {
			if query, err := dbtool.Interpolate(q.Query, params, formatter); err == nil {
				clone.Query = query
			}
		}
		if clone.Filter != "" {
			if filter, err := dbtool.Interpolate(q.Filter, params, formatter); err == nil {
				clone.Filter = filter
			}
		}
		if clone.Queries != nil && len(clone.Queries) > 0 {
			for index, inner := range clone.Queries {
				if inner.Require != "" {
					if require, err := dbtool.Interpolate(inner.Require, params, formatter); err == nil {
						inner.Require = require
						if req, ok := satisfied([]string{require}, provides); !ok {
							log.Print("Skipping subquery ", index, " of ", name, " because requirement ", req, " is not satisifed")
							inner.Query = ""
						}
					}
				}
				if inner.Query != "" {
					if query, err := dbtool.Interpolate(inner.Query, params, formatter); err == nil {
						inner.Query = query
					}
					if inner.Filter != "" {
						if filter, err := dbtool.Interpolate(inner.Filter, params, formatter); err == nil {
							inner.Filter = filter
						}
					}
				}
				clone.Queries[index] = inner
			}
		}
		result[name] = clone
	}
	return result
}

func (q *jsonQuery) mergeDefaults(defaults jsonQuery) {
	// Merge the query params with the defaults
	params := defaults.Params
	if q.Params == nil || len(q.Params) == 0 {
		q.Params = params
	} else {
		if params != nil && len(params) > 0 {
			newParams := make(dbtool.Params)
			// Set the default params first
			for k, v := range params {
				newParams[k] = v
			}
			// Add - overwrite with query - specific params
			for k, v := range q.Params {
				newParams[k] = v
			}
			// Replace the query params with the new set.
			q.Params = newParams
		}
	}
	// Merge the audience with the defaults
	scope := defaults.Scope
	if q.Scope == nil || len(q.Scope) <= 0 {
		q.Scope = scope
	} else {
		if scope != nil && len(scope) > 0 {
			q.Scope = mergeSets(scope, q.Scope)
		}
	}
}

// Merge two sets of data, without duplicates
func mergeSets(a, b []string) []string {
	// Build a map with the first set
	scratch := make(map[string]struct{})
	for _, k := range a {
		scratch[k] = struct{}{}
	}
	// Add the second set
	for _, k := range b {
		scratch[k] = struct{}{}
	}
	// Serialize to array
	vars := make([]string, 0, len(scratch))
	for k := range scratch {
		vars = append(vars, k)
	}
	return vars
}

// Merge and expand views with default views
func (q *jsonQuery) mergeViews(defaultViews map[string]View) {
	for key, view := range q.Views {
		aliased := false
		// Expand views
		cursor, top := 0, len(view)
		for cursor < top {
			step := view[cursor]
			cursor++
			// Check the view is not a default view,
			// i.e. it has object steps.
			typedStep := make(ViewStep)
			if err := json.Unmarshal(step, &typedStep); err != nil {
				// This is not a sequence of steps, but something
				// else... it makes no sense to continue.
				break
			}
			// Check the view has a name
			nameJSON, ok := typedStep["name"]
			if !ok {
				continue
			}
			// Check the JSON name is actually a string
			var name string
			if err := json.Unmarshal(nameJSON, &name); err != nil {
				continue
			}
			// Check the name matches a local or global view
			var alias View
			if localAlias, ok := q.Views[name]; ok {
				alias = localAlias
			} else if defaultViews == nil {
				continue
			} else if globAlias, ok := defaultViews[name]; ok {
				alias = globAlias
			} else {
				continue
			}
			// This particular view step is an alias!
			aliased = true
			// We have to replace it with the aliased steps
			newView := make(View, 0, len(view)+len(alias))
			for i := 0; i < cursor-1; i++ {
				newView = append(newView, view[i])
			}
			for i := 0; i < len(alias); i++ {
				newView = append(newView, alias[i])
			}
			for i := cursor; i < top; i++ {
				newView = append(newView, view[i])
			}
			view = newView
			// Return the cursor to the first position within
			// the alias, in case it has more aliases inside...
			cursor--
			top = len(view)
		}
		// If the view was aliased, update it
		if aliased {
			q.Views[key] = view
		}
	}
}

// Assigns default values (NULL) for every alias
func (q *jsonQuery) aliasToDefault() {
	// Any alias creates a default param with value null
	if q.Alias != nil {
		for alias := range q.Alias {
			if _, ok := q.Params[alias]; !ok {
				q.Params[alias] = nil
			}
		}
	}
	// Alias in subqueries create params too, in parent query
	if q.Queries != nil {
		for _, i := range q.Queries {
			if i.Alias != nil {
				for alias := range i.Alias {
					if _, ok := q.Params[alias]; !ok {
						q.Params[alias] = nil
					}
				}
			}
		}
	}
}

// Build a list of results from a ResultSet
func resultList(sets []ResultSet, set, column int) ([]interface{}, error) {
	if len(sets) <= set {
		err := errors.Errorf("Alias reference to set %v is wrong (not enough sets)", set)
		return nil, err
	}
	source := sets[set]
	if len(source.Names) <= column {
		err := errors.Errorf("Alias reference to col %v is wrong (not enough columns)", column)
		return nil, err
	}
	data := make([]interface{}, 0, len(source.Rows)+1)
	for _, row := range source.Rows {
		data = append(data, row[column])
	}
	if len(data) <= 0 {
		data = append(data, nil)
	}
	return data, nil
}

// Expand filters
func (q *jsonQuery) expandFilters(filters map[string]filter, globalParams dbtool.Params) error {
	// update innerQuery filters
	queries := q.Queries
	hasQueries := (queries != nil && len(queries) > 0)
	if !hasQueries {
		queries = []jsonInnerQuery{q.innerQuery()}
	} else {
		for index, query := range queries {
			if query.Filter == "" {
				queries[index].Filter = q.Filter
			}
		}
	}
	// Build the string updater map
	updaters := make(map[string]func(s string) string)
	params := q.Params.Merge(globalParams)
	for _, query := range queries {
		filterName := query.Filter
		if filterName != "" {
			updater, ok := updaters[filterName]
			if !ok {
				currentFilter, ok := filters[filterName]
				if !ok {
					return errors.Errorf("Filter %s is not defined", filterName)
				}
				params, updater = currentFilter.updater(params, true)
				updaters[filterName] = updater
			}
		}
	}
	q.Params = params
	// Update the queries
	if hasQueries {
		for index, query := range queries {
			if query.Filter != "" {
				q.Queries[index].Filter = query.Filter
				q.Queries[index].Query = updaters[query.Filter](query.Query)
			}
		}
	} else if q.Filter != "" {
		q.Query = updaters[q.Filter](q.Query)
	}
	return nil
}

func (r *Result) nextFeed(index int, innerQuery jsonInnerQuery) (dbtool.Feed, error) {
	// If the query is skipped because some of the requirements are not met,
	// just insert an empty result so that views that depend on the order of
	// subqueries still work.
	if innerQuery.Query == "" {
		return dbtool.NewFeed(nil, nil, nil), nil
	}
	// If query text is "repeat", repeat the previous query.
	// useful together with "alias" to build comparable series
	if innerQuery.Query == "repeat" {
		if index <= 0 {
			return dbtool.Feed{}, errors.New("Error: Trying to repeat the first query in a set")
		}
		// Look for a previous query != "repeat", in case
		// we repeat the query several times in a row
		prevIndex := index - 1
		for ; prevIndex >= 0; prevIndex-- {
			if r.queries[prevIndex].Query != "repeat" {
				break
			}
		}
		if prevIndex < 0 {
			return dbtool.Feed{}, errors.New("Error: All previous queries in the set are repeated")
		}
		// Once we arrived to a proper previous query, repeat it.
		newQuery := r.queries[prevIndex]
		newQuery.Alias = innerQuery.Alias
		innerQuery = newQuery
	}
	// Resolve aliases
	aliased, err := innerQuery.aliased(r.packed, r.defaults, r.cache)
	if err != nil {
		return dbtool.Feed{}, err
	}
	// And run the query!
	current, err := r.task(innerQuery, aliased, r.defaults)
	if err != nil {
		return dbtool.Feed{}, err
	}
	// Collect the first result of the feed, for the cache
	cursor := current.Cursor
	if cursor.Next() {
		batch := cursor.Batch()
		r.cache[index] = ResultSet{Names: current.Names, Types: current.Types, Rows: batch}
		current.Cursor = &memoCursor{memo: batch, Cursor: cursor}
	} else {
		// Add at least an empty list to the cache, so queries that
		// depend on previous queries (like tops) do not break
		r.cache[index] = ResultSet{Names: current.Names, Types: current.Types, Rows: nil}
	}
	return current, nil
}

// Stream implements saferest.Streamer interface
func (r *Result) Stream(w io.Writer) error {
	query, err := json.Marshal(r.Query)
	if err != nil {
		return errors.Wrapf(err, "Failed to marshal query %v", r.Query)
	}
	views, err := json.Marshal(r.Views)
	if err != nil {
		return errors.Wrapf(err, "Failed to marshal views %v", r.Views)
	}
	if _, err := w.Write([]byte(fmt.Sprintf("{ \"query\": %s, \"views\": %s, \"results\": [", string(query), string(views)))); err != nil {
		return errors.Wrap(err, "Failed to write Result header")
	}
	sep := ""
	for index, innerQuery := range r.queries {
		result, err := r.nextFeed(index, innerQuery)
		if err != nil {
			return err
		}
		err = func() error {
			defer result.Close()
			if _, err := w.Write([]byte(sep)); err != nil {
				return errors.Wrap(err, "Failed to write ResultSet separator")
			}
			sep = ","
			if err := result.Stream(w); err != nil {
				return err
			}
			return nil
		}()
		if err != nil {
			return err
		}
	}
	w.Write([]byte("]}"))
	return nil
}
