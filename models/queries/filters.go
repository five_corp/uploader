package queries

import (
	"fmt"
	"log"
	"regexp"
	"strings"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// Filter descriptor. Defines the criteria to filter a query:
// - Field: the name of the field to filter on.
// - Alias: Name of field to use in the "AS ..." part of a select
// - Param: the name of the parameter that contains the allowed values
// - Default: default value for the params.
// - Force: true to check the field is not null or empty string.
// - Hash: true if the value of this field is stored in a hash table.
type filterField struct {
	Field   string      `json:"field" xml:"field" yaml:"field"`
	Alias   string      `json:"alias" xml:"alias" yaml:"alias"`
	Param   string      `json:"param" xml:"param" yaml:"param"`
	Default interface{} `json:"default" xml:"default" yaml:"default"`
	Force   bool        `json:"force" xml:"force" yaml:"force"`
	Hash    bool        `json:"hash" xml:"hash" yaml:"hash"`
}

type filter []filterField
type matcher func(string) bool

const validLen = 32

// Validates field names
func newValidator() matcher {
	re := regexp.MustCompile("^[a-zA-Z][a-zA-Z0-9_]+$")
	return func(s string) bool {
		return re.Match([]byte(s))
	}
}

// Checks if the filed name, alias and param are valid SQL identifiers.
// Currently only allows identifiers built of alphanumeric or _
func (f filterField) validate(name string) error {
	validID := newValidator()
	if !validID(f.Field) {
		return errors.Errorf("Field '%s' of query %s is not a valid DB identifier", f.Field, name)
	}
	if f.Param != "" && !validID(f.Param) {
		return errors.Errorf("Param '%s' of query %s is not a valid DB identifier", f.Param, name)
	}
	if len(f.Field) > validLen {
		return errors.Errorf("The name of Field '%s' of filter %s is too long", f.Field, name)
	}
	if f.Param != "" && len(f.Param) > validLen {
		return errors.Errorf("The name of param '%s' of filter %s is too long", f.Param, name)
	}
	if f.Alias != "" {
		if !validID(f.Alias) {
			return errors.Errorf("Alias '%s' of query %s is not a valid DB identifier", f.Alias, name)
		}
		if len(f.Alias) > validLen {
			return errors.Errorf("The name of Alias '%s' of filter %s is too long", f.Alias, name)
		}
	}
	return nil
}

// Formats the field for use in a "select" clause
func (f filterField) asSelect(isQuery bool) string {
	field := fmt.Sprintf("v.%s", f.Field)
	// If using this from a view, do not rename fields
	alias := f.Field
	if isQuery {
		// If using from a query, rename fields with alias, and also
		// add 0E0 to numeric fields, to return numbers instead of strings.
		alias = f.Alias
		if strings.HasPrefix(alias, "n_") {
			field = fmt.Sprintf("%s+0E0", field)
		}
	}
	return fmt.Sprintf("%s as %s", field, alias)
}

// Formats the field for use in a "select" clause
func (f filterField) asGroup(isQuery bool) string {
	if isQuery {
		return f.Alias
	}
	return fmt.Sprintf("v.%s", f.Field)
}

// Formats the field for use in a "where" clause
func (f filterField) asWhere() string {
	// If no conditions to apply, return an empty filter.
	if (f.Param == "" || f.Default == nil) && !f.Force {
		return ""
	}
	// If there is a param name, filter by it.
	wheres := make([]string, 0, 3)
	if f.Param != "" && f.Default != nil {
		wheres = append(wheres, fmt.Sprintf("{v.%s=%s}", f.Field, f.Param))
	}
	// If Force == true, check the field is not empty
	if f.Force {
		wheres = append(wheres, fmt.Sprintf("v.%s IS NOT NULL", f.Field))
		wheres = append(wheres, fmt.Sprintf("v.%s != ''", f.Field))
	}
	return fmt.Sprintf("(%s)", strings.Join(wheres, ") AND ("))
}

// Format the field for use in a "join" clause
func (f filterField) asJoinOn(alias string) string {
	// If no conditions to apply, return an empty filter.
	if f.Param == "" || f.Field == "" {
		log.Print("asJoinOn: field ", f, " missing Name or Param")
		return ""
	}
	// Example: left join hashpempresa as p on ((v.empresa = p.value) and (p.hash = pPosted()))
	return fmt.Sprintf("left join hash%s as %s on ((v.%s = %s.value) and (%s.hash = pPosted()))",
		strings.ToLower(f.Param), alias, f.Field, alias, alias)
}

// Format the field for use in a "where" clause, after a join
func (f filterField) asJoinWhere(alias string) string {
	// If no conditions to apply, return an empty filter.
	if f.Param == "" {
		log.Print("asJoinWhere: field ", f, " missing Param")
		return ""
	}
	// Example: ((pEmpresa() = 0) or (p.value IS NOT NULL))
	return fmt.Sprintf("((%s() = 0) or (%s.value is not null))", f.Param, alias)
}

// Validates all the fields in the filter
func (inputFields filter) validate(name string) error {
	validID := newValidator()
	if !validID(name) {
		return errors.Errorf("The name of the filter '%s' is not a valid DB identifier", name)
	}
	if len(name) > validLen {
		return errors.Errorf("The name of the filter '%s' is too long", name)
	}
	if inputFields != nil && len(inputFields) > 0 {
		for _, f := range inputFields {
			if err := f.validate(name); err != nil {
				return err
			}
		}
	}
	return nil
}

// Generates a fieldset from the given filter
func (inputFields filter) fieldset(isQuery bool) string {
	return strings.Join(inputFields._pick("0 AS empty_fieldset", func(f filterField) string {
		return f.asSelect(isQuery)
	}), ", ")
}

// Generates a groupset from the given filter
func (inputFields filter) groupset(isQuery bool) string {
	return strings.Join(inputFields._pick("empty_fieldset", func(f filterField) string {
		return f.asGroup(isQuery)
	}), ", ")
}

// Generates a groupset from the given filter
func (inputFields filter) filtered(isQuery bool) string {
	return fmt.Sprintf("(%s)",
		strings.Join(inputFields._pick("true", func(f filterField) string {
			return f.asWhere()
		}), " AND "))
}

// Generates a hashjoin from the given filter
func (inputFields filter) hashjoin(isQuery bool) string {
	if isQuery {
		// No need to join if this is a query, we can use
		// the value from the parameters instead of the hash table.
		return ""
	}
	return strings.Join(inputFields._hashes(func(f filterField, alias string) string {
		return f.asJoinOn(alias)
	}), " ")
}

// Generates a hashfields from the given filter
func (inputFields filter) hashfields(isQuery bool) string {
	return strings.Join(inputFields._hashes(func(f filterField, alias string) string {
		return f.asSelect(isQuery)
	}), ", ")
}

// Generates a hashwhere from the given filter
func (inputFields filter) hashgroup(isQuery bool) string {
	return strings.Join(inputFields._hashes(func(f filterField, alias string) string {
		return f.asGroup(isQuery)
	}), ", ")
}

// Generates a hashwhere from the given filter
func (inputFields filter) hashwhere(isQuery bool) string {
	return strings.Join(inputFields._hashes(func(f filterField, alias string) string {
		if isQuery {
			return f.asWhere()
		}
		return f.asJoinWhere(alias)
	}), " and ")
}

// Builds a string of fields
func (inputFields filter) _pick(empty string, formatter func(filterField) string) []string {
	if inputFields == nil || len(inputFields) <= 0 {
		// Always return something in the fieldset, so we don't
		// have problems with commas in SQL statements (e.g.
		// "select this, that, %fieldset% from table as v" would
		// yield an error if fieldset is empty, because of the comma)
		return []string{empty}
	}
	selects := make([]string, 0, len(inputFields))
	for _, field := range inputFields {
		if field.Alias != "" && !field.Hash {
			if current := formatter(field); current != "" {
				selects = append(selects, current)
			}
		}
	}
	if len(selects) <= 0 {
		return []string{empty}
	}
	return selects
}

// Builds a string of hash fields
func (inputFields filter) _hashes(formatter func(filterField, string) string) []string {
	if inputFields == nil || len(inputFields) <= 0 {
		// You should not use hashes unless you know there are
		// actually hashes.
		return []string{" * ERROR * No fields in View requiring hashes "}
	}
	text := make([]string, 0, len(inputFields))
	for index, field := range inputFields {
		if field.Alias != "" && field.Hash {
			alias := fmt.Sprintf("p%d", index)
			if current := formatter(field, alias); current != "" {
				text = append(text, current)
			}
		}
	}
	if len(text) <= 0 {
		return []string{" * ERROR * No hashes in View requiring hashes "}
	}
	return text
}

// Builds an updater capable of replacing placeholders in queries.
func (inputFields filter) updater(params dbtool.Params, isQuery bool) (dbtool.Params, func(string) string) {
	if inputFields == nil || len(inputFields) <= 0 {
		// nothing to replace
		return params, func(s string) string {
			return s
		}
	}
	// Make sure there is a default value in the query for every parameter,
	// otherwise they will be discarded when running the query
	for _, f := range inputFields {
		// If field is searchable (Param != ""), add to where-set.
		if f.Param != "" && f.Default != nil {
			if _, ok := params[f.Param]; !ok {
				params[f.Param] = f.Default
			}
		}
	}
	// Turn the fields and conditions into SQL
	replacements := map[string]string{
		"%fieldset%":   inputFields.fieldset(isQuery),
		"%groupset%":   inputFields.groupset(isQuery),
		"%filtered%":   inputFields.filtered(isQuery),
		"%hashjoin%":   inputFields.hashjoin(isQuery),
		"%hashfields%": inputFields.hashfields(isQuery),
		"%hashgroup%":  inputFields.hashgroup(isQuery),
		"%hashwhere%":  inputFields.hashwhere(isQuery),
	}
	// Return a replacer for placeholders
	return params, func(s string) string {
		replaced := s
		for key, val := range replacements {
			replaced = strings.Replace(replaced, key, val, -1)
		}
		return replaced
	}
}

// Merge all filters into one
func mergeFilters(filters []string, filterMap map[string]filter) (filter, error) {
	if filters == nil || len(filters) <= 0 {
		return nil, nil
	}
	filterFields := make(filter, 0, 16)
	filterOrder := make(map[string]bool)
	for _, fName := range filters {
		f, ok := filterMap[fName]
		if !ok {
			return nil, errors.Errorf("Undefined filter %s", fName)
		}
		for _, field := range f {
			if field.Field != "" && !filterOrder[field.Field] {
				filterFields = append(filterFields, field)
				filterOrder[field.Field] = true
			}
		}
	}
	return filterFields, nil
}
