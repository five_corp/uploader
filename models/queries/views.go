package queries

import (
	"context"
	"fmt"
	"log"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// View descriptor. Defines a view that may support a given set of filters.
// - Templates: a view can be used as a template to build similar views,
//   with string replacement. Each entry in the templates array is turned into
//   a new view with the same items, without templates, and with all literals
//   in the templatye replaced by their string values.
// - Query: the SQL of the view.
// - Filters: the name of the filters it supports
type view struct {
	Templates map[string]dbtool.Params `json:"templates" xml:"templates" yaml:"templates"`
	Query     string                   `json:"query" xml:"query" yaml:"query"`
	Filters   []string                 `json:"filters" xml:"filters" yaml:"filters"`
	Requires  []string                 `json:"requires" xml:"requires" yaml:"requires"`
}

func deepCopy(input []string) []string {
	if input == nil || len(input) <= 0 {
		return input
	}
	output := make([]string, 0, len(input))
	return append(output, input...)
}

// Clone a view
func (v *view) clone() view {
	return view{
		Query:    v.Query,
		Filters:  deepCopy(v.Filters),
		Requires: deepCopy(v.Requires),
	}
}

// If a view is templated, expand the templates and return final views
func (v *view) replicate(name string, formatter dbtool.Formatter, globalParams dbtool.Params, provides []string) map[string]view {
	templates := v.Templates
	if templates == nil || len(templates) <= 0 {
		if globalParams == nil || len(globalParams) == 0 {
			return nil
		}
		// There are no templates but still we have globalParams.
		// Interpolate using the globalParams.
		templates = make(map[string]dbtool.Params, 1)
		templates[name] = nil
	}
	result := make(map[string]view, len(templates))
	for name, params := range templates {
		clone, params := v.clone(), params.Merge(globalParams)
		if clone.Requires != nil && len(clone.Requires) > 0 {
			for index, requirement := range clone.Requires {
				if r, err := dbtool.Interpolate(requirement, params, formatter); err == nil {
					clone.Requires[index] = r
				}
			}
		}
		if req, ok := satisfied(clone.Requires, provides); !ok {
			log.Print("Skipping view ", name, " since requirement ", req, " is not satisfied")
			continue
		}
		if query, err := dbtool.Interpolate(clone.Query, params, formatter); err == nil {
			clone.Query = query
		}
		if clone.Filters != nil && len(clone.Filters) > 0 {
			for index, filter := range clone.Filters {
				if f, err := dbtool.Interpolate(filter, params, formatter); err == nil {
					clone.Filters[index] = f
				}
			}
		}
		result[name] = clone
	}
	return result
}

// Builds an updater capable of replacing placeholders in queries.
func (v *view) update(ctx context.Context, name string, store dbtool.Store, filterMap map[string]filter, params dbtool.Params) error {
	validID := newValidator()
	if !validID(name) {
		return errors.Errorf("Name '%s' is not valid for a view", name)
	}
	if len(name) > validLen {
		return errors.Errorf("Name '%s' is too long for a view", name)
	}
	// Merge all filters into one, and get the combined fieldset
	filter, err := mergeFilters(v.Filters, filterMap)
	if err != nil {
		return err
	}
	// Updated params are no use, this is not a regular query but a view.
	// Only interpolation is useful, not param substitution.
	_, updater := filter.updater(params, false)
	query := updater(v.Query)
	// Remove the view, if it already exists.
	stmt := dbtool.Statement{SQL: fmt.Sprintf("DROP VIEW IF EXISTS %s", name)}
	if _, err := store.Exec(ctx, stmt, nil); err != nil {
		return err
	}
	// Create it anew
	stmt = dbtool.Statement{SQL: fmt.Sprintf("CREATE VIEW %s AS %s", name, query)}
	if _, err := store.Exec(ctx, stmt, nil); err != nil {
		// Errors in creating a view will not be considered critical,
		// because they may depend on tables that may not be present
		// in a particular schema. If it is not used, that would be fine.
		log.Print("Error creating view ", name, ": ", err)
	}
	return nil
}
