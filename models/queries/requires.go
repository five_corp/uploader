package queries

import (
	"strings"
)

func satisfied(requires, provides []string) (string, bool) {
	if requires == nil || len(requires) <= 0 {
		return "", true
	}
	if provides == nil || len(provides) <= 0 {
		return "", false
	}
outerLoop:
	for _, req := range requires {
		for _, prov := range provides {
			if strings.EqualFold(req, prov) {
				continue outerLoop
			}
		}
		return req, false
	}
	return "", true
}
