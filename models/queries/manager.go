package queries

import (
	"context"
	"log"
	"strings"

	"github.com/pkg/errors"

	"bitbucket.org/five_corp/saferest"
	"bitbucket.org/five_corp/uploader/models/dbtool"
	"bitbucket.org/five_corp/uploader/models/tenants"
)

// BatchSize of requests to database
const BatchSize = 100

// Section of the database schema
const querySection = "queries"

// Manager for a set of queries, with a parameter cache.
type Manager interface {
	// Adds a new set of params to the cache.
	Add(issuer string, packed dbtool.Params, hashName string) (string, error)
	// Gets a set of parameters from the cache
	Get(issuer, hash string) (packed dbtool.Params, err error)
	// Enumerate queries and views
	Queries() map[string][]string
	// Find and run a query
	Run(ctx context.Context, issuer, name string, packed dbtool.Params, limit int) (*Result, error)
	// Scope and default query parameters
	Scope(name string) ([]string, dbtool.Params, error)
	// Reload the queries file
	Reload() error
}

// Import descriptor
type importSet struct {
	Version  int           `json:"version" xml:"version" yaml:"version"`
	Views    []string      `json:"views" xml:"views" yaml:"views"`
	Filters  []string      `json:"filters" xml:"filters" yaml:"filters"`
	Queries  []string      `json:"queries" xml:"queries" yaml:"queries"`
	Params   dbtool.Params `json:"params" xml:"params" yaml:"params"`
	Provides []string      `json:"provides" xml:"provides" yaml:"provides"`
}

// Query manager
type queryManager struct {
	cacheManager
	folder         string
	fileWithoutExt string
	queries        map[string]jsonQuery
	throttle       chan bool
	tStore         tenants.Store
}

// NewManager creates a new query manager.
//
// The hashes deserve an explanation. It is an string of field names that are
// stored in particular tables in the database. For each hash, the manager expects
// a table in the database with the name "hash$Hashname", and two columns,
// "hash char(64)" and "value (whatever)".
//
// For instance, if one hash is named "principal" and can hold an array of strings,
// then there should be a table "hashPrincipal ( hash CHAR(64), value VARCHAR(255) )".
//
// When a set of packed parameters is added to the cache using a key, the manager
// looks for any parameter named after a hash. If it exists and is a slice, then every
// value of the slice is stored, along with the key, in the "value" and "hash"
// columns of the corresponding table.
//
// This way, the query can then make use of this kind of tables to read the
// value of the parameter from inside a view. For instance, back into our example
// with a hash named "principal":
//
// SELECT * FROM t INNER JOIN hashPrincipal AS p ON (t.principal=p.value AND p.hash=@key)
//
// The key has to be provided as a session parameter to the query, too.
// You can hash the params to get the key, then store the key along with the params,
// and make that a session variable for the query:
//
// Assume your queries define "key" as a session variable, and expect it to
// contain the hash value:
//
// hkey, _ := params.Hash()
// params["key"] = hkey
func NewManager(folder, fileWithoutExt string, switcher tenants.Switcher, hashes []string, cacheSize, throttle int) (Manager, error) {
	// Create the handler and reload queries
	manager := &queryManager{
		cacheManager: cacheManager{
			switcher:  switcher,
			hashes:    hashes,
			lastError: nil,
			cacheSize: cacheSize,
		},
		folder:         folder,
		fileWithoutExt: fileWithoutExt,
		queries:        nil,
	}
	// Initialize the embedded cache
	if err := manager.init(); err != nil {
		return nil, err
	}
	// Initialize the bucket
	if throttle > 0 {
		manager.throttle = make(chan bool, throttle)
	}
	//manager.bucket.init(throttle)
	// Load the file. The models do not depend on the issuer, the issuer param will be ignored
	if err := manager.Reload(); err != nil {
		// Not being able to load the file is NOT a cause for failure,
		// because we may write the file later. So just save the error,
		// and go on.
		log.Print("ERROR models::NewQueryManager: Failed to load query file ", fileWithoutExt, " from ", folder, ": ", err)
	}
	return manager, nil
}

type viewItem struct {
	name string
	view view
}

// sorts views based on their dependencies.
func sortViews(views map[string]view) []viewItem {
	sortedViews := make([]viewItem, 0, len(views))
	for name, view := range views {
		sortedViews = append(sortedViews, viewItem{name: name, view: view})
	}
	// We must sort views so that base views are created first
	// and dependent views are created later.
	iterations, current := len(sortedViews), 0
outerLoop:
	for current < len(sortedViews) {
		currView := sortedViews[current]
		for j := 0; j < current; j++ {
			// If any view in position j < current contains a reference
			// to view in position current, then swap them.
			if strings.Contains(sortedViews[j].view.Query, currView.name) {
				sortedViews[current], sortedViews[j] = sortedViews[j], sortedViews[current]
				if iterations--; iterations > 0 {
					current = j // start over from the most recently swapped position
					continue outerLoop
				} else {
					break outerLoop
				}
			}
		}
		current++
	}
	return sortedViews
}

// Reload the queries file
func (h *queryManager) Reload() error {
	h.Lock()
	defer h.Unlock()
	log.Print("models::queryManager::Reload: Reloading query cache")
	// Load the import file
	imports := importSet{}
	result := make(map[string]jsonQuery)
	filters := make(map[string]filter)
	if err := h.importFile(h.fileWithoutExt, &imports); err != nil {
		h.queries = nil
		return err
	}
	// Import and expand queries
	if imports.Queries != nil && len(imports.Queries) > 0 {
		for _, importFile := range imports.Queries {
			// Load imported file
			imported := make(map[string]jsonQuery)
			if err := h.importFile(importFile, &imported); err != nil {
				log.Print("Error loading query file ", importFile)
				return err
			}
			// concatenate queries
			for name, query := range imported {
				if expanded := query.replicate(name, h.switcher.Formatter(), imports.Params, imports.Provides); expanded == nil {
					// no templates, just store the query
					result[name] = query
				} else {
					// Query is templated, store just results
					for tname, tquery := range expanded {
						result[tname] = tquery
					}
				}
			}
		}
	}
	// Import filters
	if imports.Filters != nil && len(imports.Filters) > 0 {
		for _, importFile := range imports.Filters {
			// Load imported file
			imported := make(map[string]filter)
			if err := h.importFile(importFile, &imported); err != nil {
				log.Print("Error loading filter file ", importFile)
				return err
			}
			// concatenate filters
			for k, v := range imported {
				if err := v.validate(k); err != nil {
					return err
				}
				filters[k] = v
			}
		}
	}
	// Import and expand views.
	views := make([]viewItem, 0, 128)
	if imports.Views != nil && len(imports.Views) > 0 {
		for _, importFile := range imports.Views {
			// Load imported file
			imported := make(map[string]view)
			if err := h.importFile(importFile, &imported); err != nil {
				log.Print("Error loading view file ", importFile)
				return err
			}
			// concatenate views
			fileViews := make(map[string]view)
			for name, view := range imported {
				if expanded := view.replicate(name, h.switcher.Formatter(), imports.Params, imports.Provides); expanded == nil {
					// no templates, just store the query
					fileViews[name] = view
				} else {
					for tname, tview := range expanded {
						fileViews[tname] = tview
					}
				}
			}
			// Sort views in file, so they are created in the
			// proper order, following dependencies
			if len(fileViews) > 0 {
				views = append(views, sortViews(fileViews)...)
			}
		}
	}
	// If there are views to create, go for it.
	h.switcher.Subscribe("views", tenants.Create|tenants.Bootstrap, func(etype tenants.EventType, issuer string, tenant tenants.Tenant) {
		h.createViews(issuer, views, imports.Version, filters, imports.Params)
	})
	// if there is a "DEFAULTS" query, use it
	// for parameters, sessionVars and scope of other queries
	defaults, defaultOK := result["DEFAULTS"]
	for label, query := range result {
		if label == "DEFAULTS" {
			continue
		}
		if defaultOK {
			query.mergeDefaults(defaults)
			query.mergeViews(defaults.Views)
		} else {
			// Still need to merge views, some views may
			// reference previous views in the same query.
			query.mergeViews(nil)
		}
		// Expand filters
		if imports.Params == nil {
			imports.Params = make(dbtool.Params)
		}
		query.expandFilters(filters, imports.Params)
		// Build defaults from aliases, otherwise
		// queries with aliases will not run because parameters
		// will be removed when unpacking.
		query.aliasToDefault()
		// Overwrite the map entry
		result[label] = query
	}
	h.queries = result
	// Clean the cache
	h.cacheManager.reload()
	return nil
}

// Queries loaded
func (h *queryManager) Queries() map[string][]string {
	h.RLock()
	result := make(map[string][]string, len(h.queries))
	for key, query := range h.queries {
		views := make([]string, 0, len(query.Views))
		for name := range query.Views {
			views = append(views, name)
		}
		result[key] = views
	}
	h.RUnlock()
	return result
}

type memoCursor struct {
	memo []dbtool.Row
	curr []dbtool.Row
	dbtool.Cursor
}

// Next implements dbtool.Cursor
func (m *memoCursor) Next() bool {
	if m.memo != nil {
		m.curr = m.memo
		m.memo = nil
		return true
	}
	m.curr = nil
	return m.Cursor.Next()
}

// Batch implements dbtool.Cursor
func (m *memoCursor) Batch() []dbtool.Row {
	if m.curr != nil {
		return m.curr
	}
	return m.Cursor.Batch()
}

// Run the query against the given arguments
func (h *queryManager) Run(ctx context.Context, issuer, name string, packed dbtool.Params, maxRows int) (*Result, error) {
	store, ok := h.switcher.Switch(issuer)
	if !ok {
		return nil, errors.Errorf("ERROR trying to run query on non existing issuer %s", issuer)
	}
	query, err := h.find(name)
	if err != nil {
		return nil, err
	}
	// Turn the query definition into an array of innerQueries
	queries := query.Queries
	qLen := len(queries)
	if qLen <= 0 {
		queries = []jsonInnerQuery{query.innerQuery()}
		qLen = 1
	}
	// if debugging, return the query test too.
	queryString := ""
	if _, ok := packed["DEBUG"]; ok {
		text := make([]string, 0, len(queries))
		for _, q := range queries {
			text = append(text, q.Query)
		}
		queryString = strings.Join(text, "; ")
	}
	// Build a placeholder for the result
	result := &Result{
		Query:    queryString,
		Views:    query.Views,
		cache:    make([]ResultSet, len(queries)),
		queries:  queries,
		defaults: query.Params,
		packed:   packed,
		task: func(query jsonInnerQuery, aliased, defaults dbtool.Params) (dbtool.Feed, error) {
			return h.runInner(ctx, store, query, aliased, defaults, maxRows)
		},
	}
	// Make sure result is a streamer
	_ = saferest.Streamer(result)
	return result, nil
}

// runs an inner query
func (h *queryManager) runInner(ctx context.Context, store dbtool.Store, innerQuery jsonInnerQuery, aliased, defaults dbtool.Params, maxRows int) (dbtool.Feed, error) {
	// Build the statement
	stmt := dbtool.Statement{
		SQL:      innerQuery.Query,
		Defaults: defaults,
	}
	// And run the query!
	if h.throttle != nil {
		h.throttle <- true
		defer func() {
			<-h.throttle
		}()
	}
	return store.Run(ctx, stmt, aliased, innerQuery.Types, maxRows, BatchSize)
}

// Scope assigned to the query, for RBAC
func (h *queryManager) Scope(name string) ([]string, dbtool.Params, error) {
	// Find the scope.Scope does not depend on issuer
	query, err := h.find(name)
	if err != nil {
		return nil, nil, err
	}
	return query.Scope, query.Params, nil
}

func (h *queryManager) importFile(fileName string, result interface{}) error {
	log.Print("Importing query file ", fileName)
	parser := saferest.YAMLParser(fileName)
	// Load the query list
	if err := saferest.ParseFile(h.folder, parser, result); err != nil {
		h.lastError = err
		return err
	}
	return nil
}

// Finds the query by name
func (h *queryManager) find(name string) (*jsonQuery, error) {
	h.RLock()
	// If the file could not be opened, return an error
	if h.lastError != nil {
		h.RUnlock()
		return nil, h.lastError
	}
	query, ok := h.queries[name]
	h.RUnlock()
	if !ok {
		// If the query could not be found, return your regular error
		return nil, errors.Errorf("Query %s not found", name)
	}
	return &query, nil
}

// initializes the tenant by creating the missing views, if any
func (h *queryManager) createViews(issuer string, views []viewItem, version int, filters map[string]filter, params dbtool.Params) error {
	if views == nil || len(views) <= 0 {
		return nil
	}
	store, ok := h.switcher.Switch(issuer)
	if !ok {
		return errors.Errorf("Will not create views for issuer %s: no resources", issuer)
	}
	if version <= 0 {
		// Always update if version number <= 0 (development)
		log.Print("Found development version of query views, updating them in issuer", issuer)
	} else {
		currentVersion, err := store.GetVersion(querySection)
		if err != nil {
			log.Print("Error recovering current query version for issuer", issuer, ", updating queries: ", err)
		} else {
			if version > currentVersion {
				log.Print("Found newer version of query views for issuer", issuer, ", updating them")
			} else {
				log.Print("Query views are up to date for issuer", issuer)
				return nil
			}
		}
	}
	ctx := context.Background()
	for _, item := range views {
		if err := item.view.update(ctx, item.name, store, filters, params); err != nil {
			log.Print("Error updating view", item.name, "for issuer", issuer)
			return err
		}
	}
	if version > 0 {
		store.SetVersion(querySection, version)
	}
	return nil
}
