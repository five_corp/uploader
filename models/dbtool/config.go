package dbtool

import (
	"time"

	"github.com/pkg/errors"
)

// CertFiles contains minimum info required to set up TLS
type CertFiles struct {
	RootFile string
	CertFile string
	KeyFile  string
}

// Formatter can format a date following the rules of the store
type Formatter interface {
	// Formats a date, represented in the default way the db
	// serializes it to text, to a time.Time object.
	Format(time.Time) string
	// "Unformats" a date, represented in the default way the db
	// serializes it to text, to a time.Time object.
	Unformat(string) (time.Time, error)
}

// RFCFormatter uses RFC 3339 format for dates
type RFCFormatter struct{}

// Format implements Formatter interface
func (f RFCFormatter) Format(date time.Time) string {
	return date.Format(time.RFC3339)
}

// Unformat implements Formatter interface
func (f RFCFormatter) Unformat(date string) (time.Time, error) {
	data, err := time.Parse(time.RFC3339, date)
	return data, errors.Wrapf(err, "Failed to parse time %s", date)
}

// Config knows how to build a DSN for any supported db
type Config interface {
	Formatter
	// Return the name of the current DB
	CurrentDB() string
	// Use returns the SQL statement to switch to the given db name
	Use(string) string
	// GetDSN returns a DSN for the store
	GetDSN(ofuscate bool) (driver, dsn string)
	// SupportSession returns true if db supports session variables
	SupportSession() bool
	// Update certificate configuration
	SetCerts(useTLS bool, config CertFiles) error
	// Restart database server
	Restart() error
}
