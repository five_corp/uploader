package dbtool

import (
	"encoding/json"
	"fmt"
	"io"
	"strings"

	"github.com/pkg/errors"
)

// Feed data
type Feed struct {
	Names []string
	Types []string
	Cursor
}

// ErrorFeed builds a feed that returns an error immediately
func ErrorFeed(names, types []string, err error) Feed {
	return Feed{
		Names:  names,
		Types:  types,
		Cursor: &memory{err: err},
	}
}

// NewFeed builds a feed from an array
func NewFeed(names, types []string, rows []Row) Feed {
	return Feed{
		Names:  names,
		Types:  types,
		Cursor: onceCursor(rows),
	}
}

// HookFeed builds a feed with a hook function called on each batch.
func HookFeed(feed Feed, hook func([]Row)) Feed {
	return Feed{
		Names: feed.Names,
		Types: feed.Types,
		Cursor: hookCursor{
			Cursor: feed.Cursor,
			hook:   hook,
		},
	}
}

// Collect all feed results in a single []Row up to the given slice size
func (feed Feed) Collect(sliceSize int) ([]Row, error) {
	rows := make([]Row, 0, sliceSize)
	for feed.Next() {
		rows = append(rows, feed.Batch()...)
		if len(rows) >= sliceSize {
			break
		}
	}
	return rows, feed.Err()
}

// Stream implements saferest.Streamer interface
func (feed Feed) Stream(w io.Writer) error {
	names, err := json.Marshal(feed.Names)
	if err != nil {
		return errors.Wrapf(err, "Failed to marshal names array %v", feed.Names)
	}
	types, err := json.Marshal(feed.Types)
	if err != nil {
		return errors.Wrapf(err, "Failed to marshal types array %s", feed.Types)
	}
	if _, err := w.Write([]byte(fmt.Sprintf("{\"names\": %s, \"types\": %s, \"rows\": [", string(names), string(types)))); err != nil {
		return errors.Wrap(err, "Failed to write Feed header")
	}
	sep := ""
	for feed.Next() {
		if err := feed.streamBatch(w, feed.Batch(), sep); err != nil {
			return err
		}
		sep = ","
	}
	if err := feed.Err(); err != nil {
		return err
	}
	if _, err := w.Write([]byte("]}")); err != nil {
		return errors.Wrap(err, "Failed to write Feed tail")
	}
	return nil
}

// stream a batch of rows to the output
func (feed Feed) streamBatch(w io.Writer, batch []Row, sep string) error {
	rows := make([]string, 0, len(batch))
	for _, row := range batch {
		data, err := json.Marshal(row)
		if err != nil {
			return errors.Wrapf(err, "Failed to marshal row %v", data)
		}
		rows = append(rows, string(data))
	}
	result := fmt.Sprintf("%s%s", sep, strings.Join(rows, ","))
	if _, err := w.Write([]byte(result)); err != nil {
		return errors.Wrap(err, "Failed to write batch")
	}
	return nil
}

// Chain several Feeds, freeing each one once it is replaced by the next.
func Chain(factory func() (Feed, error)) (Feed, error) {
	// Get the first Feed from the 'next' function
	first, err := factory()
	if err != nil {
		return first, err
	}
	// wrap the 'factory' function
	wrapped := func() (Cursor, error) {
		feed, err := factory()
		if err != nil {
			return nil, err
		}
		return feed.Cursor, nil
	}
	// Replace the cursor with a chained one, and return the first feed
	first.Cursor = &chainCursor{cursor: first.Cursor, factory: wrapped}
	return first, nil
}
