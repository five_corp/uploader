package odbc

import (
	"fmt"

	"bitbucket.org/five_corp/uploader/models/dbtool"
	"github.com/pkg/errors"
)

// Pending to import: still don't know what ODBC driver to use.
// import _ "bitbucket.org/miquella/mgodbc"

// Config - set of config params for ODBC connections
type Config struct {
	dbtool.RFCFormatter
	DSN string
}

// CurrentDB implements Config interface
func (config *Config) CurrentDB() string {
	return ""
}

// GetDSN implements Config interface
func (config *Config) GetDSN(ofuscated bool) (driver, dsn string) {
	return "mgodbc", config.DSN
}

// SupportSession implements Config interface
func (config *Config) SupportSession() bool {
	return false
}

// SetCerts implements Config interface
func (config *Config) SetCerts(useTLS bool, crtConfig dbtool.CertFiles) error {
	return errors.New("Mssql Config does not support SetCerts")
}

// Restart implements Config interface
func (config *Config) Restart() error {
	return errors.New("Mssql Config does not support Restart")
}

// Use implements Config interface
func (config *Config) Use(s string) string {
	return fmt.Sprintf("USE %s;", s)
}
