package dbtool

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/pkg/errors"
)

// RFCFormatter uses RFC 3339 format for dates
type mock struct {
	dbName string
	RFCFormatter
	versions map[string]int
}

// NewMock store for testing
func NewMock() Store {
	return &mock{dbName: "mock"}
}

// mockResult mocks a DB Result
type mockResult struct {
	lastID int64
	err    error
	rows   int64
}

// LastInsertId mock interface sql.Result
func (m mockResult) LastInsertId() (int64, error) {
	return m.lastID, m.err
}

// RowsAffected mocks interface sql.Result
func (m mockResult) RowsAffected() (int64, error) {
	return m.rows, m.err
}

// CurrentDB mock Config interface
func (m *mock) CurrentDB() string {
	return m.dbName
}

// Switch mocks interface Store
func (*mock) Switch(dbName string) Store {
	return &mock{dbName: dbName}
}

// GetDSN mocks interface Store
func (*mock) GetDSN(ofuscate bool) (driver, dsn string) {
	return
}

// SupportSession mocks interface Store
func (*mock) SupportSession() bool {
	return false
}

// SetVersion mocks interface store
func (m *mock) SetVersion(section string, version int) error {
	m.versions[section] = version
	return nil
}

// SetVersion mocks interface store
func (m *mock) GetVersion(section string) (int, error) {
	if version, ok := m.versions[section]; ok {
		return version, nil
	}
	return 0, errors.Errorf("Version of section %s not found", section)
}

// SetCerts mocks interface Store
func (*mock) SetCerts(useTLS bool, config CertFiles) error {
	return errors.New("SetCerts implementation not mocked")
}

// Restart mocks interface Store
func (*mock) Restart() error {
	return errors.New("Restart implementation not mocked")
}

// Ping mocks interface Store
func (*mock) Ping() error {
	return nil
}

// Upsert mocks interface Store
func (*mock) Upsert(ctx context.Context, feed Feed, table string, updates []string) error {
	return nil
}

// Exec mocks interface Store
func (*mock) Exec(ctx context.Context, stmt Statement, packed Params) (sql.Result, error) {
	return mockResult{lastID: 1, rows: 1}, nil
}

// Run mocks interface Store
func (*mock) Run(ctx context.Context, stmt Statement, packed Params, types []string, rowLimit, batchSize int) (Feed, error) {
	names := make([]string, 0, len(types))
	for index := range types {
		names = append(names, fmt.Sprintf("col%d", index))
	}
	return NewFeed(names, types, nil), nil
}

// Use implements dbtool.Config
func (*mock) Use(s string) string {
	return fmt.Sprintf("USE %s", s)
}
