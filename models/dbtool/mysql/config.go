package mysql

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/five_corp/uploader/models/dbtool"

	"bitbucket.org/five_corp/saferest"
	"github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
)

// Config - set of parameters to configure MySQL connection
type Config struct {
	dbtool.RFCFormatter
	dbtool.CertFiles
	Host        string
	Port        int
	User        string
	Name        string
	Pass        string
	UseTLS      bool
	ServiceName string
	IniFile     string
	SQLDump     string
	SQLImport   string
}

// CurrentDB implements Config interface
func (config *Config) CurrentDB() string {
	return config.Name
}

// GetDSN implements the Config interface
func (config *Config) GetDSN(ofuscated bool) (driver, dsn string) {
	port := config.Port
	if port == 0 {
		port = 3306
	}
	pass := config.Pass
	if ofuscated {
		pass = "******"
	}
	dsnConfig := mysql.Config{
		User:      config.User,
		Passwd:    pass,
		Net:       "tcp",
		Addr:      fmt.Sprint(config.Host, ":", port),
		DBName:    config.Name,
		ParseTime: true,
		Loc:       time.Local,
		// Workaround for connecting to particular versions of mysql, see
		// https://github.com/go-sql-driver/mysql/issues/785
		AllowNativePasswords: true,
	}
	if config.UseTLS {
		tlsConfig := tls.Config{
			// Skip verification until we figure how to load
			// the correct CAs.
			InsecureSkipVerify: true,
			MinVersion:         2,
		}
		mysql.RegisterTLSConfig("mytls", &tlsConfig)
		dsnConfig.TLSConfig = "mytls"
	}
	return "mysql", dsnConfig.FormatDSN()
}

// UseRootPass replaces config credentials with root user credentials
func (config *Config) UseRootPass(rootPass string) {
	// Attach to the database as root user
	config.User = "root"
	config.Pass = rootPass
	config.Name = "mysql"
}

// SetCerts implements certConfig interface
func (config *Config) SetCerts(enable bool, certFiles dbtool.CertFiles) error {
	// Read the mysql ini file
	iniFile := config.IniFile
	ini, err := ioutil.ReadFile(iniFile)
	if err != nil {
		return errors.Wrapf(err, "Failed to read ini file %s", iniFile)
	}
	// Split the ini file before and after the [mysqld] header
	// Remove CA configuration commands
	before, current := []string{}, []string{}
	bannedPrefix := []string{"ssl-ca", "ssl-cert", "ssl-key"}
	for _, line := range strings.Split(string(ini), "\n") {
		line, skip := strings.TrimSpace(line), false
		if line == "[mysqld]" {
			if len(before) > 0 {
				return errors.New("[mysqld] header found twice")
			}
			before = append(current, line)
			current = []string{}
			skip = true
		} else {
			for _, banned := range bannedPrefix {
				if strings.HasPrefix(line, banned) {
					skip = true
					break
				}
			}
		}
		if !skip {
			current = append(current, line)
		}
	}
	// Raise an error if [mysqld] is not found
	if len(before) <= 0 {
		return errors.New("[mysqld] line not found")
	}
	// If TLS is enabled, append the certificate config
	if enable && config.RootFile != "" && config.CertFile != "" && config.KeyFile != "" {
		if runtime.GOOS == "windows" {
			before = append(before,
				// Must escape backslashes, otherwise MySQL fails to enable SSL
				"ssl-ca="+strings.Replace(certFiles.RootFile, "\\", "\\\\", -1),
				"ssl-cert="+strings.Replace(certFiles.CertFile, "\\", "\\\\", -1),
				"ssl-key="+strings.Replace(certFiles.KeyFile, "\\", "\\\\", -1))
		} else {
			before = append(before,
				"ssl-ca="+certFiles.RootFile,
				"ssl-cert="+certFiles.CertFile,
				"ssl-key="+certFiles.KeyFile)
		}
	} else {
		enable = false
	}
	// Merge file contents, use Windows line separators.
	lineSep := "\n"
	if runtime.GOOS == "windows" {
		lineSep = "\r\n"
	}
	content := strings.Join(append(before, current...), lineSep)
	// Won't encrypt the DB config, otherwise the DB may not boot.
	if err := saferest.OverwriteFile(iniFile, strings.NewReader(content), nil); err != nil {
		return err
	}
	// Everything OK, save settings
	config.UseTLS = enable
	config.CertFiles = certFiles
	return nil
}

// SupportSession implements Config interface
func (config *Config) SupportSession() bool {
	return true
}

// Search a pattern in the output of a command
func findLine(pattern string, command ...string) (string, error) {
	out, err := exec.Command(command[0], command[1:]...).Output()
	if err != nil {
		return "", errors.Wrapf(err, "Failed to run command %s", command[0])
	}
	pattern = strings.ToLower(pattern)
	for _, line := range strings.Split(string(out), "\n") {
		if strings.Contains(strings.ToLower(line), pattern) {
			return line, nil
		}
	}
	return "", errors.Errorf("Could not find the pattern %s in command output", pattern)
}

// Search a pattern in the parts of mysql command line.
// MySQL for Windows' Command line looks like:
// "C:\Program Files\MySQL\MySQL Server 5.7\bin\mysqld.exe" --defaults-file="C:\ProgramData\MySQL\MySQL Server 5.7\my.ini" MySQL57
func findPart(line, pattern string) (string, error) {
	for _, part := range strings.Split(line, "\"") {
		if strings.Contains(strings.ToLower(part), pattern) {
			return part, nil
		}
	}
	return "", errors.Errorf("Could not find pattern %s in line %s", pattern, line)
}

// Use implements Config interface
func (config *Config) Use(s string) string {
	return fmt.Sprintf("USE `%s`;", s)
}
