package mysql

import "github.com/pkg/errors"

// Restart implements Manager interface
func (config *Config) Restart() error {
	return errors.New("Restart not implemented in linux")
}

// GuessService guesses the MySQL service name
func (config *Config) GuessService(servicePattern string) error {
	return errors.New("GuessService not implemented in linux")
}
