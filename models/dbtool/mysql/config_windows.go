package mysql

import (
	"log"
	"os/exec"
	"strings"

	"github.com/pkg/errors"
)

// Restart the Database service
func (config *Config) Restart() error {
	// Shutdown database
	log.Print("Iniciando parada de base de datos")
	cmd := exec.Command("net", "stop", config.ServiceName, "/y")
	err := cmd.Run()
	log.Print("Parada de bbdd completada, ", err)
	// Start database
	log.Print("Iniciando arranque de base de datos")
	cmd = exec.Command("net", "start", config.ServiceName)
	if newErr := cmd.Run(); newErr != nil {
		return errors.Wrapf(newErr, "Failed to run net start %s", config.ServiceName)
	}
	log.Print("Arranque de base de datos completado")
	return errors.Wrap(err, "Failed to restart DB")
}

// GuessService guesses the service name
func (config *Config) GuessService(servicePattern string) error {
	if config.ServiceName == "" {
		// Curioso: si haces serviceName, err := getServiceName,
		// en lugar de almacenarte el resultado en la variable serviceName
		// global a la funcion, te crea otra variable serviceName local
		// al bloque if, que te enmascara la de fuera.
		// Me he estado peleando un buan rato, buscando por qué dentro
		// del bloque serviceName no estaba vacío, pero fuera sí.
		serviceName, err := getServiceName(servicePattern)
		if err != nil {
			return err
		}
		if serviceName == "" {
			return errors.New("Unexpected: getServiceName returned empty name")
		}
		config.ServiceName = serviceName
	}
	if config.IniFile == "" || config.SQLDump == "" || config.SQLImport == "" {
		cmdLine, err := findLine("mysqld.exe", "sc", "qc", config.ServiceName)
		if err != nil {
			return err
		}
		if cmdLine == "" {
			return errors.New("Unexpected: findFirst returned empty cmdLine")
		}
		exeFile, err := findPart(cmdLine, "mysqld.exe")
		if err != nil {
			return err
		}
		if config.IniFile == "" {
			part, err := findPart(cmdLine, "my.ini")
			if err != nil {
				return err
			}
			config.IniFile = part
		}
		if config.SQLDump == "" {
			config.SQLDump = strings.Replace(exeFile, "mysqld.exe", "mysqldump.exe", 1)
		}
		if config.SQLImport == "" {
			config.SQLImport = strings.Replace(exeFile, "mysqld.exe", "mysql.exe", 1)
		}
	}
	return nil
}

// Returns the name of the MySQL service
// Looks for the first service which name contains the pattern
func getServiceName(pattern string) (string, error) {
	service, err := findLine(pattern, "sc", "query")
	if err != nil {
		return "Error retrieving service names, ", err
	}
	if service == "" {
		msg := "Could not find the database service name"
		return msg, errors.New(msg)
	}
	descriptor := strings.SplitN(service, ":", 2)
	if len(descriptor) <= 1 {
		return "Unrecognized service line", errors.New(service)
	}
	serviceName := strings.TrimSpace(descriptor[1])
	if serviceName == "" {
		return "Unrecognized service name", errors.New(service)
	}
	return serviceName, nil
}
