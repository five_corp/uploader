package dbtool

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"fmt"
	"log"
	"net"
	"reflect"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// unpackedQuery is the internal representation of a SQL query.
type unpackedQuery struct {
	Query   string
	Params  []interface{}
	Session Params
}

// queryResult is the internal representation of a query result
type queryResult struct {
	conn  *sql.Conn
	stmt  *sql.Stmt
	Names []string
	Types []string
	rows  *sql.Rows
	// cursor management
	countdown int
	batchSize int
	err       error
	current   []Row
}

// Retry an sql query / exec a few times, with an exponential backoff between retries.
// When ErrBadConnection errors happen on a Conn instead of a DB, the sql.DB
// package does not retry them. Hence, I have to retry myself.
func retry(retries int, backoff time.Duration, task func() error) error {
	err := errors.New("Retry number must be > 0")
	for retries > 0 {
		// If no error running task, return immediately
		if err = task(); err == nil {
			return nil
		}
		// If error is not ErrBadConnection or a timeout, return too
		inner := errors.Cause(err)
		if netErr, ok := inner.(net.Error); inner != driver.ErrBadConn && (!ok || !netErr.Timeout()) {
			return err
		}
		// If error was ErrBadConnection or a timeout, back off and try again
		time.Sleep(backoff)
		retries--
		backoff *= 2
	}
	// Wrap the error just to be able to check in the logs that
	// retrying has actually kicked in
	return errors.Wrap(err, "Exceeded retry number")
}

// Exec an unpacked statement
func (unpacked *unpackedQuery) Exec(ctx context.Context, db *sql.DB, dbName string, config Config) (sql.Result, error) {
	var conn *sql.Conn
	err := retry(3, 500*time.Millisecond, func() error {
		var err error
		conn, err = db.Conn(ctx)
		if err != nil {
			return errors.Wrap(err, "Failed to create db connection")
		}
		// Select the proper database
		_, err = conn.ExecContext(ctx, config.Use(dbName))
		if err != nil {
			conn.Close()
			return errors.Wrapf(err, "Failed to select database '%s'", dbName)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	tx, err := conn.BeginTx(ctx, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to begin transaction")
	}
	// Exec the query
	result, err := tx.ExecContext(ctx, unpacked.Query, unpacked.Params...)
	if err != nil {
		tx.Rollback()
		return nil, errors.Wrapf(err, "Running query '%s' failed", unpacked.Query)
	}
	// Commit the transaction
	if err := tx.Commit(); err != nil {
		return nil, errors.Wrapf(err, "Failed to commit transaction")
	}
	return result, nil
}

// setSession set the session vars in the current transaction
func (unpacked *unpackedQuery) setSession(ctx context.Context, conn *sql.Conn) error {
	// If there are session vars, set them
	if unpacked.Session == nil || len(unpacked.Session) <= 0 {
		return nil
	}
	sQuery := make([]string, 0, len(unpacked.Session))
	sValue := make([]interface{}, 0, len(unpacked.Session))
	for part, value := range unpacked.Session {
		if value == nil {
			continue
		}
		sQuery = append(sQuery, fmt.Sprintf("@%s=?", part))
		// Try to get from custom parameters
		if !(reflect.TypeOf(value).Kind() == reflect.Slice) {
			// Scalar values can be saved as regular session variables.
			// To read a session variable from a view, use a stored procedure.
			// for instance:
			//
			// CREATE FUNCTION MyVariable() RETURNS INTEGER DETERMINISTIC NO SQL
			// BEGIN RETURN @MyVariable; END
			//
			// CREATE VIEW MyView AS SELECT * FROM t WHERE value = MyVariable()
			sValue = append(sValue, value)
		} else {
			// Arrays can only be saved to a table. We assume
			// it is named after the variable and has a single column.
			// To read a session variable array from a view,
			// you must join or subselect with the table:
			//
			// CREATE FUNCTION MyVariable() RETURNS INTEGER DETERMINISTIC NO SQL
			// BEGIN RETURN CASE WHEN @MyVariable IS NULL THEN 0 ELSE @MyVariable; END
			// CREATE TABLE MyVariable ( value VARCHAR(255) PRIMARY KEY );
			//
			// CREATE VIEW MyView AS SELECT * FROM t INNER JOIN MyVariable as v ON t.value<=>v.value
			// or
			// CREATE VIEW MyView AS SELECT * FROM t LEFT JOIN MyVariable as v ON t.value=v.value
			//   WHERE v.value IS NOT NULL
			//
			// This last one can be used if we want empty filter
			// to mean no filtering at all:
			//
			// CREATE VIEW MyView AS SELECT * FROM t WHERE (MyVariable() = 0 OR t.value IN (SELECT value FROM MyVariable))
			sValue = append(sValue, reflect.ValueOf(value).Len())
		}
	}
	if len(sQuery) > 0 {
		query := fmt.Sprintf("SET %s;", strings.Join(sQuery, ","))
		if _, err := conn.ExecContext(ctx, query, sValue...); err != nil {
			return errors.Wrapf(err, "Failed to set environment variables %s", query)
		}
	}
	return nil
}

// Run an unpacked statement
func (unpacked *unpackedQuery) Run(ctx context.Context, db *sql.DB, dbName string, config Config, types []string, rowLimit, batchSize int) (*queryResult, error) {
	res := &queryResult{
		countdown: rowLimit,
		batchSize: batchSize,
	}
	err := retry(3, 500*time.Millisecond, func() error {
		var err error
		res.conn, err = db.Conn(ctx)
		if err != nil {
			return errors.Wrap(err, "Failed to connect to database")
		}
		if err = res.withConn(ctx, dbName, config, unpacked, types); err != nil {
			res.conn.Close()
		}
		return err
	})
	if err != nil {
		log.Print("Error running query ", unpacked.Query, " with params ", unpacked.Params, ": ", err)
		return res, err
	}
	return res, nil
}

// WithConn continues the process assuming o.conn is valid
func (o *queryResult) withConn(ctx context.Context, dbName string, config Config, unpacked *unpackedQuery, types []string) error {
	// Select the proper database
	_, err := o.conn.ExecContext(ctx, config.Use(dbName))
	if err != nil {
		return errors.Wrapf(err, "Selecting database '%s' failed", dbName)
	}
	// Build a prepared statement from the query, so that MySQL
	// datatypes are returned correctly. See
	// https://github.com/go-sql-driver/mysql/issues/407
	o.stmt, err = o.conn.PrepareContext(ctx, unpacked.Query)
	if err != nil {
		return errors.Wrapf(err, "Failed to build prepared statement from '%s'", unpacked.Query)
	}
	if err = o.withStmt(ctx, config, unpacked, types); err != nil {
		o.stmt.Close()
	}
	return err
}

// WithStmt continues the process assuming o.stmt is valid
func (o *queryResult) withStmt(ctx context.Context, config Config, unpacked *unpackedQuery, types []string) error {
	// Set session variables, if any
	var err error
	if unpacked.Session != nil && config.SupportSession() {
		if err := unpacked.setSession(ctx, o.conn); err != nil {
			return err
		}
	}
	// Run the query
	if o.rows, err = o.stmt.QueryContext(ctx, unpacked.Params...); err != nil {
		return err
	}
	if err = o.withRows(ctx, config, unpacked, types); err != nil {
		o.rows.Close()
		return err
	}
	return nil
}

// withRows continues the process assuming o.rows is valid
func (o *queryResult) withRows(ctx context.Context, config Config, unpacked *unpackedQuery, types []string) error {
	// Read column names for the feed. Once here, I don't retry on fail.
	feedNames, err := o.rows.Columns()
	if err != nil {
		return errors.Wrap(err, "Failed to retrieve column names from query")
	}
	// Read colum types for the feed. These do not have to match
	// the types passed in as arguments, which are for normalization.
	feedTypes := types
	if feedTypes == nil || len(feedTypes) <= 0 {
		colTypes, err := o.rows.ColumnTypes()
		if err != nil {
			return errors.Wrap(err, "Failed to retrieve column types from database")
		}
		feedTypes = make([]string, 0, len(colTypes))
		for _, colType := range colTypes {
			feedTypes = append(feedTypes, colType.ScanType().Name())
		}
	}
	o.Names = feedNames
	o.Types = feedTypes
	return nil
}

// Close all open handles
func (o *queryResult) Close() {
	if o.rows != nil {
		o.rows.Close()
		o.stmt.Close()
		o.conn.Close()
	}
	o.rows = nil
}

// Next implements cursor
func (o *queryResult) Next() (next bool) {
	defer func() {
		// Release resources as soon as iteration finishes, so we don't
		// leak connections even if the caller forgets to Close() - as long
		// as it iterates over all rows.
		// This also helps with releasing connections early, when Cursors
		// are created in a loop and destruction is deferred. E.g.:
		// for data := range inputs {
		//     feed := createFeed(data)
		//     defer feed.Close() <-- this is not called at each iteration
		//                            of the loop, but all at once when the
		//                            enclosing function returns
		//     for feed.Next() {
		//         ...
		//     } <-- when Next() is false, resources are released
		// } <-- if the for loop returns or panic, pending feeds are
		//       closed by the defer calls.
		if !next {
			o.Close()
		}
	}()
	if o.err != nil || o.rows == nil {
		return false
	}
	batch, rowSize := make([]Row, 0, o.batchSize), len(o.Names)
	colPointers := make([]interface{}, rowSize)
	// Read rows
	for o.countdown > 0 && o.rows.Next() {
		// Scan the new column
		colValues := make([]interface{}, rowSize, rowSize)
		for i := 0; i < rowSize; i++ {
			colPointers[i] = &colValues[i]
		}
		if err := o.rows.Scan(colPointers...); err != nil {
			o.err = errors.Wrap(err, "Failed to scan query results")
			return false
		}
		// Allocate new values and save data
		for i := 0; i < rowSize; i++ {
			val := colValues[i]
			if b, ok := val.([]byte); ok {
				val = string(b)
			}
			colValues[i] = val
		}
		// Append the new item to the list
		batch = append(batch, Row(colValues))
		o.countdown--
		// Check we are not going over the number of rows...
		if len(batch) >= cap(batch) {
			o.current = batch
			return true
		}
	}
	// Once the iterator or the countdown are exhausted, check for errors
	// and make sure we release resources
	if err := o.rows.Err(); err != nil {
		o.err = errors.Wrapf(err, "Failed to complete query")
	}
	o.Close()
	// Even if there is an error, as long as we were able to collect some
	// data, we want to return it. Next call to Next() would return false
	// because o.err != nil
	o.current = batch
	return len(batch) > 0
}

// Batch returns current batch
func (o *queryResult) Batch() []Row {
	return o.current
}

// Err returns last error
func (o *queryResult) Err() error {
	return o.err
}
