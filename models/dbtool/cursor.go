package dbtool

import (
	"time"

	"github.com/pkg/errors"
)

// Row is any row in a feed
type Row []interface{}

// Format dates as strings recognized by the DB
func (row Row) Format(formatter Formatter) error {
	// Convert time.Time() values to the proper string format
	for index, value := range row {
		switch value.(type) {
		case time.Time:
			if formatter != nil {
				row[index] = formatter.Format(value.(time.Time))
			}
		}
	}
	return nil
}

// Unformat strings expressed in the db's native representation
// to proper dates. Uses the types[] array as hint.
func (row Row) Unformat(types []string, formatter Formatter) error {
	// Convert time.Time() values to string, to simplify json serialization
	for index, typename := range types {
		// Only type date is worth of special handling
		switch typename {
		case "date":
			value, ok := row[index].(string)
			if ok {
				parsed, err := formatter.Unformat(value)
				if err != nil {
					return errors.Wrapf(err, "Failed to unformat %s to date", value)
				}
				row[index] = parsed
			}
		}
	}
	return nil
}

// Cursor of results
type Cursor interface {
	// Next is true if there is something to read from the cursor
	Next() bool
	// Batch returns the current batch of rows
	Batch() []Row
	// Err returns the latest error code, if any
	Err() error
	// Close frees all resources. Must call AT LEAST once! (can call many times)
	// Cursor implementations MUST HAVE early release semantics, so that
	// they release all their internal resources once Next() returns false.
	// The Close function is only made available so that the caller can abandon
	// the iteration and free resources.
	Close()
}

// memory will wrap a Factory in a Cursor
type memory struct {
	current []Row
	err     error
	factory func() ([]Row, error)
}

// Next implements Cursor
func (m *memory) Next() bool {
	if m.err != nil || m.factory == nil {
		return false
	}
	m.current, m.err = m.factory()
	if m.current == nil {
		// finished
		m.factory = nil
		return false
	}
	return true
}

// Batch implements Cursor
func (m *memory) Batch() []Row {
	return m.current
}

// Err implements Cursor
func (m *memory) Err() error {
	return m.err
}

// Close implements Cursor
func (m *memory) Close() {
	m.factory = nil
}

// CursorFunc turns a factory function into a Cursor
func CursorFunc(factory func() ([]Row, error)) Cursor {
	return &memory{factory: factory}
}

// onceCursor builds a Cursor that yields only once
func onceCursor(rows []Row) Cursor {
	m := &memory{}
	m.factory = func() ([]Row, error) {
		m.factory = nil
		return rows, nil
	}
	return m
}

// hookCursor calls a hook on each cursor batch. Calls with None on Close.
type hookCursor struct {
	hook func([]Row)
	Cursor
}

// Batch implements Cursor
func (o hookCursor) Batch() []Row {
	batch := o.Cursor.Batch()
	if batch != nil {
		o.hook(batch)
	}
	return batch
}

// Close implements Cursor
func (o hookCursor) Close() {
	o.hook(nil)
}

// Chained cursor that keeps chaining cursors returned by the "next" callback
type chainCursor struct {
	cursor  Cursor
	factory func() (Cursor, error)
}

// Next implements Cursor
func (o *chainCursor) Next() bool {
	// While the current cursor has data, use it
	if o.cursor != nil && o.cursor.Next() {
		return true
	}
	// Otherwise, call for the next cursor
	for o.factory != nil {
		// Unless the current cursor has an error!
		if o.cursor != nil && o.cursor.Err() != nil {
			return false
		}
		// Get the next cursor
		cursor, err := o.factory()
		if err != nil {
			// Build an error cursor, and stop iterations
			cursor = &memory{err: err}
			o.factory = nil
		}
		if cursor == nil {
			// If no error but cursor == nil, leave the current cursor in place and exit
			return false
		}
		// OK, we have a new cursor to replace the old one, close it.
		if o.cursor != nil {
			o.cursor.Close()
		}
		o.cursor = cursor
		// If there is data in the cursor, return true. Otherwise, keep trying.
		if o.cursor.Next() {
			return true
		}
	}
	// All cursors exhausted!
	return false
}

// Batch implements Cursor
func (o *chainCursor) Batch() []Row {
	if o.cursor != nil {
		return o.cursor.Batch()
	}
	return nil
}

// Err implements Cursor
func (o *chainCursor) Err() error {
	if o.cursor != nil {
		return o.cursor.Err()
	}
	return nil
}

// Close implements Cursor
func (o *chainCursor) Close() {
	if o.cursor != nil {
		o.cursor.Close()
	}
	o.cursor = nil
	o.factory = nil
}
