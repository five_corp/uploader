package dbtool

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/pkg/errors"
)

// Store that manages the database state
type Store interface {
	Config
	// Ping the store to check it is healthy
	Ping() error
	// Upsert a full feed of results in a database. No slices in data!
	Upsert(ctx context.Context, feed Feed, table string, updates []string) error
	// Exec an statement with the provided params
	Exec(ctx context.Context, stmt Statement, packed Params) (sql.Result, error)
	// Run a statement with the provided params
	Run(ctx context.Context, stmt Statement, packed Params, types []string, rowLimit, batchSize int) (Feed, error)
	// Switch to a different db. Stay in the current db if dbName == ""
	Switch(dbName string) Store
	// Saves the version number for a given section name
	SetVersion(section string, version int) error
	// Gets the version number of a given section name
	GetVersion(section string) (int, error)
}

type sharedStore struct {
	Config
	// shared connection to a server
	sync.Mutex
	db *sql.DB
}

type store struct {
	*sharedStore
	dbName string
}

// New creates a new Database Manager for the given config.
func New(ctx context.Context, config Config) Store {
	s := store{
		sharedStore: &sharedStore{
			Config: config,
			Mutex:  sync.Mutex{},
			db:     nil,
		},
		dbName: config.CurrentDB(),
	}
	// Disconnect the DB Manager as soon as the context is closed.
	if ctx != nil {
		go func() {
			// Wait until context is done
			_ = <-ctx.Done()
			// disconnect the DB
			s.Lock()
			s.disconnect()
			s.Unlock()
		}()
	}
	return s
}

// Restart the DB, if supported
func (s store) Restart() error {
	// Lock the store
	s.Lock()
	defer s.Unlock()
	// Restart db
	if err := s.Config.Restart(); err != nil {
		return err
	}
	// Drop all current connections
	log.Print("Cerrando todas las conexiones activas")
	s.disconnect()
	return nil
}

// SetCerts enables certificate support for the DB, if supported
func (s store) SetCerts(useTLS bool, config CertFiles) error {
	// Lock the store
	s.Lock()
	defer s.Unlock()
	// Restart db
	return s.Config.SetCerts(useTLS, config)
}

// Ping the Store to check the DB is reachable
func (s store) Ping() error {
	db, err := s.connect()
	if err != nil {
		return err
	}
	return errors.Wrap(db.Ping(), "Failed to ping db server")
}

// Upsert a full feed of results in a database. No slices in data!
func (s store) Upsert(ctx context.Context, feed Feed, table string, updates []string) error {
	db, err := s.connect()
	if err != nil {
		return errors.Errorf("DB Manager not initialized, error: %s", err.Error())
	}
	for feed.Next() {
		unpacked, err := upsert(feed.Batch(), table, s, feed.Names, feed.Types, updates)
		if err != nil {
			return err
		}
		if _, err := unpacked.Exec(ctx, db, s.dbName, s); err != nil {
			return err
		}
	}
	if err := feed.Err(); err != nil {
		return err
	}
	return nil
}

// Exec an statement with the provided params
func (s store) Exec(ctx context.Context, stmt Statement, packed Params) (sql.Result, error) {
	db, err := s.connect()
	if err != nil {
		return nil, errors.Errorf("DB Manager not initialized, error: %s", err.Error())
	}
	// Unpack the statement
	unpacked, err := unpack(stmt, packed, s)
	if err != nil {
		return nil, err
	}
	// Exec the unpacked version
	return unpacked.Exec(ctx, db, s.dbName, s)
}

// Run a statement with the provided params
func (s store) Run(ctx context.Context, stmt Statement, packed Params, types []string, rowLimit, batchSize int) (Feed, error) {
	db, err := s.connect()
	if err != nil {
		return Feed{}, errors.Errorf("DB Manager not initialized, error: %s", err.Error())
	}
	// Unpack the statement with the provided arguments
	unpacked, err := unpack(stmt, packed, s)
	if err != nil {
		return Feed{}, err
	}
	rows, err := unpacked.Run(ctx, db, s.dbName, s, types, rowLimit, batchSize)
	if err != nil {
		return Feed{}, err
	}
	// Turn the result into a feed, pulled in the background
	result := Feed{
		Names:  rows.Names,
		Types:  rows.Types,
		Cursor: rows,
	}
	return result, nil
}

// Switch to a different db. Stays in the current db if dbName == ""
func (s store) Switch(dbName string) Store {
	if dbName != "" {
		return store{sharedStore: s.sharedStore, dbName: dbName}
	}
	return s
}

// Connect to the DB (reuse a connection)
func (s store) connect() (*sql.DB, error) {
	// Lock the store
	s.Lock()
	defer s.Unlock()
	// Get the connection from the cache
	if s.db != nil {
		// Ping the db before returning the connection
		if err := s.db.Ping(); err == nil {
			return s.db, nil
		}
		// If we are here, ping failed. Close and try again.
		s.db.Close()
		s.db = nil
		_, ofuscated := s.GetDSN(true) // ofuscate for logging purposes
		log.Print("SQL connection to ", ofuscated, " failed, closing and reopening")
	}
	// If not existing, open new connection and save it
	driver, dsn := s.GetDSN(false)
	db, err := sql.Open(driver, dsn)
	if err != nil {
		_, ofuscated := s.GetDSN(true)
		return nil, errors.Wrapf(err, "Failed to connect to database %s", ofuscated)
	}
	db.SetMaxOpenConns(100)
	db.SetMaxIdleConns(10)
	// Important setting for MySQL - it seems the server closes connections
	// without the client noticing, and that leads to connection errors.
	// Make it lower than database wait_timeout (default 28800 secs in MySQL:
	// https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_wait_timeout)
	db.SetConnMaxLifetime(time.Second * 1800)
	if err := db.Ping(); err != nil {
		db.Close()
		_, ofuscated := s.GetDSN(true)
		return nil, errors.Wrapf(err, "Failed to ping database %s", ofuscated)
	}
	s.db = db
	return db, nil
}

// Disconnects all open connections. Unsafe! must be wrapped in mutex.
func (s store) disconnect() {
	if s.db != nil {
		s.db.Close()
		s.db = nil
	}
}

// GetVersion gets the current version of some section
func (s store) GetVersion(section string) (int, error) {
	stmt := Statement{
		SQL: fmt.Sprintf("SELECT %s_version();", section),
	}
	feed, err := s.Run(context.Background(), stmt, nil, nil, 1, 1)
	if err != nil {
		return 0, errors.Errorf("Error checking current %s version: %v", section, err)
	}
	defer feed.Close()
	if !feed.Next() {
		return 0, errors.Errorf("Error recovering data about current %s version: %v", section, feed.Err())
	}
	rows := feed.Batch()
	if len(rows) <= 0 {
		return 0, errors.Errorf("%s_version() returned no data", section)
	}
	if len(rows[0]) <= 0 {
		return 0, errors.Errorf("%s_version() returned a row with no columns", section)
	}
	version, ok := rows[0][0].(int)
	if !ok {
		return 0, errors.Errorf("%s_version() %v is not an integer", section, version)
	}
	return version, nil
}

// SetVersion stores the current version of a section in the database
func (s store) SetVersion(section string, version int) error {
	stmt := Statement{
		SQL: fmt.Sprintf("DROP FUNCTION %s_version IF EXISTS;", section),
	}
	if _, err := s.Exec(context.Background(), stmt, nil); err != nil {
		log.Print(fmt.Sprintf("Warning: could not remove %s_version procedure: %v", section, err))
	}
	stmt = Statement{
		SQL: fmt.Sprintf("CREATE FUNCTION %s_version() RETURNS int(11) NO SQL DETERMINISTIC RETURN %d;", section, version),
	}
	if _, err := s.Exec(context.Background(), stmt, nil); err != nil {
		return errors.Errorf("Error creating %s_version procedure: %v", section, err)
	}
	return nil
}
