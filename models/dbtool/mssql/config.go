package mssql

import (
	"fmt"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/five_corp/uploader/models/dbtool"

	// Make sure to import MSSQL driver
	_ "github.com/denisenkom/go-mssqldb"
	"github.com/pkg/errors"
)

// Config - set of config params for SQL Server
type Config struct {
	dbtool.CertFiles
	Host    string
	Port    int
	User    string
	Name    string
	Pass    string
	UseTLS  bool
	Timeout int
}

// CurrentDB implements Config interface
func (config *Config) CurrentDB() string {
	return config.Name
}

// GetDSN implements Config interface
func (config *Config) GetDSN(ofuscated bool) (driver, dsn string) {
	query := url.Values{}
	query.Add("database", config.Name)
	query.Add("keepAlive", "10")
	if config.Timeout > 0 {
		query.Add("connection timeout", fmt.Sprintf("%d", config.Timeout))
	}
	switch {
	case config.UseTLS:
		query.Add("encrypt", "true")
		query.Add("certificate", config.CertFiles.RootFile)
		query.Add("TrustServerCertificate", "false")
	case strings.EqualFold(config.Host, "127.0.0.1"):
		fallthrough
	case strings.EqualFold(config.Host, "::1"):
		fallthrough
	case strings.EqualFold(config.Host, "localhost"):
		// Otherwise cannot connect to SQL Server pre 2008 R2
		// See https://github.com/denisenkom/go-mssqldb/issues/42
		query.Add("encrypt", "disable")
	default:
		// For the time being, unsafe connection in this case too.
		// Once tunnel works, we can restore this back to true
		// query.Add("encrypt", "true")
		// query.Add("TrustServerCertificate", "true")
		query.Add("encrypt", "disable")
	}
	port := config.Port
	if port == 0 {
		port = 1433
	}
	pass := config.Pass
	if ofuscated {
		pass = "******"
	}
	u := &url.URL{
		Scheme: "sqlserver",
		User:   url.UserPassword(config.User, pass),
		Host:   fmt.Sprintf("%s:%d", config.Host, port),
		// Path:  instance, // if connecting to an instance instead of a port
		RawQuery: query.Encode(),
	}
	return "mssql", u.String()
}

// Format implements Formatter interface
func (config *Config) Format(date time.Time) string {
	return date.Format("2006-01-02T15:04:05.00Z")
}

// Unformat implements Formatter interface
func (config *Config) Unformat(date string) (time.Time, error) {
	data, err := time.Parse("2006-01-02T15:04:05.00Z", date)
	return data, errors.Wrapf(err, "Failed to unformat %s to date", date)
}

// SupportSession implements Config interface
func (config *Config) SupportSession() bool {
	return false
}

// SetCerts implements Config interface
func (config *Config) SetCerts(useTLS bool, crtConfig dbtool.CertFiles) error {
	return errors.New("Mssql Config does not support SetCerts")
}

// Restart implements Config interface
func (config *Config) Restart() error {
	return errors.New("Mssql Config does not support Restart")
}

// Use implements Config interface
func (config *Config) Use(s string) string {
	return fmt.Sprintf("USE [%s];", s)
}
