package dbtool

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Params maps parameter names to parameter values.
// Parameter values can be anything, i.e. interface{}
type Params map[string]interface{}

// Statement model defining two basic properties:
//
// - SQL: SQL of the query, e.g.
//    "SELECT * FROM myTable WHERE field1={value1} AND field2 IN ({value2})"
//
// - Defaults: default parameter values:
//    { "value1": "scalar sample", "value2": ["slice", "sample"] }
//
// Statements accept named parameters - and only named parameters.
// named parameters are enclosed in {}, and can be slices too.
//
// A simplified form of the statement allows specifying both the
// field name and the parameter between the brackets, e.g.:
//
// "SELECT * FROM myTable WHERE {field1=value1} AND {field2=value2}"
//
// This alternative syntax is beneficial when values can be arrays.
// Parameter interpolation automatically decides how to expand this abbreviated form
// depending on the type of parameter:
//
// - Scalar: {field1=value1} becomes field1=?"
// - Empty slice: {field1=value1} becomes "true"
// - Slice: {field1=value1} becomes "field1 IN (?, ?, ...)"
//
// Defaults must be provided for every parameter of the query,
// because the type of the default value is used to check if
// the parameters format is right. i.e. if the default value is
// an array, the provided value must be an array.
type Statement struct {
	SQL      string
	Defaults Params
}

// Merge two sets of parameters. Parameters from 'other'
// are copied to 'p', only if they don't exist.
// Returns either 'p' or a new set, not 'other'
func (p Params) Merge(other Params) Params {
	// If some of them is nil, return the other
	if other == nil {
		return p
	}
	if p == nil {
		return other.Clone()
	}
	// Else if some of them is empty, return the other
	if len(other) == 0 {
		return p
	}
	if len(p) == 0 {
		return other.Clone()
	}
	// both have data, merge them
	merged := make(Params, len(p)+len(other))
	for k, v := range other {
		merged[k] = v
	}
	for k, v := range p {
		merged[k] = v
	}
	return merged
}

// Clone the dict
func (p Params) Clone() Params {
	result := make(Params, len(p))
	for k, v := range p {
		result[k] = v
	}
	return result
}

// Hash a parameter set to get a unique key
func (p Params) Hash() (string, error) {
	// Serialize the query params and get a hash
	serialized, err := json.Marshal(p)
	if err != nil {
		return "", errors.Wrap(err, "Failed to marshal hash parameters")
	}
	hash := fnv.New128a()
	hash.Write([]byte(serialized))
	hkey := hex.EncodeToString(hash.Sum(nil))
	return hkey, nil
}

// Unpack the query using the current parameters
func unpack(query Statement, packed Params, formatter Formatter) (*unpackedQuery, error) {
	unpacked := &unpackedQuery{}
	// Merge defaults and custom parameters. Only consider
	// custom parameters named like default parameters
	defaults := query.Defaults
	params := defaults
	// If the query has no defaults, then leave packed parameters
	// as is. These kind of queries come from conectors, which use
	// contexts with no defaults.
	if defaults == nil || len(defaults) <= 0 {
		params = packed
	} else {
		if packed != nil && len(packed) > 0 {
			// Duplicate params to avoid overwriting the default values
			params = make(Params, len(defaults))
			for k, v := range defaults {
				// If there is an updated value, set it
				if update, ok := packed[k]; ok {
					// nil values may not be typed
					if v != nil && reflect.TypeOf(v) != reflect.TypeOf(update) {
						return unpacked, errors.Errorf("Type of query parameter %s (%v) is not the same as type of default value (%v)", k, update, v)
					}
					v = update
				}
				// Keep copying
				params[k] = v
			}
		}
	}
	// Build query session values
	unpacked.Session = params
	// Replace textual params
	sqlQuery, err := Interpolate(query.SQL, params, formatter)
	if err != nil {
		return unpacked, err
	}
	// Split the query to locate placeholders escaped with {}
	parts := strings.FieldsFunc(sqlQuery, func(r rune) bool {
		return (r == '{' || r == '}')
	})
	finalParams := make([]interface{}, 0, (len(parts)/2)+1)
	// Even (0, 2, 4 ...) must be part of the SQL query
	// Odd (1, 3, 5...) must be variable names
	result := make([]string, 0, len(parts))
	for i, part := range parts {
		// Text parts are copied verbatim
		if i%2 == 0 {
			result = append(result, part)
			continue
		}
		// Parameter parts are replaced by whatever belongs
		field, assignment := "", strings.SplitN(part, "=", 2)
		if len(assignment) >= 2 {
			field = strings.TrimSpace(assignment[0])
			part = strings.TrimSpace(assignment[1])
		}
		param, ok := params[part]
		if !ok {
			err := errors.Errorf("Undefined parameter %s for query %s in %v", part, query.SQL, params)
			return unpacked, err
		} else if param == nil {
			if field == "" {
				err := errors.Errorf("NULL value for parameter %s not supported for query %s in %v", part, query.SQL, params)
				return unpacked, err
			}
			result = append(result, fmt.Sprintf("%s IS NULL", field))
		} else if reflect.TypeOf(param).Kind() == reflect.Slice {
			// DB driver does not accept slices. Got to expand it...
			quest, value := sqlSerialize(param, "?")
			if field != "" {
				// Filter with field part must get replaced by "field IN (?, ?, ...)"
				if reflect.ValueOf(param).Len() > 0 {
					quest = fmt.Sprintf("%s IN (%s)", field, quest)
				} else {
					quest = "true"
					value = nil
				}
			}
			result = append(result, quest)
			if value != nil {
				finalParams = append(finalParams, value...)
			}
		} else {
			// Scalar value, single index
			quest := "?"
			if field != "" {
				quest = fmt.Sprintf("%s=?", field)
			}
			result = append(result, quest)
			finalParams = append(finalParams, param)
		}
	}
	// Rebuild the query string
	unpacked.Query = strings.Join(result, "")
	unpacked.Params = finalParams
	return unpacked, nil
}

// Interpolate replaces placeholders (surrounded by %%) in string
func Interpolate(sqlQuery string, packed Params, formatter Formatter) (string, error) {
	for key, val := range packed {
		if val == nil || (reflect.TypeOf(val).Kind() == reflect.Slice) {
			continue
		}
		// Try to convert parameter to string
		strVal, err := toString(key, val, formatter)
		if err != nil {
			return "", err
		}
		// If parameter can be converted to text, it may be used
		// directly in the query as %param%
		placeholder := fmt.Sprintf("%%%s%%", key)
		sqlQuery = strings.Replace(sqlQuery, placeholder, strVal, -1)
	}
	return sqlQuery, nil
}

// Returns the string value of an interface, if any
func toString(k string, v interface{}, f Formatter) (string, error) {
	switch val := v.(type) {
	case string:
		return val, nil
	case time.Time:
		return f.Format(val), nil
	case int, int8, int16, int32, int64, uint, uint8, uint16, uint32, uint64:
		intVal, ok := val.(int64)
		if ok {
			return strconv.FormatInt(intVal, 10), nil
		}
	case float32, float64:
		floatVal, ok := val.(float64)
		if ok {
			return strconv.FormatFloat(floatVal, 'G', -1, 64), nil
		}
	case fmt.Stringer:
		return val.String(), nil
	}
	return "", errors.Errorf("Cannot convert parameter %v (%T: %v) to string", k, v, v)
}

// Serializes an enumerable 'param' into a string and an array suitable to use in an SQL query.
// For instance, say you need to run:
//   "SELECT * FROM t WHERE c IN (param)", where param is an array.
// The driver needs this turned into:
//   "SELECT * FROM t WHERE c IN (?, ... ?)", [param[0], ... param[N]]
// This function creates both the string of placeholders
// "?, ... ?" and the array [params...]
func sqlSerialize(param interface{}, placeholder string) (string, []interface{}) {
	items := reflect.ValueOf(param)
	quest := make([]string, 0, items.Len()+1)
	value := make([]interface{}, 0, items.Len()+1)
	if items.Len() == 0 {
		// Even if the input array is empty, we have to return some value.
		// Otherwise some queries would yield sintax error, e.g.
		// "SELECT * FROM table WHERE param IN(?)", []
		quest = append(quest, placeholder)
		value = append(value, nil)
	} else {
		for i := 0; i < items.Len(); i++ {
			quest = append(quest, placeholder)
			value = append(value, items.Index(i).Interface())
		}
	}
	return strings.Join(quest, ","), value
}

// Inserts a batch of results in a database. No slices in data!
// Only MySQL supported.
func upsert(rows []Row, table string, config Config, columns, types, updates []string) (*unpackedQuery, error) {
	if rows == nil || len(rows) <= 0 || columns == nil || len(columns) <= 0 {
		// Nothing to upsert
		return nil, nil
	}
	// Serialize query data
	params := make([]string, 0, len(rows))
	values := make([]interface{}, 0, len(columns)*len(rows))
	for _, row := range rows {
		sMarks, sValues := sqlSerialize(row, "?")
		params = append(params, sMarks)
		values = append(values, sValues...)
	}
	// Build the insert statement
	template := "INSERT INTO %s (`%s`) VALUES (%s)"
	// If there are update keys, append "ON DUPLICATE KEY UPDATE..."
	if updates != nil && len(updates) > 0 {
		// Build an array of "field=VALUES(field)"
		updatedCols := make([]string, 0, len(updates))
		for _, field := range updates {
			updatedCols = append(updatedCols, fmt.Sprintf("%s=VALUES(%s)", field, field))
		}
		// Add the "ON DUPLICATE KEY" clause
		template = fmt.Sprintf("%s ON DUPLICATE KEY UPDATE %s", template,
			strings.Join(updatedCols, ","))
	}
	// Build insert query
	unpacked := &unpackedQuery{
		// Join the statement parts
		Query: fmt.Sprintf(template, table,
			strings.Join(columns, "`,`"),
			strings.Join(params, "),(")),
		Params: values,
	}
	return unpacked, nil
}
